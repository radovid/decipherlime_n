import sublime, sublime_plugin, sys, re
try:
    from .Edit import Edit as Edit
except:
    from Edit import Edit as Edit

# clean whitespace
def cleanInput(input):
    #CLEAN UP THE TABS
    input = re.sub("\t+", " ", input)
    #CLEAN UP SPACES
    input = re.sub("\n +\n", "\n\n", input)
    #CLEAN UP THE EXTRA LINE BREAKS
    input = re.sub("\n{2,}", "\n", input)
    return input

# Get cursor in empty attribute/tag for easier writing
def getSelection(self, sels):
    selectReg = []
    for sel in sels:
        input = self.view.substr(sel).strip()
        if not re.compile('(>\n?\n? *<|"")').search(input):
            selectReg.append(sel)
        else:
            start = sel.begin()
            if self.view.find('(>\n?\n? *<|"")', start):
                selectReg.append(self.view.find('(>\n?\n? *<|"")', start).begin() + self.view.find('(>\n?\n? *<|"")', start).size()/2)
            else:
                selectReg.append(self.view.find(self.view.substr(sel).strip(), start))

    self.view.sel().clear()
    for reg in selectReg:
        self.view.sel().add(reg)

# Set appropriate attribute delimiter
def getAttrDelimiter(self):
    if self.view.settings().get('surveyType') in ['v3']:
        delmiter = " "
    else:
        delmiter = "\n  "
    return delmiter

######################## LIB #########################

# Escape all special characters and ampersands in selected text
class cleanUp(sublime_plugin.TextCommand):
    def run (self,edit):
        try:
            sels = self.view.sel()
            input = ''

            for sel in sels:
                printPage = ''
                input = self.view.substr(sel)

                #REPLACE SMART QUOTES, ELLIPSIS
                # funkyChars = [('…','...'),('‘','\''),('’','\''),('“','"'),('”','"'),('–','-')]
                # for pair in funkyChars:
                #     input = input.replace(pair[0],pair[1])

                #REPLACE AMPERSTANDS WITH ENTITIES
                if re.compile('&(?!(#\d+|lt|gt|amp);)').search(input):
                    input = re.sub('&(?!(#.+|lt|gt|amp);)', '&amp;', input)

                # escapes [
                if re.compile('\[(?!pipe|res|rel|loopvar)(.*?)(\])').search(input):
                    input = re.sub('\[(?!pipe|res|rel|loopvar)(.*?)(\])', '&amp;#91;\g<1>\g<2>', input);

                printPage = input.encode(encoding="ascii",errors="xmlcharrefreplace").decode()

                self.view.replace(edit,sel, printPage)
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)

######################## TEXT FORMATTING #########################

class textFormattingTag(sublime_plugin.TextCommand):
    tags = {
        'color':['<span style="color: ;">', '</span>'],
        'bold':['<b>', '</b>'],
        'italic':['<i>', '</i>'],
        'underline':['<u>', '</u>'],
        'sup':['<sup>', '</sup>'],
        'sub':['<sub>', '</sub>'],
        'ul':['<ul>', '</ul>'],
        'ol':['<ol>', '</ol>'],
        'center':['<p align="center">', '</p>'],
        'dbreak':['', '<br/><br/>'],
        'td':['<td>', '</td>'],
        'tr':['<tr>\n    <td>', '</td>\n</tr>'],
        'table':['<table class="">\n    <tr>\n        <td>','</td>\n        <td> </td>\n    </tr>\n</table>'],
        'img':['<img src="[rel ', ']" style="max-width:100%; height:auto;/>'],
        'respimg':['<div style="max-width: 800px">\n  <img src="[rel ', ']" style="max-width:100%; height:auto; display:block;" />\n</div>'],
        'zoomimg':['<img src="[rel ', ']" style="max-width:300px" class="popup-img" data-caption=" (OPTIONAL CAPTION TEXT) "/>'],
    }
    def is_visible(self, which):
        if bool( self.view.settings().get('useHpr') ):
            return not 'respimg' in which
        else:
            return not 'zoomimg' in which
    def run (self, edit, which):
        try:
            sels = self.view.sel()
            input = ''
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel).strip()
                printPage = self.tags[which][0] + input + self.tags[which][1]

                self.view.replace(edit,sel, printPage)
        except Exception as e:
            print (e)

            
class makeLiPerLine(sublime_plugin.TextCommand):
    def run(self, edit):
        try:
            sels = self.view.sel()
            input = ''
            for sel in sels:
                count = 0
                printPage = ''
                extra = ''
                input = self.view.substr(sel)

                input = cleanInput(input)
                input = input.strip().split("\n")

                for x in input:
                    printPage += "<li>{0}</li>\n".format(input[count].strip())
                    count += 1
                # thanks to replace the regions keep updated with their start and end point
                self.view.replace(edit,sel, printPage)
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)

class makeBreak(sublime_plugin.TextCommand):
    def run (self,edit):
        try:
            sels = self.view.sel()
            input = ''
            selInputs = [sel.end() for sel in sels] 
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel).strip()
                printPage = "{0}<br/>".format(input)
                self.view.replace(edit,sel, printPage)  

            self.view.sel().clear()
            for pnt in selInputs:   
                self.view.sel().add(self.view.find('<br/>', pnt).end()) 
        except Exception as e:
            print (e)

class makeHyperlink(sublime_plugin.TextCommand):
    def run (self,edit):
        try:
            sels = self.view.sel()
            input = ''
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel).strip()
                if 'http' in input or 'mailto' in input or 'www.' in input:
                    printPage = """<a href="{0}" target="_blank"></a>""".format(input)
                else:
                    printPage = """<a href="" target="_blank">{0}</a>""".format(input)

                self.view.replace(edit, sel, printPage)
            getSelection(self, sels)
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)

class makeSectionTitle(sublime_plugin.TextCommand):
    def run (self,edit):
        try:
            sels = self.view.sel()
            input = ''
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel).strip()
                if self.view.settings().get('surveyType') in ['v3']:
                    printPage = """<!-- ===========================================================================================================
                                {0}
============================================================================================================= -->""".format(input)
                else:
                    printPage = """<note>\n===========================================================================================================
                                {0}
=============================================================================================================\n</note>""".format(input)
                self.view.replace(edit,sel, printPage)
        except Exception as e:
            print (e)


######################## STYLES #########################

class makeHover(sublime_plugin.TextCommand):
    def is_visible(self):
        return not bool( self.view.settings().get('useHpr') )
    def run (self,edit):
        try:
            sels = self.view.sel()
            printPage = """<a class="show_inlineContent">(MOUSE OVER TEXT HERE)<span class="content">(MOUSEOVER DESCRIPTION HERE)</span></a>"""
            for sel in sels:
                self.view.insert(edit,sel.end(), printPage)
        except Exception as e:
            print (e)

class makeHoverHpr(sublime_plugin.TextCommand):
    def is_visible(self):
        return bool( self.view.settings().get('useHpr') )

    def run (self,edit):
        try:
            sels = self.view.sel()
            printPage = """<span class="tooltipped" data-tooltip=" (MOUSEOVER DESCRIPTION HERE) ">(MOUSE OVER TEXT HERE)</span>"""
            for sel in sels:
                self.view.insert(edit,sel.end(), printPage)
        except Exception as e:
            print (e)


class makePopup(sublime_plugin.TextCommand):
    def is_visible(self):
        return not bool( self.view.settings().get('useHpr') )
    def run (self,edit):
        try:
            popup = """<a href="javascript:void(0)" onclick="Survey.uidialog.make($ (this).next('div').clone(), {{width: 600, height: 400, title: ''}} );" style="color:blue;">{0}</a>
<div style="display: none;">{1}</div>"""
            sels = self.view.sel()
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel)

                if input:
                    if 'here' in input or 'click' in input or 'view' in input:
                        printPage = popup.format(input, '<!-- POP-UP CONTENT HERE -->')
                        self.view.replace(edit,sel, printPage)
                    elif '<img' in input or '<table' in input or '[res' in input or '[pipe' in input or '${' in input:
                        printPage = popup.format('<!-- POP-UP LINK HERE -->', input)
                        self.view.replace(edit,sel, printPage)
                    else:
                        printPage = popup.format('<!-- POP-UP LINK HERE -->', '<!-- POP-UP CONTENT HERE -->')
                        self.view.insert(edit, sel.end(), printPage)
                else:
                    printPage = popup.format('<!-- POP-UP LINK HERE -->', '<!-- POP-UP CONTENT HERE -->')
                    self.view.insert(edit,sel.end(), printPage)
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)

class makePopupHpr(sublime_plugin.TextCommand):
    def is_visible(self):
        return bool( self.view.settings().get('useHpr') )
    def run (self,edit):
        try:
            for pun in range(1,99):
                if not self.view.find('id="def{}"'.format(pun), 0):
                    popupId = 'def{}'.format(pun)
                    break
            popup = """<a href="#{2}" class="popup-link">{0}</a>
<div id="{2}" class="popup">
  <h4>Popup Title</h4>
  {1}
</div>"""
            sels = self.view.sel()
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel)

                if input:
                    if 'here' in input or 'click' in input or 'view' in input:
                        printPage = popup.format(input, '<!-- POP-UP CONTENT HERE -->', popupId)
                        self.view.replace(edit,sel, printPage)
                    elif '<img' in input or '<table' in input or '[res' in input or '[pipe' in input or '${' in input:
                        printPage = popup.format('<!-- POP-UP LINK HERE -->', input, popupId)
                        self.view.replace(edit,sel, printPage)
                    else:
                        printPage = popup.format('<!-- POP-UP LINK HERE -->', '<!-- POP-UP CONTENT HERE -->', popupId)
                        self.view.insert(edit, sel.end(), printPage)
                else:
                    printPage = popup.format('<!-- POP-UP LINK HERE -->', '<!-- POP-UP CONTENT HERE -->', popupId)
                    self.view.insert(edit,sel.end(), printPage)
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)


class makeResponsivePopup(sublime_plugin.TextCommand):
    def is_visible(self):
        return not bool( self.view.settings().get('useHpr') )
    def run (self,edit):
        try:
            for pun in range(1,99):
                if not self.view.find('id="def{}"'.format(pun), 0):
                    popupId = 'popup{}'.format(pun)
                    break
            popup = """<a href="#{2}" class="popup-link">{0}</a>
<div id="{2}" class="popup mfp-hide" style="max-width:900px">
{1}
</div>"""
            if self.view.settings().get('surveyType') not in ['v3', 'AG'] and not( self.view.find('<survey[^>]*?(magnific\.[jcs]{1,2}s)(.|\s)*?>', 0) and self.view.find('<survey[^>]*?(inline-popup\.js)(.|\s)*?>', 0) ):
                popup += """<!-- ##### In order to use this popup you need to: #####
1. Copy the 3 files from the zip below in your survey's /static directory.
  https://radovid.eu/decipherlime/assets/magnific-popup.zip
2. Add the 2 attributes below in the survey tag.
  ss:includeCSS="[rel magnific.css]"
  ss:includeJS="[rel magnific.js]"
-->"""
            sels = self.view.sel()
            for sel in sels:
                input = self.view.substr(sel)

                if input:
                    if 'here' in input or 'click' in input or 'view' in input:
                        printPage = popup.format(input, '<!-- POP-UP CONTENT HERE -->', popupId)
                        self.view.replace(edit,sel, printPage)
                    elif '<img' in input or '<table' in input or '[res' in input or '[pipe' in input or '${' in input:
                        printPage = popup.format('<!-- POP-UP LINK HERE -->', input, popupId)
                        self.view.replace(edit,sel, printPage)
                    else:
                        printPage = popup.format('<!-- POP-UP LINK HERE -->', '<!-- POP-UP CONTENT HERE -->', popupId)
                        self.view.insert(edit, sel.end(), printPage)
                else:
                    printPage = popup.format('<!-- POP-UP LINK HERE -->', '<!-- POP-UP CONTENT HERE -->', popupId)
                    self.view.insert(edit,sel.end(), printPage)
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)


class makeStylevar(sublime_plugin.TextCommand):
    def run (self,edit):
        try:
            sels = self.view.sel()
            input = ''
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel).strip()
                printPage = """<stylevar name="{0}" type="string"/>""".format(input)
                self.view.replace(edit,sel, printPage)
            getSelection(self, sels)
        except Exception as e:
            print (e)


class insertStyle(sublime_plugin.TextCommand):
    styles = {
        'css_global': """ <style name="respview.client.css" mode="after"><![CDATA[
<style type="text/css">

</style>
]]></style>""",
        'js_global': """ <style name="respview.client.js" mode="after"> <![CDATA[
<script>
    $ (document).ready( function() {

    });
</script>
]]></style>""",
        'css_local': """ <style name="question.footer" label="" mode="after"><![CDATA[
<style>

</style>
]]></style>""",
        'js_local': """ <style name="question.footer" label="" wrap="ready" mode="after"><![CDATA[
    $q = $ ("#question_$(this.label)");
]]></style>""",
        'js_css': """ <style name="question.footer" label="" mode="after"><![CDATA[
<script>
    $ (document).ready( function() {

    });
</script>
<style>

</style>
]]></style>""",
        'atm1d': """<note> ### atm1d turns responses to buttons on non-grid radio or checkbox questions ###
uses="atm1d.11"
atm1d:viewMode="Tiled" <!-- Tiled (grid) / Vertical (in rows) / Horizontal (in columns) -->
atm1d:showInput="0" <!-- Whether to show a radio/checkbox buttons on each response or just the card -->
atm1d:numCols="3" <!-- The number of columns the response grid will be shown in (small displays will be in 1) -->
<!-- Large screens -->
atm1d:large_contentAlign="center" <!-- Alignment of the content within each button (left/center/right) -->
atm1d:large_buttonAlign="center" <!-- Alignment of the buttons on the page (left/center/right) -->
atm1d:large_maxWidth="200px" <!-- Maximum width per button (px/em/%) -->
atm1d:large_minHeight="1000px"  <!-- Minimum height per button, only for Vertical view-mode (px/em/%) -->
<!-- Small screens -->
atm1d:small_contentAlign="left" <!-- Alignment of the content within each button (left/center/right) -->
atm1d:small_buttonAlign="center" <!-- Alignment of the buttons on the page (left/center/right) -->
atm1d:small_maxWidth="80px" <!-- Maximum width per button (px/em/%) -->
atm1d:small_minHeight="50px"  <!-- Minimum height per button, only for Vertical view-mode (px/em/%) -->
</note>""",
        'cardsort': """<note> ### cardsort displays a radio/checkbox question row-by-row (cards) with the columns as buckets below, ###
            ### similar to scrolling matrix (sometimes called carousel) ###
uses="cardsort.7"
cardsort:automaticAdvance="0" <!-- Auto-advance to next card |default:1| -->
cardsort:dragAndDrop="0" <!-- Allow drag-and-dropping cards on buckets |default:1| -->
cardsort:displayNavigation="0" <!-- Show or hide Prev/Next buttons |default:1| -->
cardsort:displayProgress="0" <!-- Show the X/N counter for how many cards have been answered |default:1| -->
cardsort:displayCounter="0" <!-- Show counter on each buckets for the number of cards in it |default:1| -->
cardsort:completionHTML=" " <!-- Text to be shown after last card is answered |default:"All done! Click Continue..."| -->
cardsort:wrapBuckets="0" <!-- Wrap buckets on multiple rows |default:1| -->
cardsort:bucketsPerRow="5" <!-- How many buckets to show on a row (mobile is always vertical 1/row) -->
cardsort:cardCSS="max-width:90px; border:none;" <!-- Write CSS to be apllied to cards -->
cardsort:bucketCSS="max-width:100px; height:90px;" <!-- Write CSS to be apllied to buckets -->
</note>""",
        'multicol': """<note> ### multicol splits a row-only question into multiple columns ###
uses="multicol.7"
multicol:count="3" <!-- number of columns to split the rows into |default: 2| -->
multicol:flow="horizontal" <!-- horizontal: rows are ordered from left to right, Z-like flow / vertical |default|: rows are ordered top to bottom -->
</note>""",
        'ranksort': """<note> ### ranksort turns a select into a drag-and-drop ranking question ###
uses="ranksort.7"
ranksort:cardCSS="height:130px;" <!-- CSS for cards (rows) -->
ranksort:cardDroppedCSS="height:130px;" <!-- CSS for already ranked/answered cards. Shoudl have same dimensions as buckets -->
ranksort:bucketCSS="height:130px;" <!-- CSS for the buckets (choices) -->
ranksort:uiDraggableHelperCSS="height:130px;" <!-- CSS for cards while being dragged -->
</note>""",
        'sliderpoints': """<note> ### sliderpoints turns a select question to a slider with the choices as points ###
uses="sliderpoints.3"
sliderpoints:sliderPosition="Middle" <!-- Where to put the handle (Off Scale |default| / Left End / Middle / Right End) -->
sliderpoints:legendPosition="Above Slider" <!-- Where should the legend (text) appear (Above Slider / Below Slider |default|)
sliderpoints:showRange="0" <!-- Whether to highlight the slider track to where the handle is |default:1| -->
sliderpoints:OO="1" <!-- Put on last choice to simulate noanswer option -->
</note>""",
        'videoplayer': """<note> ### Use a text questions with no rows to display a video ###
uses="videoplayer.3"
videoplayer:video_id="" <!-- Upload in System Files and get video's id -->
videoplayer:pause_enable="1" <!-- Whether to allow pausing of video |default:0| -->
videoplayer:autosubmit="1" <!-- Auto-continue on video end |default:0| -->
videoplayer:disable_continue="0" <!-- Whether to allow continuation without watching |default:1| -->
videoplayer:enable_continue_after="15" <!-- Show continue button after this many seconds have passed  -->
</note>""",
        'accordionstyle': """ surveyDisplay="mobile" uses="mobileaccord.1384\"""",
        'datepicker': """<note> ### Use a text question with this DQ for date questions ###
uses="fvdatepicker.2"
verify="daterange(FORMAT, START, END)" <!-- Formats can be mm/dd/yyyy, dd/mm/yyyy, yyyy/mm/dd, mm-dd-yyyy, dd-mm-yyyy, yyyy-mm-dd; any & today are also acceptable-->
fvdatepicker:firstDay="Monday" <!-- or Sunday -->
fvdatepicker:suggestedDate="14/01/2024" <!-- The initially shown daye -->
</note>
""",
        'hidecells': """<note> ### Put the below stylevar and styletag somewhere in your survey, usually in the beginning. ### </note>
<stylevar desc="List of 'col.row' button labels as strings to hide" name="cs:hidecells" title="Button hide list" type="text">[]</stylevar>
<style label="hideCells" cond="'%s.%s' % (col.label, row.label) in this.styles.cs.hidelist.split(',')" name="question.element"><![CDATA[
    <$(tag) $(headers) class="cell nonempty $(levels) ${"desktop" if this.grouping.cols else "mobile"} border-collapse $(extraClasses) ${col.group.styles.ss.groupClassNames if col.group else (row.group.styles.ss.groupClassNames if row.group else "")} $(col.styles.ss.colClassNames) $(row.styles.ss.rowClassNames) hidden-cell"$(extra)>
    </$(tag)>
]]></style>

<note> ### Call style within a question and define elements to hide with cs:hidelist="" in the question tag. ###
### Syntax is col.row LABELS, and cells are separated with , (c1.r1,c2.r2). Can be set in exec with Q1.styles.cs.hidelist = "c1.r1,c2.r2" ### </note>
<style copy="hideCells"/>
""",
        'corner': """<note> ### Put the below tag within a question where you want corner legend. Define a res tag with the text and call it below.  ### </note>
<style name='question.left-blank-legend'> <![CDATA[
    <$(tag) scope="col" class="cell nonempty legend col-legend col-legend-top col-legend-basic border-collapse $(levels) ${"desktop" if this.grouping.cols else "mobile"} $(colError)">
        ${res.CORNER_TEXT_HERE}
    </$(tag)>
]]></style>
""",
        'hideelements': """<note> ### Put the below tag within a question where you want corner legend. Define a res tag with the text and call it below.  ### </note>
<style name='question.left-blank-legend'> <![CDATA[
    <$(tag) scope="col" class="cell nonempty legend col-legend col-legend-top col-legend-basic border-collapse $(levels) ${"desktop" if this.grouping.cols else "mobile"} $(colError)">
        ${res.CORNER_TEXT_HERE}
    </$(tag)>
]]></style>
""",
        'footerlinks': """<note> ### Put style in your survey. Change @(support) to some html/text to change help/privacy links at bottom. Leave empty to hide them ### </note>
<style name="survey.respview.footer.support"> <![CDATA[
@(support)
]]></style>
""",
        'recaptcha': """<logic uses="recaptcha.2" label="recaptcha" sst="0"/>
<note>You might want to remove the term.</note>
<term label="Recaptcha_Term" cond="not recaptcha.human" markers="QC_term" sst="0" incidence="0">Recaptcha term: not human</term>
""",
        'numericrange': """  <style label="NumericArrowsRange" name="question.after" wrap="ready"><![CDATA[
   var jsexport = ${jsexport()};
   var min = jsexport.minValue
   var max = jsexport.maxValue
    $ ("input").attr({
       "max" : max,
       "min" : min 
    });
]]></style>
  <style mode="after" name="question.footer"><![CDATA[
<style>
.answers input {
    width: calc(40px + 17px* 2) !important;
}
/* For Chrome and Safari  */
input[type="number"]::-webkit-outer-spin-button,
input[type="number"]::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
}
/* For Firefox  */
input[type="number"] {
    -moz-appearance: textfield;
}
</style>
]]></style>
""",
        'dropdowntext': """<note> ### Put inside select question and write text in a res tag (dropdownText). ### </note>
 <style label="changeDefault" arg:defaultText="${res.<note> ### Put the below stylevar and styletag somewhere in your survey, usually in the beginning. ### </note>}" name="el.select.default"><![CDATA[
<option value="-1" $(selected)>$(defaultText)</option>
]]></style>""",
        'mimicqarts': """<note> ### Put the style and the themevars at the beginning of your survey. Non-styled questions will look like QArts. fir="on" needed in survey tag. ### </note>
<themevars>
  <themevar name="fir-size">29px</themevar>
  <themevar name="fir-border">#ff9e25</themevar>
  <themevar name="fir-border-hover">#ff9e25</themevar>
  <themevar name="fir-border-selected">#ff9e25</themevar>
  <themevar name="fir-border-width">3px</themevar>
  <themevar name="fir-inner">#ffffff</themevar>
  <themevar name="fir-inner-hover">#ffffff</themevar>
  <themevar name="fir-inner-selected">#ff9e25</themevar>
</themevars>

<style name="respview.client.css" mode="after"><![CDATA[
<style type="text/css">
.legend, .answers label {
    font-size: 18px !important; 
    font-weight: 500 !important; 
}
.post-text {
    font-size: 18px !important;
    font-weight: 500 !important;
}

.text-input {
    border-top: 1px solid transparent !important; 
    border-left: none !important; 
    border-right: none !important; 
    border-bottom: 1px solid #CCC !important; 
    border-radius: 0 !important; 
    font-size: 18px !important;
    text-align: center;
} 
.text-input:focus {
    border-top: none !important; 
    border-bottom: 2px solid #ff9e25 !important; 
} 
.text-input::placeholder {
    color: rgb(158, 158, 158);
    transition: transform 250ms ease 0s, width 250ms ease 0s, opacity 100ms ease 0s;
    white-space: nowrap;
    text-overflow: ellipsis;
    overflow: visible;
    transform-origin: left top;
    bottom: 2px;
}
.text-input:focus::placeholder {
    transform: scale(0.65) translate(-25%, -.5em);
}
.grid th, .grid td {
    border-left: 1px solid transparent !important;
    border-right: 1px solid transparent !important;
}
.grid th, .grid tr:not(.hasError) td {
    background: #fff !important;
}
.row-legend.row-legend-group {
    background-color: #f2f2f2 !important;
    font-weight: bold !important;
}

.answers select, .answers select option {
    padding: 5px 35px 5px 5px;
    font-size: 16px;
    border: 1px solid #ccc;
    appearance: none !important;
    color: rgb(69, 69, 69);
    cursor: pointer!important;
    font-family: Roboto, "Open Sans"!important;
    font-size: 18px!important;
    font-weight: normal!important;
    height: 45px!important;
}
</style>
]]></style>
""",

    }
    def is_visible(self, which):
        if which == 'accordionnfx':
            return bool(self.view.settings().get('surveyType') == 'nf')
        elif 'qarts' in which:
            return bool(self.view.settings().get('surveyType') in ['v2','fv'])
        return 'css' in which or 'js' in which or not (bool( self.view.settings().get('trainingMode') ) or bool( self.view.settings().get('usesHpr') ))
    def run (self,edit, which):
        try:
            sels = self.view.sel()
            printPage = self.styles[which]
            for sel in sels:
                self.view.insert(edit,sel.end(), printPage)
        except Exception as e:
            print (e)


class makeStyleCopy(sublime_plugin.TextCommand):
    def is_visible(self):
        return not bool( self.view.settings().get('usesHpr') )
    def run (self,edit):
        try:
            sels = self.view.sel()
            input = ''
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel).strip()
                printPage = """<style copy="{0}"/>""".format(input)

                self.view.replace(edit,sel, printPage)
            getSelection(self, sels)
        except Exception as e:
            print (e)


######################## OTHER ELEMENTS #########################

class addGenTag(sublime_plugin.TextCommand):
    tags = {
        'exec': """<exec>\n{0}\n</exec>""",
        'validate': """<validate>\n{0}\n</validate>""",
        'virtual': """<virtual>\n{0}\n</virtual>""",
        'comment': """<comment>{0}</comment>""",
        'condition': """<condition label="{0}" cond=""/>""",
        'marker': """<marker name="{0}" cond=""/>""",
        'goto': """<goto target="{0}" cond="" />""",
        'note': """<note>{0}</note>""",
        'define': """<define label="">\n{0}\n</define>""",
        'insert': """<insert source="{0}" as=""/>""",
        'random': """<random label="">\n{0}\n</random>""",
        'block': """<block label=\"\" cond=\"1\" randomize=\"1\">\n{0}\n</block>""",
        'blockrandomize': """<block label=\"\" cond=\"1\" randomizeChildren=\"1\">\n{0}\n</block>""",
    }
    def is_visible(self, which):
        return 'tag' in which or not 'tag' in which
    def run (self,edit, which):
        try:
            sels = self.view.sel()
            input = ''
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel).strip()
                printPage = self.tags[which].format(input)
                self.view.replace(edit,sel, printPage)
            getSelection(self, sels)
        except Exception as e:
            print (e)


class makeTerm(sublime_plugin.TextCommand):
    def run (self,edit):
        try:
            sels = self.view.sel()
            input = ''
            surveyCompatReg = self.view.find('compat="(\d+)"', 0)
            surveyCompat = int(self.view.substr(surveyCompatReg).split('"')[1]) if surveyCompatReg else 143

            for sel in sels:
                printPage = ''
                input = self.view.substr(sel).strip()

                if surveyCompat >= 143:
                    printPage = """<term label="_Term" cond="">{0}</term>""".format(input)
                else:
                    printPage = """<term cond="">{0}</term>""".format(input)

                self.view.replace(edit,sel, printPage)
            getSelection(self, sels)
            if surveyCompat >= 143:
                for sel in sels:
                    start = self.view.text_point(self.view.rowcol(sel.begin())[0], 0)
                    if self.view.find('="_T', start):
                        selectLabelPoint = self.view.find('="_T', start).begin() + self.view.find('="_T', start).size()/2
                        self.view.sel().add(selectLabelPoint)
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)

class makeRes(sublime_plugin.TextCommand):
    def run (self,edit):
        try:
            sels = self.view.sel()
            input = ''
            selectReg = []
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel).strip()

                input = cleanInput(input)
                input = input.strip().split("\n")

                for line in input:
                    line = re.sub("^[a-zA-Z0-9]{1,2}[\.:\)][ \t]+", "\n", line)
                    printPage += """<res label="">{0}</res>\n""".format(line.strip())

                self.view.replace(edit,sel, printPage.strip('\n'))

            for reg in self.view.find_all('<res label=""'):
                if sels.contains(reg):
                    selectReg.append(reg.begin() + 12)
            self.view.sel().clear()
            for reg in selectReg:
                self.view.sel().add(reg)
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)
            
class addQuota(sublime_plugin.TextCommand):
    def run (self,edit):
        try:
            sels = self.view.sel()
            input = ''
            surveyCompatReg = self.view.find('compat="(\d+)"', 0)
            surveyCompat = int(self.view.substr(surveyCompatReg).split('"')[1]) if surveyCompatReg else 143

            for sel in sels:
                printPage = ''
                input = self.view.substr(sel).strip()

                if surveyCompat >= 143:
                    printPage = """<quota label="{0}_sheet" sheet="{0}" overquota="noqual"/>""".format(input)
                else:
                    printPage = """<quota sheet="{0}" overquota="noqual"/>""".format(input)

                self.view.replace(edit, sel, printPage)
            getSelection(self, sels)
            if surveyCompat >= 143 and not input:
                for sel in sels:
                    start = sel.end()
                    if self.view.find('sheet="{0}"', start):
                        selectLabelPoint = self.view.find('sheet="{0}"', start).begin() + 7
                        self.view.sel().add(selectLabelPoint)
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)


class makeLoopvars(sublime_plugin.TextCommand):
    def run (self,edit):
        try:
            sels = self.view.sel()
            input = ''
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel).strip()

                input = cleanInput(input)
                input = input.split("\n")

                for line in input:
                    line = re.sub("^[a-zA-Z0-9]{1,2}[\.:\)][ \t]+", "\n", line)
                    printPage += """<loopvar name="VAR_NAME">{0}</loopvar>\n""".format(line.strip())

                self.view.replace(edit,sel, printPage)
            # Select VAR_NAME or text if empty
            selectVarNames = []
            skipLoopVars = False
            selectVarsInLoop = None
            for sel in sels:
                start = sel.begin()
                if len(self.view.lines(sel)) == 1 and self.view.find('(>\h*<)', start) and sel.contains(self.view.find('(>\h*<)', start)):
                    selectVarNames.append(self.view.find('(>\h*<)', start).begin() + self.view.find('(>\h*<)', start).size()/2)
                    skipLoopVars = True
                else:
                    selectVarNames += [foundReg for foundReg in self.view.find_all('VAR_NAME') if sel.contains(foundReg)]

            if not skipLoopVars:
                # find and edit vars="" in Loop tag if it's not set
                if len(self.view.find_all('<loop[\s\n]+label="\w*_[Ll]oop"[\w"=\s\n]+?vars="VAR_NAME"')) == 1:
                    print ( 'makeLoopvars: one <loop> with no vars found' )
                    selectVarsInLoop = self.view.find('VAR_NAME', self.view.find_all('<loop[\s\n]+label="\w*_[Ll]oop"[\w"=\s\n]+?vars="VAR_NAME"')[0].begin())
                elif self.view.find_all('<loop[\s\n]+label="\w*_[Ll]oop"[\w"=\s\n]+?vars="VAR_NAME"'):
                    foundRegStarts = [foundReg.begin() for foundReg in self.view.find_all('<loop[\s\n]+label="\w*_[Ll]oop"[\w"=\s\n]+?vars="VAR_NAME"')]
                    foundRegStarts = sorted(foundRegStarts, key=lambda x: start - x if x < start else start + x)
                    selectVarsInLoop = self.view.find('VAR_NAME', foundRegStarts[0])

            self.view.sel().clear()

            for reg in selectVarNames:
                self.view.sel().add(reg)
            if not skipLoopVars and selectVarsInLoop:
                self.view.sel().add(selectVarsInLoop)
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)

class makeLooprows(sublime_plugin.TextCommand):
    def run (self,edit):
        try:
            sels = self.view.sel()
            input = ''
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel).strip()

                input = cleanInput(input)
                input = input.split("\n")
                count = 0
                for line in input:
                    count += 1
                    line = re.sub("^[a-zA-Z0-9]{1,2}[\.:\)][ \t]+", "\n", line)
                    printPage += """<looprow label=\"{0}\">\n  {1}\n</looprow>\n""".format(str(count),line.strip())

                self.view.replace(edit,sel, printPage)
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)

class addLoopBlock(sublime_plugin.TextCommand):
    def run (self,edit):
        try:
            sels = self.view.sel()
            input = ''
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel).strip()
                printPage = """ <block label="_Block" cond="QUESTION.r[loopvar:label]">
{0}
 </block>""".format(input)
                self.view.replace(edit,sel, printPage)

            selectLabelPoints = []
            for sel in sels:
                start = self.view.text_point(self.view.rowcol(sel.begin())[0], 0)
                if self.view.find('="_B', start):
                    selectLabelPoints.append(self.view.find('="_B', start).begin() + self.view.find('="_B', start).size()/2)
            self.view.sel().clear()
            for reg in selectLabelPoints:
                self.view.sel().add(reg)
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)


class addLoop(sublime_plugin.TextCommand):
    def run (self,edit):
        try:
            sels = self.view.sel()
            input = ''
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel).strip()
                printPage = """<loop label="_Loop" vars="VAR_NAME" randomizeChildren="0">
  {0}
</loop>""".format(input)
                self.view.replace(edit,sel, printPage)
            getSelection(self, sels)

            selectLabelPoints = []
            for sel in sels:
                start = self.view.text_point(self.view.rowcol(sel.begin())[0], 0)
                if self.view.find('="_L', start):
                    selectLabelPoints.append(self.view.find('="_L', start).begin() + self.view.find('="_L', start).size()/2)
            self.view.sel().clear()
            for reg in selectLabelPoints:
                self.view.sel().add(reg)
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)

######################## MUTATORS #########################

class addMutator(sublime_plugin.TextCommand):
    onloads = {
        'copy_rows':[1, """onLoad="copy('', rows=True)\""""],
        'copy_cols':[1, """onLoad="copy('', cols=True)\""""],
        'copy_choices':[1, """onLoad="copy('', choices=True)\""""],
        'make_rows':[1, """onLoad="makeLabelledRows([('', ''), ('', '')])\""""],
        'del_oe':[2, """<onLoad>
def removeOEs(d):
    n = d.copy()
    if 'open' in n.keys():
        if n['open'] in [1,'1', True]:
            n['open'] = '0'
    return n
copy('', rows=True, transform=removeOEs)
</onLoad>"""],
        'relabel':[2, """<onLoad>
def relabel(Dict):
    newDict = Dict.copy()
    newDict["label"] = "r" + str(LABEL + int(newDict["label"].split("r")[1]))
    return newDict
copy('', rows=True, transform=relabel, prepend=True)
</onLoad>"""],
        'del_groups':[1, """onLoad="copy('', rows=True, transform=lambda x: (lambda f=x.copy(): f.__setitem__('groups', '') or f)())\""""],
        'add_cond':[2, """<onLoad>
def addCond(Dict):
    newDict = Dict.copy()
    newDict["cond"] = "" + newDict["label"] + ""
    return newDict
copy('', rows=True, transform=addCond)
</onLoad>"""],
      
    }
    def run (self, edit, which):
        attrDelim = getAttrDelimiter(self)
        try:
            sels = self.view.sel()
            if self.onloads[which][0] == 1:
                printPage = attrDelim + self.onloads[which][1]
            elif self.onloads[which][0] == 2:
                printPage = '\n' + self.onloads[which][1]
            for sel in sels:
                self.view.insert(edit,sel.end(), printPage)
        except Exception as e:
            print (e)


######################## ATTRIBUTES #########################


# class addAttr(sublime_plugin.TextCommand):
#     anAttr = ('oe', 'randomize', 'exclusive', 'optional', 'aggregate', 'incidence', 'alts', 'altlabels', 'values', 'shuffle', 'grouping', 'styledev', 'translateable', 'mls')
#     qnAppropriate =     (False, False,  False, True,  False,  False, True,  True,  True,  True,  True,  True,  True)
#     elemAppropriate = ( True,   True,   True,  True,  True,   False, True,  True,  False, False, False, True,  True)
#     otherAppropriate = (False, 'block', False, False, False, 'term', False, False, False, False, False, False, 'html,res', 'html,res')
#     excluding =('choice,group')
#     AttributeS = ('','{delim}type="rating"','{delim}atleast="1"','{delim}optional="0"','{delim}size="40"{delim}optional="0"','{delim}optional="0"','{delim}size="3"{delim}optional="0"{delim}verify="range(0,9999)"','{delim}size="3"{delim}optional="0"{delim}range="100"','{delim}hpr:type="date"{delim}hpr:minDate="date(1990,1,1)"{delim}hpr:maxDate="today()"')
#     def run(self,edit):
#         pass

class addOe(sublime_plugin.TextCommand):
    def run (self,edit):
        try:
            sels = self.view.sel()
            if self.view.settings().get('useHpr'):
                oeTxt = ' open="1" openSize="25" randomize="0" hpr:rplaceholder="Please specify..."'
            elif self.view.settings().get('useQarts'):
                oeTxt = ' open="1" openSize="25" randomize="0" qa:rplaceholder="Please specify..."'
            elif self.view.settings().get('surveyType') in ['AG', 'AG0']:
                oeTxt = ' open="1" openSize="25" randomize="0" openOptional="1"'
            else:
                oeTxt = ' open="1" openSize="25" randomize="0"'
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel)

                if input:
                    input = cleanInput(input)
                    input = input.split("\n")
                    for line in input:
                        if re.compile('<(row|col)').search(line) and not 'open="1"' in line:
                            line = line.replace('>',oeTxt+'>',1)
                            printPage += line + '\n'
                        else:
                            printPage += line + '\n'
                    self.view.replace(edit,sel, printPage.strip('\n'))
                else:
                    printPage = oeTxt
                    self.view.insert(edit,sel.end(), printPage)
        except Exception as e:
            print (e)

class addRandomize(sublime_plugin.TextCommand):
    def run (self,edit):
        try:
            sels = self.view.sel()
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel)

                if input:
                    input = cleanInput(input)
                    input = input.split("\n")
                    for line in input:
                        if re.compile('<(row|col|choice)').search(line) and not 'randomize' in line:
                            line = line.replace('>', ' randomize="0">',1)
                            printPage += line + '\n'
                        else:
                            printPage += line + '\n'
                    self.view.replace(edit,sel, printPage.strip('\n'))
                else:
                    printPage = ' randomize="0"'
                    self.view.insert(edit,sel.end(), printPage)
        except Exception as e:
            print (e)

class addExclusive(sublime_plugin.TextCommand):
    def run (self,edit):
        try:
            sels = self.view.sel()
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel)

                if input:
                    input = cleanInput(input)
                    input = input.split("\n")
                    for line in input:
                        if re.compile('<(row|col)').search(line) and not 'exclusive' in line:
                            line = line.replace('>', ' exclusive="1" randomize="0">',1)
                            printPage += line + '\n'
                        else:
                            printPage += line + '\n'
                    self.view.replace(edit,sel, printPage.strip('\n'))
                else:
                    printPage = ' exclusive="1" randomize="0"'
                    self.view.insert(edit,sel.end(), printPage)
        except Exception as e:
            print (e)

class addOptional(sublime_plugin.TextCommand):
    def run (self,edit):
        try:
            sels = self.view.sel()
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel)

                if input:
                    if re.compile('<(radio|checkbox|select|text|textarea|number|float)').search(input):
                        qTag = re.compile('(<(radio|checkbox|select|text|textarea|number|float))(.*?)(>)', re.DOTALL).search(input).group()
                        multiLine = bool(re.match('<(radio|checkbox|select|text|textarea|number|float)(?=\s*\n)', qTag))
                        if not 'optional' in input:
                            printPage = re.sub('(<)(radio|checkbox|select|text|textarea|number|float)(.*?)(>)', '\g<1>\g<2>\g<3>{0}optional="1"\g<4>'.format('\n  ' if multiLine else ' '), input, 0, re.DOTALL)
                        else:
                            printPage = input.replace('optional="0"', 'optional="1"')

                        self.view.replace(edit,sel, printPage)
                    else:
                        input = cleanInput(input)
                        input = input.split("\n")
                        for line in input:
                            if re.compile('<(row|col)').search(line) and not 'optional' in line:
                                line = line.replace('>', ' optional="1">',1)
                                printPage += line + '\n'
                            else:
                                printPage += line + '\n'
                        self.view.replace(edit,sel, printPage.strip('\n'))
                else:
                    printPage = ' optional="1"'
                    self.view.insert(edit,sel.end(), printPage)
        except Exception as e:
            print (e)

class addAggregate(sublime_plugin.TextCommand):
    def run (self,edit):
        try:
            sels = self.view.sel()
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel)

                if input:
                    input = cleanInput(input)
                    input = input.split("\n")
                    for line in input:
                        if re.compile('<(row|col|choice)').search(line) and not 'aggregate' in line:
                            line = line.replace('>', ' aggregate="0" percentages="1">',1)
                            printPage += line + '\n'
                        else:
                            printPage += line + '\n'
                    self.view.replace(edit,sel, printPage.strip('\n'))
                else:
                    printPage = ' aggregate="0" percentages="1"'
                    self.view.insert(edit,sel.end(), printPage)
        except Exception as e:
            print (e)


class addIncidence(sublime_plugin.TextCommand):
    def run (self,edit):
        try:
            sels = self.view.sel()
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel)

                if input:
                    input = cleanInput(input)
                    input = input.split("\n")
                    for line in input:
                        if re.compile('<term').search(line) and not 'incidence' in line:
                            line = line.replace('>', ' incidence="0">',1)
                            printPage += line + '\n'
                        else:
                            printPage += line + '\n'
                    self.view.replace(edit,sel, printPage.strip('\n'))
                else:
                    printPage = ' incidence="0"'
                    self.view.insert(edit,sel.end(), printPage)
        except Exception as e:
            print (e)


class addAlts(sublime_plugin.TextCommand):
    def run (self,edit):
        try:
            sels = self.view.sel()
            input = ''
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel)

                if input:
                    input = re.sub("(<(row|col|choice|group).*?)(>)(.*?)(</(row|col|choice|group)>)", "\g<1> alt=\"\g<4>\"\g<3>\g<4>\g<5>", input)
                    question = re.compile("(<(radio|checkbox|select|text|textarea|number|float).*?label=\".*?\")(.*?>)(.*?)((<title>)(.*?)(</title>))", re.DOTALL)
                    printPage = question.sub("\g<1>\g<3>\g<4>\g<5>\n  <alt>\g<7></alt>", input)

                    self.view.replace(edit,sel, printPage)
                else:
                    printPage = ' alt=""'
                    self.view.insert(edit,sel.end(), printPage)
        except Exception as e:
            print (e)

class addAltlabels(sublime_plugin.TextCommand):
    def run (self,edit):
        attrDelim = getAttrDelimiter(self)
        try:
            sels = self.view.sel()
            input = ''
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel)

                if input:
                    if re.compile('<(radio|checkbox|select|text|textarea|number|float)').search(input):
                        qTag = re.compile('(<(radio|checkbox|select|text|textarea|number|float))(.*?)(>)', re.DOTALL).search(input).group()
                        multiLine = bool(re.match('<(radio|checkbox|select|text|textarea|number|float)(?=\s*\n)', qTag))
                        if not 'altlabel="' in input:
                            printPage = re.sub("(<(radio|checkbox|select|text|textarea|number|float).*?label=\"(.*?)\")(.*?>)", "\g<1>{0}altlabel=\"\g<3>\"\g<4>".format('\n  ' if multiLine else ' '), input, 0, re.DOTALL)
                        else:
                            printPage = input
                        self.view.replace(edit,sel, printPage)
                    else:
                        printPage = input + '{0}altlabel=""'.format(attrDelim)
                    self.view.replace(edit,sel, printPage)
                else:
                    printPage = ' altlabel=""'.format(attrDelim)
                    self.view.insert(edit,sel.end(), printPage)
        except Exception as e:
            print (e)

class addGroups(sublime_plugin.TextCommand):
    def run (self,edit):
        try:
            sels = self.view.sel()
            input = ''
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel)

                if input:
                    input = cleanInput(input)
                    input = input.split("\n")
                    for line in input:
                        if re.compile('<(row|col|choice)').search(line) and not "groups=" in line:
                            if "alt=" in line:
                                line = line.replace(" alt=\"", " groups=\"g\" alt=\"")
                            else:
                                initial = re.compile('<(row|col|choice).*?>').search(line).group()
                                post = initial.replace("\">", "\" groups=\"g\">")
                                line = line.replace(initial, post)
                        printPage += line + '\n'

                    self.view.replace(edit,sel, printPage.strip('\n'))
                else:
                    printPage = ' groups="g"'
                    self.view.insert(edit,sel.end(), printPage)
        except Exception as e:
            print (e)

class addValues(sublime_plugin.TextCommand):
    def run(self, edit):
        try:
            sels = self.view.sel()
            input = ''
            for sel in sels:
                count = 0
                printPage = ''
                extra = ''
                input = self.view.substr(sel)

                if input:
                    if re.compile('<(radio|select)').search(input):
                        qTag = re.compile('(<(radio|select))(.*?)(>)', re.DOTALL).search(input).group()
                        multiLine = bool(re.match('<(radio|select)(?=\s*\n)', qTag))
                        if not 'values' in input:
                            printPage = re.sub('(<)(radio|select)(.*?)(>)', '\g<1>\g<2>\g<3>{0}values="order"\g<4>'.format('\n  ' if multiLine else ' '), input, 0, re.DOTALL)
                        else:
                            printPage = input
                        self.view.replace(edit,sel, printPage.strip('\n'))
                    else:
                        input = cleanInput(input)
                        input = input.split("\n")

                        for line in input:
                            if re.compile('<(row|col|choice).*label=".*"').search(line):
                                initial = re.compile('<(row|col|choice).*label=".*">').search(line).group()
                                prelabel = re.compile('label=".*?"').search(initial).group()
                                label = re.compile('\d+').search(prelabel.replace("label=","")).group()
                                postlabel = prelabel + ' value="{0}"'.format(label)
                                printPage += "{0}\n".format(line.replace(prelabel, postlabel))
                            else:
                                printPage += line + '\n'
                        self.view.replace(edit,sel, printPage.strip('\n'))
                else:
                    printPage = ' value=""'
                    self.view.insert(edit,sel.end(), printPage)
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)

class addShuffleRows(sublime_plugin.TextCommand):
    def run (self,edit):
        attrDelim = getAttrDelimiter(self)
        try:
            sels = self.view.sel()
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel)

                if input:
                    if re.compile('<(radio|checkbox|select|text|textarea|number|float)').search(input):
                        qTag = re.compile('(<(radio|checkbox|select|text|textarea|number|float))(.*?)(>)', re.DOTALL).search(input).group()
                        multiLine = bool(re.match('<(radio|checkbox|select|text|textarea|number|float)(?=\s*\n)', qTag))
                        if not 'shuffle="rows"' in input:
                            printPage = re.sub('(<)(radio|checkbox|select|text|textarea|number|float)(.*?)(>)', '\g<1>\g<2>\g<3>{0}shuffle="rows"\g<4>'.format('\n  ' if multiLine else ' '), input, 0, re.DOTALL)
                        else:
                            printPage = input
                    else:
                        printPage = input + '{0}shuffle="rows"'.format(attrDelim)
                    self.view.replace(edit,sel, printPage)
                else:
                    printPage = '{0}shuffle="rows"'.format(attrDelim)
                    self.view.insert(edit,sel.end(), printPage)
        except Exception as e:
            print (e)

class addGroupingCols(sublime_plugin.TextCommand):
    def run (self,edit):
        attrDelim = getAttrDelimiter(self)
        try:
            sels = self.view.sel()
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel)

                if input:
                    if re.compile('<(radio|checkbox|select|text|textarea|number|float)').search(input):
                        qTag = re.compile('(<(radio|checkbox|select|text|textarea|number|float))(.*?)(>)', re.DOTALL).search(input).group()
                        multiLine = bool(re.match('<(radio|checkbox|select|text|textarea|number|float)(?=\s*\n)', qTag))
                        if not 'grouping="cols" adim="cols"' in input:
                            printPage = re.sub('(<)(radio|checkbox|select|text|textarea|number|float)(.*?)(>)', '\g<1>\g<2>\g<3>{0}grouping="cols"{0}adim="cols"\g<4>'.format('\n  ' if multiLine else ' '), input, 0, re.DOTALL)
                        else:
                            printPage = input
                    else:
                        printPage = input + '{0}grouping="cols"{0}adim="cols"'.format(attrDelim)
                    self.view.replace(edit,sel, printPage)
                else:
                    printPage = '{0}grouping="cols"{0}adim="cols"'.format(attrDelim)
                    self.view.insert(edit,sel.end(), printPage)
        except Exception as e:
            print (e)
            
class makeHidden(sublime_plugin.TextCommand):
    def run (self,edit):
        attrDelim = getAttrDelimiter(self)
        try:
            sels = self.view.sel()
            input = ''
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel)
                
                if re.compile('^<(radio|checkbox|select|text|textarea|number|float)').search(input):
                    qTag = re.compile('(<(radio|checkbox|select|text|textarea|number|float))(.*?)(>)', re.DOTALL).search(input).group()
                    multiLine = bool(re.match('<(radio|checkbox|select|text|textarea|number|float)(?=\s*\n)', qTag))
                    question = re.compile("(<(radio|checkbox|select|text|textarea|number|float))(.*?)(>)", re.DOTALL)
                    if not "where=" in qTag:
                        input = question.sub("\g<1>\g<3>{0}where=\"execute\"\g<4>".format('\n  ' if multiLine else ' '), input, 0)
                    elif not re.compile('where="[a-z,]?execute').search(qTag):
                        input = input.replace('where="', 'where="execute,')

                    if re.compile('^<checkbox').search(qTag):
                        if 'atleast="' in qTag:
                            atlst = re.compile(' atleast="\d+"').search(qTag).group()
                            input = input.replace(atlst, '')
                    else:
                        if not 'optional="' in qTag:
                            input = input.replace('where="', 'optional="1"{0}where="'.format('\n  ' if multiLine else ' '))
                        else:
                            input = input.replace('optional="0"', 'optional="1"')

                        if not 'translateable="' in qTag:
                            input = input.replace('where="', 'translateable="0"{0}where="'.format('\n  ' if multiLine else ' '))

                    if re.compile('<comment>.*?<\/comment>').search(input):
                        comment = re.compile('[ \t]*<comment>(.|\s)*?<\/comment>\n*').search(input).group()
                        input = input.replace(comment,'')

                    printPage = input.strip()
                    self.view.replace(edit,sel, printPage)
                else:
                    printPage = '{0}where="execute"'.format(attrDelim)
                    self.view.insert(edit,sel.end(), printPage)
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)

class addStyleDev(sublime_plugin.TextCommand):
    def run (self,edit):
        attrDelim = getAttrDelimiter(self)
        try:
            sels = self.view.sel()
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel)

                if input:
                    if re.compile('<(radio|checkbox|select|text|textarea|number|float)').search(input):
                        qTag = re.compile('(<(radio|checkbox|select|text|textarea|number|float))(.*?)(>)', re.DOTALL).search(input).group()
                        multiLine = bool(re.match('<(radio|checkbox|select|text|textarea|number|float)(?=\s*\n)', qTag))
                        if not 'style="dev"' in input:
                            printPage = re.sub('(<)(radio|checkbox|select|text|textarea|number|float)(.*?)(>)', '\g<1>\g<2>\g<3>{0}style="dev"{0}cond="not gv.isSST() and (gv.survey.root.state.dev or gv.survey.root.state.testing)"\g<4>'.format('\n  ' if multiLine else ' '), input, 0, re.DOTALL)
                        else:
                            printPage = input
                    else:
                        printPage = input + '{0}style="dev"{0}cond="not gv.isSST() and (gv.survey.root.state.dev or gv.survey.root.state.testing)"'.format(attrDelim)
                    self.view.replace(edit,sel, printPage)
                else:
                    printPage = '{0}style="dev"{0}cond="not gv.isSST() and (gv.survey.root.state.dev or gv.survey.root.state.testing)"'.format(attrDelim)
                    self.view.insert(edit,sel.end(), printPage)
        except Exception as e:
            print (e)

class addTranslateable(sublime_plugin.TextCommand):
    def run (self,edit):
        attrDelim = getAttrDelimiter(self)
        try:
            sels = self.view.sel()
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel)

                if input:
                    if re.compile('<(radio|checkbox|select|text|textarea|number|float|html|res)').search(input):
                        qTag = re.compile('(<(radio|checkbox|select|text|textarea|number|float|html|res))(.*?)(>)', re.DOTALL).search(input).group()
                        multiLine = bool(re.match('<(radio|checkbox|select|text|textarea|number|float)(?=\s*\n)', qTag))
                        if not 'translateable' in input:
                            printPage = re.sub('(<)(radio|checkbox|select|text|textarea|number|float|html|res)(.*?)(>)', '\g<1>\g<2>\g<3>{0}translateable="0"\g<4>'.format('\n  ' if multiLine else ' '), input, 0, re.DOTALL)
                        else:
                            printPage = input
                        self.view.replace(edit,sel, printPage)
                    else:
                        input = cleanInput(input)
                        input = input.split("\n")
                        for line in input:
                            if re.compile('<(row|col|choice|group)').search(line) and not 'translateable' in line:
                                line = line.replace('>', ' translateable="0">',1)
                                printPage += line + '\n'
                            else:
                                printPage += line + '\n'
                        self.view.replace(edit,sel, printPage.strip('\n'))
                else:
                    printPage = ' translateable="0"'.format(attrDelim)
                    self.view.insert(edit,sel.end(), printPage)
        except Exception as e:
            print (e)

class addMls(sublime_plugin.TextCommand):
    def run (self,edit):
        attrDelim = getAttrDelimiter(self)
        try:
            sels = self.view.sel()
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel)

                if input:
                    if re.compile('<(radio|checkbox|select|text|textarea|number|float|html|res)').search(input):
                        qTag = re.compile('(<(radio|checkbox|select|text|textarea|number|float|html|res))(.*?)(>)', re.DOTALL).search(input).group()
                        multiLine = bool(re.match('<(radio|checkbox|select|text|textarea|number|float)(?=\s*\n)', qTag))
                        if not 'mls' in input:
                            printPage = re.sub('(<)(radio|checkbox|select|text|textarea|number|float|html|res)(.*?)(>)', '\g<1>\g<2>\g<3>{0}mls="english"\g<4>'.format('\n  ' if multiLine else ' '), input, 0, re.DOTALL)
                        else:
                            printPage = input
                        self.view.replace(edit,sel, printPage)
                    else:
                        input = cleanInput(input)
                        input = input.split("\n")
                        for line in input:
                            if re.compile('<(row|col|choice|group)').search(line) and not 'mls' in line:
                                line = line.replace('>', ' mls="english">',1)
                                printPage += line + '\n'
                            else:
                                printPage += line + '\n'
                        self.view.replace(edit,sel, printPage.strip('\n'))
                else:
                    printPage = ' mls="english"'.format(attrDelim)
                    self.view.insert(edit,sel.end(), printPage)
        except Exception as e:
            print (e)

class addCustomAttr(sublime_plugin.TextCommand):
    def run (self,edit):
        self.view.window().show_input_panel(
            'Define attribute and value to add (e.g. cond="1"):',
            '',
            self.on_done_input,
            on_change=None,
            on_cancel=None
        )
    def on_done_input(self, inputAttr):
        try:
            if re.compile('\s*\w[\w\d:]+(=".*")?').search(inputAttr):
                sels = self.view.sel()
                attribute = inputAttr.strip()  if '="' in inputAttr else  inputAttr.strip() + '=""'
                input = ''
                for sel in sels:
                    printPage = ''
                    input = self.view.substr(sel)

                    if input:
                        input = cleanInput(input)
                        input = input.split("\n")
                        for line in input:
                            if re.compile('<\w.*?>').search(line) and not attribute in line:
                                initial = re.compile('<\w.*?>').search(line).group()
                                post = initial.replace('">', '" %s>' % attribute)
                                line = line.replace(initial, post)
                            printPage += line + '\n'
                        with Edit(self.view) as edit:
                            edit.replace(sel, printPage.strip('\n'))
                    else:
                        printPage = attribute
                        with Edit(self.view) as edit:
                            edit.insert(sel.end(), printPage.strip('\n'))
            else:
                print('Not a valid attribute')
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)

class delCustomAttr(sublime_plugin.TextCommand):
    def run (self,edit):
        self.view.window().show_input_panel(
            'Define attribute to remove with/without its value',
            '',
            self.on_done_input,
            on_change=None,
            on_cancel=None
        )
    def on_done_input(self, inputAttr):
        try:
            if re.compile('\s*\w[\w\d:]+(=".*?")?').search(inputAttr):
                sels = self.view.sel()
                attribute = re.escape(inputAttr.strip())
                input = ''
                for sel in sels:
                    printPage = ''
                    input = self.view.substr(sel)

                    if input:
                        input = cleanInput(input)
                        input = input.split("\n")
                        for line in input:
                            if re.compile('<\w.*?>').search(line) and re.compile(attribute).search(line):
                                line = re.sub(' %s(=".*?")?' % attribute, '', line)
                            printPage += line + '\n'
                        
                        with Edit(self.view) as edit:
                            edit.replace(sel, printPage.strip('\n'))
            else:
                print('Not a valid attribute')
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)

################################################################
