#!/usr/bin/python3

import sys
import json
from datetime import datetime

if len(sys.argv) < 3:
    print('No version passed.')
    sys.exit(1)

version = sys.argv[1]
package = sys.argv[2]

now = datetime.now()
date = now.strftime('%Y-%m-%d %H:%M:%S')

with open('channel.json', 'r', encoding='utf-8') as chnl_file:
    chnl_data = json.load(chnl_file)
    for p in chnl_data['packages_cache']['https://radovid.eu/decipherlime/packages.json']:
        if p['name'] == package:
            p['releases'].append({
                    "platforms": [ "*" ],
                    "sublime_text": "*",
                    "version": version,
                    "url": f"https://radovid.eu/decipherlime/packages/{package}-{version}.zip",
                    "date": date,
                    "dependencies": ["mdpopups"]
                })

with open('channel.json', 'w', encoding='utf-8') as chnl_outfile:
    json.dump(chnl_data, chnl_outfile, indent=4)

with open('packages.json', 'r', encoding='utf-8') as pkgs_file:
    pkgs_data = json.load(pkgs_file)
    for p in pkgs_data['packages']:
        if p['name'] == package:
            p['releases'].append({
                    "platforms": [ "*" ],
                    "sublime_text": "*",
                    "version": version,
                    "url": f"https://radovid.eu/decipherlime/packages/{package}-{version}.zip",
                    "date": date,
                    "dependencies": ["mdpopups"]
                })

with open('packages.json', 'w', encoding='utf-8') as pkgs_outfile:
    json.dump(pkgs_data, pkgs_outfile, indent=4)
