import sublime, sublime_plugin, sys, re
from xml.sax.saxutils import escape


# generates regex. Remove <note> if you want those to be read as well
commentedReg = '<!--(.|\s)*?-->'
noteReg = '<note(.|\s)*?<\/note>'
radioReg = '<radio(.|\s)*?<\/radio>'
checkboxReg = '<checkbox(.|\s)*?<\/checkbox>'
selectReg = '<select(.|\s)*?<\/select>'
textareaReg = '<textarea(.|\s)*?<\/textarea>'
textReg = '<text(.|\s)*?<\/text>'
numberReg = '<number(.|\s)*?<\/number>'
floatReg = '<float(.|\s)*?<\/float>'
htmlReg = '<html(.|\s)*?<\/html>'
questionReg = '({0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}|{9})'.format(commentedReg,noteReg,radioReg,checkboxReg,selectReg,textareaReg,textReg,numberReg,floatReg,htmlReg)

# the left part of each regex takes into account selfclosed tags
rowReg = '<row((?!<row)(.|\s))*?(>((?!<row)(.|\s))*?<\/row|\/)>'
colReg = '<col((?!<col)(.|\s))*?(>((?!<col)(.|\s))*?<\/col|\/)>'
choiceReg = '<choice((?!<choice)(.|\s))*?(>((?!<choice)(.|\s))*?<\/choice|\/)>'
groupReg = '<group((?!<group)(.|\s))*?(>((?!<group)(.|\s))*?<\/group|\/)>'
noanswerReg = '<noanswer((?!<noanswer)(.|\s))*?(>((?!<noanswer)(.|\s))*?<\/noanswer|\/)>'
virtualReg = '<virtual((?!<virtual)(.|\s))*?(>((?!<virtual)(.|\s))*?<\/virtual|\/)>'
execReg = '<exec((?!<exec)(.|\s))*?(>((?!<exec)(.|\s))*?<\/exec|\/)>'
validateReg = '<validate((?!<validate)(.|\s))*?(>((?!<validate)(.|\s))*?<\/validate|\/)>'
styleReg = '<style((?!<style)(.|\s))*?(>((?!<style)(.|\s))*?<\/style|\/)>'
onloadReg = '<onLoad((?!<onLoad)(.|\s))*?(>((?!<onLoad)(.|\s))*?<\/onLoad|\/)>'
questionElementsReg = '({0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}|{9}|{10})'.format(commentedReg,rowReg,colReg,choiceReg,groupReg,noanswerReg,virtualReg,validateReg,execReg,styleReg,onloadReg)

cardsortScript = """<style label="cardsortAutosubmit" name="question.after" wrap="ready">
 $ ('#btn_continue, #btn_finish').hide();
 var $buckets = $ (".sq-cardsort-card-item").length;
 $ ('.sq-cardsort-bucket,#btn_randomize').on('click', function() {
    var lastCard = $ ('.sq-cardsort-progress').html();
    if (lastCard == '-/-' || $ ( ":radio:checked" ).length == $buckets){
        $ ('#btn_continue, #btn_finish').show();
        $ ('#btn_continue, #btn_finish').trigger('click');
    }
 });
</style>\n"""

nkNumArrows = """  <style label="NumericArrowsRange" name="question.after" wrap="ready"><![CDATA[
var jsexport = ${jsexport()};
   var min = jsexport.minValue
   var max = jsexport.maxValue
   $ ("input").attr({
       "max" : max,
       "min" : min 
    });
]]></style>
  <style mode="after" name="question.footer"><![CDATA[
<style>
.answers input {
    width: calc(40px + 17px* 2) !important;
}
/* For Chrome and Safari  */
input[type="number"]::-webkit-outer-spin-button,
input[type="number"]::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
}
/* For Firefox  */
input[type="number"] {
    -moz-appearance: textfield;
}
</style>
]]></style>\n"""


"""
Common/Helper Functions
"""

def splitCk(item):
    return bool( item and (len(item.strip()) > 1 and item[0] != '>' or item == '\n') )

def rreplace(s, old, new, occurrence):
    li = s.rsplit(old, occurrence)
    return new.join(li)

# Add attribute and value to the given element(s)
def addAttr(questElem, addAttr, addValue):
    multiLine = bool(re.match('<(radio|checkbox|select|text|textarea|number|float)(\s*\n)', questElem))
    attrDelim = '\n  ' if multiLine else ' '
    addValue = escape(addValue)
    if not ckAttr(questElem, addAttr) and addValue:
        attributeToAdd = '{0}{1}="{2}"'.format(attrDelim, addAttr, addValue)
        if re.compile('\/>[ \s]*?$').search(questElem):
            questElem = questElem.replace('/>','{0}/>'.format(attributeToAdd),1)
        else:
            questElem = questElem.replace('>','{0}>'.format(attributeToAdd),1)
    return questElem

# Remove an attribute
def delAttr(questElem, remAttribute):
    if re.compile('\s{0}=".*?"'.format(remAttribute)).search(questElem):
        toReplace = re.compile('[\n\s]*?{0}=".*?"'.format(remAttribute)).search(questElem).group()
        questElem = questElem.replace(toReplace,'')
    return questElem

# Check if attribute and value (optional) exist
def ckAttr(questElem, attribute, value=''):
    return re.compile('[\s]{0}="{1}'.format(attribute,value)).search(questElem)

# Get the value of an attribute
def attrValue(questElem, getAttr):
    if ckAttr(questElem, getAttr):
        value = questElem.split('%s="' %getAttr)[1].split('"')[0]
        return str(value)
    return None

# get element/tag text
def elem_text(elem):
    textRe = re.compile('[\w"]>((?:.|\s)*?)<\/(?:row|col|choice|group|noanswer|title|comment)')
    if textRe.search(elem):
        return textRe.search(elem).group(1)
    else:
        return elem

# check if matched is good for parsing and not hidden
def goodForParse(initialTag, questionSplit):
    if questionSplit.strip().startswith('<html'):
        return False
    if not re.compile('^<(radio|checkbox|select|text|number|float)').search(initialTag):
        return False
    if '<virtual' in questionSplit:
        return False
    if re.compile('where=".*?(none|execute).*?"|style=".*?dev.*?"|virtual=".*?"|label="v[A-Z]|cond="0"').search(initialTag):
        return False
    return True



"""
Specific-Action Functions
"""

# Check question for grouping by cols
def groupingCols(wholeQuest):
    if ckAttr(wholeQuest,'grouping','cols'):
        return True
    else:
        return False

# Check if question has amount and get its value
def amountCheck(wholeQuest):
    if ckAttr(wholeQuest,'amount'):
        amount = attrValue(wholeQuest, 'amount')
        return amount
    elif ckAttr(wholeQuest,'hpr:amount'):
        amount = attrValue(wholeQuest, 'hpr:amount')
        return amount
    elif ckAttr(wholeQuest,'autosum:amount'):
        amount = attrValue(wholeQuest, 'autosum:amount')
        return amount
    else:
        return False

# Try to find out if question is a percentage one
def isPercentage(wholeQuest):
    if ckAttr(wholeQuest,'ss:postText','%') or ckAttr(wholeQuest,'autosum:postText','%'):
        return True
    elif re.compile('(what percent)|(enter percent)|(enter the percent)', re.IGNORECASE).search(wholeQuest):
        return True
    else:
        return False

def rankCheck(self, wholeQuest):
    if ckAttr(wholeQuest,'minRanks') or ckAttr(wholeQuest,'uses','ranksort\.\d') or ckAttr(wholeQuest,'uses','bmrranking\.\d'):
        return True
    elif self.cntrChoice > 1 and re.compile('(rank)|(from 1)|(of 1)|(the top \d)', re.IGNORECASE).search(wholeQuest):
        return True
    else:
        return False

# Adjust size attribute for numbers according to range
def fixNumSize(questTagSt, wholeQuest):
    size = None
    if 'verify=' in wholeQuest:
        validateRanges = re.findall('verify=".*?range\s*\(.*?"', wholeQuest)
        if len(validateRanges) > 1:
            ranges = []
            for eachRange in validateRanges:
                maxRange = re.compile('range\s*\((?:\s*\d+\s*,\s*)?(\d+)\s*\)').search(eachRange).group(1)
                ranges.append(len(maxRange))
            size = max(ranges)
        elif validateRanges and re.compile('verify=".*?range\s*\((?:\s*\d+\s*,\s*)?(\d+)\s*\).*?"').search(questTagSt):
            maxRange = re.compile('verify=".*?range\s*\((?:\s*\d+\s*,\s*)?(\d+)\s*\).*?"').search(questTagSt).group(1)
            size = len(maxRange)
    if size:
        size = str(2) if size < 3 else str(size)

    if ckAttr(questTagSt, 'points') or ckAttr(questTagSt, 'range'):
        questTagSt = delAttr(questTagSt, 'size')
    elif ckAttr(questTagSt, 'size') and size:
        questTagSt = re.sub('size="\d+"', 'size="%s"' % size, questTagSt)
    elif not ckAttr(questTagSt, 'size') and size:
        questTagSt = addAttr(questTagSt, 'size', size)
    return questTagSt

# Calculate and set fwidth for numbers and floats
def addFwidth(questTagSt, wholeQuest):
    fwid = 0
    if re.compile('^<number').search(questTagSt):
        if 'verify=' in wholeQuest:
            validateRanges = re.findall('verify=".*?range\s*\(.*?"', wholeQuest)
            if len(validateRanges) > 1:
                ranges = []
                for eachRange in validateRanges:
                    maxRange = re.compile('range\s*\((?:\s*\d+\s*,\s*)?(\d+)\s*\)').search(eachRange).group(1)
                    ranges.append(len(maxRange))
                fwid = max(ranges)
            elif validateRanges and re.compile('verify=".*?range\s*\((?:\s*\d+\s*,\s*)?(\d+)\s*\).*?"').search(questTagSt):
                maxRange = re.compile('verify=".*?range\s*\((?:\s*\d+\s*,\s*)?(\d+)\s*\).*?"').search(questTagSt).group(1)
                fwid = len(maxRange)
        elif 'range=' in questTagSt or 'points=' in questTagSt:
            if re.compile('(?:range|points)="\s*(?:.*?\D\s*(?!\w))?(\d+?)\s*"').search(questTagSt):
                maxRange = re.compile('(?:range|points)="\s*(?:.*?\D\s*(?!\w))?(\d+?)\s*"').search(questTagSt).group(1)
                fwid = len(maxRange)
    elif re.compile('^<float').search(questTagSt):
        if 'range=' in questTagSt or 'points=' in questTagSt:
            if re.compile('range="\s*(?:[\d,\-\s]*\D\s*)?(\d+?)\s*"').search(questTagSt):
                maxRange = re.compile('range="\s*(?:[\d,\-\s]*\D\s*)?(\d+?)\s*"').search(questTagSt).group(1)
                fwid = len(maxRange) + 3
    if fwid:
        fwid = str(fwid)
        if attrValue(questTagSt, 'fwidth') != fwid:
            questTagSt = delAttr(questTagSt, "fwidth")
            questTagSt = addAttr(questTagSt, "fwidth", fwid)
    return questTagSt

# Make an autosum/percentage question
def addAutosum(self, questTagSt, wholeQuest):
    label = attrValue(questTagSt, 'label')

    if ckAttr(questTagSt, 'ss:postText'):
        questTagSt = questTagSt.replace('ss:postText', 'autosum:postText')
    else:
        if amountCheck(wholeQuest) in ['100','100+','100-']:
            questTagSt = addAttr(questTagSt, 'autosum:postText', '%')
            questTagSt = questTagSt.replace('range(0,9999)', 'range(0,100)')

    if self.view.settings().get('useHpr'):
        stylePrefix = 'hpr:'
    else:
        stylePrefix = ''

    # Make optional=1 and set for 0-autofill exec to be added
    if amountCheck(wholeQuest) != "100-":
        questTagSt = questTagSt.replace('optional="0"', 'optional="1"')
        questTagSt = addAttr(questTagSt, 'optional', '1')
        questTagSt = addAttr(questTagSt, 'autosum:prefill', '0')
        self.autofillZeroes = True

    # Add grouping=cols for multicol questions
    if self.cntrCol > 1 and self.cntrRow > 1 and not re.compile('<row.*?(autosum:\w+?|amount)=').search(wholeQuest):
        questTagSt = addAttr(questTagSt, 'grouping', 'cols')
        questTagSt = addAttr(questTagSt, 'adim', 'cols')

    # Check for 100+ amount
    if amountCheck(wholeQuest) == "100+":
        questTagSt = delAttr(questTagSt, "amount")
        resErrKantar = ' <res label="amnt100plus">100 or more</res>\n'
        resErrFv = ' <res label="amount100PlusErr">Your answers must add up to 100 or more.</res>\n'

        if self.cntrCol > 1 and self.cntrRow > 1:
            if self.surtype == 'M3':
                if ckAttr(questTagSt, 'grouping', 'cols'):
                    validate100plus = "\nfor eachCol in this.cols:\n    if eachCol.sum lt 100:\n        error(res.qErrors4, col = eachCol)"
                else:
                    validate100plus = "\nfor eachRow in this.rows:\n    if eachRow.sum lt 100:\n        error(res.qErrors5, row = eachRow)"
            elif self.view.settings().get('useHpr'):
                if ckAttr(questTagSt, 'grouping', 'cols'):
                    validate100plus = "\nfor (eachCol of this.cols) {\n  if (eachCol.sum &lt; 100) {\n    error(hlang['amountMismatch'].replace('$(sum)', res.amnt100plus), eachCol)\n  }\n }"
                else:
                    validate100plus = "\nfor (eachRow of this.rows) {\n  if (eachRow.sum &lt; 100) {\n    error(hlang['amountMismatch'].replace('$(sum)', res.amnt100plus), eachRow)\n  }\n }"
            elif self.surtype in ['AG','AG0']:
                if ckAttr(questTagSt, 'grouping', 'cols'):
                    validate100plus = """\nfor eachCol in this.cols:\n\tif eachCol.sum lt 100:\n\t\terror(hlang.get("errTxt20", pipe1=res.amnt100plus), col = eachCol)"""
                else:
                    validate100plus = """\nfor eachRow in this.rows:\n\tif eachRow.sum lt 100:\n\t\terror(hlang.get("errTxt20", pipe1=res.amnt100plus), row = eachRow)"""
            elif self.surtype == 'fv':
                if ckAttr(questTagSt, 'grouping', 'cols'):
                    validate100plus = """\nfor eachCol in this.cols:\n    if eachCol.sum lt 100:\n        error(res.amount100PlusErr, col = eachCol)"""
                else:
                    validate100plus = """\nfor eachRow in this.rows:\n    if eachRow.sum lt 100:\n        error(res.amount100PlusErr, row = eachRow)"""
        else:
            if self.surtype == 'M3':
                validate100plus = "\nif this.sum lt 100:\n    error(res.qErrors4)"
            elif self.view.settings().get('useHpr'):
                validate100plus = "\nif (this.sum &lt; 100) {\n  error(hlang['amountMismatch'].replace('$(sum)', res.amnt100plus))"
            elif self.surtype in ['AG','AG0']:
                validate100plus = "\nif this.sum lt 100:\n\terror(hlang.get(\"errTxt20\", pipe1=res.amnt100plus))"
            elif self.surtype == 'fv':
                validate100plus = "\nif this.sum lt 100:\n    error(res.amount100PlusErr)"

        if re.compile('<(?:hpr:)?validate>(.|\s)*?<\/(?:hpr:)?validate>').search(wholeQuest):
            if not re.compile('(this|x|eachRow|eachCol|%s)\.sum (lt|<|&lt;) 100(:|)' %label).search(wholeQuest):
                preValidate = re.compile('<(?:hpr:)?validate>(.|\s)*?<\/(?:hpr:)?validate>').search(wholeQuest).group()
                validate = re.compile('<(?:hpr:)?validate>(.|\s)*?<\/(?:hpr:)?validate>').search(wholeQuest).group()
                validate = validate.replace('<{0}validate>'.format(stylePrefix), '<{0}validate>{1}'.format(stylePrefix, validate100plus))
                wholeQuest = wholeQuest.replace(preValidate,validate)
        else:
            wholeQuest = wholeQuest.replace(re.compile('</number>').search(wholeQuest).group(),' <{2}validate>{0}\n </{2}validate>\n{1}'.format(validate100plus, re.compile('</number>').search(wholeQuest).group(), stylePrefix))

        if (self.surtype in ['AG','AG0'] or self.view.settings().get('useHpr')) and not self.view.find('label="amnt100plus"', 0):
            wholeQuest = resErrKantar + wholeQuest
        if self.surtype == 'fv' and not self.view.find('label="amount100PlusErr"', 0):
            wholeQuest = resErrFv + wholeQuest

    # Check for 100- amount
    if amountCheck(wholeQuest) == "100-":
        self.autofillZeroes = True
        questTagSt = delAttr(questTagSt, "amount")
        resErrKantar = ' <res label="amnt100minus">Your answers cannot exceed 100.</res>\n'
        resErrFv = ' <res label="amount100MinusErr">Your answers cannot exceed 100.</res>\n'

        if self.cntrCol > 1 and self.cntrRow > 1:
            if self.surtype == 'M3':
                if ckAttr(questTagSt, 'grouping', 'cols'):
                    validate100minus = "\nfor eachCol in this.cols:\n    if eachCol.sum gt 100:\n        error(res.qErrors3, col = eachCol)"
                else:
                    validate100minus = "\nfor eachRow in this.rows:\n    if eachRow.sum gt 100:\n        error(res.qErrors2, row = eachRow)"
            elif self.view.settings().get('useHpr'):
                if ckAttr(questTagSt, 'grouping', 'cols'):
                    validate100minus = """\nfor (eachCol of this.cols) {\n  if (eachCol.sum > 100) {\n    error(res.amnt100minus, eachCol)\n  }\n}"""
                else:
                    validate100minus = """\nfor (eachRow of this.rows) {\n  if (eachRow.sum > 100) {\n    error(res.amnt100minus, eachRow)\n  }\n}"""
            elif self.surtype in ['AG','AG0']:
                if ckAttr(questTagSt, 'grouping', 'cols'):
                    validate100minus = """\nfor eachCol in this.cols:\n\tif eachCol.sum gt 100:\n\t\terror(res.amnt100minus, col = eachCol)"""
                else:
                    validate100minus = """\nfor eachRow in this.rows:\n\tif eachRow.sum gt 100:\n\t\terror(res.amnt100minus, row = eachRow)"""
            elif self.surtype == 'fv':
                if ckAttr(questTagSt, 'grouping', 'cols'):
                    validate100minus = """\nfor eachCol in this.cols:\n    if eachCol.sum gt 100:\n        error(res.amount100MinusErr, col = eachCol)"""
                else:
                    validate100minus = """\nfor eachRow in this.rows:\n    if eachRow.sum gt 100:\n        error(res.amount100MinusErr, row = eachRow)"""
        else:
            if self.surtype == 'M3':
                validate100minus = "\nif this.sum gt 100:\n    error(res.qErrors2)"
            elif self.view.settings().get('useHpr'):
                validate100minus = "\nif (this.sum > 100) {\n  error(res.amnt100minus)\n}"
            elif self.surtype in ['AG0','AG']:
                validate100minus = "\nif this.sum gt 100:\n\terror(res.amnt100minus)"
            elif self.surtype == 'fv':
                validate100minus = "\nif this.sum gt 100:\n    error(res.amount100MinusErr)"

        if re.compile('<(?:hpr:)?validate>(.|\s)*?<\/(?:hpr:)?validate>').search(wholeQuest):
            if not re.compile('(this|x|eachRow|eachCol|%s)\.sum gt 100:' %label).search(wholeQuest):
                preValidate = re.compile('<(?:hpr:)?validate>(.|\s)*?<\/(?:hpr:)?validate>').search(wholeQuest).group()
                validate = re.compile('<(?:hpr:)?validate>(.|\s)*?<\/(?:hpr:)?validate>').search(wholeQuest).group()
                validate = validate.replace('<{0}validate>'.format(stylePrefix), '<{0}validate>{1}'.format(stylePrefix, validate100minus))
                wholeQuest = wholeQuest.replace(preValidate,validate)
        else:
            wholeQuest = wholeQuest.replace(re.compile('</number>').search(wholeQuest).group(),' <{2}validate>{0}\n </{2}validate>\n{1}'.format(validate100minus, re.compile('</number>').search(wholeQuest).group()), stylePrefix)

        if (self.surtype in ['AG','AG0'] or self.view.settings().get('useHpr')) and not self.view.find('label="amnt100minus"', 0):
            wholeQuest = resErrKantar + wholeQuest
        if self.surtype == 'fv' and not self.view.find('label="amount100MinusErr"', 0):
            wholeQuest = resErrFv + wholeQuest

    # add uses=autosum
    if not re.compile('autosum\.\d').search(questTagSt):
        autosumVer = '2' if self.surtype == 'AG0' else '5'
        if 'uses="' in questTagSt:
            questTagSt = questTagSt.replace('uses="','uses="autosum.{},'.format(autosumVer))
        else:
            questTagSt = addAttr(questTagSt, 'uses', 'autosum.{}'.format(autosumVer))

    # fix attrs for hpr
    if self.view.settings().get('useHpr'):
        if re.compile('autosum\.\d').search(questTagSt):
            questTagSt = re.sub(re.compile('\s*uses="autosum\.\d"').search(questTagSt).group(), ' hpr:type="autosum"', questTagSt)
        questTagSt = questTagSt.replace('autosum:postText', 'ss:postText')
        questTagSt = questTagSt.replace('autosum:amount', 'hpr:amount')
        questTagSt = questTagSt.replace('autosum:sumPreText', 'hpr:sumPretext')

    # Check for autosum:amount in row/col and add validate
    elif re.compile('<(row|col) label="(.*?)".*?(?:autosum|hpr):amount="(.*?)"').search(wholeQuest):
        if not re.compile('<(?:hpr:)?validate>(.|\s)*?\.sum(.|\s)*?<\/(?:hpr:)?validate>').search(wholeQuest):
            elemLabelAmount = re.findall('<(?:row|col) label="(.*?)".*?(?:autosum|hpr):amount="(.*?)"', wholeQuest)
            validate = ""
            if self.surtype in ['AG','AG0']:
                for el in elemLabelAmount:
                    validate += "\nif this.{0}.sum != {1}:\n\terror(hlang.get(\"errTxt20\", pipe1=str({1})), this.{0})".format(str(el[0]), str(el[1]).strip('${}'))
            else:
                for el in elemLabelAmount:
                    amntPerEl = str(el[1]).strip('${}') + '.ival' if val not in el[1] else str(el[1]).strip('${}')
                    validate += "\nif this.{0}.sum != {1}:\n    error(res.errMsg % str({1})), this.{0})\n#TODO: Set resource tag".format(str(el[0]), amntPerEl)
            wholeQuest = wholeQuest.replace(re.compile('</number>').search(wholeQuest).group(),' <{0}validate>'.format(stylePrefix) + validate + '\n </{0}validate>\n'.format(stylePrefix) + re.compile('</number>').search(wholeQuest).group())

    # Add 0-autofill exec
    if self.autofillZeroes:
        if not self.view.find('<exec cond="{0}.displayed">\nfor eachRow in {0}.rows:'.format(label), 0) and not self.view.find('<exec>\nautofillZero\({0}\)'.format(label), 0) and not self.view.find('<exec>\nsetZero\({0}\)'.format(label), 0):
            if self.surtype == 'M3':
                wholeQuest = wholeQuest + ('\n\n<exec>\nsetZero({0})\n</exec>'.format(label))
            elif self.surtype == 'AG':
                wholeQuest = wholeQuest + ('\n\n<exec>\nautofillZero({0})\n</exec>'.format(label))
            elif self.cntrRow and not self.cntrCol:
                wholeQuest = wholeQuest + ('\n\n<exec cond="{0}.displayed">\nfor eachRow in {0}.rows:\n\tif eachRow.val == None and eachRow.displayed and "read_only" not in str(eachRow.styles.ss.rowClassNames):\n\t\teachRow.val = 0\n</exec>'.format(label))
            elif self.cntrCol:
                wholeQuest = wholeQuest + ('\n\n<exec cond="{0}.displayed">\nfor eachRow in {0}.rows:\n\tfor eachCol in {0}.cols:\n\t\tif eachRow[eachCol.index].val == None and eachRow.displayed and eachCol.displayed and "read_only" not in str(eachRow.styles.ss.rowClassNames) and "read_only" not in str(eachCol.styles.ss.colClassNames):\n\t\t\teachRow[eachCol.index].val = 0\n</exec>'.format(label))

    return [questTagSt,wholeQuest]


# Check if element needs exclusive=1
def exclusiveCheck(qnElement):
    if re.compile('>[  \t]*prefer not to|>[  \t]*none of|>([  \t]*(none|refuse to answer|not sure|(i )?don.t know)[  \t]*)</|[\(\[](?!pipe|.*?[\[\(]).*?(exclusive|excl|exlc).*?[\)\]]', re.IGNORECASE).search(qnElement):
        qnElement = addAttr(qnElement,'exclusive','1')
        qnElement = addAttr(qnElement,'randomize','0')
        
        if re.compile('[  \t*]*[\(\[](?!pipe|.*?[\[\(]).*?(exclusive|excl|exlc).*?[\)\]][  \t*]*</', re.IGNORECASE).search(qnElement):
            excText = re.compile('[  \t*]*[\(\[](?!pipe|.*?[\[\(]).*?(exclusive|excl|exlc).*?[\)\]][  \t*]*', re.IGNORECASE).search(qnElement).group()
            qnElement = qnElement.replace(excText,'')
    return qnElement

# Check if element should be anchored
def anchorCheck(qnElement):
    if re.compile('[  \t*]*[\(\[](?!pipe|.*?[\[\(]).*?(anchor|dnr|do not randomize|fixed).*?[\)\]][  \t*]*', re.IGNORECASE).search(qnElement):
        qnElement = addAttr(qnElement,'randomize','0')
        anchorText = re.compile('[  \t*]*[\(\[](?!pipe|.*?[\[\(]).*?(anchor|dnr|do not randomize|fixed).*?[\)\]][  \t*]*', re.IGNORECASE).search(qnElement).group()
        qnElement = qnElement.replace(anchorText,'')
    return qnElement

# Find instructions in Q-text and remove it as a <!-- --> or set it as a comment tag
def givenSubmessCheck(wholeQuest, setComment=False):
    if re.compile(r'(\.|\?|\)|:|[^title]\>|\n)\s*("|”|\'|’)?\s*\(?((Please)|(Select)|(Check)).*?\.?\)?\s*<\/title>', re.IGNORECASE).search(wholeQuest):
        instruction = re.compile(r'(\.|\?|\)|:|\>|\n)\s*("|”|\'|’)?\s*\(?((Please)|(Select)|(Check)).*?\.?\)?\s*<\/title>', re.IGNORECASE).search(wholeQuest).group()
        instruction = re.compile(r'\s*\(?((Please)|(Select)|(Check)).*?\.?\)?\s*<\/title>', re.IGNORECASE).search(instruction,1).group().replace('</title>','')
        wholeQuest = wholeQuest.replace(instruction,'',1)
        while '<br/></title>' in wholeQuest:
            wholeQuest = wholeQuest.replace('<br/></title>','</title>',1)
        # Code to set it as <comment> instead
        if setComment:
            if re.compile('<comment>.*?<\/comment>').search(wholeQuest):
                #if re.compile('((<comment>)(\s*|Select one( (in|for) each( row| column)?)?|Select all that apply( (in|for) each( row| column)?)?|Please be( as)? specific( as possible)?|Enter a( whole)? number|@\(instructionTxt\d+\)|\$\{hInstrText_\w+?\})(<\/comment>)) ').search(wholeQuest):
                preComment = re.compile('<comment>(.|\s)*?<\/comment>').search(wholeQuest).group()
                comment = '<comment>{0}</comment>'.format(instruction.strip())
                wholeQuest = wholeQuest.replace(preComment,comment)
            else:
                wholeQuest = wholeQuest.replace('</title>', '</title>\n  <comment>{0}</comment>'.format(instruction.strip(),1))
        else:
            if len(instruction.strip().split(' ')) < 8:
                wholeQuest = wholeQuest.replace('</title>','</title> <!--{0}-->'.format(instruction.strip(),1))
    return wholeQuest

# Add shuffle to question when instruction found
def shuffleAdd(self, wholeQuest):
    if re.compile('(\(|\[)?(randomize(?!=) ?\w*|rotate(?!") ?\w*)(\)|\])?', re.IGNORECASE).search(wholeQuest):
        rnd = re.compile('((\(|\[)\W*?)?(randomize(?!=) ?\w*|rotate(?!") ?\w*)(\W*?(\)|\]))?', re.IGNORECASE).search(wholeQuest).group()
        wholeQuest = wholeQuest.replace(rnd,'',1)
        if 'shuffle="' not in wholeQuest:
            if self.cntrRow > 1 and self.cntrChoice == 0 and self.cntrCol == 0:
                rndElem = 'rows'
            elif self.cntrRow == 0 and self.cntrChoice > 1 and self.cntrCol == 0:
                rndElem = 'choices'
            elif self.cntrRow == 0 and self.cntrChoice == 0 and self.cntrCol > 1:
                rndElem = 'cols'
            else:
                rndElem = 'rows'
            attrDelim = '\n  ' if self.surtype not in ['v2','v3'] else ' '
            if "rotate" in rnd:
                wholeQuest = re.sub('((?:<)(?:radio|checkbox|select|textarea|text|number|float)(?:.*?))(>)','\\g<1>{0}shuffle="{1}"{0}{2}Shuffle="flip"\\g<2>'.format(attrDelim, rndElem, rndElem.strip('s')), wholeQuest, 0, re.DOTALL)
            else:
                wholeQuest = re.sub('((?:<)(?:radio|checkbox|select|textarea|text|number|float)(?:.*?))(>)','\\g<1>{0}shuffle="{1}"\\g<2>'.format(attrDelim, rndElem), wholeQuest, 0, re.DOTALL)
    return wholeQuest

# Check and set open-end on element
def oeCheck(self, qnElement, wholeQuest):
    rowDef = re.compile('<(?:row|col).*?>', re.IGNORECASE).search(qnElement).group()
    rowTextPre = qnElement.replace(rowDef,'',1).replace('</row>','',1).replace('</col>','',1)
    rowText = rowTextPre.replace('_','')
    qnElement = qnElement.replace(rowTextPre, rowText)
    if re.compile('((,*[  \t]*\(*(please\s*)*(specify|describe|explain)[\):_\s]*)|[  \t*]*[\(\[](?!pipe|.*?[\[\(]).*?(open.?end|text.?box|specify).*?[\)\]][  \t*]*_*)[  \t*]*', re.IGNORECASE).search(rowText):
        specifyText = re.compile('((,*[  \t]*\(*(please\s*)*(specify|describe|explain)[\):_\s]*)|[  \t*]*[\(\[](?!pipe|.*?[\[\(]).*?(open.?end|text.?box|specify).*?[\)\]][  \t*]*_*)[  \t*]*', re.IGNORECASE).search(rowText).group()
        # Remove PN and set AG (SPECIFY)
        if self.surtype in ['AG','AG0']:
            qnElement = qnElement.replace(specifyText, ' (SPECIFY)',1)
        elif self.view.settings().get('useQarts'):
            qnElement = qnElement.replace(specifyText, '',1)
        elif self.view.settings().get('useHpr'):
            qnElement = qnElement.replace(specifyText, '',1)
        # Remove PN if there is one
        elif re.compile('[  \t*]*[\(\[](?!pipe|.*?[\[\(]).*?(open.?end|text.?box).*?[\)\]][  \t*]*_*[  \t*]*', re.IGNORECASE).search(qnElement):
            specifyText = re.compile('[  \t*]*[\(\[](?!pipe|.*?[\[\(]).*?(open.?end|text.?box).*?[\)\]][  \t*]*_*[  \t*]*', re.IGNORECASE).search(qnElement).group()
            qnElement = qnElement.replace(specifyText, '',1)
        if re.compile('[  \t*]*[\(\[](?!pipe|.*?[\[\(]).*?(open.?end|text.?box|specify).*?[\)\]][  \t*]*_*', re.IGNORECASE).search(specifyText):
            placeholder = "Please specify..."
        else:
            placeholder = specifyText.strip(',').strip()
        qnElement = addAttr(qnElement,'open','1')
        if self.surtype in ['AG','AG0'] or (self.surtype in ['M3'] and re.compile('^<(number|float)').search(wholeQuest)):
            qnElement = addAttr(qnElement,'openOptional','1')
        qnElement = addAttr(qnElement,'openSize','25')
        qnElement = addAttr(qnElement,'randomize','0')
        if (self.surtype in ['v3'] or self.view.settings().get('useQarts')) and not ckAttr(qnElement,'qa:rplaceholder'):
            qnElement = addAttr(qnElement,'qa:rplaceholder', placeholder)
        elif (self.surtype in ['v3'] or self.view.settings().get('useHpr')) and not ckAttr(qnElement,'hpr:placeholder'):
            qnElement = addAttr(qnElement,'hpr:placeholder', placeholder)
    # Add openOptional for AG and M3 if row already has open="1"
    elif self.surtype in ['AG','AG0'] or (self.surtype in ['M3'] and re.compile('^<(number|float)').search(wholeQuest)):
        if ckAttr(qnElement,'open'):
            qnElement = addAttr(qnElement,'openOptional','1')
    elif self.surtype in ['v3'] or self.view.settings().get('useQarts'):
        if ckAttr(qnElement,'open'):
            qnElement = addAttr(qnElement,'qa:rplaceholder', "Please specify...")
    elif self.view.settings().get('useHpr'):
        if ckAttr(qnElement,'open'):
            qnElement = addAttr(qnElement,'hpr:placeholder', "Please specify...")
    return qnElement

# Add OE validates to question (AG/M3 requirement)
def addOeValidate(self, wholeQuest):
    specifies = ''
    for row in self.cntrRowOeLabels:
        if self.surtype == 'M3' and re.compile('^<(number|float)').search(wholeQuest):
            specifies += "\nOtherSpecify('%s')" % str(row)
        elif re.compile('^<(radio|checkbox|select)').search(wholeQuest):
            specifies += '\nOpenSpecifyChk(%s)' % str(row)
        elif re.compile('^<(textarea|text|number|float)').search(wholeQuest):
            specifies += '\nOpenSpecifyChkAll(%s)' % str(row)
    if re.compile('<validate>(.|\s)*?<\/validate>').search(wholeQuest):
        preValidate = re.compile('<validate>(.|\s)*?<\/validate>').search(wholeQuest).group()
        validate = re.compile('<validate>(.|\s)*?<\/validate>').search(wholeQuest).group()
        if re.compile('OtherSpecify|OpenSpecifyChk|OpenSpecifyChkAll').search(wholeQuest):
            validate = re.sub('(OtherSpecify|OpenSpecifyChk|OpenSpecifyChkAll)\(.+?\)\n', '', validate)
        validate = validate.replace('<validate>', '<validate>%s' % specifies)

        wholeQuest = wholeQuest.replace(preValidate,validate)
    elif specifies:
        wholeQuest = wholeQuest.replace(re.compile('</(radio|checkbox|select|number|textarea|text)>').search(wholeQuest).group(),'  <validate>{0}\n  </validate>\n{1}'.format(specifies, re.compile('</(radio|checkbox|select|number|textarea|text)>').search(wholeQuest).group()))
    if self.cntrColOe > 0:
        wholeQuest += '<!--NOTE: The OE validate does not work for col OEs. Should be added manually!-->'
    return wholeQuest

# Add AG0 validate to text/textarea questions
def addTextValidate(wholeQuest):
    if not re.compile('OpenQuestionChk').search(wholeQuest):
        specifies = '\nOpenQuestionChk()'
        if re.compile('<validate>(.|\s)*?<\/validate>').search(wholeQuest):
            preValidate = re.compile('<validate>(.|\s)*?<\/validate>').search(wholeQuest).group()
            validate = re.compile('<validate>(.|\s)*?<\/validate>').search(wholeQuest).group()
            validate = validate.replace('<validate>', '<validate>' + specifies)
            wholeQuest = wholeQuest.replace(preValidate,validate)
        else:
            wholeQuest = wholeQuest.replace(re.compile('</(textarea|text)>').search(wholeQuest).group(),' <validate>{0}\n </validate>\n{1}'.format(specifies, re.compile('</(textarea|text)>').search(wholeQuest).group()))
    return wholeQuest

# if re.compile('').search(initialTag):
# Add OE gibberish check to text/textarea questions
def addGibberishCheck(self, wholeQuest):
    allTxtQns = [self.view.substr(txtReg) for txtReg in self.view.find_all('<text(area)?.*?>')]
    textQns = []
    for tqn in allTxtQns:
        if not re.compile('where=".*?(none|execute).*?"|style=".*?dev.*?"|virtual=".*?"|label="v[A-Z]|cond="0"').search(tqn):
            textQns.append(tqn)
    if self.view.settings().get('surveyCompany') == 'EMEA' and (self.view.find('GibberishPIIcheck\(',0) or len(textQns) < 2):
        label = attrValue(wholeQuest, 'label')
        if not self.view.find('GibberishPIIcheck\({0}'.format(label), 0):
            wholeQuest = wholeQuest + ('\n\n<exec>\nGibberishPIIcheck({0}, hFlag_PII)\n</exec>'.format(label))
    return wholeQuest

# Find [programer notes] left in text and comment them out
def pnCheck(qnElement):
    if re.compile('^<(row|col|choice|group|noanswer)').search(qnElement) or re.compile('\[([^]]*)\][  \t]*<\/title>').search(qnElement):
        if re.compile('(\[([^]]*)\][  \t]*)+<\/(row|col|choicerow|col|choice|group|noanswer|title)>').search(qnElement) or re.compile('">([  \t]*\[([^]]*)\][  \t]*)+').search(qnElement):
            prognote = re.compile('(">|tilte>)([  \t]*\[([^]]*)\][  \t]*)+|([  \t]*\[([^]]*)\][  \t]*)+(<\/(row|col|choice|group|noanswer|title)>)').search(qnElement).group().replace('">','').replace('</row>','').replace('</col>','').replace('</choice>','').replace('</group>','').replace('</noanswer>','').replace('</title>','')
            if len(prognote) > 1 and not "pipe" in prognote and not "insert" in prognote and not "res" in prognote and not "loopvar" in prognote:
                qnElement = qnElement.replace(prognote,'')
                closeTag = re.compile('<\/(row|col|choice|group|noanswer|title)>').search(qnElement).group()
                qnElement = re.sub(closeTag, '\\g<0> <!--{0}-->'.format(prognote.strip()), qnElement)
    qnElement = qnElement.replace('<!---->','')
    return qnElement

# Escape ampersands and special characters
def textEscape(self, qnElement):
    if not re.compile('^\s*(<virtual|<exec|<style)').search(qnElement):

        # escapes &
        if re.compile('&(?!(#\d+|lt|gt|amp|quot|apos);)').search(qnElement):
            qnElement = re.sub('&(?!(#\d+|lt|gt|amp|quot|apos);)', '&amp;', qnElement)

         # escapes <
        if re.compile('<([^A-Za-z\/])').search(qnElement):
            qnElement = re.sub('<([^A-Za-z\/])', '&lt;\g<1>', qnElement)
        
        # replace smart quotes, ellipsis and em-dashes and add supscripts
        #beforeEscape = ['…','‘','’','“','”','–']
        #afterEscape = ['...','\'','\'','"','"','-']

        unsupDigits = ['1st','2nd','3rd','4th','5th','6th','7th','8th','9th','0th','1th','2th','3th']
        suppedDigits = ['1<sup>st</sup>','2<sup>nd</sup>','3<sup>rd</sup>','4<sup>th</sup>','5<sup>th</sup>','6<sup>th</sup>','7<sup>th</sup>','8<sup>th</sup>','9<sup>th</sup>','0<sup>th</sup>','1<sup>th</sup>','2<sup>th</sup>','3<sup>th</sup>']

        #for pair in zip(beforeEscape, afterEscape):
        #    qnElement = qnElement.replace(pair[0],pair[1])

        for pair in zip(unsupDigits, suppedDigits):
            if re.compile(pair[0]).search(qnElement):
                for num in pair[0].finditer(qnElement):
                    numPt = num.span()[0]
                    if 'string.quoted.double.xml' not in self.view.scope_name(numPt) and 'comment.block.xml' not in self.view.scope_name(numPt) and 'string.unquoted.cdata.xml' not in self.view.scope_name(numPt):
                        qnElement = qnElement.replace(pair[0],pair[1])
    return qnElement

# Auto set li tags
def addLiTags(qnElement):
    liReg = re.compile('(&#8226;|•|&#9679;|●|&#9702;|◦|&#9670;|◆|&#9671;|◇)(.+?)(\n|<\/(title|html))')
    if liReg.search(qnElement):
        if qnElement.find('<li>') == -1:
            qnElement = liReg.sub( '    <li>\g<2></li>\g<3>', qnElement)
            qnElement = qnElement.replace( '<li>', '<ul>\n    <li>', 1)
            qnElement = rreplace(qnElement, '</li>', '</li>\n</ul>', 1)
    return qnElement

# Make each question attribute on new line
def newLineAttrs(questTagSt):
    questTagSt = re.sub('(")\s+(\w)','\g<1>\n  \g<2>', questTagSt)
    questTagSt = re.sub('(<)(radio|checkbox|select|textarea|text|number|float|html)\s+(\w)','\g<1>\g<2>\n  \g<3>', questTagSt)
    return questTagSt



"""
Element Processing Functions
"""

# Check and get elements from a define tag if inserted in question
def findDefineBySource(self, source, asType, excluded):
    # found define tag region
    foundReg = self.view.find('<(define)[\s\n]*?label="{0}"(.|\n)*?</(define)>'.format(source), 100)
    if foundReg:
        foundSplit = self.view.substr(foundReg)
        questionElements = [el for el in re.split('({0}|{1})'.format(rowReg,groupReg), foundSplit) if splitCk(el)]

        for elementSplit in questionElements:
            if 'label="' in elementSplit:
                eLabel = attrValue(elementSplit, 'label')
                if not re.search(eLabel + '\b', excluded):
                    if re.compile('^<row').search(elementSplit):
                        # count elements from the specified type in as=""
                        if asType == 'cols':
                            elementSplit = elementSplit.replace('row', 'col')
                        elif asType == 'choices':
                            elementSplit = elementSplit.replace('row', 'choice')

                        _ = rowsColsCheck(self, elementSplit, 'None')

                    elif re.compile('^<group').search(elementSplit) and 'where="none"' not in elementSplit:
                        self.cntrGroup += 1
    else:
        return False


# Check and get element from another question if onLoad called
def findElemByLabel(self, label, elements, excluded):
    # find the region for question with that label
    foundReg = self.view.find('<(number|float|radio|checkbox|select|text|textarea)[\s\n]*?label="{0}"(.|\n)*?</(number|float|radio|checkbox|select|text|textarea)>'.format(label), 100)
    matchEms = '|'.join(em.rstrip('s') for em in elements)

    if foundReg:
        foundSplit = self.view.substr(foundReg)

        # creates row, col, choice, group, and noanswer arrays with non-relevant code inbetween
        questionElements = [el.strip() for el in re.split(questionElementsReg, foundSplit) if splitCk(el)]

        # goes through the question elements (rows cols etc | elementSplit)
        for elementSplit in questionElements:
            #ignores commented code
            if not re.compile('^\s*(<!--|<note>)').search(elementSplit):
               # check for predefined lists
                if "<insert" in elementSplit:
                    attrValue(qnElement, 'label')
                    source = attrValue(elementSplit, 'source')
                    asType = attrValue(elementSplit, 'as') if 'as' in elementSplit else 'row'
                    exclude = attrValue(elementSplit, 'exclude') if 'exclude' in elementSplit else ''
                    findDefineBySource(self, source, asType, exclude)

               # check for another onLoad
                if 'onLoad' in elementSplit:
                    for onefun in re.findall("(copy\()(.+?)(\))", elementSplit):
                        copyArguments = re.split("\s*,\s*", onefun[1])
                        copyLabel = copyArguments[0].strip("'")
                        elemTypes = [tp.split('=')[0] for tp in copyArguments[1:] if tp.endswith('True') and 'prepend' not in tp]
                        if 'exclude' in onefun[1]:
                            excludeElems = [tp.split('=')[1].strip("'") for tp in copyArguments[1:] if tp.startswith('exclude')][0]
                        else:
                            excludeElems = ''
                       # some recursion
                        findElemByLabel(self, copyLabel, elemTypes, excludeElems)
                
                # check element if specified in the copy() and not excluded
                if re.compile('^<({})'.format(matchEms)).search(elementSplit) and not any('label="{0}"'.format(exLbl) in elementSplit for exLbl in excluded.split(' ')):
                    _ = rowsColsCheck(self, elementSplit, 'None')


# for preprocessing elements only when no question was found
def onlyElementsNoQn(self, initialInput):
    # creates an array with element tags
    input = [el for el in re.split(questionElementsReg, initialInput) if splitCk(el)]

    # goes through the question elements (rows cols etc | elementSplit)
    for eind, elementSplit in enumerate(input):
        beforeRead = elementSplit

        if not re.compile('^\s*(<!--|<note>)').search(elementSplit):
            elementSplit = rowsColsCheck(self, elementSplit, 'None')

        afterRead = elementSplit
        initialInput = initialInput.replace(beforeRead, afterRead, 1)
    return initialInput


# Split question elements and process them
def preprocessQuestion(self, questionElementsReg, questionSplit):
    # creates row, col, choice, group, and noanswer arrays with non-relevant code inbetween
    questionElements = [el.strip() for el in re.split(questionElementsReg, questionSplit) if splitCk(el)]
    # goes through the question elements (rows cols etc | elementSplit)
    for eind, elementSplit in enumerate(questionElements):
        elementBefore = elementSplit

        # ignores commented code
        if not re.compile('^\s*(<!--|<note>)').search(elementSplit):
            elementSplit = rowsColsCheck(self, elementSplit, questionSplit)

           # get the list that is inserted
            if "<insert" in elementSplit:
                source = attrValue(elementSplit, 'source')
                asType = attrValue(elementSplit, 'as') if 'as' in elementSplit else 'row'
                exclude = attrValue(elementSplit, 'exclude') if 'exclude' in elementSplit else ''
                findDefineBySource(self, source, asType, exclude)

           # find question from which elements were copied
            if 'onLoad' in elementSplit:
                for onefun in re.findall("(copy\()(.+?)(\))", elementSplit):
                    copyArguments = re.split("\s*,\s*", onefun[1])
                    copyLabel = copyArguments[0].strip("'")
                    elemTypes = [tp.split('=')[0] for tp in copyArguments[1:] if tp.endswith('True') and 'prepend' not in tp]
                    if 'exclude' in onefun[1]:
                        excludeElems = [tp.split('=')[1].strip("'") for tp in copyArguments[1:] if tp.startswith('exclude')][0]
                    else:
                        excludeElems = ''
                    findElemByLabel(self, copyLabel, elemTypes, excludeElems)

        questionSplit = questionSplit.replace(elementBefore, elementSplit, 1)
        questionElements[eind] = elementSplit

    # questionTag is the question defining tag
    questionTagBefore = [x for x in re.split('(<.*>)',questionElements[0]) if x and (len(x))][0]
    questionTag = questionTagBefore

    return [questionTag, questionSplit]


# count, check and edit elements
def rowsColsCheck(self, qnElement, wholeQuest):
   # various text escapes
    qnElement = textEscape(self, qnElement)
    if re.compile('^<(row|col)').search(qnElement):
        if re.compile('^<(checkbox)').search(wholeQuest) or self.view.settings().get('useQarts'):
           # exclusive check
            qnElement = exclusiveCheck(qnElement)
        if re.compile('^<row').search(qnElement):
            self.cntrRow += 1
            if ckAttr(qnElement, 'optional', '1'):
                self.cntrRowOptional += 1
            if re.compile('[\|\-–—~]|\s+OR\s+').search(elem_text(qnElement)):
                self.cntrLeftRight += 1
                self.isRating = True
           # OE elements check
            qnElement = oeCheck(self, qnElement, wholeQuest)
            if ('open="1"' in qnElement):
                self.cntrRowOe += 1
               # get labels for validates
                if self.surtype == 'M3':
                    self.cntrRowOeLabels.append(attrValue(qnElement, 'label'))
                elif self.surtype == 'AG0':
                    self.cntrRowOeLabels.append(attrValue(qnElement, 'label')[1:])
        elif re.compile('^<col').search(qnElement):
            self.cntrCol += 1
            if ckAttr(qnElement, 'value'):
                self.hasValues = True
            if elem_text(qnElement).replace('+','').strip().isdigit():
                self.cntrColDigits += 1
           #OE elements check
            qnElement = oeCheck(self, qnElement, wholeQuest)                
            if ('open="1"' in qnElement):
                self.cntrColOe += 1
       # anchor check
        qnElement = anchorCheck(qnElement)
       # add li tags if bullets found
        qnElement = addLiTags(qnElement)
       # PN check
        qnElement = pnCheck(qnElement)
       # qarts-specific
        if self.view.settings().get('useQarts'):
            qnElement = qartsRowsCols(self, qnElement)
    else:
        if re.compile('^<choice').search(qnElement):
            self.cntrChoice += 1
           #anchor check
            qnElement = anchorCheck(qnElement)
        elif re.compile('^<group').search(qnElement) and 'where="none"' not in qnElement:
            self.cntrGroup += 1
        elif self.hasNoanswer and re.compile('^<noanswer').search(qnElement) and self.view.settings().get('useQarts'):
            qnElement = qnElement + "<!--NOTE: Only 1 noanswer is supported with QArts!-->"
        elif re.compile('^<noanswer').search(qnElement):
            self.hasNoanswer = True
       # PN check
        qnElement = pnCheck(qnElement)
    return qnElement

# additional qarts-only element parsing
def qartsRowsCols(self, qnElement):
   # image check
    if "<img" in qnElement:
        matchSrc = re.findall('src=(?:\'|").+?(?:\'|")', elem_text(qnElement))
        source = matchSrc[0].replace('"','').replace('\'','').replace('src=','')
        qnElement = addAttr(qnElement, 'qa:image', source)
        qnElement = re.sub('(<img.*?>)', '', qnElement)
        if len(re.sub('\s', '', elem_text(qnElement))) < 2:
            qnElement = addAttr(qnElement, 'alt', re.sub('\[rel (.+?)\.\w+\]', '\g<1>', source))
    if ckAttr(qnElement, 'qa:image'):
        if re.search('^<row', qnElement):
            self.rowatype = "image"
        elif re.search('^<col', qnElement):
            self.colatype = "image"
        qnElement = re.sub('(<img.*?>)', '', qnElement)
   # subtitle check
    prefix = "r" if re.search('^<row', qnElement) else "c"
    if "(" in elem_text(qnElement) and not ckAttr(qnElement, 'qa:%ssubtitle' %prefix) and re.search('\)\s*?<', qnElement) and not re.search('(please|specify|explain|describe)', qnElement) and not re.search('\((?:s|ren)\)\s*?<', qnElement):
        if re.compile( '(\(([^)]*)\)[^(]*)' ).search(elem_text(qnElement)):
            capturedText = re.compile( '(\(([^)]*)\)[^(]*)' ).search(elem_text(qnElement)).group()
            tempText = elem_text(qnElement).replace(capturedText, '').strip()
            if re.compile('\[\bpipe:.*?\]').search(capturedText):
                toReplace = re.compile('\[\bpipe:.*?\]').search(capturedText).group()
                replaceWith = re.compile('\[\bpipe:.*?\]').search(capturedText).group().replace('[pipe:','${pipe.').replace(']','}').replace(' lower','.lower()').replace(' upper','.upper()').replace(' ','')
                capturedText = escape( capturedText.replace(toReplace, replaceWith) )
            qnElement = addAttr(qnElement, 'qa:%ssubtitle' %prefix, capturedText.strip())
            altText = escape( tempText + " " + capturedText.strip() )
            qnElement = qnElement.replace(elem_text(qnElement), tempText)
            qnElement = addAttr(qnElement, 'alt', altText)
   # row
    if re.search('^<row', qnElement):
        if ckAttr(qnElement, 'qa:rsubtitle') or ckAttr(qnElement, 'qa:rdescription'):
            if ["qa:rflexdir","row"] not in self.additionalAttr:
                self.additionalAttr.append(["qa:rflexdir","row"])
        if ckAttr(qnElement, 'optional', '1'):
            self.cntrRowOptional += 1
        if not ckAttr(qnElement, 'qa:rdelabellt'):
            if '|' in elem_text(qnElement):
                self.isDoubleEnded = True
        else:
            self.isDoubleEnded = True
   # col
    elif re.search('^<col', qnElement):
        if ckAttr(qnElement, 'value'):
            self.isScale = True
        if elem_text(qnElement).replace('+','').strip().isdigit():
            self.cntrColDigits += 1
    return qnElement


"""
SurveyType Bots
"""


############################# Vicky #############################

class runVicky(sublime_plugin.TextCommand):
    cntrRow = 0
    cntrRowOe = 0
    cntrCol = 0
    cntrColOe = 0
    cntrChoice = 0
    cntrGroup = 0
    cntrRowOptional = 0
    cntrColDigits = 0
    hasNoanswer = False
    isScale = False
    isRating = False
    autofillZeroes = False
    cntrLeftRight = 0
    hasValues = False

    def addSubmess(self, wholeQuest):
        instruction = ''
        if re.compile('^<(radio)').search(wholeQuest):
            if self.cntrRow < 2 or self.cntrCol < 2:
                instruction = 'Select one'
            elif self.cntrRow > 1 and self.cntrCol > 1:
                if groupingCols(wholeQuest):
                    instruction = 'Select one in each column'
                else:
                    instruction = 'Select one in each row'
        elif re.compile('^<(select)').search(wholeQuest):
            instruction = '' 
        elif re.compile('^<(checkbox)').search(wholeQuest):
            if self.cntrRow < 2 or self.cntrCol < 2:
                instruction = 'Select all that apply'
            elif self.cntrRow > 1 and self.cntrCol > 1:
                if groupingCols(wholeQuest):
                    instruction = 'Select all that apply in each column'
                else:
                    instruction = 'Select all that apply in each row'
        elif re.compile('^<(textarea)').search(wholeQuest):
            instruction = 'Please be as specific as possible'
        elif re.compile('^<(text)').search(wholeQuest):
            instruction = 'Please be specific'
        elif re.compile('^<(float)').search(wholeQuest):
            instruction = "Enter a number"
        elif re.compile('^<(number)').search(wholeQuest):
            if self.cntrRow < 2 and self.cntrCol < 2:
                instruction = 'Enter a whole number'
            else:
                instruction = 'Enter a whole number in each field below'

        if re.compile('<comment>.*?<\/comment>').search(wholeQuest):
            if re.compile('((<comment>)(\s*|Select one( (in|for) each( row| column)?)?|Select all that apply( (in|for) each( row| column)?)?|Please be( as)? specific( as possible)?|Enter a( whole)? number|@\(instructionTxt\d{0,2}\))(<\/comment>))').search(wholeQuest):
                preComment = re.compile('<comment>(.|\s)*?<\/comment>').search(wholeQuest).group()
                comment = '<comment>{0}</comment>'.format(instruction)
                wholeQuest = wholeQuest.replace(preComment,comment)
        else:
            wholeQuest = wholeQuest.replace('</title>','</title>\n  <comment>{0}</comment>'.format(instruction,0))
        return wholeQuest

    def runSelfserveBot(self, selectedInput):
        initialInput = selectedInput

        # checks if there are actually any questions
        if re.compile(questionReg).search(initialInput):
            # creates an array with question tags as elements. all non-question code is grouped into arrays inbetween the questions
            input = [qn for qn in re.split(questionReg, initialInput) if splitCk(qn)]
            # reads for each element(question | questionSplit) of the survey        
            for qind,questionSplit in enumerate(input):
                # text to be replaced after the script is finished
                beforeRead = questionSplit
                # get question tag
                initialTag = [x for x in re.split('(<.*?>)', questionSplit) if splitCk(x)][0]

                if goodForParse(initialTag, questionSplit):
                    # generates checks and counters  
                    self.cntrRow = 0
                    self.cntrRowOe = 0
                    self.cntrCol = 0
                    self.cntrColOe = 0
                    self.cntrChoice = 0
                    self.cntrGroup = 0
                    self.cntrRowOptional = 0
                    self.cntrColDigits = 0
                    self.hasNoanswer = False
                    self.isScale = False
                    self.isRating = False
                    self.autofillZeroes = False
                    self.cntrLeftRight = 0
                    isLeftRight = False

                    questionTag, questionSplit = preprocessQuestion(self, questionElementsReg, questionSplit)

                    # radio
                    if re.compile('^<radio').search(questionTag):
                        if ckAttr(questionTag, 'values'):
                            self.isScale = True
                        scaleCases = ["day","week","month","year","never"]
                        scaleCases2 = ["satisfied","likely","important","agree","prefer"]
                        countScaleCases = 0
                        countScaleCases2 = 0
                        if self.cntrCol == 0:
                            rowElements = [r.strip() for r in re.split('({0})'.format(rowReg), questionSplit) if splitCk(r) and re.search(rowReg, r)]
                            for row in rowElements:
                                for oneCase in scaleCases:
                                    if oneCase in elem_text(row).lower():
                                        countScaleCases += 1
                                        break
                                for oneCase2 in scaleCases2:
                                    if oneCase2 in elem_text(row).lower():
                                        countScaleCases2 += 1
                                        break
                            if countScaleCases >= 3 or countScaleCases2 >= 3:
                                self.isRating = True
                        else:
                            colElements = [c.strip() for c in re.split('({0})'.format(colReg), questionSplit) if splitCk(c) and re.search(colReg, c)]
                            for col in colElements:
                                for oneCase in scaleCases:
                                    if oneCase in elem_text(col).lower():
                                        countScaleCases += 1
                                        break
                                for oneCase2 in scaleCases2:
                                    if oneCase2 in elem_text(col).lower():
                                        countScaleCases2 += 1
                                        break
                            if countScaleCases >= 3 or countScaleCases2 >= 3:
                                self.isRating = True
                        if self.isRating:
                            questionTag = addAttr(questionTag, 'type', 'rating')
                            if not self.hasValues:
                                questionTag = addAttr(questionTag, 'values', 'order')

                        if self.cntrCol > 1 and self.cntrLeftRight == self.cntrRow:
                            isLeftRight = True
                            rlSplitters = []
                            rowElements = [r.strip() for r in re.split('({0})'.format(rowReg), questionSplit) if splitCk(r) and re.search(rowReg, r)]
                            # detect character used beteen left and right
                            for elementSplit in rowElements:
                                rlSplitters += [_.strip() for _ in re.findall(r'([\|\-–—~]|\s+OR\s+)', elem_text(elementSplit))]
                            for char in ['|','-','–','—','~','OR']:
                                if rlSplitters.count(char) == self.cntrLeftRight:
                                    splitRL = re.escape(char)
                            # split and set row text to left and right legend
                            for elementSplit in rowElements:
                                elementBefore = elementSplit
                                if not re.compile('^\s*(<!--|<note>)').search(elementSplit) and not ckAttr(elementSplit, 'rightLegend') and re.search(splitRL, elem_text(elementSplit)):
                                    rowText = elem_text(elementSplit)
                                    leftText = re.split(splitRL, rowText, 1)[0].strip()
                                    rightText = re.split(splitRL, rowText, 1)[1].strip()
                                    elementSplit = elementSplit.replace(rowText, leftText)
                                    elementSplit = addAttr(elementSplit, 'alt', escape(rowText))
                                    elementSplit = addAttr(elementSplit, 'rightLegend', escape(rightText))
                                questionSplit = questionSplit.replace(elementBefore, elementSplit, 1)
                            # questionTag = addAttr(questionTag, 'uses', 'leftright.1')

                    # radio/checkbox - cardsort
                    if re.compile('^<(radio|checkbox)').search(questionTag):
                        if re.compile('cardsort').search(questionTag):
                            autosubmitStyle = '\n'
                            questionTag = addAttr(questionTag, 'cardsort:completionHTML', ' ')
                            questionTag = addAttr(questionTag, 'cardsort:dragAndDrop', '0')
                            if re.compile('^<radio').search(questionTag):
                                if not self.view.find('label="cardsortAutosubmit"', 0):
                                    autosubmitStyle = cardsortScript
                                elif 'copy="cardsortAutosubmit"' not in questionSplit and 'label="cardsortAutosubmit"' not in questionSplit:
                                    autosubmitStyle = """  <style copy="cardsortAutosubmit"/>\n"""
                                questionSplit = questionSplit.replace('</radio>', '%s</radio>' % autosubmitStyle)
                                questionTag = addAttr(questionTag, 'cardsort:displayCounter', '0')
                                questionTag = addAttr(questionTag, 'cardsort:displayNavigation', '0')

                            elif re.compile('^<checkbox').search(questionTag):
                                if not self.view.find('label="cardsortAutosubmitCheckbox"', 0):
                                    autosubmitStyle = cardsortScript
                                elif 'copy="cardsortAutosubmitCheckbox"' not in questionSplit and 'label="cardsortAutosubmitCheckbox"' not in questionSplit:
                                    autosubmitStyle = """  <style copy="cardsortAutosubmitCheckbox"/>\n"""
                                questionSplit = questionSplit.replace('</checkbox>', autosubmitStyle + '</checkbox>')

                            # questionTag = addAttr(questionTag, 'cardsort:bucketHoverCSS', 'background-color:#bb29bb; color:white;')
                            questionTag = addAttr(questionTag, 'cardsort:cardCSS', 'font-size:18px; min-height:80px;')
                            if 'type="rating"' in questionTag or self.cntrCol <= 6:
                                questionTag = addAttr(questionTag, 'cardsort:bucketsPerRow', str(self.cntrCol))
                                questionTag = addAttr(questionTag, 'cardsort:bucketCSS', 'font-size:15px; border:1px solid rgb(219, 219, 210); min-height:66px; width:%dpx; ' % int(950/self.cntrCol-20))
                            else:
                                questionTag = addAttr(questionTag, 'cardsort:bucketCSS', 'font-size:15px; border:1px solid rgb(219, 219, 210); min-height:66px;')

                   # select                
                    elif re.compile('^<select').search(questionTag):
                        if rankCheck(self, questionSplit):
                            questionTag = addAttr(questionTag, 'uses', 'ranksort.7')
                            try:
                                questionTag = addAttr(questionTag, 'minRanks', self.cntrChoice)
                            except:
                                questionTag = addAttr(questionTag, 'minRanks', '')

                    # number
                    elif re.compile('^<number').search(questionTag):
                        if amountCheck(questionSplit):
                            questionTag,questionSplit = addAutosum(self, questionTag, questionSplit)
                        questionTag = fixNumSize(questionTag,questionSplit)
                        if self.view.settings().get('surveyType') == 'nk':
                            if not self.view.find('label="NumericArrowsRange"', 0):
                                nikeNumStyle = nkNumArrows
                            elif 'copy="NumericArrowsRange"' not in questionSplit and 'label="NumericArrowsRange"' not in questionSplit:
                                nikeNumStyle = """  <style copy="NumericArrowsRange"/>\n"""
                            questionSplit = questionSplit.replace('</number>', nikeNumStyle + '</number>')

                    # PN check
                    questionTag = pnCheck(questionTag)

                    # make attrs on new line
                    questionTag = newLineAttrs(questionTag)

                    questionSplit = questionSplit.replace(initialTag,questionTag)
                    questionSplit = questionSplit.replace('><title>','>\n  <title>')
                    questionSplit = questionSplit.replace('><!--NOTE:','>\n<!--NOTE:')

                    questionSplit = shuffleAdd(self, questionSplit)

                    # PN check
                    questionSplit = pnCheck(questionSplit)

                    # Submessages
                    if self.view.settings().get('parseDefaultComments'):
                        questionSplit = self.addSubmess(questionSplit)
                    setFound = self.view.settings().get('setFoundComment') if self.view.settings().has('setFoundComment') else True
                    questionSplit = givenSubmessCheck(questionSplit, setComment=setFound)

                    afterRead = questionSplit
                    initialInput = initialInput.replace(beforeRead, afterRead, 1)
                    
                    # Putting the 0 autofil exec after the suspend
                    if self.autofillZeroes and '</exec>\n<suspend/>' in initialInput:
                        initialInput = initialInput.replace('</exec>\n<suspend/>','</exec>').replace('</number>', '</number>\n<suspend/>')
                else:
                    # text fixes and new line attributes in htmls/hidden questions
                    questionTag = newLineAttrs(initialTag)
                    questionSplit = questionSplit.replace(initialTag, questionTag)
                    questionSplit = textEscape(self, questionSplit)
                    questionSplit = addLiTags(questionSplit)
                    afterRead = questionSplit
                    initialInput = initialInput.replace(beforeRead, afterRead, 1)

        # reading rows only
        elif re.compile(questionElementsReg).search(initialInput):
            initialInput = onlyElementsNoQn(self, initialInput)

        return initialInput

    def run(self, edit):
        try:
            self.surtype = self.view.settings().get('surveyType')
            sels = self.view.sel()
            input = ''
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel).strip()
                printPage = self.runSelfserveBot(input)
                self.view.replace(edit,sel, printPage)
        # except Exception as e:
        #     command = self.__class__.__name__
        #     self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
        #     print ("Vicky bot failed:")
        #     print (e)
        except IndexError:
           print ('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))

############################# Clarry #############################

class runClarry(sublime_plugin.TextCommand):
    cntrRow = 0
    cntrRowOe = 0
    cntrCol = 0
    cntrColOe = 0
    cntrChoice = 0
    cntrGroup = 0
    cntrRowOptional = 0
    cntrColDigits = 0
    hasNoanswer = False
    isScale = False
    autofillZeroes = False
    isDoubleEnded = False
    rowatype = "text"
    colatype = "text"
    additionalAttr = []

    def addSubmess(self, wholeQuest, qartsInstruction):
        instruction = qartsInstruction
        if re.compile('<comment>.*?<\/comment>').search(wholeQuest):
            if re.compile('((<comment>)(\s*|Select one( (in|for) each( row| column)?)?|Select all that apply( (in|for) each( row| column)?)?|Please be( as)? specific( as possible)?|Enter a( whole)? number|@\(instructionTxt\d{0,2}\))(<\/comment>))').search(wholeQuest):
                preComment = re.compile('<comment>(.|\s)*?<\/comment>').search(wholeQuest).group()
                comment = '<comment>{0}</comment>'.format(instruction) if instruction else ''
                wholeQuest = wholeQuest.replace(preComment, comment)
                wholeQuest = wholeQuest.replace('</title>\n  \n', '</title>\n')
        elif instruction:
            wholeQuest = wholeQuest.replace('</title>','</title>\n  <comment>{0}</comment>'.format(instruction,0))
        return wholeQuest

    def runQartsBot(self, selectedInput):
        initialInput = selectedInput

        # checks if there are actually any questions
        if re.compile(questionReg).search(initialInput):
            # creates an array with question tags as elements. all non-question code is grouped into arrays inbetween the questions
            input = [qn for qn in re.split(questionReg, initialInput) if splitCk(qn)]
            # reads for each element(question | questionSplit) of the survey        
            for qind,questionSplit in enumerate(input):
                # text to be replaced after the script is finished
                beforeRead = questionSplit
                # get question tag
                initialTag = [x for x in re.split('(<.*?>)', questionSplit) if splitCk(x)][0]

                if goodForParse(initialTag, questionSplit):
                    # generates checks and counters
                    self.cntrRow = 0
                    self.cntrRowOe = 0
                    self.cntrCol = 0
                    self.cntrColOe = 0
                    self.cntrChoice = 0
                    self.cntrGroup = 0
                    self.hasNoanswer = False
                    self.isScale = False
                    self.autofillZeroes = False
                    self.isDoubleEnded = False
                    self.rowatype = "text"
                    self.colatype = "text"
                    self.additionalAttr = []

                    qatool = "rp"
                    qatheme = "text"
                    questionComment = ""
                    errorComment = ""
                    isRating = False
                    isTextarea = False

                    questionTag, questionSplit = preprocessQuestion(self, questionElementsReg, questionSplit)

                # radio
                    if re.compile('^<radio').search(questionTag):
                        isRating = True if ckAttr(questionTag, 'type') else False
                        if ckAttr(questionTag, 'values'):
                            self.isScale = True
                        scaleCases = ["day","week","month","year","never"]
                        scaleCases2 = ["satisfied","likely","important","agree"]
                        countScaleCases = 0
                        countScaleCases2 = 0
                        questionComment = ""
                        if self.cntrCol == 0:
                            questionComment = "Select one"
                            rowElements = [r.strip() for r in re.split('({0})'.format(rowReg), questionSplit) if splitCk(r) and re.search(rowReg, r)]
                            for row in rowElements:
                                for oneCase in scaleCases:
                                    if oneCase in elem_text(row).lower():
                                        countScaleCases += 1
                                        break
                                for oneCase2 in scaleCases2:
                                    if oneCase2 in elem_text(row).lower():
                                        countScaleCases2 += 1
                                        break
                            if countScaleCases >= 3 or countScaleCases2 >= 3:
                                self.isScale = True
                            if self.rowatype == "text" and not self.isScale and not isRating:
                                qatheme = "text"
                                if self.cntrGroup:
                                    qatheme = "grouped"
                            elif self.rowatype == "image" and not self.isScale and not isRating:
                                qatheme = "logo"
                                if self.cntrGroup:
                                    qatheme = "logo_grouped"
                            elif self.rowatype == "text" and (isRating or self.isScale):
                                qatheme = "scale-text"
                            elif self.rowatype == "image" and isRating:
                                qatheme = "scale-icon"
                        else:
                            colElements = [c.strip() for c in re.split('({0})'.format(colReg), questionSplit) if splitCk(c) and re.search(colReg, c)]
                            for col in colElements:
                                for oneCase in scaleCases:
                                    if oneCase in elem_text(col).lower():
                                        countScaleCases += 1
                                        break
                                for oneCase2 in scaleCases2:
                                    if oneCase2 in elem_text(col).lower():
                                        countScaleCases2 += 1
                                        break
                            if countScaleCases >= 3 or countScaleCases2 >= 3:
                                self.isScale = True
                            questionComment = "Select one for each"
                            if self.cntrRow == 0:
                                qatool = "gm"
                                qatheme = "slider-{}_{}".format(self.rowatype, self.colatype)
                                self.additionalAttr.append(["qa:sldrsnap","1"])
                                self.additionalAttr.append(["qa:sldrinitpercent","0"])
                                questionComment = "Drag the slider to answer the question"
                            elif (self.isScale or isRating) and not self.isDoubleEnded:
                                if self.cntrCol >= 5 and self.cntrRow <= 5:
                                    qatool = "gm"
                                    qatheme = "slider-{}_{}".format(self.rowatype, self.colatype)
                                    self.additionalAttr.append(["qa:sldrsnap","1"])
                                    self.additionalAttr.append(["qa:sldrinitpercent","0"])
                                    questionComment = "Drag each slider to answer the question"
                                else:
                                    if self.rowatype == "image":
                                        self.rowatype = "logo"
                                    if self.colatype == "image":
                                        self.colatype = "logo"
                                    qatool = "sm"
                                    qatheme = "{}_scale-{}".format(self.rowatype, self.colatype)
                            elif (self.isScale or isRating) and self.isDoubleEnded:
                                qatool = "gm"
                                qatheme = "double-ended-slider"
                                self.additionalAttr.append(["qa:sldrsnap","1"])
                                questionComment = "Drag each slider to answer the question"
                                # add col text as subtitle
                                colElements = [c.strip() for c in re.split('({0})'.format(colReg), questionSplit) if splitCk(c) and re.search(colReg, c)]
                                for elementSplit in colElements:
                                    elementBefore = elementSplit
                                    if not re.compile('^\s*(<!--|<note>)').search(elementSplit) and not ckAttr(elementSplit, 'qa:csubtitle'):
                                        elementSplit = addAttr(elementSplit, 'qa:csubtitle', elem_text(elementSplit))
                                    questionSplit = questionSplit.replace(elementBefore, elementSplit, 1)
                                # split and set row text by |
                                rowElements = [r.strip() for r in re.split('({0})'.format(rowReg), questionSplit) if splitCk(r) and re.search(rowReg, r)]
                                for elementSplit in rowElements:
                                    elementBefore = elementSplit
                                    if not re.compile('^\s*(<!--|<note>)').search(elementSplit) and not ckAttr(elementSplit, 'qa:rdelabellt'):
                                        if "|" in elem_text(elementSplit):
                                            colatype = "text"
                                            rowText = elem_text(elementSplit)
                                            leftText = rowText.split("|")[0].strip()
                                            rightText = rowText.split("|")[1].strip()
                                            elementSplit = elementSplit.replace(rowText, '')
                                            elementSplit = addAttr(elementSplit, 'alt', escape(rowText))
                                            elementSplit = addAttr(elementSplit, 'qa:rdelabellt', escape(leftText))
                                            elementSplit = addAttr(elementSplit, 'qa:rdelabelrt', escape(rightText))
                                    questionSplit = questionSplit.replace(elementBefore, elementSplit, 1)
                            else:
                                if self.rowatype == "image":
                                    self.rowatype = "logo"
                                if self.colatype == "image":
                                    self.colatype = "logo"
                                qatool = "sm"
                                qatheme = "{}_{}".format(self.rowatype, self.colatype)
                            if self.cntrCol == self.cntrColDigits and self.colatype == "text" and self.cntrCol < 12 and self.cntrRow > 0 and not self.isDoubleEnded:
                                qatool = "gm"
                                if self.rowatype == "logo":
                                    self.rowatype = "image"
                                qatheme = "{}_number".format(self.rowatype)
                                self.additionalAttr = ""
                            if (self.cntrCol - self.cntrColDigits) < self.cntrCol and (isRating and self.isScale) and self.cntrCol < 16 and self.cntrRow > 0 and not self.isDoubleEnded:
                                qatool = "sm"
                                questionComment = "Select one for each"
                                if self.colatype == "image":
                                    self.colatype = "logo"
                                qatheme = "{}_scale-{}".format(self.rowatype, self.colatype)
                                self.additionalAttr = ""
                            elif (self.cntrCol - self.cntrColDigits) < self.cntrCol and (isRating and self.isScale) and self.cntrCol > 15 and self.cntrRow > 0 and not self.isDoubleEnded:
                                qatool = "df"
                                if self.colatype == "logo":
                                    self.colatype = "image"
                                if self.rowatype == "logo":
                                    self.rowatype = "image"
                                questionComment = "Please drag each option and place on the range below"
                                qatheme = "{}_zone-{}".format(self.rowatype, self.colatype)
                                self.additionalAttr = ""
                            if self.hasNoanswer and self.rowatype == "text" and self.cntrRow > 0 and not self.isDoubleEnded:
                                qatool = "gm"
                                qatheme = "{}_{}".format(self.rowatype, self.colatype)
                                self.additionalAttr = ""
                # checkbox
                    elif re.compile('^<checkbox').search(questionTag):
                        questionComment = ""
                        if self.cntrCol == 0:
                            questionComment = "Select all that apply"
                            if self.rowatype == "text":
                                qatheme = "text"
                                if self.cntrGroup:
                                    qatheme = "grouped"
                            elif self.rowatype == "image":
                                qatheme = "logo"
                                if self.cntrGroup:
                                    qatheme = "logo_grouped"
                        else:
                            questionComment = "Select all that apply for each"
                            if self.rowatype == "image":
                                self.rowatype = "logo"
                            if self.colatype == "image":
                                self.colatype = "logo"
                            qatool = "sm"
                            qatheme = "{}_{}".format(self.rowatype, self.colatype)
                            if self.hasNoanswer and self.rowatype=="text":
                                if self.colatype == "logo":
                                    self.colatype = "image"
                                qatool = "gm"
                                qatheme = "{}_{}".format(self.rowatype, self.colatype)
                            if self.cntrCol == self.cntrColDigits and self.colatype=="text" and self.cntrCol < 12:
                                qatool = "gm"
                                if self.rowatype == "logo":
                                    self.rowatype = "image"
                                qatheme = "{}_number".format(self.rowatype)
                        if ckAttr(questionTag, "atleast"):
                            if int(attrValue(questionTag, "atleast")) > 1:
                                self.additionalAttr.append(["qa:captype", "min"])
                                self.additionalAttr.append(["qa:capvalue", attrValue(questionTag, "atleast")])
                        if ckAttr(questionTag, "atmost"):
                            if int(attrValue(questionTag, "atmost")) > 1:
                                self.additionalAttr.append(["qa:captype", "max"])
                                self.additionalAttr.append(["qa:capvalue", attrValue(questionTag, "atmost")])
                        if ckAttr(questionTag, "atleast") and ckAttr(questionTag, "atmost"):
                            if int(attrValue(questionTag, "atmost")) == int(attrValue(questionTag, "atleast")):
                                self.additionalAttr.append(["qa:captype", "hard"])
                                self.additionalAttr.append(["qa:capvalue", int(attrValue(questionTag, "atmost"))])
                        if ckAttr(questionTag, "exactly"):
                            if int(attrValue(questionTag, "exactly")) > 1:
                                self.additionalAttr.append(["qa:captype", "hard"])
                                self.additionalAttr.append(["qa:capvalue", attrValue(questionTag, "exactly")])
                # select
                    elif re.compile('^<select').search(questionTag):
                        if self.cntrRow > 0:
                            qatool = "rr"
                            qatheme = self.rowatype
                            questionComment = "Please click on the choices in order of your preference"
                            if self.cntrGroup:
                                errorComment = "There is no predifened theme for ranking using groups. Please contact your QArts champion."
                            if ckAttr(questionTag, "minRanks"):
                                self.additionalAttr.append(["qa:captype", "hard"])
                                self.additionalAttr.append(["qa:capvalue", str(child.attrib["minRanks"])])
                            else:
                                self.additionalAttr.append(["minRanks",str(self.cntrChoice)])
                                self.additionalAttr.append(["qa:captype","hard"])
                                self.additionalAttr.append(["qa:capvalue",str(self.cntrChoice)])
                        elif self.cntrRow == 0:
                            qatool = "None"
                        if self.cntrChoice == 0:
                            errorComment = "There are no choices here! Please add choices and adjust attributes."
                        if self.cntrCol > 0:
                            errorComment = "Select (rank) questions DON'T support columns!"
                # text
                    elif re.compile('^<text').search(questionTag):
                        if re.compile('^<textarea').search(questionTag):
                            isTextarea = True
                            errorComment = "Textarea questions not supported. Changed to text."
                            questionTag = questionTag.replace('<textarea', '<text')
                            questionSplit = questionSplit.replace('</textarea', '</text')
                            questionTag = addAttr(questionTag, "size", "40")
                        if self.cntrCol > 0:
                            errorComment = "Text questions DON'T support columns!"
                        if self.cntrRow == 0 or isTextarea:
                            qatool = "rp"
                            qatheme = "oe-long"
                            placeholder = "Please be specific"
                        else:
                            qatool = "rp"
                            if self.rowatype == "image":
                                qatheme = "oe-short-logo"
                            else:
                                qatheme = "oe-short"
                            placeholder = "Please type in"
                            # set validation/error/placeholder per row if needed
                            rowElements = [r.strip() for r in re.split('({0})'.format(rowReg), questionSplit) if splitCk(r) and re.search(rowReg, r)]
                            for elementSplit in rowElements:
                                elementBefore = elementSplit
                                if not re.compile('^\s*(<!--|<note>)').search(elementSplit):
                                    if ckAttr(elementSplit, "verify"):
                                        if attrValue(elementSplit, "verify") == "zipcode":
                                            elementSplit = addAttr(elementSplit, "qa:validation", "zipcode")
                                            elementSplit = addAttr(elementSplit, "qa:rerror", "Enter valid zipcode")
                                            elementSplit = addAttr(elementSplit, "qa:rplaceholder", "Enter a 5-digit zipcode")
                                        elif attrValue(elementSplit, "verify") == "email":
                                            elementSplit = addAttr(elementSplit, "qa:validation", "email")
                                            elementSplit = addAttr(elementSplit, "qa:rerror", "Enter valid e-mail")
                                            elementSplit = addAttr(elementSplit, "qa:rplaceholder", "Enter a valid e-mail")
                                        elif attrValue(elementSplit, "verify") == "phoneUS":
                                            elementSplit = addAttr(elementSplit, "qa:validation", "^(\d{10})$")
                                            elementSplit = addAttr(elementSplit, "qa:rerror", "Enter valid phone number in '5551234567' format")
                                            elementSplit = addAttr(elementSplit, "qa:rplaceholder", "Enter a valid phone number")
                                        elif "range" in attrValue(elementSplit, "verify") or attrValue(elementSplit, "verify") in ["number","digits"]:
                                            elementSplit = addAttr(elementSplit, "qa:rinputnumeric", "1")
                                            elementSplit = addAttr(elementSplit, "qa:rplaceholder", "Enter a whole number")
                                            elementSplit = addAttr(elementSplit, "qa:rplaceholder", "Enter a valid number")
                                        elif "len" in attrValue(elementSplit, "verify"):
                                            length = attrValue(elementSplit, "verify").split('(')[1].split(')')[0]
                                            if ',' not in length:
                                                length += ','
                                            elementSplit = addAttr(elementSplit, "qa:validation","^(.{%s})$" %length)
                                            elementSplit = addAttr(elementSplit, "qa:rerror","Enter at least {} characters".format(length.split(',')[0]))
                                questionSplit = questionSplit.replace(elementBefore, elementSplit, 1)
                            # set capvalue
                            if self.cntrRow - self.cntrRowOptional > 1:
                                if ckAttr(questionTag, "optional", "0"):
                                    capvalue = str(self.cntrRow - self.cntrRowOptional) if not ckAttr(questionTag, 'rowCond') else '1'
                                    self.additionalAttr.append(["qa:capvalue", capvalue])
                                    self.additionalAttr.append(["qa:captype", "min"])
                        # remove comment / set as placeholder
                        if re.compile('((<comment>)(\s*|Please be( as)? specific( as possible)?)(<\/comment>))').search(questionSplit):
                            questionComment = ""
                        elif re.compile('<comment>.*?<\/comment>').search(questionSplit):
                            comment = re.compile('<comment>(.|\s)*?<\/comment>').search(questionSplit).group()
                            if len(comment) < 30 and len(comment) > 2:
                                questionComment = ""
                                placeholder = comment
                        # set validation/error per question if needed
                        if ckAttr(questionTag, "verify"):
                            qatheme = "oe-short"
                            if attrValue(questionTag, "verify") == "zipcode":
                                questionComment = ""
                                self.additionalAttr.append(["qa:validation","zipcode"])
                                self.additionalAttr.append(["qa:rerror","Enter valid zipcode"])
                                placeholder = "Enter a 5-digit zipcode"
                            elif attrValue(questionTag, "verify") == "email":
                                questionComment = ""
                                self.additionalAttr.append(["qa:validation","email"])
                                self.additionalAttr.append(["qa:rerror","Enter valid e-mail"])
                                placeholder = "Enter a valid e-mail"
                            elif attrValue(questionTag, "verify") == "phoneUS":
                                questionComment = ""
                                self.additionalAttr.append(["qa:validation","^(\d{10})$"])
                                self.additionalAttr.append(["qa:rerror","Enter valid phone number in '5551234567' format"])
                                placeholder = "Enter a valid phone number"
                            elif "range" in attrValue(questionTag, "verify") or attrValue(questionTag, "verify") in ["number","digits"]:
                                questionComment = ""
                                self.additionalAttr.append(["qa:rinputnumeric","1"])
                                placeholder = "Enter a whole number"
                            elif "len" in attrValue(questionTag, "verify"):
                                length = attrValue(questionTag, "verify").split('(')[1].split(')')[0]
                                if ',' not in length:
                                    length += ','
                                self.additionalAttr.append(["qa:validation","^(.{%s})$" %length])
                                self.additionalAttr.append(["qa:rerror","Enter at least {} characters".format(length.split(',')[0]) ])
                        # add placeholder
                        self.additionalAttr.append(["qa:rplaceholder", placeholder])
                        questionSplit = addGibberishCheck(self, questionSplit)
                # number
                    elif re.compile('^<number').search(questionTag):
                        if ckAttr(questionTag, "range") or ckAttr(questionTag, "points"):
                            qatool = "None"
                            errorComment = "Range/points (dropdown) number questions don't work with QArts!"
                            questionTag = fixNumSize(questionTag, questionSplit)
                        elif self.cntrCol > 1:
                            qatool = "None"
                            errorComment = "Number questions DON'T support multiple columns!"
                            questionTag = fixNumSize(questionTag, questionSplit)
                        elif self.cntrRow == 0:
                            qatool = "rp"
                            qatheme = "oe-short"
                            placeholder = "Enter a whole number"
                            self.additionalAttr.append(["qa:rinputnumeric","1"])
                        else:
                            if ckAttr(questionTag, "amount"):
                                qatool = "ls"
                                qatheme = self.rowatype
                                self.additionalAttr.append(["qa:maxpool", attrValue(questionTag, "amount")])
                                if int(attrValue(questionTag, "amount")) < 16:
                                    self.additionalAttr.append(["qa:sldrstep","1"])
                                self.additionalAttr.append(["qa:enableinput","1"])
                                self.additionalAttr.append(["qa:remainstr","Remaining:"])
                                questionTag = delAttr(questionTag, "optional")
                                questionTag = delAttr(questionTag, "verify")
                                questionTag = addAttr(questionTag, "optional", "1")
                                questionTag = addAttr(questionTag, "verify", "range(0,{})".format(attrValue(questionTag, "amount")))
                                self.autofillZeroes = True
                                questionComment = "Please move each slider to indicate what proportion of the total you would allocate to each option"
                            elif self.cntrCol == 1:
                                if ckAttr(questionTag, "verify"):
                                    if "range" in attrValue(questionTag, "verify"):
                                        tempVal = child.attrib["verify"].replace("range(","").replace(")","").split(",")[-1]
                                        if int(tempVal) > 4:
                                            qatool = "df"
                                            qatheme = "{}_range-text".format(self.rowatype)
                                            questionComment = "Please drag each option and place on the range below"                    
                            else:
                                qatool = "rp"
                                if self.rowatype == "image":
                                    qatheme = "oe-short-logo"
                                else:
                                    qatheme = "oe-short"
                                self.additionalAttr.append(["qa:rinputnumeric","1"])
                                placeholder = "Enter a whole number"
                                # set validation/error per row if needed
                                rowElements = [r.strip() for r in re.split('({0})'.format(rowReg), questionSplit) if splitCk(r) and re.search(rowReg, r)]
                                for elementSplit in rowElements:
                                    elementBefore = elementSplit
                                    if not re.compile('^\s*(<!--|<note>)').search(elementSplit):
                                        minValNum = "No value"
                                        maxValNum = "No value"
                                        if ckAttr(elementSplit, "verify"):
                                            if "range" in attrValue(elementSplit, "verify"):
                                                minValNum = attrValue(elementSplit, "verify").replace("range(","").replace(")","").split(",")[0].strip()
                                                maxValNum = attrValue(elementSplit, "verify").replace("range(","").replace(")","").split(",")[-1].strip()
                                            if minValNum != "No value" and maxValNum != "No value":
                                                elementSplit = addAttr(elementSplit, "qa:validation", "minmax~~{}~~{}".format(minValNum, maxValNum))
                                                elementSplit = addAttr(elementSplit, "qa:rerror", "Enter a whole number in range {}-{}".format(minValNum, maxValNum))
                                    questionSplit = questionSplit.replace(elementBefore, elementSplit, 1)
                                # set capvalue
                                if self.cntrRow - self.cntrRowOptional > 1:
                                    if ckAttr(questionTag, "optional", "0"):
                                        capvalue = str(self.cntrRow - self.cntrRowOptional) if not ckAttr(questionTag, 'rowCond') else '1'
                                        self.additionalAttr.append(["qa:capvalue", capvalue])
                                        self.additionalAttr.append(["qa:captype", "min"])
                        if qatool == 'rp':
                            # remove comment / set as placeholder
                            if re.compile('<comment>.*?<\/comment>').search(questionSplit):
                                comment = re.compile('<comment>(.|\s)*?<\/comment>').search(questionSplit).group()
                                if len(comment) < 30 and len(comment) > 2:
                                    questionComment = ""
                                    if not "Enter a whole number" in comment:
                                        placeholder = comment
                            # set validation/error per question
                            if ckAttr(questionTag, "verify"):
                                if "range" in attrValue(questionTag, "verify"):
                                    minValNum = attrValue(questionTag, "verify").replace("range(","").replace(")","").split(",")[0].strip()
                                    maxValNum = attrValue(questionTag, "verify").replace("range(","").replace(")","").split(",")[-1].strip()
                                self.additionalAttr.append(["qa:rerror","Enter a whole number in range {}-{}".format(minValNum, maxValNum)])
                                self.additionalAttr.append(["qa:validation","minmax~~{}~~{}".format(minValNum, maxValNum)])
                            # add placeholder
                            self.additionalAttr.append(["qa:rplaceholder", placeholder])
                # float
                    elif re.compile('^<float').search(questionTag):
                        if self.cntrCol > 1:
                            qatool = "None"
                            errorComment = "Float questions DON'T support multiple columns!"
                        if self.cntrRow == 0:
                            qatool = "rp"
                            qatheme = "oe-short"
                            placeholder = "Enter a number"
                            self.additionalAttr.append(["qa:rinputnumeric","1"])
                        else:
                            if self.cntrCol == 1:
                                if ckAttr(questionTag, "range"):
                                    if "," in attrValue(questionTag, "verify"):
                                        tempVal = attrValue(questionTag, "range").split(",")[-1]
                                    else:
                                        tempVal = attrValue(questionTag, "range")
                                    if int(tempVal) > 4:
                                        qatool = "df"
                                        qatheme = "{}_range-text".format(self.rowatype)
                                        questionComment = "Please drag each option and place on the range below"                    
                            else:
                                qatool = "rp"
                                qatheme = "oe-short"
                                placeholder = "Enter a number"
                                self.additionalAttr.append(["qa:rinputnumeric","1"])
                                # set validation/error per row if needed
                                rowElements = [r.strip() for r in re.split('({0})'.format(rowReg), questionSplit) if splitCk(r) and re.search(rowReg, r)]
                                for elementSplit in rowElements:
                                    elementBefore = elementSplit
                                    if not re.compile('^\s*(<!--|<note>)').search(elementSplit):
                                        minValNum = "No value"
                                        maxValNum = "No value"
                                        if ckAttr(elementSplit, "range"):
                                            if "," in attrValue(elementSplit, "range"):
                                                minValNum = attrValue(elementSplit, "range").split(",")[0].strip() + ".0"
                                                maxValNum = attrValue(elementSplit, "range").split(",")[-1].strip() + ".0"
                                            else:
                                                minValNum = "0.0"
                                                maxValNum = attrValue(elementSplit, "range").strip() + ".0"
                                            if minValNum != "No value" and maxValNum != "No value":
                                                elementSplit = addAttr(elementSplit, "qa:validation", "minmax~~{}~~{}".format(minValNum, maxValNum))
                                                elementSplit = addAttr(elementSplit, "qa:rerror", "Enter a number in range {}-{}".format(minValNum, maxValNum))
                                    questionSplit = questionSplit.replace(elementBefore, elementSplit, 1)
                                # set capvalue
                                if self.cntrRow - self.cntrRowOptional > 1:
                                    if ckAttr(questionTag, "optional", "0"):
                                        capvalue = str(self.cntrRow - self.cntrRowOptional) if not ckAttr(questionTag, 'rowCond') else '1'
                                        self.additionalAttr.append(["qa:capvalue", capvalue])
                                        self.additionalAttr.append(["qa:captype", "min"])
                        if qatool == 'rp':
                            # remove comment / set as placeholder
                            if re.compile('<comment>.*?<\/comment>').search(questionSplit):
                                comment = re.compile('<comment>(.|\s)*?<\/comment>').search(questionSplit).group()
                                if len(comment) < 30 and len(comment) > 2:
                                    questionComment = ""
                                    if not "Enter a number" in comment:
                                        placeholder = comment
                            # set validation/error per question
                            if ckAttr(questionTag, "range"):
                                if "," in attrValue(questionTag, "range"):
                                    minValNum = attrValue(questionTag, "range").split(",")[0].strip() + ".0"
                                    maxValNum = attrValue(questionTag, "range").split(",")[-1].strip() + ".0"
                                else:
                                    minValNum = "0.0"
                                    maxValNum = attrValue(questionTag, "range").strip() + ".0"

                                self.additionalAttr.append(["qa:rerror","Enter a number in range {}-{}".format(minValNum, maxValNum)])
                                self.additionalAttr.append(["qa:validation","minmax~~{}~~{}".format(minValNum, maxValNum)])
                            # add placeholder
                            self.additionalAttr.append(["qa:rplaceholder", placeholder])

                    # check for incompatible attributes
                    if ckAttr(questionTag, "rightOf") or ckAttr(questionTag, "below"):
                        qatool = "None"
                        errorComment = "Мerged questions don't work with QArts!"
                    elif ckAttr(questionTag, "uses") and 'qarts' not in attrValue(questionTag, "uses"):
                        qatool = "None"

                    # add qarts
                    if qatool != "None":
                        if ckAttr(questionTag, "uses") and 'qarts' in attrValue(questionTag, "uses"):
                            questionTag = delAttr(questionTag, 'uses')
                            questionTag = re.sub('[\n\s]*?qa:[A-Za-z]+?=".*?"', '', questionTag)
                        questionTag = addAttr(questionTag, "qa:tool", qatool)
                        questionTag = addAttr(questionTag, "qa:atype", qatheme)
                        questionTag = addAttr(questionTag, "uses", "qarts.%s" %self.qartsV)
                        for attr in sorted(self.additionalAttr):
                            questionTag = addAttr(questionTag, attr[0], attr[1])

                        if self.autofillZeroes and not '<exec' in questionSplit:
                            lsExec = '<exec>\nfor eachRow in {}.rows:\n    if eachRow.val == None and eachRow.displayed:\n        eachRow.val = 0\n</exec>\n'.format(attrValue(questionTag, 'label'))
                            questionSplit = re.sub('(<\/\s*number>)', '{}\g<1>'.format(lsExec), questionSplit)

                    # PN check
                    questionTag = pnCheck(questionTag)

                    questionSplit = questionSplit.replace(initialTag, questionTag)
                    if errorComment:
                        questionSplit = re.sub('(</(?:radio|checkbox|select|text|number|float)>)', '<!-- {} -->\n\g<1>'.format(errorComment), questionSplit)
                    questionSplit = questionSplit.replace('><title>','>\n  <title>')
                    questionSplit = questionSplit.replace('><!--NOTE:','>\n<!--NOTE:')

                    questionSplit = shuffleAdd(self, questionSplit)

                    # PN check
                    questionSplit = pnCheck(questionSplit)

                    # Submessages
                    if self.view.settings().get('parseDefaultComments'):
                        questionSplit = self.addSubmess(questionSplit, questionComment)

                    setFound = self.view.settings().get('setFoundComment') if self.view.settings().has('setFoundComment') else True
                    questionSplit = givenSubmessCheck(questionSplit, setComment=setFound)

                    afterRead = questionSplit
                    initialInput = initialInput.replace(beforeRead, afterRead, 1)

                    if '</exec>\n<suspend/>' in initialInput:
                        initialInput = initialInput.replace('</exec>\n<suspend/>','</exec>').replace('</text>',  '</text>\n<suspend/>').replace('</textarea>',  '</textarea>\n<suspend/>')
        #reading rows only
        elif re.compile(questionElementsReg).search(initialInput):
            initialInput = onlyElementsNoQn(self, initialInput)

        return initialInput

    def run(self, edit):
        try:
            self.surtype = self.view.settings().get('surveyType')
            self.qartsV = self.view.settings().get('qartsVersion')
            sels = self.view.sel()
            input = ''
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel).strip()
                printPage = self.runQartsBot(input)
                self.view.replace(edit,sel, printPage)
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print ("Clarry bot failed:")
            print (e)



############################# Gerry #############################

class runGerry(sublime_plugin.TextCommand):
    cntrRow = 0
    cntrRowOe = 0
    cntrCol = 0
    cntrColOe = 0
    cntrChoice = 0
    cntrGroup = 0
    hasNoanswer = False
    autofillZeroes = False

    def addSubmess(self, wholeQuest):
        instruction = ''
        if re.compile('^<(radio)').search(wholeQuest):
            if self.cntrRow < 2 or self.cntrCol < 2:
                instruction = 'Select one'
            elif self.cntrRow > 1 and self.cntrCol > 1:
                if groupingCols(wholeQuest):
                    instruction = 'Select one in each column'
                else:
                    instruction = 'Select one in each row'
        elif re.compile('^<(select)').search(wholeQuest):
            instruction = '' 
        elif re.compile('^<(checkbox)').search(wholeQuest):
            if self.cntrRow < 2 or self.cntrCol < 2:
                instruction = 'Select all that apply'
            elif self.cntrRow > 1 and self.cntrCol > 1:
                if groupingCols(wholeQuest):
                    instruction = 'Select all that apply in each column'
                else:
                    instruction = 'Select all that apply in each row'
        elif re.compile('^<(textarea)').search(wholeQuest):
            instruction = 'Please be as specific as possible'
        elif re.compile('^<(text)').search(wholeQuest):
            instruction = 'Please be specific'
        elif re.compile('^<(float)').search(wholeQuest):
            instruction = "Enter a number"
        elif re.compile('^<(number)').search(wholeQuest):
            if self.cntrRow < 2 and self.cntrCol < 2:
                instruction = 'Enter a whole number'
            else:
                instruction = 'Enter a whole number in each field below'

        if re.compile('<comment>.*?<\/comment>').search(wholeQuest):
            if re.compile('((<comment>)(\s*|Select one( (in|for) each( row| column)?)?|Select all that apply( (in|for) each( row| column)?)?|Please be( as)? specific( as possible)?|Enter a( whole)? number|@\(instructionTxt\d{0,2}\))(<\/comment>))').search(wholeQuest):
                preComment = re.compile('<comment>(.|\s)*?<\/comment>').search(wholeQuest).group()
                comment = '<comment>{0}</comment>'.format(instruction)
                wholeQuest = wholeQuest.replace(preComment,comment)
        else:
            wholeQuest = wholeQuest.replace('</title>','</title>\n  <comment>{0}</comment>'.format(instruction,0))
        return wholeQuest

    def runBareBot(self, selectedInput):
        initialInput = selectedInput

        # checks if there are actually any questions
        if re.compile(questionReg).search(initialInput):
            # creates an array with question tags as elements. all non-question code is grouped into arrays inbetween the questions
            input = [qn for qn in re.split(questionReg, initialInput) if splitCk(qn)]
            # reads for each element(question | questionSplit) of the survey        
            for qind,questionSplit in enumerate(input):
                # text to be replaced after the script is finished
                beforeRead = questionSplit
                # get question tag
                initialTag = [x for x in re.split('(<.*?>)', questionSplit) if splitCk(x)][0]

                if goodForParse(initialTag, questionSplit):
                    # generates checks and counters  
                    self.cntrRow = 0
                    self.cntrRowOe = 0
                    self.cntrCol = 0
                    self.cntrColOe = 0
                    self.cntrChoice = 0
                    self.cntrGroup = 0
                    self.hasNoanswer = False
                    self.autofillZeroes = False

                    questionTag, questionSplit = preprocessQuestion(self, questionElementsReg, questionSplit)

                    if re.compile('^<text').search(questionTag):
                        questionSplit = addGibberishCheck(self, questionSplit)
                    elif re.compile('^<number').search(questionTag):
                        questionTag = fixNumSize(questionTag,questionSplit)

                    # PN check
                    questionTag = pnCheck(questionTag)

                    questionSplit = questionSplit.replace(initialTag,questionTag)
                    questionSplit = questionSplit.replace('><title>','>\n  <title>')
                    questionSplit = questionSplit.replace('><!--NOTE:','>\n<!--NOTE:')

                    questionSplit = shuffleAdd(self, questionSplit)

                    # PN check
                    questionSplit = pnCheck(questionSplit)

                    # Submessages
                    if self.view.settings().get('parseDefaultComments'):
                        questionSplit = self.addSubmess(questionSplit)
                    setFound = self.view.settings().get('setFoundComment') if self.view.settings().has('setFoundComment') else True
                    questionSplit = givenSubmessCheck(questionSplit, setComment=setFound)

                    afterRead = questionSplit
                    initialInput = initialInput.replace(beforeRead, afterRead, 1)

                    if '</exec>\n<suspend/>' in initialInput:
                        initialInput = initialInput.replace('</exec>\n<suspend/>','</exec>').replace('</text>',  '</text>\n<suspend/>').replace('</textarea>',  '</textarea>\n<suspend/>')
                else:
                    # text fixes in htmls/hidden questions
                    questionSplit = textEscape(self, questionSplit)
                    questionSplit = addLiTags(questionSplit)
                    afterRead = questionSplit
                    initialInput = initialInput.replace(beforeRead, afterRead, 1)

        # reading rows only
        elif re.compile(questionElementsReg).search(initialInput):
            initialInput = onlyElementsNoQn(self, initialInput)

        return initialInput

    def run(self, edit):
        try:
            self.surtype = self.view.settings().get('surveyType')
            sels = self.view.sel()
            input = ''
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel).strip()
                printPage = self.runBareBot(input)
                self.view.replace(edit,sel, printPage)
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print ("Gerry bot failed:")
            print (e)



############################# Chrissy #############################

class runChrissy(sublime_plugin.TextCommand):
    cntrRow = 0
    cntrRowOe = 0
    cntrCol = 0
    cntrColOe = 0
    cntrChoice = 0
    cntrGroup = 0
    hasNoanswer = False
    autofillZeroes = False
    cntrRowOeLabels = []

    def addSubmess(self, wholeQuest):
        instruction = ''
        if re.compile('^<(radio)').search(wholeQuest):
            if self.cntrRow < 2 or self.cntrCol < 2:
                instruction = '@(instructionTxt1)'
            elif self.cntrRow > 1 and self.cntrCol > 1:
                if groupingCols(wholeQuest):
                    instruction = '@(instructionTxt5)'
                else:
                    instruction = '@(instructionTxt3)'
        elif re.compile('^<(select)').search(wholeQuest):
            if re.compile('uses=".*?ranksort\.\d.*?"').search(wholeQuest):
                instruction = '@(instructionTxt33)'
            elif re.compile('uses=".*?bmrranking\.\d.*?"').search(wholeQuest):
                instruction = '@(instructionTxt32)'
            else:
                instruction = False
        elif re.compile('^<(checkbox)').search(wholeQuest):
            if self.cntrRow < 2 or self.cntrCol < 2:
                instruction = '@(instructionTxt2)'
            elif self.cntrRow > 1 and self.cntrCol > 1:
                if groupingCols(wholeQuest):
                    instruction = '@(instructionTxt4)'
                else:
                    instruction = '@(instructionTxt26)'
        elif re.compile('^<(textarea)').search(wholeQuest):
            if self.cntrRow or self.cntrCol:
                instruction = '@(instructionTxt16)'
            else:
                instruction = '@(instructionTxt15)'
        elif re.compile('^<(text)').search(wholeQuest):
            instruction = '@(instructionTxt17)'
        elif re.compile('^<(float)').search(wholeQuest):
            instruction = "ENTER A NUMBER"
        elif re.compile('^<(number)').search(wholeQuest):
            if self.cntrRow < 2 and self.cntrCol < 2:
                if isPercentage(wholeQuest):
                    instruction = '@(instructionTxt10)'
                else:
                    instruction = '@(instructionTxt6)'
            elif self.cntrRow < 2 or self.cntrCol < 2:
                if isPercentage(wholeQuest):
                    if amountCheck(wholeQuest):
                        if amountCheck(wholeQuest) == "100":
                            instruction = '@(instructionTxt13)'
                        else:
                            instruction = '@(instructionTxt11)'
                    elif "if this.sum lt 100:" in wholeQuest or "if eachRow.sum lt 100:" in wholeQuest or "if eachCol.sum lt 100:" in wholeQuest:
                        instruction = '@(instructionTxt14)'
                    else:
                        instruction = '@(instructionTxt11)'
                else:
                    if amountCheck(wholeQuest) == "100":
                        instruction = '@(instructionTxt8)'
                    elif "if this.sum lt 100:" in wholeQuest or "if eachRow.sum lt 100:" in wholeQuest or "if eachCol.sum lt 100:" in wholeQuest:
                        instruction = '@(instructionTxt9)'
                    else:
                        instruction = '@(instructionTxt7)'
            else:
                if isPercentage(wholeQuest):
                    if groupingCols(wholeQuest):
                        if amountCheck(wholeQuest) == "100":
                            if self.cntrCol > 1:
                                instruction = '@(instructionTxt12)'
                            else:
                                instruction = '@(instructionTxt13)'
                        elif "if this.sum lt 100:" in wholeQuest or "if eachRow.sum lt 100:" in wholeQuest or "if eachCol.sum lt 100:" in wholeQuest:
                            if self.cntrCol > 1:
                                instruction = '@(instructionTxt25)'
                            else:
                                instruction = '@(instructionTxt14)'
                        else:
                            instruction = '@(instructionTxt11)'
                    else:
                        if amountCheck(wholeQuest) == "100":
                            if self.cntrCol > 1:
                                instruction = '@(instructionTxt30)'
                            else:
                                instruction = '@(instructionTxt13)'
                        elif "if this.sum lt 100:" in wholeQuest or "if eachRow.sum lt 100:" in wholeQuest or "if eachCol.sum lt 100:" in wholeQuest:
                            if self.cntrCol > 1:
                                instruction = '@(instructionTxt31)'
                            else:
                                instruction = '@(instructionTxt14)'
                        else:
                            instruction = '@(instructionTxt11)'
                else:
                    if groupingCols(wholeQuest):
                        if amountCheck(wholeQuest) == "100":
                            instruction = '@(instructionTxt27)'
                        else:
                            instruction = '@(instructionTxt7)'
                    else:
                        if amountCheck(wholeQuest) == "100":
                            instruction = '@(instructionTxt28)'
                        elif "if this.sum lt 100:" in wholeQuest or "if eachRow.sum lt 100:" in wholeQuest or "if eachCol.sum lt 100:" in wholeQuest:
                            instruction = '@(instructionTxt29)'
                        else:
                            instruction = '@(instructionTxt7)'

        if instruction:
            if re.compile('<comment>.*?<\/comment>').search(wholeQuest):
                if re.compile('((<comment>)(\s*|Select one( (in|for) each( row| column)?)?|Select all that apply( (in|for) each( row| column)?)?|Please be( as)? specific( as possible)?|Enter a( whole)? number|@\(instructionTxt\d{0,2}\))(<\/comment>))').search(wholeQuest):
                    preComment = re.compile('<comment>(.|\s)*?<\/comment>').search(wholeQuest).group()
                    comment = '<comment>{0}</comment>'.format(instruction)
                    wholeQuest = wholeQuest.replace(preComment,comment)
            else:
                wholeQuest = wholeQuest.replace('</title>','</title>\n  <comment>{0}</comment>'.format(instruction,0))
        return wholeQuest

    def runHprBot(self, selectedInput):
        initialInput = selectedInput

        # checks if there are actually any questions
        if re.compile(questionReg).search(initialInput):
            # creates an array with question tags as elements. all non-question code is grouped into arrays inbetween the questions
            input = [qn for qn in re.split(questionReg, initialInput) if splitCk(qn)]
            # reads for each element(question | questionSplit) of the survey        
            for qind,questionSplit in enumerate(input):
                # text to be replaced after the script is finished
                beforeRead = questionSplit
                # get question tag
                initialTag = [x for x in re.split('(<.*?>)', questionSplit) if splitCk(x)][0]

                if goodForParse(initialTag, questionSplit):
                    # generates checks and counters  
                    self.cntrRow = 0
                    self.cntrRowOe = 0
                    self.cntrCol = 0
                    self.cntrColOe = 0
                    self.cntrChoice = 0
                    self.cntrGroup = 0
                    self.hasNoanswer = False
                    self.autofillZeroes = False
                    self.cntrRowOeLabels = []

                    questionTag, questionSplit = preprocessQuestion(self, questionElementsReg, questionSplit)

                    # Add OE validates
                    if self.cntrRowOe and len(self.cntrRowOeLabels):
                        questionSplit  = addOeValidate(self, questionSplit)

                    # radio and checkbox
                    if re.compile('^<(radio|checkbox)').search(questionTag):
                        
                    
                        # adds cardsort attributes
                        if re.compile('cardsort').search(questionTag):
                            autosubmitStyle = '\n'
                            questionTag = addAttr(questionTag, '\ncardsort:completionHTML', ' ')
                            questionTag = addAttr(questionTag, '\ncardsort:dragAndDrop', '0')
                            questionTag = addAttr(questionTag, '\ncardsort:displayCounter', '0')
                            if re.compile('^<radio').search(questionTag):
                                if not self.view.find('label="cardsortAutosubmit"', 0):
                                    autosubmitStyle = cardsortScript
                                elif 'copy="cardsortAutosubmit"' not in questionSplit and 'label="cardsortAutosubmit"' not in questionSplit:
                                    autosubmitStyle = """  <style copy="cardsortAutosubmit"/>\n"""
                                questionSplit = questionSplit.replace('</radio>', '%s</radio>' % autosubmitStyle)
                                questionTag = addAttr(questionTag, '\ncardsort:displayNavigation', '0')

                            elif re.compile('^<checkbox').search(questionTag):
                                if not self.view.find('label="cardsortAutosubmit', 0):
                                    autosubmitStyle = cardsortScript
                                elif 'copy="cardsortAutosubmit' not in questionSplit and 'label="cardsortAutosubmit' not in questionSplit:
                                    autosubmitStyle = """  <style copy="cardsortAutosubmit"/>\n"""
                                questionSplit = questionSplit.replace('</checkbox>', autosubmitStyle + '</checkbox>')
                                questionTag = addAttr(questionTag, '\ncardsort:iconButtonCSS', 'background-color:#bb29bb;')
                                questionTag = addAttr(questionTag, '\ncardsort:contentsCardCSS', 'font-size:16px; background-color:#bb29bb; color:white;')
                                questionTag = addAttr(questionTag, '\ncardsort:bucketSelectCSS', 'background-color:#bb29bb; color:white;')

                            questionTag = addAttr(questionTag, '\ncardsort:bucketHoverCSS', 'background-color:#bb29bb; color:white;')
                            questionTag = addAttr(questionTag, '\ncardsort:cardCSS', 'font-size:18px; border:2px solid rgb(219, 219, 210); min-height:80px;')
                            if 'type="rating"' in questionTag or self.cntrCol <= 6:
                                questionTag = addAttr(questionTag, '\ncardsort:bucketsPerRow', str(self.cntrCol))
                                questionTag = addAttr(questionTag, '\ncardsort:bucketCSS', 'font-size:15px; color: #454545; background-color:white; border:1px solid rgb(219, 219, 210); min-height:82px; margin:5px 1px; width:%dpx; ' % int(950/self.cntrCol-20))
                            else:
                                questionTag = addAttr(questionTag, '\ncardsort:bucketCSS', 'font-size:15px; color: #454545; background-color:white; border:1px solid rgb(219, 219, 210); min-height:82px; margin:5px 1px;')

                    # text
                    elif re.compile('^<(text|textarea)').search(questionTag):
                        pass
                    # number
                    elif re.compile('^<number').search(questionTag):
                        if amountCheck(questionSplit):
                            questionTag,questionSplit = addAutosum(self, questionTag, questionSplit)
                        questionTag = fixNumSize(questionTag,questionSplit)
                        questionTag = addFwidth(questionTag,questionSplit)
                        # add fir if there is <noanswer>
                        if re.compile('<noanswer').search(questionSplit) and not re.compile('fir').search(questionTag):
                            if 'uses="' in questionTag and not 'autosum.' in questionTag:
                                questionTag = questionTag.replace('uses="','uses="fir.2,')
                                questionTag = questionTag.replace(questionTag,questionTag.replace('">','"\nfir:image_css="width: 30px;height: 30px;"\nfir:image_checkbox_css="background-image: url(\'/s/local/2013Template/fir-checkbox-purple_30x30.png\')">'))
                            elif 'uses="' not in questionTag:
                                questionTag = addAttr(questionTag,'uses','fir.2')
                                questionTag = questionTag.replace(questionTag,questionTag.replace('">','"\nfir:image_css="width: 30px;height: 30px;"\nfir:image_checkbox_css="background-image: url(\'/s/local/2013Template/fir-checkbox-purple_30x30.png\')">'))
                    # select                
                    elif re.compile('^<select').search(questionTag):
                        # add minRanks
                        if re.compile('(="|,)(ranksort|bmrranking)\.\d').search(questionTag):
                            questionTag = addAttr(questionTag, 'minRanks', self.cntrChoice)
                        # add ranksort colors
                        if re.compile('(="|,)ranksort\.\d').search(questionTag):
                            if re.compile('ranksort:\w*CSS=".*?background.*').search(questionTag):
                                bgColor = re.compile('ranksort:\w*CSS=".*?background(?:-color)?: ?(rgba?|#.*?(?=;|"))').search(questionTag).group(1)
                            else:
                                bgColor = "background-color: #bb29bb"
                            questionTag = addAttr(questionTag,'\nranksort:cardHoverCSS', bgColor)
                            questionTag = addAttr(questionTag,'\nranksort:cardDroppedCSS', bgColor)
                            questionTag = addAttr(questionTag,'\nranksort:uiDraggableHelperCSS', bgColor)
                        # add fir if there is <noanswer>
                        if re.compile('<noanswer').search(questionSplit) and not re.compile('fir').search(questionTag):
                            if 'uses="' in questionTag and not 'ranksort' in questionTag:
                                questionTag = questionTag.replace('uses="','uses="fir.2,')
                                questionTag = questionTag.replace(questionTag,questionTag.replace('">','"\nfir:image_css="width: 30px;height: 30px;"\nfir:image_checkbox_css="background-image: url(\'/s/local/2013Template/fir-checkbox-purple_30x30.png\')">'))
                            elif 'uses="' not in questionTag:
                                questionTag = addAttr(questionTag,'uses','fir.2')
                                questionTag = questionTag.replace(questionTag,questionTag.replace('">','"\nfir:image_css="width: 30px;height: 30px;"\nfir:image_checkbox_css="background-image: url(\'/s/local/2013Template/fir-checkbox-purple_30x30.png\')">'))

                    # float        
                    elif re.compile('^<float').search(questionTag):
                        questionTag = addFwidth(questionTag,questionSplit)
                        

                    questionSplit = questionSplit.replace(initialTag,questionTag)
                    questionSplit = questionSplit.replace('><title>','>\n  <title>')
                    questionSplit = questionSplit.replace('><!--NOTE:','>\n<!--NOTE:')

                    questionSplit = shuffleAdd(self, questionSplit)

                    # PN check
                    questionSplit = pnCheck(questionSplit)

                    # Submessages
                    if self.view.settings().get('parseDefaultComments'):
                        questionSplit = self.addSubmess(questionSplit)
                    setFound = self.view.settings().get('setFoundComment') if self.view.settings().has('setFoundComment') else False
                    questionSplit = givenSubmessCheck(questionSplit, setComment=setFound)

                    afterRead = questionSplit
                    initialInput = initialInput.replace(beforeRead, afterRead, 1)

                    # Putting the 0 autofil exec after the suspend
                    if self.autofillZeroes and '</exec>\n<suspend/>' in initialInput:
                        initialInput = initialInput.replace('</exec>\n<suspend/>','</exec>').replace('</number>',  '</number>\n<suspend/>')
                else:
                    # text fixes in htmls/hidden questions
                    questionSplit = textEscape(self, questionSplit)
                    questionSplit = addLiTags(questionSplit)
                    afterRead = questionSplit
                    initialInput = initialInput.replace(beforeRead, afterRead, 1)

        # reading rows only
        elif re.compile(questionElementsReg).search(initialInput):
            initialInput = onlyElementsNoQn(self, initialInput)

        return initialInput

    def run(self, edit):
        try:
            self.surtype = self.view.settings().get('surveyType')
            sels = self.view.sel()
            input = ''
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel).strip()
                printPage = self.runHprBot(input)
                self.view.replace(edit,sel, printPage)
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print ("Chrissy bot failed:")
            print (e)


############################# Maggie #############################

class runMaggie(sublime_plugin.TextCommand):
    cntrRow = 0
    cntrRowOe = 0
    cntrCol = 0
    cntrColOe = 0
    cntrChoice = 0
    cntrGroup = 0
    autofillZeroes = False

    def addSubmess(self, wholeQuest):
        instruction = ''
        if re.compile('^<(radio)').search(wholeQuest):
            if self.cntrRow < 2 or self.cntrCol < 2:
                instruction = '@(instructionTxt1)'
            elif self.cntrRow > 1 and self.cntrCol > 1:
                if groupingCols(wholeQuest):
                    instruction = '@(instructionTxt5)'
                else:
                    instruction = '@(instructionTxt3)'
        elif re.compile('^<(select)').search(wholeQuest):
            if re.compile('uses=".*?ranksort\.\d.*?"').search(wholeQuest):
                instruction = '@(instructionTxt33)'
            elif re.compile('uses=".*?bmrranking\.\d.*?"').search(wholeQuest):
                instruction = '@(instructionTxt32)'
            else:
                instruction = False
        elif re.compile('^<(checkbox)').search(wholeQuest):
            if self.cntrRow < 2 or self.cntrCol < 2:
                instruction = '@(instructionTxt2)'
            elif self.cntrRow > 1 and self.cntrCol > 1:
                if groupingCols(wholeQuest):
                    instruction = '@(instructionTxt4)'
                else:
                    instruction = '@(instructionTxt26)'
        elif re.compile('^<(textarea)').search(wholeQuest):
            if self.cntrRow or self.cntrCol:
                instruction = '@(instructionTxt16)'
            else:
                instruction = '@(instructionTxt15)'
        elif re.compile('^<(text)').search(wholeQuest):
            instruction = '@(instructionTxt17)'
        elif re.compile('^<(float)').search(wholeQuest):
            instruction = "ENTER A NUMBER"
        elif re.compile('^<(number)').search(wholeQuest):
            if self.cntrRow < 2 and self.cntrCol < 2:
                if isPercentage(wholeQuest):
                    instruction = '@(instructionTxt10)'
                else:
                    instruction = '@(instructionTxt6)'
            elif self.cntrRow < 2 or self.cntrCol < 2:
                if isPercentage(wholeQuest):
                    if amountCheck(wholeQuest):
                        if amountCheck(wholeQuest) == "100":
                            instruction = '@(instructionTxt13)'
                        else:
                            instruction = '@(instructionTxt11)'
                    elif "if this.sum lt 100:" in wholeQuest or "if eachRow.sum lt 100:" in wholeQuest or "if eachCol.sum lt 100:" in wholeQuest:
                        instruction = '@(instructionTxt14)'
                    else:
                        instruction = '@(instructionTxt11)'
                else:
                    if amountCheck(wholeQuest) == "100":
                        instruction = '@(instructionTxt8)'
                    elif "if this.sum lt 100:" in wholeQuest or "if eachRow.sum lt 100:" in wholeQuest or "if eachCol.sum lt 100:" in wholeQuest:
                        instruction = '@(instructionTxt9)'
                    else:
                        instruction = '@(instructionTxt7)'
            else:
                if isPercentage(wholeQuest):
                    if groupingCols(wholeQuest):
                        if amountCheck(wholeQuest) == "100":
                            if self.cntrCol > 1:
                                instruction = '@(instructionTxt12)'
                            else:
                                instruction = '@(instructionTxt13)'
                        elif "if this.sum lt 100:" in wholeQuest or "if eachRow.sum lt 100:" in wholeQuest or "if eachCol.sum lt 100:" in wholeQuest:
                            if self.cntrCol > 1:
                                instruction = '@(instructionTxt25)'
                            else:
                                instruction = '@(instructionTxt14)'
                        else:
                            instruction = '@(instructionTxt11)'
                    else:
                        if amountCheck(wholeQuest) == "100":
                            if self.cntrCol > 1:
                                instruction = '@(instructionTxt30)'
                            else:
                                instruction = '@(instructionTxt13)'
                        elif "if this.sum lt 100:" in wholeQuest or "if eachRow.sum lt 100:" in wholeQuest or "if eachCol.sum lt 100:" in wholeQuest:
                            if self.cntrCol > 1:
                                instruction = '@(instructionTxt31)'
                            else:
                                instruction = '@(instructionTxt14)'
                        else:
                            instruction = '@(instructionTxt11)'
                else:
                    if groupingCols(wholeQuest):
                        if amountCheck(wholeQuest) == "100":
                            instruction = '@(instructionTxt27)'
                        else:
                            instruction = '@(instructionTxt7)'
                    else:
                        if amountCheck(wholeQuest) == "100":
                            instruction = '@(instructionTxt28)'
                        elif "if this.sum lt 100:" in wholeQuest or "if eachRow.sum lt 100:" in wholeQuest or "if eachCol.sum lt 100:" in wholeQuest:
                            instruction = '@(instructionTxt29)'
                        else:
                            instruction = '@(instructionTxt7)'

        if instruction:
            if re.compile('<comment>.*?<\/comment>').search(wholeQuest):
                if re.compile('((<comment>)(\s*|Select one( (in|for) each( row| column)?)?|Select all that apply( (in|for) each( row| column)?)?|Please be( as)? specific( as possible)?|Enter a( whole)? number|@\(instructionTxt\d+?\))(<\/comment>))').search(wholeQuest):
                    preComment = re.compile('<comment>(.|\s)*?<\/comment>').search(wholeQuest).group()
                    comment = '<comment>{0}</comment>'.format(instruction)
                    wholeQuest = wholeQuest.replace(preComment,comment)
            else:
                wholeQuest = wholeQuest.replace('</title>','</title>\n  <comment>{0}</comment>'.format(instruction,0))
        return wholeQuest

    def runAgBot(self, selectedInput):
        initialInput = selectedInput

        # checks if there are actually any questions
        if re.compile(questionReg).search(initialInput):
            # creates an array with question tags as elements. all non-question code is grouped into arrays inbetween the questions
            input = [qn for qn in re.split(questionReg, initialInput) if splitCk(qn)]
            # reads for each element(question | questionSplit) of the survey        
            for qind,questionSplit in enumerate(input):
                # text to be replaced after the script is finished
                beforeRead = questionSplit
                # get question tag
                initialTag = [x for x in re.split('(<.*?>)', questionSplit) if splitCk(x)][0]

                if goodForParse(initialTag, questionSplit):
                    # generates checks and counters  
                    self.cntrRow = 0
                    self.cntrRowOe = 0
                    self.cntrCol = 0
                    self.cntrColOe = 0
                    self.cntrChoice = 0
                    self.cntrGroup = 0
                    self.hasNoanswer = False
                    self.autofillZeroes = False

                    questionTag, questionSplit = preprocessQuestion(self, questionElementsReg, questionSplit)

                    # radio and checkbox
                    if re.compile('^<(radio|checkbox)').search(questionTag):

                        # adds cardsort attributes
                        if re.compile('cardsort').search(questionTag):
                            autosubmitStyle = '\n'
                            questionTag = addAttr(questionTag, '\ncardsort:completionHTML', ' ')
                            questionTag = addAttr(questionTag, '\ncardsort:dragAndDrop', '0')
                            questionTag = addAttr(questionTag, '\ncardsort:displayCounter', '0')
                            if re.compile('^<radio').search(questionTag):
                                if not self.view.find('label="cardsortAutosubmit"', 0):
                                    autosubmitStyle = cardsortScript
                                elif 'copy="cardsortAutosubmit"' not in questionSplit and 'label="cardsortAutosubmit"' not in questionSplit:
                                    autosubmitStyle = """  <style copy="cardsortAutosubmit"/>\n"""
                                questionSplit = questionSplit.replace('</radio>', '%s</radio>' % autosubmitStyle)
                                questionTag = addAttr(questionTag, '\ncardsort:displayNavigation', '0')

                            elif re.compile('^<checkbox').search(questionTag):
                                if not self.view.find('label="cardsortAutosubmitCheckbox"', 0):
                                    autosubmitStyle = cardsortScript
                                elif 'copy="cardsortAutosubmitCheckbox"' not in questionSplit and 'label="cardsortAutosubmitCheckbox"' not in questionSplit:
                                    autosubmitStyle = """  <style copy="cardsortAutosubmitCheckbox"/>\n"""
                                questionSplit = questionSplit.replace('</checkbox>', autosubmitStyle + '</checkbox>')
                                questionTag = addAttr(questionTag, '\ncardsort:iconButtonCSS', 'background-color:#bb29bb;')
                                questionTag = addAttr(questionTag, '\ncardsort:contentsCardCSS', 'font-size:16px; background-color:#bb29bb; color:white;')
                                questionTag = addAttr(questionTag, '\ncardsort:bucketSelectCSS', 'background-color:#bb29bb; color:white;')

                            questionTag = addAttr(questionTag, '\ncardsort:bucketHoverCSS', 'background-color:#bb29bb; color:white;')
                            questionTag = addAttr(questionTag, '\ncardsort:cardCSS', 'font-size:18px; border:2px solid rgb(219, 219, 210); min-height:80px;')
                            if 'type="rating"' in questionTag or self.cntrCol <= 6:
                                questionTag = addAttr(questionTag, '\ncardsort:bucketsPerRow', str(self.cntrCol))
                                questionTag = addAttr(questionTag, '\ncardsort:bucketCSS', 'font-size:15px; color: #454545; background-color:white; border:1px solid rgb(219, 219, 210); min-height:66px; width:%dpx; ' % int(950/self.cntrCol-20))
                            else:
                                questionTag = addAttr(questionTag, '\ncardsort:bucketCSS', 'font-size:15px; color: #454545; background-color:white; border:1px solid rgb(219, 219, 210); min-height:66px;')

                    # text
                    elif re.compile('^<(text|textarea)').search(questionTag):
                        # add bmrnoanswer if there is <noanswer>
                        if re.compile('<noanswer').search(questionSplit):
                            if 'uses="' not in questionTag:
                                questionTag = addAttr(questionTag,'uses','bmrnoanswer.3')
                            elif 'uses="' in questionTag and 'bmrnoanswer.' not in questionTag:
                                questionTag = questionTag.replace('uses="','uses="bmrnoanswer.3,')
                    # number
                    elif re.compile('^<number').search(questionTag):
                        if amountCheck(questionSplit):
                            questionTag,questionSplit = addAutosum(self, questionTag, questionSplit)
                        questionTag = fixNumSize(questionTag,questionSplit)
                        questionTag = addFwidth(questionTag,questionSplit)

                    # select                
                    elif re.compile('^<select').search(questionTag):
                        # add minRanks
                        if re.compile('(="|,)(ranksort|bmrranking)\.\d').search(questionTag):
                            questionTag = addAttr(questionTag,'minRanks',self.cntrChoice)
                        # add ranksort colors
                        if re.compile('(="|,)ranksort\.\d').search(questionTag):
                            if re.compile('ranksort:\w*CSS=".*?background.*').search(questionTag):
                                bgColor = re.compile('ranksort:\w*CSS=".*?background(?:-color)?: ?(rgba?|#.*?(?=;|"))').search(questionTag).group(1)
                            else:
                                bgColor = "background-color: #bb29bb"
                            questionTag = addAttr(questionTag,'\nranksort:cardHoverCSS', bgColor)
                            questionTag = addAttr(questionTag,'\nranksort:cardDroppedCSS', bgColor)
                            questionTag = addAttr(questionTag,'\nranksort:uiDraggableHelperCSS', bgColor)

                    # float
                    elif re.compile('^<float').search(questionTag):
                        questionTag = addFwidth(questionTag,questionSplit)

                    questionSplit = questionSplit.replace(initialTag,questionTag)
                    questionSplit = questionSplit.replace('><title>','>\n  <title>')
                    questionSplit = questionSplit.replace('><!--NOTE:','>\n<!--NOTE:')

                    questionSplit = shuffleAdd(self, questionSplit)

                    # PN check                      
                    questionSplit = pnCheck(questionSplit)

                    # Submessages
                    if self.view.settings().get('parseDefaultComments'):
                        questionSplit = self.addSubmess(questionSplit)

                    setFound = self.view.settings().get('setFoundComment') if self.view.settings().has('setFoundComment') else False
                    questionSplit = givenSubmessCheck(questionSplit, setComment=setFound)

                    afterRead = questionSplit
                    initialInput = initialInput.replace(beforeRead, afterRead, 1)

                    # Putting the 0 autofil exec after the suspend
                    if self.autofillZeroes and '</exec>\n<suspend/>' in initialInput:
                        initialInput = initialInput.replace('</exec>\n<suspend/>','</exec>').replace('</number>',  '</number>\n<suspend/>')
                else:
                    # text fixes in htmls/hidden questions
                    questionSplit = textEscape(self, questionSplit)
                    questionSplit = addLiTags(questionSplit)
                    afterRead = questionSplit
                    initialInput = initialInput.replace(beforeRead, afterRead, 1)

        #reading rows only
        elif re.compile(questionElementsReg).search(initialInput):
            initialInput = onlyElementsNoQn(self, initialInput)

        return initialInput

    def run(self, edit):
        try:
            self.surtype = self.view.settings().get('surveyType')
            sels = self.view.sel()
            input = ''
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel).strip()
                printPage = self.runAgBot(input)
                self.view.replace(edit,sel, printPage)
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print ("Maggie bot failed:")
            print (e)


############################# Merry #############################

class runMerry(sublime_plugin.TextCommand):
    cntrRow = 0
    cntrRowOe = 0
    cntrCol = 0
    cntrColOe = 0
    cntrChoice = 0
    cntrGroup = 0
    hasNoanswer = False
    autofillZeroes = False
    cntrRowOeLabels = []

    def addSubmess(self, wholeQuest):
        instruction = ''
        if re.compile('^<(radio)').search(wholeQuest):
            if self.cntrRow < 2 or self.cntrCol < 2:
                instruction = '${res.hInstrText_s}'
            elif self.cntrRow > 1 and self.cntrCol > 1:
                if groupingCols(wholeQuest):
                    instruction = '${res.hInstrText_s3}'
                else:
                    instruction = '${res.hInstrText_g}'
        elif re.compile('^<(select)').search(wholeQuest):
            if re.compile('uses=".*?ranksort\.\d.*?"').search(wholeQuest):
                instruction = '${res.hInstrText_csc}'
            elif re.compile('uses=".*?bmrranking\.\d.*?"').search(wholeQuest):
                instruction = '${res.hInstrText_rc}'
            else:
                instruction = False
        elif re.compile('^<(checkbox)').search(wholeQuest):
            if self.cntrRow < 2 or self.cntrCol < 2:
                instruction = '${res.hInstrText_m}'
            elif self.cntrRow > 1 and self.cntrCol > 1:
                if groupingCols(wholeQuest):
                    instruction = '${res.hInstrText_m3}'
                else:
                    instruction = '${res.hInstrText_mg}'
        elif re.compile('^<(textarea)').search(wholeQuest) or re.compile('^<(text)').search(wholeQuest):
            instruction = '${res.hInstrText_o}'
        elif re.compile('^<(float)').search(wholeQuest):
            instruction = "ENTER A NUMBER"
        elif re.compile('^<(number)').search(wholeQuest):
            if self.cntrRow < 2 and self.cntrCol < 2:
                if re.compile("if this\.i?val gt (\w|\d).*?:\n\s*error").search(wholeQuest):
                    instruction = '${res.hInstrText_lp}'
                elif re.compile("if this\.i?val lt (\w|\d).*?:\n\s*error").search(wholeQuest):
                    instruction = '${res.hInstrText_gep}'
                else:
                    instruction = '${res.hInstrText_n}'
            elif self.cntrRow < 2 or self.cntrCol < 2:
                if amountCheck(wholeQuest) == "100":
                    instruction = '${res.hInstrText_seh}'
                elif "if this.sum lt 100:" in wholeQuest or "if eachRow.sum lt 100:" in wholeQuest or "if eachCol.sum lt 100:" in wholeQuest:
                    instruction = '${res.hInstrText_sgeh}'
                else:
                    instruction = '${res.hInstrText_nl}'
            else:
                if groupingCols(wholeQuest):
                    if amountCheck(wholeQuest) == "100":
                        instruction = '${res.hInstrText_sehc}'
                    elif "if this.sum lt 100:" in wholeQuest or "if eachRow.sum lt 100:" in wholeQuest or "if eachCol.sum lt 100:" in wholeQuest:
                        instruction = '${res.hInstrText_sgehc}'
                    else:
                        instruction = '${res.hInstrText_nl}'
                else:
                    if amountCheck(wholeQuest) == "100":
                        instruction = '${res.hInstrText_sehr}'
                    elif "if this.sum lt 100:" in wholeQuest or "if eachRow.sum lt 100:" in wholeQuest or "if eachCol.sum lt 100:" in wholeQuest:
                        instruction = '${res.hInstrText_sgehr}'
                    else:
                        instruction = '${res.hInstrText_nl}'

        if instruction:
            if re.compile('<comment>.*?<\/comment>').search(wholeQuest):
                if '<comment>${res.hInstrText' in wholeQuest or re.compile('((<comment>)(\s*|Select one( (in|for) each( row| column)?)?|Select all that apply( (in|for) each( row| column)?)?|Please be( as)? specific( as possible)?|Enter a( whole)? number( in each field below)?)(<\/comment>))').search(wholeQuest):
                    preComment = re.compile('<comment>(.|\s)*?<\/comment>').search(wholeQuest).group()
                    comment = '<comment>{0}</comment>'.format(instruction)
                    wholeQuest = wholeQuest.replace(preComment,comment)
            else:
                wholeQuest = wholeQuest.replace('</title>','</title>\n  <comment>{0}</comment>'.format(instruction,0))
        return wholeQuest

    def runM3Bot(self, selectedInput):
        initialInput = selectedInput

        # checks if there are actually any questions
        if re.compile(questionReg).search(initialInput):
            # creates an array with question tags as elements. all non-question code is grouped into arrays inbetween the questions
            input = [qn for qn in re.split(questionReg, initialInput) if splitCk(qn)]
            # reads for each element(question | questionSplit) of the survey        
            for qind,questionSplit in enumerate(input):
                # text to be replaced after the script is finished
                beforeRead = questionSplit
                # get question tag
                initialTag = [x for x in re.split('(<.*?>)', questionSplit) if splitCk(x)][0]

                if goodForParse(initialTag, questionSplit):
                    # generates checks and counters  
                    self.cntrRow = 0
                    self.cntrRowOe = 0
                    self.cntrCol = 0
                    self.cntrColOe = 0
                    self.cntrChoice = 0
                    self.cntrGroup = 0
                    self.hasNoanswer = False
                    self.autofillZeroes = False
                    self.cntrRowOeLabels = []

                    questionTag, questionSplit = preprocessQuestion(self, questionElementsReg, questionSplit)

                    # Add OE validates
                    if self.cntrRowOe and len(self.cntrRowOeLabels):
                        questionSplit = addOeValidate(self, questionSplit)

                    # radio and checkbox
                    # if re.compile('^<(radio|checkbox)').search(questionTag):
                       # if re.compile('^<checkbox').search(questionTag):

                    # text
                    # elif re.compile('^<(textarea|text)').search(questionTag):
                        # questionSplit = addTextValidate(questionSplit)

                    # number
                    if re.compile('^<number').search(questionTag):
                        if amountCheck(questionSplit):
                            questionTag,questionSplit = addAutosum(self, questionTag, questionSplit)
                        questionTag = fixNumSize(questionTag,questionSplit)
                        questionTag = addFwidth(questionTag,questionSplit)
                        if self.cntrRow:
                            questionTag = addAttr(questionTag,'ss:listDisplay', '0')
                        # questionTag = addAttr(questionTag,'ss:postText', '${res.}')

                    # select                
                    elif re.compile('^<select').search(questionTag):
                        # add minRanks
                        if re.compile('(="|,)(ranksort|bmrranking)\.\d').search(questionTag):
                            questionTag = addAttr(questionTag,'minRanks',self.cntrChoice)
                             
                    # float        
                    elif re.compile('^<float').search(questionTag):
                        questionTag = addFwidth(questionTag,questionSplit)
                        if self.cntrRow:
                            questionTag = addAttr(questionTag,'ss:listDisplay', '0')
                        # questionTag = addAttr(questionTag,'ss:postText', '${res.}')

                    # make attrs on new line
                    questionTag = newLineAttrs(questionTag)

                    questionSplit = questionSplit.replace(initialTag,questionTag)
                    questionSplit = questionSplit.replace('><title>','>\n  <title>')
                    questionSplit = questionSplit.replace('><!--NOTE:','>\n<!--NOTE:')

                    questionSplit = shuffleAdd(self, questionSplit)

                    # PN check
                    questionSplit = pnCheck(questionSplit)

                    # Submessages
                    if self.view.settings().get('parseDefaultComments'):
                        questionSplit = self.addSubmess(questionSplit)
                    setFound = self.view.settings().get('setFoundComment') if self.view.settings().has('setFoundComment') else False
                    questionSplit = givenSubmessCheck(questionSplit, setComment=setFound)

                    afterRead = questionSplit
                    initialInput = initialInput.replace(beforeRead, afterRead, 1)

                    # Putting the 0 autofil exec after the suspend
                    if self.autofillZeroes and '</exec>\n<suspend/>' in initialInput:
                        initialInput = initialInput.replace('</exec>\n<suspend/>','</exec>').replace('</number>', '</number>\n<suspend/>')

                    initialInput = initialInput.replace('>\n<suspend/>', '>\n\n<suspend/>')
                else:
                    # text fixes and new line attributes in htmls/hidden questions
                    questionTag = newLineAttrs(initialTag)
                    questionSplit = questionSplit.replace(initialTag, questionTag)
                    questionSplit = textEscape(self, questionSplit)
                    questionSplit = addLiTags(questionSplit)
                    afterRead = questionSplit
                    initialInput = initialInput.replace(beforeRead, afterRead, 1)
                    initialInput = initialInput.replace('>\n<suspend/>', '>\n\n<suspend/>')

        # reading rows only
        elif re.compile(questionElementsReg).search(initialInput):
            initialInput = onlyElementsNoQn(self, initialInput)

        return initialInput

    def run(self, edit):
        try:
            self.surtype = self.view.settings().get('surveyType')
            sels = self.view.sel()
            input = ''
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel).strip()
                printPage = self.runM3Bot(input)
                self.view.replace(edit,sel, printPage)
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print ("Merry bot failed:")
            print (e)

