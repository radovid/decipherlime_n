import sublime, sublime_plugin, sys, re

# Find and parse questions when auto-parse has been triggered without disrupting user's activity'
class autoParse(sublime_plugin.TextCommand):
    def is_visible(self):
        return not bool( self.view.settings().get('trainingMode') )
    def run(self, edit):
        try:
            currentSelected = self.view.sel()
            currSelectionInParseRegion = False
            currSelsVars = []
            qns2parse = []

            # find regions for questions to parse
            for iv,qnLabel in enumerate(self.view.settings().get('parseQnLabels')):
                qnLabel = qnLabel.replace('$','\\$').replace('(','\\(').replace(')','\\)').replace('[','\\[').replace(']','\\]')
                qns2parse.append(self.view.find('(<(number|float|radio|checkbox|select|text|textarea)[\s\n]+label="{0}"(.|\n)*?</(number|float|radio|checkbox|select|text|textarea)>(\n?<suspend/>)?)'.format(qnLabel), self.view.settings().get('parseRegStart')[iv] - 1000))

            if qns2parse:
                for qn in qns2parse:
                    for selArea in currentSelected:
                        if qn.contains(selArea):
                            currSelectionInParseRegion = True
                            break

                if not currSelectionInParseRegion:

                    # define current selection(s)
                    for selArea in currentSelected:
                        selDict = {}
                        currInputPoint = selArea.begin()
                        selDict['inputCol'] = self.view.rowcol(currInputPoint)[1]
                        selDict['regionStart'] = self.view.text_point(self.view.rowcol(currInputPoint)[0],0)
                        selDict['lineText'] = self.view.substr(self.view.line(selDict['regionStart']))

                        if selArea.begin() != selArea.end():
                            selDict['isPoint'] = False
                            selDict['inputText'] = self.view.substr(selArea)
                        else:
                            selDict['isPoint'] = True
  
                        currSelsVars.append(selDict)

                if not currSelectionInParseRegion or self.view.settings().get('parseNow'):

                    if self.view.settings().has('parseNow'):
                        self.view.settings().erase('parseNow')

                    # select question region and parse
                    self.view.sel().clear()
                    for qn in qns2parse:
                        self.view.sel().add(qn)
                    self.view.run_command('run_parser')

                    # Clear the settings for Q-waiting to be parsed, as it already is
                    #self.view.settings().erase('parseRegStart')
                    #self.view.settings().erase('parseQnLabels')
                    self.view.settings().set('parseAwaiting', False)

                    # de-select parsed question and get current selection again
                    self.view.sel().clear()

                    for sel in currSelsVars:
                        selectAgain = sublime.Region(-1, -1)
                        if sel['isPoint']:
                            sel['inputRow'] = self.view.rowcol(self.view.find(sel['lineText'], sel['regionStart'], sublime.LITERAL).begin())[0]
                            if sel['inputRow']:
                                selectAgain = self.view.text_point(sel['inputRow'], sel['inputCol'])
                            else:
                                knownRow = self.view.rowcol(selDict['regionStart'])[0]
                                for x in range(knownRow, knownRow + 10):
                                    if self.view.substr(self.view.line(self.view.text_point(x, 0))) in ['', ' ', sel['lineText']]:
                                        selectAgain = self.view.text_point(x, sel['inputCol'])
                                        break
                        else:
                            sel['inputRow'] = self.view.rowcol(self.view.find(sel['lineText'], sel['regionStart'], sublime.LITERAL).begin())[0]
                            selectAgain = self.view.find(sel['inputText'], self.view.text_point(sel['inputRow'], sel['inputCol']), sublime.LITERAL)

                        # check if a region found, to prevent jumping to beginning of file
                        if selectAgain:
                            self.view.show(selectAgain)
                            self.view.sel().add(selectAgain)

            else:
                print ("Couldn't find the question(s) that had to be parsed.")
                self.view.settings().erase('parseQnLabels')
                self.view.settings().set('parseAwaiting', False)
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)

# Dummy command to activate auto-parsing with F7
class keyToParse(sublime_plugin.TextCommand):
    def run (self,edit):
        pass

# Run right bot according to surveyType
class runParser(sublime_plugin.TextCommand):
    def is_visible(self):
        return not bool( self.view.settings().get('trainingMode') )
    def run(self, edit):
        try:
            if not bool( self.view.settings().get('trainingMode') ):
                if self.view.settings().get('tryingToParseWithNoneType'):
                    self.view.settings().erase('tryingToParseWithNoneType')
                    if self.view.settings().get('useHpr'):
                        self.view.run_command('run_chrissy')
                    elif self.view.settings().get('surveyType') in ['fv','nk']:
                        self.view.run_command('run_vicky')
                    else:
                        print ("Running parser unsuccessful: surveyType is None")
                else:
                    if self.view.settings().get('useHpr'):
                        self.view.run_command('run_chrissy')
                    elif self.view.settings().get('surveyType') in ['fv','nk']:
                        self.view.run_command('run_vicky')
                    else:
                    # Ask for surveyType if it's not set and the Parser is ran
                        if not self.view.settings().get('parseAwaiting'):
                            # self.view.run_command('set_survey_type_setting')
                            self.view.settings().set('surveyType', 'fv')
                            self.view.settings().set('tryingToParseWithNoneType', True)
                        else:
                            print ("Auto parser is disabled: surveyType is unset")
                self.view.settings().set('parseAwaiting', False)
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)

# surveyType setting chooser
class setSurveyTypeSetting(sublime_plugin.TextCommand):
    def is_visible(self):
        return not bool( self.view.settings().get('trainingMode') )
    surtypes = ['fv', 'nk', 'nf', 'dn', 'pp', 'pl', 'wm']
    def run(self, edit):
        try:
            if self.view.settings().get('surveyType') in self.surtypes:
                preselected = self.surtypes.index(self.view.settings().get('surveyType'))
            else:
                preselected = 0
            # show quick panel list with survey types
            self.types = (
                'Forsta (generic)',
                'NIKE',
                'NETFLIX',
                'DISNEY',
                'PAYPAL',
                'PEERLESS',
                'WALLMART',
                'None [Disable Parser]'
            )
            self.view.window().show_quick_panel (
                self.types,
                self.on_done,
                selected_index = preselected,
                placeholder = 'Set Survey as:'
            )
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)

    def on_done(self, index):
        if index == -1:
            return 
        elif index == 7:
            self.view.settings().erase('surveyType')
        else:
            self.view.settings().set('surveyType', self.surtypes[index])        

        if self.view.settings().get('tryingToParseWithNoneType'):
            self.view.run_command('run_parser')

        self.view.settings().set('surveyTypeAskedForView', True)

