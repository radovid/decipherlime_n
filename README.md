
## Decipherlime

A repo for the Decipherlime Sublime channel. It contains plugins useful for programming Decipher (Forsta) surveys. There are 2 packages in repository:

1. **DecipherLib** that has many commands/clips for easier XML creation. Common tags and attributes don't need to be typed by hand, and there are features for streamlining the programming process like shortcuts, snippets, and a 'smart' parser aiming to automise some of the work. Originally based on the NoteTab libraries and kept up to date with changes there, though a lot of clips have been fixed or rewritten.

2. **DecipherXML** is an XML syntax highlighter based on the default one in Sublime, but upgraded for Decipher. The additions are macros recognition, Python syntax in exec/validate tags and JS/CSS highlighting in styles.


#### Installation

In Sublime Text select `Preferences ⇨ Package Control ⇨ Add Channel`, then in the input box enter this url: https://radovid.eu/decipherlime/channel.json. Both packages will be available via `Package Control ⇨ Install Package` (search for *Decipher*) and auto-updated in the future.

---

For additional information visit: https://radovid.eu/decipherlime
