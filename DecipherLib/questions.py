import sublime, sublime_plugin, sys, re, string

# clean whitespace
def cleanInput(input):
    #CLEAN UP THE TABS
    input = re.sub("\t+", " ", input)
    #CLEAN UP SPACES
    input = re.sub("\n +\n", "\n\n", input)
    #CLEAN UP THE EXTRA LINE BREAKS
    input = re.sub("\n{2,}", "\n", input)
    return input
    #CLEAN UP INITIAL SPACES AND THE EXTRA LINE BREAKS
    input = re.sub("\n\s{2,}", "\n", input)

def getAttrDelimiter(self):
    if self.view.settings().get('surveyType') in ['v3']:
        delmiter = " "
    else:
        delmiter = "\n  "
    return delmiter

### QUESTION TYPES ###
class makeQuestion(sublime_plugin.TextCommand):
    QtypeS = ('radio','rating','checkbox','select','text','textarea','number','float','date')
    TagS = ('radio','radio','checkbox','select','text','textarea','number','float','text')
    AttributeS = ('','{delim}type="rating"','{delim}atleast="1"','{delim}optional="0"','{delim}size="40"{delim}optional="0"','{delim}optional="0"','{delim}size="3"{delim}optional="0"{delim}verify="range(0,9999)"','{delim}size="3"{delim}optional="0"{delim}range="100"','{delim}hpr:type="date"{delim}hpr:minDate="date(1990,1,1)"{delim}hpr:maxDate="today()"')

    def is_visible(self, qType):
        if qType == 'date':
            return bool(self.view.settings().get('useHpr'))
        elif qType in ['rating','select']:
            return not bool(self.view.settings().get('useHpr'))
        else:
            return True

    def run(self, edit, qType):
        try:
            attrDelim = getAttrDelimiter(self)
            sels = self.view.sel()
            input = ''

            qTag = self.TagS[self.QtypeS.index(qType)]
            qAttrs = self.AttributeS[self.QtypeS.index(qType)].format(delim=attrDelim)

            labelsForParse = []
            hiddens = []

            for sel in sels:
                printPage = ''
                isHidden = False
                
                input = self.view.substr(sel).strip()
                
                # clear whitespace and some special chars in question
                input = re.sub("\t+", " ", input)
                input = re.sub("\n\s+\n", "\n\n", input)
                #funkyChars = [(chr(133),'...'),(chr(145),"'"),(chr(146),"'"),(chr(147),'"'),(chr(148),'"'),(chr(151),'--')]
                #for pair in funkyChars:
                #    input = input.replace(pair[0],pair[1])
                
                input = re.sub("\n{3,}", "\n\n", input)
                if re.compile('&(?!(#\d+|lt|gt|amp|quot|apos);)').search(input):
                    input = re.sub('&(?!(#\d+|lt|gt|amp|quot|apos);)', '&amp;', input)


                # catch label
                label = re.split(r"^(\[?(?:\w|-(?=\w)|\.(?=\w))+\]?(?:_?\[loopvar:\s*\w+\]|\$\(\w+\))?(?:\w|-(?=\w)|\.(?=\w))*)(\.|-|:|\)|_?\s)", input, 1)[1].replace('-','_').replace('.','_')
                if re.compile('\[\s*(?!loopvar)(.+?)\]').search(label):
                    label = re.sub('\[\s*(?!loopvar)(.+?)\]', '\g<1>', label)
                input = re.split(r"^(\[?(?:\w|-(?=\w)|\.(?=\w))+\]?(?:_?\[loopvar:\s*\w+\]|\$\(\w+\))?(?:\w|-(?=\w)|\.(?=\w))*)(\.|-|:|\)|_?\s)", input, 1)[-1]


                # escapes [
                # if re.compile('\[(?!pipe|res|rel)(.*?)(\])').search(input):
                #     input = re.sub('\[(?!pipe|res|rel)(.*?)(\])', '&amp;#91;\g<1>\g<2>', input);

                #Add a Q if the first character is a digit
                if label[0].isdigit():
                    label = "Q" + label

                #isolate the title
                if re.compile('(.|\n)*?(@|<(row|col|choice|comment|group|noanswer|net|exec|virtual|validate|onLoad|style|insert))').search(input):
                    title = re.compile('((.|\n)*?)(?=@|<(row|col|choice|comment|group|noanswer|net|exec|virtual|validate|onLoad|style|insert))').search(input).group(1)
                else:
                    title = input

                # try to check if this is a hidden question
                if re.compile('^h[A-Za-z].*').search(label.strip()) or re.compile('^hidden.*', re.IGNORECASE).search(title.strip()) or re.compile('(?i)(hidden|recode|show)', re.IGNORECASE).search(label):
                    isHidden = True

                # remove title from input
                output = input.replace(title, "").strip()
                # add double breaks to title if found
                title = re.sub('(?<!/>)\n\n(?!<br)', '\n<br/><br/>\n', title.strip())

                # set submessage
                comment = ""
                if self.view.settings().get('parseDefaultComments') and "<comment>" not in input:
                    if qType in self.QtypeS[:2]:
                        #adjust comment for 2d question             
                        if (("<row" in output) or ("rows=True" in output)) and (("<col" in output) or ("cols=True" in output)):
                            comment = "  <comment>Select one in each row</comment>\n"
                        else:
                            comment = "  <comment>Select one</comment>\n"
                    elif qType == self.QtypeS[2]:
                        comment = "  <comment>Select all that apply</comment>\n"
                    elif qType in self.QtypeS[4:-2]:
                        comment = "  <comment>Please be as specific as possible</comment>\n"
                    elif qType == self.QtypeS[6]:
                        comment = "  <comment>Enter a whole number</comment>\n"
                    elif qType == self.QtypeS[7]:
                        comment = "  <comment>Enter a number</comment>\n"
                    elif qType == self.QtypeS[8]:
                        comment = "  <comment>Select a date</comment>\n"

                if (("<row" in output) or ("rows=True" in output)) and (("<col" in output) or ("cols=True" in output)):
                    if qType == self.QtypeS[1]:
                        qAttrs += '{delim}shuffle="rows"'.format(delim=attrDelim)

                # compose the question
                printPage = "<{tag}{delim}label=\"{lbl}\"{attrs}>\n  <title>{title}</title>\n{comment}  {content}\n</{tag}>".format(tag=qTag, lbl=label.strip(), delim=attrDelim, attrs=qAttrs, title=title.strip(), comment=comment, content=output)
                if not self.view.settings().get('useHpr'):
                    printPage += "\n<suspend/>"
                self.view.replace(edit,sel, printPage)

                labelsForParse.append(label)
                hiddens.append(isHidden)

            allQs = zip(labelsForParse, hiddens, [sel for sel in sels])
            self.autoParseSet(list(allQs))
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print ("Make Question failed:")
            print (e)


    def autoParseSet(self, questions):
        # Parse previous question if still awaiting
        if self.view.settings().get('parseAwaiting') and not self.view.settings().get('trainingMode'):
            self.view.run_command('auto_parse')

        # Set stuff for autoParse
        if self.view.settings().get('autoParse') and not self.view.settings().get('trainingMode'):
            for qn in questions:
                # Make hidden if flagged
                if qn[1]:
                    self.view.sel().clear()
                    self.view.sel().add(qn[2])
                    self.view.run_command('make_hidden')
                    self.view.sel().clear()
                    questions.remove(qn)

            for qn in questions:
                self.view.sel().add(self.view.find('(<(number|float|radio|checkbox|select|text|textarea)[\s\n]+label="{0}"(.|\n)*?</(number|float|radio|checkbox|select|text|textarea)>(\n?<suspend/>)?)'.format(qn[0]), qn[2].begin() - 900))

            qnSelectionStart = [sel.begin() for sel in self.view.sel()]
            labels = [qn[0] for qn in questions]
            # Set settings for the autoParse
            self.view.settings().set('parseAwaiting', True)
            self.view.settings().set('parseQnLabels', labels)
            self.view.settings().set('parseRegStart', qnSelectionStart)


class makePipe(sublime_plugin.TextCommand):
    def run (self,edit):
        try:
            attrDelim = getAttrDelimiter(self)
            sels = self.view.sel()
            input = ''

            for sel in sels:
                printPage = ''
                input = self.view.substr(sel).strip()

                try:
                    label = re.split(r"^((?:\w|-(?=\w)|\.(?=\w))+(?:\[loopvar:\s*\w+\]|\$\(\w+\))?(?:\w|-(?=\w)|\.(?=\w))*)(\.|-|:|\)|_?\s)", input, 1)[1].replace('-','_').replace('.','_')
                    input = re.split(r"^((?:\w|-(?=\w)|\.(?=\w))+(?:\[loopvar:\s*\w+\]|\$\(\w+\))?(?:\w|-(?=\w)|\.(?=\w))*)(\.|-|:|\)|_?\s)", input, 1)[-1]
                    
                except:
                    label = ''
                    if "<case" in input:
                        someText = input[0:input.index("<case")]
                    input = '  ' + input.replace(someText, '')

                # add pipe to label
                if label and not re.search('pipe', label, re.IGNORECASE):
                    lPipe = "pipe" if re.match("[A-Z0-9_]", label[-1]) else "_pipe"
                    label += lPipe
 
                while "\n\n" in input:
                    input = input.replace("\n\n", "\n")
                if label and label[0].isdigit():
                    label = "Q" + label
                # add the all important line breakage
                output = input
                
                if not re.search('<case label=".*?" cond="1">', output):
                    #Determine the last label, if there is one
                    try:
                        lastLabel = int(re.sub('\D', '', re.findall('label=".*?"', output)[-1].split('"')[1]))
                    except:
                        lastLabel = 98
                    undefCase = """\n  <case label="p{0}" cond="1">&#160;</case>""".format(lastLabel+1)
                else:
                    undefCase = ""

                if self.view.settings().get('surveyType') in ['v3']:
                    printPage = """<pipe label="{lbl}" capture="" onLoad="fixupPipe()">\n{cases}{lastc}\n</pipe>""".format(lbl=label.strip(), cases=output, lastc=undefCase)
                else:            
                    printPage = """<pipe{delim}label="{lbl}"{delim}capture="">\n{cases}{lastc}\n</pipe>""".format(lbl=label.strip(), cases=output, lastc=undefCase, delim=attrDelim)

                self.view.replace(edit,sel, printPage)
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print ("Make Pipe failed:")
            print (e)

class makeComment(sublime_plugin.TextCommand):
    def run (self,edit):
        try:
            attrDelim = getAttrDelimiter(self)
            sels = self.view.sel()
            input = ''
            selectReg = []
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel)
                input = re.sub('(?<!>)\n(?!<)', '<br/>\n', input)
                printPage = """<html{0}label=""{0}where="survey">\n{1}\n</html>\n<suspend/>""".format( attrDelim, input.strip())
                self.view.replace(edit,sel, printPage)
                selectReg.append(self.view.find('label=""', sel.begin()).begin() + 7)
            self.view.sel().clear()
            for reg in selectReg:
                self.view.sel().add(reg)
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print ("Make Survey Comment failed:")
            print (e)

class makeHtmlFinal(sublime_plugin.TextCommand):
    def run (self,edit):
        try:
            attrDelim = getAttrDelimiter(self)
            sels = self.view.sel()
            input = ''
            selectReg = []
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel).strip()
                printPage = """<html{0}label=""{0}cond=""{0}where="survey"{0}final="1">\n<div class="error">{1}</div>\n</html>""".format( attrDelim, input )

                self.view.replace(edit,sel, printPage)
                selectReg.append(self.view.find('label=""', sel.begin()).begin() + 7)
            self.view.sel().clear()
            for reg in selectReg:
                self.view.sel().add(reg)
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)

### QUESTION ELEMENTS ###
class makeQuestionElements(sublime_plugin.TextCommand):
    elTypes = ('row','col','choice', 'group', 'noanswer')
    elLabels = ('r', 'c', 'ch', 'g', 'n')
    commands = ('labels', 'values', 'high', 'low', 'reversed', 'alpha', 'underscore', 'img')

    def is_visible(self, elem, add=None):
        if elem == 'choice':
            return not bool(self.view.settings().get('useHpr'))
        return True

    def run(self, edit, elem, add=None):
        try:
            sels = self.view.sel()
            input = ''
            element = self.elTypes[self.elTypes.index(elem[:-1])]
            label = self.elLabels[self.elTypes.index(elem[:-1])] if add != 'underscore' else '_'
            if add == 'alpha':
                letters = list(string.ascii_lowercase)

            for sel in sels:
                count = 0
                printPage = ''

                input = self.view.substr(sel)
                input = cleanInput(input)
                input = input.strip().split("\n")
                length = len(input)

                if add == 'alpha':
                    if length > 78:
                        letters.extend([a + b for a in letters for b in letters])
                    else:
                        letters.extend([b+b for b in letters])
                        letters.extend([b+b+b for b in list(string.ascii_lowercase)])

                for line in input:
                    line = line.strip()
                    count += 1

                    if add in ['labels', 'values']:
                        # split on whitespace -- remove leading and trailing ws
                        parts = re.split('\s', line, 1) 
                        # get rid of extra spaces
                        ordinal = re.sub('[^a-zA-Z0-9_-]', '', parts[0]).rstrip('-')

                        if add == 'labels':
                            content = parts[1].strip() if len(parts) == 2 else line
                            ordinal = ordinal.replace('-','')
                            ordinal = label + ordinal if (ordinal.isalpha() and len(ordinal) <= 3 and len(parts) == 2) or ordinal.isdigit() else (ordinal if len(parts) == 2 else label + str(count))
                            printPage += """  <{elem} label="{val}">{txt}</{elem}>\n""".format(elem=element, val=ordinal, txt=content)
                        elif add == 'values':
                            if ordinal.lstrip('-').isdigit():
                                content = parts[1].strip() if len(parts) == 2 else line
                            else:
                                content = line
                            ordinal = ordinal if ordinal.lstrip('-').isdigit() else str(count)
                            printPage += """  <{elem} label="{lbl}{lval}" value="{val}">{txt}</{elem}>\n""".format(elem=element, lbl=label, lval=ordinal.replace('-',''), val=ordinal, txt=content)
                    else:
                        # remove qnr labels
                        content = re.sub("^\(?[a-zA-Z0-9]{1,2}[\.:\)-]\s+", "\n", line).strip()

                        if add in ['high', 'low']:
                            ordinal = length - count + 1 if add == 'high' else count
                            printPage += """  <{elem} label="{lbl}{val}" value="{val}">{txt}</{elem}>\n""".format(elem=element, lbl=label, val=ordinal, txt=content)                            
                        elif add == 'img':
                            ordinal = count
                            printPage += """  <{elem} label="{lbl}{val}" alt="{img}"><img src="[rel {img}]" style="max-width:400px; height:auto; display:block;" /></{elem}>\n""".format(elem=element, lbl=label, val=ordinal, img=content)
                        else:
                            if add == 'reversed':
                                ordinal = length - count + 1
                            elif add == 'alpha':
                                ordinal = letters[count-1]
                            else:
                                ordinal = count
                            printPage += """  <{elem} label="{lbl}{val}">{txt}</{elem}>\n""".format(elem=element, lbl=label, val=ordinal, txt=content)

                self.view.replace(edit,sel, printPage.strip('\n'))
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print ("Make {} failed:".format(elem.capitalize()))
            print (e)

### OTHER ###
class makeCases(sublime_plugin.TextCommand):
    def run (self,edit):
        try:
            sels = self.view.sel()
            input = ''
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel)
                input = cleanInput(input)
                input = input.strip().split("\n")

                for x in range(0,len(input)):
                    input[x] = re.sub("^[a-zA-Z0-9]{1,2}[\.:\)][ \t]+", "\n", input[x])
                count = 0
                for x in input:
                    printPage += """  <case label="p{lbl}" cond="">{txt}</case>\n""".format(lbl=str(count+1), txt=input[count].strip())
                    count += 1
            self.view.replace(edit,sel, printPage.strip('\n'))
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)

class makeNets(sublime_plugin.TextCommand):
    def run (self, edit, useType):
        try:
            sels = self.view.sel()
            input = ''
            if useType not in ['labels', 'indices', 'values']:
                useType = 'labels'
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel)
                input = cleanInput(input)
                input = input.strip().split("\n")

                for x in range(0,len(input)):
                    input[x] = re.sub("^[a-zA-Z0-9]{1,2}[\.:\)][ \t]+", "\n", input[x])
                for x in range(len(input)):
                    printPage += """  <net label="" {}="">{}</net>\n""".format(useType, re.sub(r"^[a-zA-Z0-9]+(\.|:)|^[a-zA-Z0-9]+[a-zA-Z0-9]+(\.|:)", "", input[x]).strip())

            self.view.replace(edit,sel, printPage.strip('\n'))
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)


### FUNCTIONS ###
class relabelElements(sublime_plugin.TextCommand):
    def run(self, edit):
        try:
            letters = list(string.ascii_lowercase)
            letters += [a + b for a in letters for b in letters]

            sels = self.view.sel()
            input = ''
            for sel in sels:
                count = 0
                printPage = ''
                extra = ''
                input = self.view.substr(sel)
                input = input.split("\n")

                if len(input) > 78:
                    letters.extend([a + b for a in letters for b in letters])
                else:
                    letters.extend([b+b for b in letters])
                    letters.extend([b+b+b for b in list(string.ascii_lowercase)])

                # Get first label and remove alpha characters
                startlabel = re.findall("label=\"\w+\"", input[0])[0].replace("label=","").strip("\"'") 
                nonalphalabel = re.sub("[a-zA-Z]*", "", startlabel)

                if nonalphalabel.isdigit():
                    startindex = int(nonalphalabel)
                    lblType = 'digit'
                else:
                    import difflib
                    nextlabel = re.findall('label="\w+"', input[1])[0].replace("label=","").strip("\"'")
                    matching = [ch[-1] for ch in difflib.ndiff(startlabel, nextlabel) if ch[0] == ' ']
                    dalabel = ''
                    for ch in matching:
                        if startlabel.startswith(dalabel + ch):
                            dalabel += ch
                        else:
                            break
                    if not dalabel:
                        dalabel = startlabel[:1] #startlabel[:-1]
                        
                    if startlabel.strip(dalabel)[-1] in letters:
                        startindex = letters.index(startlabel.strip(dalabel)[-1])
                        lblType = 'alpha'
                    elif startlabel.strip(dalabel)[-1].lower() in letters:
                        letters = [a.upper() for a in letters]
                        startindex = letters.index(startlabel.strip(dalabel)[-1])
                        lblType = 'alpha'
                    else:
                        startindex = ord(startlabel.strip(dalabel)[-1])
                        lblType = ''

                lblCntr = 0
                for line in range(len(input)):
                    if 'label="' in input[line]:
                        if lblType == 'digit':
                            newlabel = re.sub("[0-9]+", str(startindex + lblCntr), startlabel)
                        elif lblType == 'alpha':
                            newlabel = dalabel + letters[startindex + lblCntr]
                        else:
                            newlabel = dalabel + str(startindex + lblCntr)
                        input[line] = re.sub('label="\w+"', 'label="%s"' % newlabel, input[line])
                        lblCntr += 1

                printPage = "\n".join(input)

                self.view.replace(edit,sel, printPage)
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)


class swapElemTypes(sublime_plugin.TextCommand):
    def run(self, edit, switch2):
        try:
            switchEl = switch2
            sels = self.view.sel()
            input = ''
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel)
                input = input.split("\n")

                replaceStrR = '<row\g<2>r\g<4></row>'
                addReplacementsR = ('qa:rsubtitle','qa:rdescription')
            
                replaceStrC = '<col\g<2>c\g<4></col>'
                addReplacementsC = ('qa:csubtitle','qa:cdescription')
            
                replaceStrCh = '<choice\g<2>ch\g<4></choice>'
                addReplacementsCh = ('','')
                
                for line in input:
                    if switch2 == 'choice':
                        line = re.sub('<(row|col|choice)(.*?label=")(ch|r|c)(.*?)(</(row|col|choice)>)', replaceStrCh, line).replace('qa:rsubtitle',addReplacementsCh[0]).replace('qa:rdescription',addReplacementsCh[1])
                    elif switch2 == 'col':
                        line = re.sub('<(row|col|choice)(.*?label=")(ch|r|c)(.*?)(</(row|col|choice)>)', replaceStrC, line).replace('qa:rsubtitle',addReplacementsR[0]).replace('qa:rdescription',addReplacementsR[1])
                    elif switch2 == 'row':
                        line = re.sub('<(row|col|choice)(.*?label=")(ch|r|c)(.*?)(</(row|col|choice)>)', replaceStrR, line).replace('qa:csubtitle',addReplacementsC[0]).replace('qa:cdescription',addReplacementsC[1])
                    printPage += line + '\n'

                self.view.replace(edit,sel, printPage.strip('\n'))
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)

