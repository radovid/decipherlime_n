# DecipherLib

A package (lib) that adds commands/clips for creating [Decipher XML](https://forstasurveys.zendesk.com/hc/en-us/categories/5915479218331-XML-Programming) tags and attributes. Based on the NoteTab libraries, replicating their functionality, but also extending and improving it. It is aimed to be easy for those migrating, that are used to Notetab.

## Usage

The clips / commands can be used through several interfaces:

 + All available commands can be run from Sublime’s quick panel. By default it is shown on `Ctrl+Shift+P`, but the DecipherLib adds another key shortcut - `F1` (`Escape` also works, but isn't recommended). The DecipherLib's quick panel key shortcuts add a *Decipher:* prefix to the search, so only the related commands are listed and searching is easier.  
  You don't need to match the command name exactly, as in Notetab - for example, *add popup* will show both the Decipher and the Responsive one.
 + A lot of tags, attributes and other have [auto-completion]() suggestions that appear while writing.
 + Many commands also have [keyboard shortcuts]() assigned. If a command has a shortcut assigned, it’s displayed in the **`Decipher`** menu, so it can be used as a reference.
 + An application (window) menu named **`Decipher`** is added, where all commands are accessible and sorted in categories.
 + Some of the more common used commands are available through the context menu (right click).


## Features {#features}

If you take a quick look in the `Decipher` menu, the commands there don't seem that different than the Notetab clips. Most of the changes are under-the-hood and in the accessibility and functionality of commands (there is almost no matching code between the 2 left). That doesn't include only the quick panel, key shortcuts and completions, but also the behavior of some commands.

An addition to many commands is smarter cursor placement. For example, making a survey comment (html tag) put the cursor at *label=""*, loopvars get *VAR_NAME* selected, and so on. Learning to use that can distinctly speed up your work flow. You may only be saving 1-2 clicks per command, but multiply that by all the commands you run per shift. And every "click" is a waste of time and efforts.  
All commands from the Attributes category aren't just text insertions, but they recognize what output is needed. An attribute can be added to multiple rows/cols for example.

DecipherLib aims to be exactly what its called - a library for Decipher. There is just one library, that is updated automatically and tries to be universal for all survey types.

There are also quite a few bugs and issues that have been patched or updated. Commands like make question types and elements have been completely rewritten, because of the many imperfections they had. Except DecipherLib's features there are some very useful ones, that come with Sublime. For example, multiple selections are available, and they are wonderful for some edits. The split layout view is useful if you're working with 2 files, or just so you can leave there some commonly used snippet. There is also an integrated Python console where you can quickly run and check some code.


### Parser {#parser}

The DecipherLib has a universal parser, which automatically detects and puts some attributes to the question(s) being parsed. It also adds Qarts tools, following their KPI guidelines. Except adding Qarts styles it also does some of the following and other.

 + Correct submessages/instructions are set, according to question type and attributes, if enabled in [preferences]();
 + Question elements are checked if they should be exclusive, anchored, or open-end;
 + Question text is checked for randomize instruction;
 + Programmer notes are commented out of the element (this is done for texts in [brackets]);
 + If an instruction is found in the question text, it set as a comment tag if [preferences]() is enabled, or commented out.

So if we run the Parser on this question:
```xml
<checkbox label="Q1" atleast="1">
  <title>Some question here? 
   Please select all that apply. [Randomize]</title>
  <row label="r1">A simple row</row>
  <row label="r2">Another simple row (Anchor)</row>
  <row label="r3">Some option [terminate]</row>
  <row label="r4">Another non-elig option [terminate]</row>
  <row label="r5">Other row (Please specify)</row>
  <row label="r6">None row [exclusive]</row>
</checkbox>
<suspend/>
```

The result will be:
```xml
<checkbox label="Q1" atleast="1" qa:atype="text" qa:tool="rp" shuffle="rows" uses="qarts.2360">
  <title>Some question here?</title> <!--Please select all that apply.-->
  <comment>Select all that apply</comment>
  <row label="r1">A simple row</row>
  <row label="r2" randomize="0">Another simple row</row>
  <row label="r3">Some option </row> <!--[terminate]-->
  <row label="r4">Another non-elig option </row> <!--[terminate]-->
  <row label="r5" open="1" openSize="25" qa:rplaceholder="Please specify" randomize="0">Other row</row>
  <row label="r6" exclusive="1" randomize="0">None row</row>
</checkbox>
<suspend/>
```

#### Auto-Parsing {#autoparse}

There is also an **auto-parse** feature which runs the Parser on each question after creation, so there is no need to select the question and manually run the Forsta Parser command. As some additional attributes or elements might be added to a question, it’s not parsed immediately. Instead the Parser detects where you’re working, and if it’s outside the question, the Parser is ran. For example, if a question is made and new rows are created in it, it won't be parsed, but once a row is made *under* the question, it will be parsed.

The Parser can be triggered, after a question has been made, by `F7`, with no need to select the question again. It is also activated on file save.

The auto-parser tries to detect hidden (*where="execute"*) questions on creation and doesn't run the Parser on them. Instead, it makes them hidden directly.

Auto-Parser can be enabled/disabled via `Decipher ⇨ Settings ⇨ Toggle Auto-Parser`. This setting is global and saved between files and sessions.


### Completions {#completions}

**Tab completions** are suggestions that you get while typing. They are shown in a popup next to the cursor and selected with `Tab`. A lot of tags and attributes have been implemented already. What's best about completions is that they aren't just static text that gets inserted. They can be modified after paste, again with the `Tab` key. Depending on the element they can have up to 3-4 'Tabs'. For example, the verify attribute gets the *max* range selected on first Tab, the *min* on second, and the whole *range()* on third (so it can be changed to another verifier, like *len()*).

A lot of completions don't have a corresponding command (quick panel/Decipher menu), especially the attributes. But even the ones that have, are best used as auto-completions. Opening the `F1` panel, searching for *make terminate* and then setting label first, then the cond and finally the text, takes so much longer than writing *term*, pressing `Tab`, writing the cond, pressing `Tab` again, and writing the text, because the label and the text beginning are auto-captured from the condition. Similar case with quota.

The list of preset conditions is quite long, so it's not posted here, but you see them while writing, so if you just take a look now and then you'll be able to learn them. There are short notes in the completion popup defining each completion that you can use for reference.


### Shortcuts {#shortcuts}

DecipherLib comes with a rich set of keyboard bindings for commonly used commands. As mentioned somewhere above, clicking is very slow, but typing command names constantly is not that quick either, so starting to use the shortcuts will make your life easier.

The key bindings follow specific logic, which you'll get after reviewing the table. Under **Misc.** there are a few key bindings that come from Sublime, not DecipherLib, but they are very useful and have been included. All shortcuts not in **Misc.** are only active for XML files, so if syntax isn't set to XML they just won't activate.

You can also assign your own shortcuts through `Decipher ⇨ Settings ⇨ Key Bindings`.

>         Misc.

 - Show Commands Panel with 'Decipher:'              — `F1` / `Esc`\*
 - Show Commands Panel with last entered text        — `Shift`+`F1` / `Shift`+`Esc`
 - Show Python Console                               — `Ctrl`+`~`
 - Toggle In-Selection (Find & Replace panel)        — `Alt`+`S`
 - Toggle Regular Expressions (Find & Replace panel) — `Alt`+`R`
 - Fold Current Tag                                  — `Ctrl`+`0`
 - Expand selection to tag                           — `Ctrl`+`Shift`+`A`
 - Expand selection to scope (color)                 — `Ctrl`+`Shift`+*`space`*
 - Select next instance of the selected text         — `Ctrl`+`D`
 - Column Selection - Add Line Up                    — `Ctrl`+`Shift`+`🠅`
 - Column Selection - Add Line Down                  — `Ctrl`+`Shift`+`🠇`
 - Column Selection                                  — *Middle click (Scroll)* / `Shift`+*right click*
 - New Survey                                        — `Ctrl`+`Alt`+`Y`
 - Run Forsta Parser                                    — `F8`
 - Auto-Parse the Last Question Created Last         — `F7`

 >        Question Types
 
 - Make Radio                                        — `Ctrl`+`Alt`+`R`
 - Make Rating                                       — `Ctrl`+`Alt`+`G`
 - Make Checkbox                                     — `Ctrl`+`Alt`+`C`
 - Make Number                                       — `Ctrl`+`Alt`+`N`
 - Make Text                                         — `Ctrl`+`Alt`+`T`
 - Make Textarea                                     — `Ctrl`+`Alt`+`A`
 - Make Float                                        — `Ctrl`+`Alt`+`F`
 - Make Survey Comment                               — `Ctrl`+`Alt`+`H`
 - Make Pipe                                         — `Ctrl`+`Alt`+`P`

>        Question Elements

 - Make Rows                                         — `Ctrl`+`1`
 - Make Rows Match Label                             — `Ctrl`+`Shift`+`1`
 - Make Rows Match Value                             — `Ctrl`+`Alt`+`1`
 - Make Cols                                         — `Ctrl`+`2`
 - Make Cols Match Label                             — `Ctrl`+`Shift`+`2`
 - Make Cols Match Value                             — `Ctrl`+`Alt`+`2`
 - Make Choices                                      — `Ctrl`+`3`
 - Make Choices Match Value                          — `Ctrl`+`Alt`+`3`
 - Make Groups                                       — `Ctrl`+`4`
 - Make Noanswers                                    — `Ctrl`+`5`
 - Make Cases                                        — `Ctrl`+`6`
 - Make Res Tags                                     — `Ctrl`+`7`

>         Loop

 - Add Loop                                          — `Ctrl`+*`space`*+`L`
 - Add Loop Block                                    — `Ctrl`+*`space`*+`B`
 - Make Looprows                                     — `Ctrl`+*`space`*+`R`
 - Make Loopvars                                     — `Ctrl`+*`space`*+`V`

>         Attributes

 - Make Hidden                                       — `Alt`+`X` / `Alt`+*`space`*+`X`
 - Add Shuffle Rows                                  — `Alt`+*`space`*+`S`
 - Add Values                                        — `Alt`+`V` / `Alt`+*`space`*+`V`
 - Add Alts                                          — `Alt`+`A` / `Alt`+*`space`*+`A`
 - Add Groups                                        — `Alt`+*`space`*+`G`
 - Add Exclusive                                     — `Alt`+*`space`*+`E`
 - Add Translateable                                 — `Alt`+*`space`*+`T`
 - Add Custom Attribute                              — `Alt`+`C` / `Alt`+*`space`*+`C`
 - Remove Custom Attribute                           — `Alt`+`R` / `Alt`+*`space`*+`R`

>        Text Formatting

 - Make Bold Text                                    — `Ctrl`+`B`
 - Make Italic Text                                  — `Ctrl`+`I`
 - Make Underline Text                               — `Ctrl`+`U`
 - Insert Break &lt;br/>                             — `Ctrl`+`E`
 - Make Li Per Line                                  — `Ctrl`+`L`+`i`

> \* `Esc` closes any open panels and menus, and deselects multiple selections to a single one before opening the commands panel. That's what Escape is supposed to do after all and none of its regular functions have been disabled. The only reason that key binding exists is because of Notetab's legacy and its usage is discouraged. Switching to `F1` instead is strongly recommended.


### Preferences {#preferences}

DecipherLib has several additional features that may not be suitable for everyone, so there are preferences implemented from which they can be enabled/disabled. They can be found in `Decipher ⇨ Settings`. Here they are:

 - **Toggle Qarts Usage** — switch Qarts usage in the active file on/off. Can be used to stop Qarts styling when programming some custom non-Qarts section. Auto turned on.
 - **Auto-Parser** —  enables or disabled the [auto-parsing]() feature globally or just for the current file. When disabled, manually running the Parser (`F8`) still works, but the Parser will never be auto-triggered. Enabled by default.
 - **Escape-On-Save** —  can be set globally or just for the current file. Non-ascii (special) characters and ampersands are auto-escaped when this preferences is enabled. While generally quite useful, there are some cases when enabled escape-on-save can cause issues and even break a survey. Enabled by default.
 - **Message Dialogs** —  defines if errors/warnings are shown in a popup dialog on save. They will still be shown in the status bar if setting is disabled. Currently XML syntax errors and usage of *"or 1"* in conds trigger a message. Sometimes usage of macros causes false errors. It can be set globally or just for current file. Enabled by default.
 - **Found Comment** —  will set potential instructions from the question text as comment tag automatically in the Parser. It finds *Please/Select/Check* sentences at the end of the Q-text. When this preference is disabled the instruction gets commented out after the title. Setting applies only for the current file. Enabled by default.
 - **Comment Tags Parsing** —  is for whether to set default comments (submessages/instructions) when parsing. There are 3 modes:
    + *Strict*: comments are set for all survey types/clients;
    + *Standard*: only for survey types with specific instructions (QArts/AG);
    + *Off*: no comments are set in any mode.
Preference is applied globally. Set to *"off"* by default.
- **Kantar Vars** —  Should the popup for selecting kantar:clienttype/office/complexity/etc. be displayed when creating new Survey (after entering the alt name) or not. Enabled by default.

