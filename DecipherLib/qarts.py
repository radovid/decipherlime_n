import sublime, sublime_plugin, sys, re
try:
    from .Edit import Edit as Edit
except:
    from Edit import Edit as Edit

# clean whitespace
def cleanInput(input):
    #CLEAN UP THE TABS
    input = re.sub("\t+", " ", input)
    #CLEAN UP SPACES
    input = re.sub("\n +\n", "\n\n", input)
    #CLEAN UP THE EXTRA LINE BREAKS
    input = re.sub("\n{2,}", "\n", input)
    return input

######################## QARTS SPECIFIC LIB #########################

class updateQartsVersion(sublime_plugin.TextCommand):
    def is_visible(self):
        return bool( self.view.settings().get('surveyQarts') or self.view.settings().get('useQarts') ) or True
    def run (self,edit):
        try:
            sels = self.view.sel()
            wholeViewReg = sublime.Region(0, self.view.size())
            wholeSurvey = self.view.substr(wholeViewReg)
            # input = self.view.substr(wholeViewReg)
            printPage = ''
            newSurvey = wholeSurvey
            qartsVer = self.view.settings().get('latestQartsVersion')

            if re.compile('qa:template="(.*?)"').search(wholeSurvey):
                newSurvey = re.sub('qa:template="(.*?)"', 'qa:template="5.1"', wholeSurvey)

            if re.compile('uses="(.*,)?qarts\.(\d+)(,.*)?"', re.MULTILINE).search(newSurvey):
                newSurvey = re.sub('(uses="(?:.*,)?qarts\.)(?:\d+)((?:,.*)?")', '\g<1>{}\g<2>'.format(qartsVer), newSurvey, flags=re.MULTILINE)

            printPage = wholeSurvey.replace(wholeSurvey, newSurvey)

            self.view.replace(edit, wholeViewReg, printPage)
        except Exception as e:
            print (e)


### QUESTION ATTRIBUTES ###
class addQaAutosubmit(sublime_plugin.TextCommand):
    def is_visible(self):
        return bool( self.view.settings().get('surveyQarts') or self.view.settings().get('useQarts') ) or True
    def run (self,edit):
        try:
            sels = self.view.sel()
            printPage = ' qa:autosubmit="0"'
            for sel in sels:
                self.view.insert(edit,sel.end(), printPage)
        except Exception as e:
            print (e)

class addRowCtz(sublime_plugin.TextCommand):
    def is_visible(self):
        return bool( self.view.settings().get('surveyQarts') or self.view.settings().get('useQarts') ) or True
    def run (self,edit):
        try:
            sels = self.view.sel()
            printPage = ' qa:rowctz="1"'
            for sel in sels:
                self.view.insert(edit,sel.end(), printPage)
        except Exception as e:
            print (e)

class addColCtz(sublime_plugin.TextCommand):
    def is_visible(self):
        return bool( self.view.settings().get('surveyQarts') or self.view.settings().get('useQarts') ) or True
    def run (self,edit):
        try:
            sels = self.view.sel()
            printPage = ' qa:colctz="1"'
            for sel in sels:
                self.view.insert(edit,sel.end(), printPage)
        except Exception as e:
            print (e)

class addSldrSnap(sublime_plugin.TextCommand):
    def is_visible(self):
        return bool( self.view.settings().get('surveyQarts') or self.view.settings().get('useQarts') ) or True
    def run (self,edit):
        try:
            sels = self.view.sel()
            printPage = ' qa:sldrsnap="1"'
            for sel in sels:
                self.view.insert(edit,sel.end(), printPage)
        except Exception as e:
            print (e)

class addSldrInitial(sublime_plugin.TextCommand):
    def is_visible(self):
        return bool( self.view.settings().get('surveyQarts') or self.view.settings().get('useQarts') ) or True
    def run (self,edit):
        try:
            sels = self.view.sel()
            printPage = ' qa:sldrinitpercent="50"'
            for sel in sels:
                self.view.insert(edit,sel.end(), printPage)
        except Exception as e:
            print (e)

### ROW & COL ATTRIBUTES ###
class addQaImages(sublime_plugin.TextCommand):
    def is_visible(self):
        return bool( self.view.settings().get('surveyQarts') or self.view.settings().get('useQarts') ) or True
    def run (self,edit):
        try:
            sels = self.view.sel()
            input = ''
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel)

                if input:
                    input = cleanInput(input)
                    input = input.split("\n")
                    for line in input:
                      if not "qa:image" in line:
                          line = line.replace('>',' qa:image="[rel ]">',1)
                          printPage += line + '\n'

                    self.view.replace(edit,sel, printPage.strip('\n'))
                else:
                    printPage = ' qa:image="[rel ]"'
                    self.view.insert(edit,sel.end(), printPage)
        except Exception as e:
            print (e)

class addQaStamps(sublime_plugin.TextCommand):
    def is_visible(self):
        return bool( self.view.settings().get('surveyQarts') or self.view.settings().get('useQarts') ) or True
    def run (self,edit):
        try:
            sels = self.view.sel()
            input = ''
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel)

                if input:
                    input = cleanInput(input)
                    input = input.split("\n")
                    for line in input:
                      if not "qa:stamp" in line:
                          line = line.replace('>',' qa:stamp="[rel ]">',1)
                          printPage += line + '\n'

                    self.view.replace(edit,sel, printPage.strip('\n'))
                else:
                    printPage = ' qa:stamp="[rel ]"'
                    self.view.insert(edit,sel.end(), printPage)
        except Exception as e:
            print (e)

class addQaDescriptions(sublime_plugin.TextCommand):
    def is_visible(self):
        return bool( self.view.settings().get('surveyQarts') or self.view.settings().get('useQarts') )
    def run (self,edit):
        try:
            sels = self.view.sel()
            input = ''
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel)

                if input:
                    input = cleanInput(input)
                    input = input.split("\n")
                    for line in input:
                        if "<row" in line and not "qa:rdescription" in line:
                            line = line.replace('>',' qa:rdescription="">',1)
                        elif "<col" in line and not "qa:cdescription" in line:
                            line = line.replace('>',' qa:cdescription="">',1)
                        elif "<$(" in line:
                           pref = line.split("<")[1].split(" ")[0]
                           if not "qa:"+pref+"description" in line:
                                line = line.replace('>',' qa:'+pref+'description="">',1)
                        printPage += line + '\n'

                    self.view.replace(edit,sel, printPage.strip('\n'))
                else:
                    line = self.view.substr(self.view.line(sel.end()))
                    if "<col" in line:
                        printPage = ' qa:cdescription=""'
                    else:
                        printPage = ' qa:rdescription=""'
                    
                    self.view.insert(edit,sel.end(), printPage)
        except Exception as e:
            print (e)

class addQaSubtitles(sublime_plugin.TextCommand):
    def is_visible(self):
        return bool( self.view.settings().get('surveyQarts') or self.view.settings().get('useQarts') ) or True
    def run (self,edit):
        try:
            sels = self.view.sel()
            input = ''
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel)

                if input:
                    input = cleanInput(input)
                    input = input.split("\n")
                    for line in input:
                        if "<row" in line and not "qa:rsubtitle" in line:
                            line = line.replace('>',' qa:rsubtitle="">',1)
                        elif "<col" in line and not "qa:csubtitle" in line:
                            line = line.replace('>',' qa:csubtitle="">',1)
                        elif "<$(" in line:
                           pref = line.split("<")[1].split(" ")[0]
                           if not "qa:"+pref+"subtitle" in line:
                                line = line.replace('>',' qa:'+pref+'subtitle="">',1)
                        printPage += line + '\n'

                    self.view.replace(edit,sel, printPage.strip('\n'))
                else:
                    line = self.view.substr(self.view.line(sel.end()))
                    if "<col" in line:
                        printPage = ' qa:csubtitle=""'
                    else:
                        printPage = ' qa:rsubtitle=""'
                    
                    self.view.insert(edit,sel.end(), printPage)
        except Exception as e:
            print (e)

### ROW ONLY ATTRIBUTES ###
class addQaPlaceholders(sublime_plugin.TextCommand):
    def is_visible(self):
        return bool( self.view.settings().get('surveyQarts') or self.view.settings().get('useQarts') ) or True
    def run (self,edit):
        try:
            sels = self.view.sel()
            input = ''
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel)

                if input:
                    input = cleanInput(input)
                    input = input.split("\n")
                    for line in input:
                      if "<row" in line and not "qa:rplaceholder" in line:
                          line = line.replace('>',' qa:rplaceholder="">',1)
                          printPage += line + '\n'

                    self.view.replace(edit,sel, printPage.strip('\n'))
                else:
                    printPage = ' qa:rplaceholder=""'
                    self.view.insert(edit,sel.end(), printPage)
        except Exception as e:
            print (e)

class addQaGroupsubtitles(sublime_plugin.TextCommand):
    def is_visible(self):
        return bool( self.view.settings().get('surveyQarts') or self.view.settings().get('useQarts') ) or True
    def run (self,edit):
        try:
            sels = self.view.sel()
            input = ''
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel)

                if input:
                    input = cleanInput(input)
                    input = input.split("\n")
                    for line in input:
                      if "<row" in line and not "qa:rgroupsubtitle" in line:
                          line = line.replace('>',' qa:rgroupsubtitle="">',1)
                          printPage += line + '\n'

                    self.view.replace(edit,sel, printPage.strip('\n'))
                else:
                    printPage = ' qa:rgroupsubtitle=""'
                    self.view.insert(edit,sel.end(), printPage)
        except Exception as e:
            print (e)

class addQaColmask(sublime_plugin.TextCommand):
    def is_visible(self):
        return bool( self.view.settings().get('surveyQarts') or self.view.settings().get('useQarts') ) or True
    def run (self,edit):
        try:
            sels = self.view.sel()
            input = ''
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel)

                if input:
                    input = cleanInput(input)
                    input = input.split("\n")
                    for line in input:
                      if "<row" in line and not "qa:columnmask" in line:
                          line = line.replace('>',' qa:columnmask="">',1)
                          printPage += line + '\n'

                    self.view.replace(edit,sel, printPage.strip('\n'))
                else:
                    printPage = ' qa:columnmask=""'
                    self.view.insert(edit,sel.end(), printPage)
        except Exception as e:
            print (e)

### MISC ###
class addQaSplash(sublime_plugin.TextCommand):
    def is_visible(self):
        return bool( self.view.settings().get('surveyQarts') or self.view.settings().get('useQarts') ) or True
    def run (self,edit):
        try:
            sels = self.view.sel()
            printPage = """
<checkbox label="splash" atleast="0" style="splash" cs:restxt="${this.title}" cs:showb="0" cs:bimg="/s/local/qarts/template/bg/lake.jpg" where="notdp">
<title>
<h1>QuestionArts</h1>
<br />
<p>Survey design requires a detailed focus on the consumer experience.</p>
<br />
<br />
<p>It's an art and a science</p>
<br />
<br />
<br />
</title>
</checkbox>
<suspend/>"""
            for sel in sels:
                self.view.insert(edit,sel.end(), printPage)
        except Exception as e:
            print (e)

class addQaRtlStyle(sublime_plugin.TextCommand):
    def is_visible(self):
        return bool( self.view.settings().get('surveyQarts') or self.view.settings().get('useQarts') ) or True
    def run (self,edit):
        try:
            sels = self.view.sel()
            printPage = """
<style cond="" mode="after" name="respview.client.css"><![CDATA[
<style>
#menu {visibility: hidden;}
.survey-body .fir-hidden {left: auto !important; right: -9999px !important;}
#surveyContent, .survey-body, .question-text, .instruction-text, .grid.grid-table-mode .row-legend, .grid-desktop-mode.grid-list-mode .cell-section, .grid-single-col .cell, .grid[data-settings*='single-col'] .cell, .noRows .grid.grid.grid-table-mode .colCount-1 .element, .noRows .grid.grid.grid-table-mode .colCount-1 .col-legend {text-align: right !important;}
body, .qwidget_background {direction: rtl;}
</style>
]]></style>"""
            for sel in sels:
                self.view.insert(edit,sel.end(), printPage)
        except Exception as e:
            print (e)

class makeQaGender(sublime_plugin.TextCommand):
    def is_visible(self):
        return bool( self.view.settings().get('surveyQarts') or self.view.settings().get('useQarts') ) or True
    def run (self,edit):
        try:
            qartsV = self.view.settings().get('qartsVersion')
            sels = self.view.sel()
            input = ''
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel).strip()

                #isolate label and the rest
                label = re.split(r"^((?:\w|-(?=\w)|\.(?=\w))+(?:\[loopvar:\s*\w+\]|\$\(\w+\))?(?:\w|-(?=\w)|\.(?=\w))*)(\.|-|:|\)|_?\s)", input, 1)[1].replace('-','_').replace('.','_')
                input = re.split(r"^((?:\w|-(?=\w)|\.(?=\w))+(?:\[loopvar:\s*\w+\]|\$\(\w+\))?(?:\w|-(?=\w)|\.(?=\w))*)(\.|-|:|\)|_?\s)", input, 1)[-1]
                input = cleanInput(input)

                #Add a q if the first character is a digit
                if label[0].isdigit():
                    label = "Q" + label
                title = input
                # compose our new radio question
                printPage = """<radio label="{0}" uses="qarts.{2}" qa:tool="rp" qa:atype="logo">
  <title>{1}</title>
  <comment>Select one</comment>
  <row label="r1" qa:image="/s/local/bmr/iQArtsMan.png">Male</row>
  <row label="r2" qa:image="/s/local/bmr/iQArtsWoman.png">Female</row>
</radio>
<suspend/>""".format(label.strip(), title.strip(), qartsV)

                self.view.replace(edit,sel, printPage)
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)


######################## QARTS TOOLS #########################

class qartsQuestion(sublime_plugin.TextCommand):
    ToolS = ("dd","df","gm","ls","rp","rr","sm")
    QuestionS = (
        ("radio",),#dd
        ("radio","number","float"),#df
        ("radio","checkbox"),#gm
        ("number","float"),#ls
        ("radio","checkbox","text","number","float"),#rp
        ("select",),#rr
        ("radio","checkbox")#sm
    )
    AtypeS = (
        (("image_image","image_text","text_image","text_text"),), #dd
        (
            ("image_end-image","image_text","image_zone-image","image_zone-text","text_end-image","text_text","text_zone-image","text_zone-text"), #df radio
            ("image_range-text","text_range-text"), #df number
            ("image_range-text","text_range-text") #df float
        ),
        (
            ("image_number","slider-image_image","slider-image_text","slider-text_image","slider-text_text","stars_image","stars_text","text_image","text_number","text_text","double-ended-slider"),#gm radio
            ("image_number","text_image","text_number","text_text") #gm checkbox
        ),
        (
            ("image","text"),#ls number
            ("image","text")#ls float
        ),
        (
            ("ad-l","ad-l_grouped","ad-p","ad-p_grouped","grouped","icon","icon_grouped","logo","logo_grouped","product","product_grouped","scale-icon","scale-text","text"), #rp radio
            ("ad-l","ad-l_grouped","ad-p","ad-p_grouped","grouped","icon","icon_grouped","logo","logo_grouped","product","product_grouped","text"), #rp checkbox
            ("oe-short","oe-short-icon","oe-short-logo","oe-short-product","oe-long"), #rp text
            ("oe-short","oe-short-icon","oe-short-logo","oe-short-product"), #rp number
            ("oe-short",)#rp float
        ),
        (("image","text"),),#rr
        (
            ("ad-l_ad-l","ad-l_ad-p","ad-l_icon","ad-l_logo","ad-l_product","ad-l_scale-icon","ad-l_scale-text","ad-l_text","ad-p_ad-l","ad-p_ad-p","ad-p_icon","ad-p_logo","ad-p_product","ad-p_scale-icon","ad-p_scale-text","ad-p_text","icon_ad-l","icon_ad-p","icon_icon","icon_logo","icon_product","icon_scale-icon","icon_scale-text","icon_text","logo_ad-l","logo_ad-p","logo_icon","logo_logo","logo_product","logo_scale-icon","logo_scale-text","logo_text","product_ad-l","product_ad-p","product_icon","product_logo","product_product","product_scale-icon","product_scale-text","product_text","text_ad-l","text_ad-p","text_icon","text_logo","text_product","text_scale-icon","text_scale-text","text_text"), #sm radio
            ("ad-l_ad-l","ad-l_ad-p","ad-l_icon","ad-l_logo","ad-l_product","ad-l_text","ad-p_ad-l","ad-p_ad-p","ad-p_icon","ad-p_logo","ad-p_product","ad-p_text","icon_ad-l","icon_ad-p","icon_icon","icon_logo","icon_product","icon_text","logo_ad-l","logo_ad-p","logo_icon","logo_logo","logo_product","logo_text","product_ad-l","product_ad-p","product_icon","product_logo","product_product","product_text","text_ad-l","text_ad-p","text_icon","text_logo","text_product","text_text") #sm checkbox
        )
    )

    def is_visible(self):
        return bool( self.view.settings().get('surveyQarts') or self.view.settings().get('useQarts')  or True) and not bool( self.view.settings().get('trainingMode') )

    def run(self, edit, qaTool):
        self.qatool = qaTool
        self.qntype = ""
        self.atype = ""
        try:
        # choose question type
            self.toolIdx = self.ToolS.index(self.qatool)
            if len(self.QuestionS[self.toolIdx]) > 1:
                self.view.show_popup_menu(
                    self.QuestionS[self.toolIdx],
                    self.chooseAtype
                )
            else:
                self.chooseAtype(0)
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)

    def chooseAtype(self, index):
        try:
        # handle question type and choose qa:atype
            self.qnIdx = index
            if self.qnIdx == -1:
                return 

            self.qntype = self.QuestionS[self.toolIdx][self.qnIdx]
            if len(self.AtypeS[self.toolIdx][self.qnIdx]) > 1:
                self.view.show_popup_menu(
                    self.AtypeS[self.toolIdx][self.qnIdx],
                    self.makeQuestion
                )
            else:
                self.makeQuestion(0)

        except Exception as e:
            print (e)

    def makeQuestion(self, index):
        try:
        # handle qa:atype and make question
            self.atypeIdx = index
            if self.atypeIdx == -1:
                return 

            self.atype = self.AtypeS[self.toolIdx][self.qnIdx][self.atypeIdx]
            
            qartsV = self.view.settings().get('qartsVersion')
            sels = self.view.sel()
            input = ''
            for sel in sels:
    
                questionAttr = ""
                qartsAttr = ['uses="qarts.{0}"'.format(qartsV), 'qa:tool="{0}"'.format(self.qatool), 'qa:atype="{0}"'.format(self.atype)]
                comment = ""
                error = ""

                printPage = ''
                input = self.view.substr(sel).strip()

                input = re.sub("\t+", " ", input)
                input = re.sub("\n +\n", "\n\n", input)
                funkyChars = [(chr(133),'...'),(chr(145),"'"),(chr(146),"'"),(chr(147),'"'),(chr(148),'"'),(chr(151),'--')]
                for pair in funkyChars:
                    input = input.replace(pair[0],pair[1])
                input = re.sub("\n{3,}", "\n\n", input)
                if re.compile('&(?!(#\d+|lt|gt|amp);)').search(input):
                    input = re.sub('&(?!(#\d+|lt|gt|amp);)', '&amp;', input)

                label = re.split(r"^((?:\w|-(?=\w)|\.(?=\w))+(?:\[loopvar:\s*\w+\]|\$\(\w+\))?(?:\w|-(?=\w)|\.(?=\w))*)(\.|-|:|\)|_?\s)", input, 1)[1].replace('-','_').replace('.','_')
                input = re.split(r"^((?:\w|-(?=\w)|\.(?=\w))+(?:\[loopvar:\s*\w+\]|\$\(\w+\))?(?:\w|-(?=\w)|\.(?=\w))*)(\.|-|:|\)|_?\s)", input, 1)[-1]

                #Add a q if the first character is a digit
                if label[0].isdigit():
                    label = "Q" + label

            #isolate the title
                if re.compile('(.|\n)*?(@|<(row|col|choice|comment|group|noanswer|net|exec|virtual|validate|onLoad|style|insert))').search(input):
                    title = re.compile('((.|\n)*?)(?=@|<(row|col|choice|comment|group|noanswer|net|exec|virtual|validate|onLoad|style|insert))').search(input).group(1)
                else:
                    title = input

                output = input.replace(title, "").strip()

                rowCount = output.count('<row')
                if 'rows=True' in output:
                    rowCount += 5
                colCount = output.count('<col')
                if 'cols=True' in output:
                    colCount += 5

            # question attributes
                if self.qntype == 'checkbox':
                    questionAttr = ' atleast="1"'
                elif self.qntype == 'select':
                    questionAttr = ' optional="0"'
                elif self.qntype == 'text':
                    questionAttr = ' size="40" optional="0"'
                elif self.qntype == 'number':
                    questionAttr = ' size="3" optional="0" verify="range(0,9999)"'
                elif self.qntype == 'float':
                    questionAttr = ' size="3" optional="0" range="100"'

            # submessages
                if self.qntype == 'radio' and self.qatool == 'rp':
                    comment = '  <comment>Select one</comment>\n'
                elif self.qntype == 'checkbox' and self.qatool == 'rp':
                    comment = '  <comment>Select all that apply</comment>\n'
                elif self.qatool == 'rr':
                    comment = '  <comment>Please click on the choices in order of your preference</comment>\n'
                elif self.qatool == 'ls':
                    comment = '  <comment>Please move each slider to indicate what proportion of the total you would allocate to each option</comment>\n'
                elif self.qatool == 'df':
                    comment = '  <comment>Please drag each option and place on the range below</comment>\n'
                elif self.qatool == 'dd':
                    comment = '  <comment>Please drag and drop each option onto one of the choices below</comment>\n'
                elif 'slider' in self.atype:
                    if rowCount > 1:
                        comment = '  <comment>Drag each slider to answer the question</comment>\n'
                    else:
                        comment = '  <comment>Drag the slider to answer the question</comment>\n'
                elif self.qntype == 'radio':
                    comment = '  <comment>Select one for each</comment>\n'
                elif self.qntype == 'checkbox':
                    comment = '  <comment>Select all that apply for each</comment>\n'

            # errors
                if self.qatool in self.ToolS[3:-1] and colCount:
                    error = "<!-- QArts Error: This tool requires only rows. -->\n"
                elif self.qatool in ['sm','dd'] and not (rowCount and colCount):
                    error = "<!-- QArts Error: This tool requires rows and columns. -->\n"
                elif self.qatool == 'gm':
                    if not 'slider' in self.atype and not (rowCount and colCount):
                        error = "<!-- QArts Error: This tool requires rows and columns. -->\n"
                    elif 'slider' in self.atype and not colCount:
                        error = "<!-- QArts Error: This tool requires columns (rows recommended too, but not necessary). -->\n"
                elif self.qatool == 'dd' and colCount > 6:
                    error = "<!-- QArts Error: This tool allows up to 6 columns. -->\n"
                elif self.qatool == 'df':
                    if 'range' in self.atype and colCount > 1:
                        error = "<!-- QArts Error: This tool requires exactly one column. -->\n"
                    elif not 'range' in self.atype and not (rowCount and colCount):
                        error = "<!-- QArts Error: This tool requires rows and columns. -->\n"

            # qa:attributes
                if self.qatool == 'rp':
                    if self.qntype == 'text':
                        if self.atype == 'oe-long':
                            qartsAttr.append('qa:rplaceholder="Please be specific"')
                        else:
                            qartsAttr.append('qa:rplaceholder="Please type in"')
                    elif self.qntype == 'number':
                        qartsAttr.append('qa:rplaceholder="Enter a whole number"')
                        qartsAttr.append('qa:rinputnumeric="1"')
                    elif self.qntype == 'float':
                        qartsAttr.append('qa:rplaceholder="Enter a number"')
                        qartsAttr.append('qa:rinputnumeric="1"')
                elif self.qatool == 'gm' and 'slider' in self.atype:
                    qartsAttr.append('qa:sldrsnap="1"')
                    if self.atype != 'double-ended-slider':
                        qartsAttr.append('qa:sldrinitpercent="0"')
                elif self.qatool == 'rr':
                    questionAttr += ' minRanks="{0}"'.format(str(rowCount))
                    qartsAttr.append('qa:captype="hard"')
                    qartsAttr.append('qa:capvalue="{0}"'.format(str(rowCount)))
                elif self.qatool == 'ls':
                    if self.qntype == 'number':
                        questionAttr += ' amount="100"'
                        qartsAttr.append('qa:maxpool="100"')
                    qartsAttr.append('qa:enableinput="1"')
                    qartsAttr.append('qa:remainstr="Remaining:"')

            # add images
                imgFind = re.compile('image|icon|logo|product|ad-l|ad-p')
                if self.qatool in ['dd','df','gm','sm'] and self.atype != 'double-ended-slider':
                    if imgFind.search(self.atype.split('_')[0]):
                        output = re.sub('((?:<row)(?!.*?(?:qa:image|exclusive|open)).*?)>', '\g<1> qa:image="[rel ]">', output)
                    if imgFind.search(self.atype.split('_')[1]):
                        output = re.sub('((?:<col)(?!.*?(?:qa:image|exclusive|open)).*?)>', '\g<1> qa:image="[rel ]">', output)
                elif imgFind.search(self.atype):
                    output = re.sub('((?:<row)(?!.*?(?:qa:image|exclusive|open)).*?)>', '\g<1> qa:image="[rel ]">', output)

            #inside elements
                addElems = ""
                if self.qatool == 'df' and 'range' in self.atype and colCount == 0:
                    addElems = "\n  <col label=\"c1\" />"
                if self.qatool == 'rr' and not "<choice" in input:
                    for chNum in range (1,int(rowCount+1)):
                        addElems += "\n  <choice label=\"ch{0}\">{0}</choice>".format(str(chNum))

                printPage = "<{tag} label=\"{lbl}\"{attrs}\n{qaAttrs}\n>\n{errs}  <title>{title}</title>\n{comment}  {content}{additionalEls}\n</{tag}>\n<suspend/>".format(tag=self.qntype, lbl=label.strip(), attrs=questionAttr, qaAttrs='\n'.join(qartsAttr), title=title.strip(), comment=comment, content=output, additionalEls=addElems, errs=error)
            
                with Edit(self.view) as edit:
                    edit.replace(sel, printPage)
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)

# converts elements to JSON for Qarts builder
class createDesignForUi(sublime_plugin.TextCommand):
    def is_visible(self):
        return bool( self.view.settings().get('surveyQarts') or self.view.settings().get('useQarts') or True) and not bool( self.view.settings().get('trainingMode') )

    def run (self,edit):
        try:
            sels = self.view.sel()
            input = ''

            def parExtractor(inputRead,par):
                try:
                    tempData = inputRead.split(par)[1].split("=")[1].strip()
                    separator = tempData[0]
                    return tempData.split(separator)[1]
                except:
                    return ""

            for sel in sels:
                printPage = ''
                input = self.view.substr(sel)

                input = cleanInput(input)
                input = input.strip().split("\n")
                
                uiTypeRows = ""

                hasRows = False
                inputRows = []
                subsRows = []
                descRows = []
                imgRows = []
                exRows = []
                mdInpRows = []
                grpsRows = []
                grpsSubRows = []

                hasCols = False
                inputCols = []
                subsCols = []
                descCols = []
                imgCols = []
                exCols = []
                mdInpCols = []
                grpsCols = []
                grpsSubCols = []
                
                isRadio = "true"
                for k in input:
                    if "<checkbox" in k:
                      isRadio = "false"
                    if "qa:tool" in k:
                        if '"rr"' in k or "'rr'" in k:
                            isRadio="false"
                    if "<number" in k or "<text" in k or "<float" in k:
                        uiTypeRows='\t\t"uitype":"mdinput",\n'

                for k in input:
                    if "<row" in k:
                        hasRows = True
                        inputRows.append( k.split(">")[1].split("<")[0] )
                        subsRows.append(parExtractor(k,"qa:rsubtitle"))
                        descRows.append(parExtractor(k,"qa:rdescription"))
                        imgRows.append(parExtractor(k,"qa:image"))
                        grpsRows.append(parExtractor(k,"qa:rgrouptitle"))
                        grpsSubRows.append(parExtractor(k,"qa:rgroupsubtitle"))
                        if parExtractor(k,"open")=='1':
                            mdInpRows.append('\n\t\t\"uitype":"mdinput",')
                        else:
                            mdInpRows.append("")

                        if parExtractor(k,"exclusive")=='1':
                            exRows.append('''true,
        "extrasmall": {
            "width": 100
        },
        "small": {
            "width": 100
        },
        "medium": {
            "width": 100
        },
        "large": {
            "width": 100
        },
        "extralarge": {
            "width": 100
        }''')
                        else:
                            exRows.append(isRadio)
                    elif "<col" in k:
                        hasCols = True
                        inputCols.append( k.split(">")[1].split("<")[0] )
                        subsCols.append(parExtractor(k,"qa:csubtitle"))
                        descCols.append(parExtractor(k,"qa:cdescription"))
                        imgCols.append(parExtractor(k,"qa:image"))
                        grpsCols.append(parExtractor(k,"qa:cgrouptitle"))
                        grpsSubCols.append(parExtractor(k,"qa:cgroupsubtitle"))
                        if parExtractor(k,"open")=='1':
                            mdInpCols.append('\n\t\t\"uitype":"mdinput",')
                        else:
                            mdInpCols.append("")

                        if parExtractor(k,"exclusive")=='1':
                            exCols.append('''true,
        "extrasmall": {
            "width": 100
        },
        "small": {
            "width": 100
        },
        "medium": {
            "width": 100
        },
        "large": {
            "width": 100
        },
        "extralarge": {
            "width": 100
        }''')
                        else:
                            exCols.append(isRadio)

                if not hasRows and not hasCols:
                    printPage = "Please create <row> and/or <col> tags"

                if hasRows: 
                    printPage = '"rowArray": [\n'
                    for x in inputRows:
                        printPage += "\t{\n\t\t\"title\": \"%s\"," % (x.strip()) + '\n'
                        printPage += """\t\t\"subtitle\": \"%s\",
%s\t\t\"description\": \"%s\",
\t\t\"grouptitle\": \"%s\",
\t\t\"groupsubtitle\": \"%s\",
\t\t\"image\": \"%s\",%s
\t\t\"isRadio\": %s\n\t},""" % (subsRows[inputRows.index(x)], uiTypeRows, descRows[inputRows.index(x)], grpsRows[inputRows.index(x)], grpsSubRows[inputRows.index(x)], imgRows[inputRows.index(x)],mdInpRows[inputRows.index(x)],exRows[inputRows.index(x)]) + '\n'

                    printPage += "],\n"

                if hasCols:
                    printPage += '"columnArray": [\n'
                    for x in inputCols:
                        printPage += "\t{\n\t\t\"title\": \"%s\"," % (x.strip()) + '\n'
                        printPage += """\t\t\"subtitle\": \"%s\",
\t\t\"description\": \"%s\",
\t\t\"grouptitle\": \"%s\",
\t\t\"groupsubtitle\": \"%s\",
\t\t\"image\": \"%s\",%s
\t\t\"isRadio\": %s\n\t},""" % (subsCols[inputCols.index(x)], descCols[inputCols.index(x)], grpsCols[inputCols.index(x)], grpsSubCols[inputCols.index(x)], imgCols[inputCols.index(x)],mdInpCols[inputCols.index(x)],exCols[inputCols.index(x)]) + '\n'

                    printPage += "],\n"

                self.view.replace(edit,sel, printPage)
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)

################################################################
