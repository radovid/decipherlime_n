import sublime, sublime_plugin, sys, re, webbrowser, time
from xml.dom import minidom
from threading import Timer
try:
    from .Edit import Edit as Edit
except:
    from Edit import Edit as Edit

class showStatusErr(sublime_plugin.TextCommand):
    def run(self, edit, on_command, error_msg):
        try:
            self.view.window().status_message( "{}: {}".format(str(on_command), str(error_msg)) )
        except Exception as e:
            print ("showStatusErr from settings.py failed:")
            print (e)

# listens for different events in the background
class eventChecker(sublime_plugin.ViewEventListener):
    libCommands = ('insert_best_completion', 'run_parser', 'set_survey_type_setting', 'set_hpr_usage', 'make_question', 'make_pipe', 'make_comment', 'make_html_final', 'make_question_elements', 'make_cases', 'make_nets', 'make_nets', 'make_nets', 'relabel_elements', 'swap_elem_types', 'swap_elem_types', 'swap_elem_types', 'new_survey', 'convert_to_delphi', 'clean_up', 'legacy_encoding_save', 'open_decipher_url', 'open_decipher_url', 'text_formatting_tag', 'make_li_per_line', 'make_break', 'text_formatting_tag', 'text_formatting_tag', 'make_hyperlink', 'make_section_title', 'toggle_comment', 'make_hover', 'make_hover_hpr', 'make_popup', 'make_popup_hpr', 'make_responsive_popup', 'insert_style', 'make_style_copy', 'make_stylevar', 'insert_style', 'add_exec', 'add_validate', 'add_virtual', 'make_res', 'make_term', 'add_quota', 'make_question_comment', 'add_condition', 'add_marker', 'add_goto', 'make_note', 'add_define_tag', 'add_insert_tag', 'add_mutator', 'add_oe', 'add_randomize', 'add_exclusive', 'add_optional', 'add_aggregate', 'add_incidence', 'add_alts', 'add_altlabels', 'add_groups', 'add_values', 'add_shuffle_rows', 'add_grouping_cols', 'make_hidden', 'add_translateable', 'add_mls', 'add_custom_attr', 'del_custom_attr', 'make_loopvars', 'make_looprows', 'add_loop_block', 'add_loop', 'add_rand_tag', 'add_block_tag', 'add_block_rnd_children', 'add_hpr_section', 'add_hpr_page', 'add_hpr_container', 'make_virtual_hidden_country', 'make_languages', 'add_datasource', 'add_exec_rows_ord', 'add_samplesource', 'us_states_recode', 'us_states_div_recode', 'make_zip_us', 'make_zip_fr', 'make_zip_de')
    addNewLines = []

    # def on_load(self):
    #     if 'XML' in self.view.settings().get('syntax'):
    #         self.view.settings().set('fileSaved', 0)
    # def on_post_save(self):
    #     if 'XML' in self.view.settings().get('syntax'):
    #         self.view.settings().set('fileSaved', int(time.time()))

    def on_pre_save(self):
        try:
            # auto-parse on save
            if self.view.settings().get('parseAwaiting') and not self.view.settings().get('autoParseUndo') and not self.view.settings().get('trainingMode'):
                self.view.run_command('auto_parse')

            # run cleanUp on save
            if self.view.settings().get('escapeOnSave') and self.view.file_name() and 'XML' in self.view.settings().get('syntax') and not self.view.settings().get('trainingMode'):
                if re.compile('.*survey\.xml$').search(self.view.file_name()):
                    printPage = ''
                    wholeViewReg = sublime.Region(0, self.view.size())
                    input = self.view.substr(wholeViewReg)
                    #replace smart quotes, ellipsis
                    # funkyChars = [('…','...'),('‘','\''),('’','\''),('“','"'),('”','"'),('–','-')]
                    # for pair in funkyChars:
                    #    input = input.replace(pair[0],pair[1])

                    # escape ampersands &
                    amperRe = re.compile('&(?!(#\d+|lt|gt|amp|quot|apos);)')
                    if amperRe.search(input):
                        escCnt = -2
                        for amp in amperRe.finditer(input):
                            ampPt = amp.span()[0]
                            if 'comment.block.xml' not in self.view.scope_name(ampPt) and 'string.unquoted.cdata.xml' not in self.view.scope_name(ampPt) and 'source.js' not in self.view.scope_name(ampPt) and 'source.css' not in self.view.scope_name(ampPt):# and 'string.quoted.double.xml' not in self.view.scope_name(ampPt)
                                escCnt += 1
                                remainingInput = input[ampPt + (escCnt*4):]
                                escapedInput = re.sub('&(?!(#.+|lt|gt|amp|quot|apos);)', '&amp;', remainingInput, 1)
                                input = input.replace(remainingInput, escapedInput)

                    # escapes left bracket [
                    bracketRe = re.compile('\[(?!\s*(?:rel|res|pipe))')
                    if bracketRe.search(input):
                        escCnt = -2
                        for brk in bracketRe.finditer(input):
                            brkPt = brk.span()[0]
                            excScope = self.view.scope_name(brkPt).strip()
                            if 'text.xml' == excScope: # and 'string.unquoted.cdata.xml' not in self.view.scope_name(brkPt) and 'source.js' not in self.view.scope_name(brkPt) and 'source.css' not in self.view.scope_name(brkPt):
                                escCnt += 1
                                remainingInput = input[brkPt + (escCnt*9):]
                                escapedInput = re.sub(bracketRe, '&amp;#91;', remainingInput, 1)
                                input = input.replace(remainingInput, escapedInput)

                    # escape all special characters
                    printPage = input.encode(encoding="ascii", errors="xmlcharrefreplace").decode()

                    with Edit(self.view) as edit:
                        edit.replace(wholeViewReg, printPage)
        except Exception as e:
            self.view.run_command('show_status_err', {"on_command": "eventChecker.on_pre_save", "error_msg": str(e)})
            print (e)

    def statusXmlErr(self, mest, err):
        print ("xml {}: {}".format(mest, err))
        self.view.window().status_message(" {}: {}".format(str(mest).upper(), str(err).upper()))

    def on_post_save(self):
        # check and show errors
        if 'XML' in self.view.settings().get('syntax'):
            try:
                wholeViewReg = sublime.Region(0, self.view.size())
                input = self.view.substr(wholeViewReg).replace(':', 'BmR').replace('@fir_multicol', 'uses="multicol.1"').replace('@fir', 'uses="fir.2"').replace('@ratingscale', 'uses="ratingscale.1"')
                minidom.parseString(input)
                orOnLines = []
                for orOne in self.view.find_all('ond=(".+? or 1"|"1 or .+?")'):
                    if not 'comment.block.xml' in self.view.scope_name(orOne.begin()):
                        orOnLines.append( str(self.view.rowcol(orOne.begin())[0] + 1) )
                if orOnLines and not self.view.settings().get('trainingMode'):
                    if len(orOnLines) > 1:
                        msg = 'cond with "or 1" used on lines '
                        lns = '{} and {}'.format( ", ".join(orOnLines[:-1]), orOnLines[-1] )
                    else:
                        msg = 'cond with "or 1" used on line '
                        lns = orOnLines[0]
                    if self.view.settings().get('errorDialogs'):
                        sublime.message_dialog( 'Warning: %s' %(msg + lns) )
                    t = Timer(1.2, self.statusXmlErr, ['warning', msg + lns])
                    t.start()
            except Exception as e:
                if self.view.settings().get('errorDialogs'):
                    sublime.message_dialog('Syntax error: %s' %e)
                t = Timer(1.0, self.statusXmlErr, ['error', e])
                t.start()

    def on_modified(self):
        try:
            # parseAwaiting and runs auto_parse
            if self.view.settings().get('parseAwaiting') and not self.view.settings().get('trainingMode'):
                command_name = self.view.command_history(0, True)[0]
                if command_name in self.libCommands:
                    if not self.view.settings().get('autoParseUndo'):
                        self.view.run_command('auto_parse')
                    elif self.view.settings().get('autoParseUndo') < 1:
                        self.view.run_command('auto_parse')
            # make autoParse undo-able
            if self.view.command_history(1)[0] == 'auto_parse':
                if self.view.settings().get('autoParseUndo'):
                    self.view.settings().set('autoParseUndo', self.view.settings().get('autoParseUndo') + 1)
                    if self.view.settings().get('autoParseUndo') < 2:
                        self.view.settings().set('parseAwaiting', True)
                else:
                    self.view.settings().set('autoParseUndo', 1)
                    self.view.settings().set('parseAwaiting', True)
        except Exception as e:
            self.view.run_command('show_status_err', {"on_command": "eventChecker.on_modified", "error_msg": str(e)})
            print (e)

    def on_text_command(self, command_name, *args):
        try:
            # auto-parse on F7
            if self.view.settings().get('parseAwaiting') and not self.view.settings().get('trainingMode'):
                if command_name == 'key_to_parse':
                    self.view.settings().set('parseNow', True)
                    self.view.run_command('auto_parse')
            if command_name == 'make_question':
                self.view.settings().erase('autoParseUndo')

            # checks if a new line is needed or should be stripped
            if command_name in self.libCommands:
                self.addNewLines.clear()
                sels = self.view.sel()
                for iv,sel in enumerate(sels):
                    self.addNewLines.append([False,False])
                    selInput = self.view.substr(sel)
                    if selInput:
                        if selInput[0] == '\n' and self.view.line(sel.begin()).begin() != self.view.line(sel.begin()).end():
                            self.addNewLines[iv][0] = True
                        if selInput[-1] == '\n' and self.view.line(sel.end()).begin() != self.view.line(sel.end()).end():
                            self.addNewLines[iv][1] = True
        except Exception as e:
            self.view.run_command('show_status_err', {"on_command": "eventChecker.on_text_command", "error_msg": str(e)})
            print (e)

    def on_post_text_command(self, command_name, *args):
        try:
            # hide popups on enter
            if self.view.is_popup_visible() and command_name == 'return_key':
                self.view.hide_popup()
            # adds new line(s) if needed
            if command_name in self.libCommands:
                sels = self.view.sel()
                for iv,selRes in enumerate(self.addNewLines):
                    if iv >= len(sels):
                        selLines = self.view.substr(sels[iv])
                        if selRes[0]:
                            selLines = '\n' + selLines
                        if selRes[1]:
                            selLines = selLines + '\n'
                        if selRes[0] or selRes[1]:
                            with Edit(self.view) as edit:
                                edit.replace(sels[iv], selLines)

        except Exception as e:
            self.view.run_command('show_status_err', {"on_command": "eventChecker.on_post_text_command", "error_msg": str(e)})
            print (e)



# sets surveyType and autoParse settings on file load
class settingsChecker(sublime_plugin.EventListener):
    def checkAutoParsePref(self, view):
    # set autoParse setting for file
        loadsettings = sublime.load_settings("DecipherLib.sublime-settings")
        if loadsettings.get("auto_parse"):
            view.settings().set('autoParse', True)
        else:
            view.settings().set('autoParse', False)

    def checkEscapeOnSavePref(self, view):
    # set escapeOnSave setting for file
        loadsettings = sublime.load_settings("DecipherLib.sublime-settings")
        if loadsettings.get("escape_on_save"):
            view.settings().set('escapeOnSave', True)
        else:
            view.settings().set('escapeOnSave', False)

    def checkErrorDialogsPref(self, view):
    # set errorDialogs setting for file
        loadsettings = sublime.load_settings("DecipherLib.sublime-settings")
        if loadsettings.get("error_dialogs"):
            view.settings().set('errorDialogs', True)
        else:
            view.settings().set('errorDialogs', False)

    def checkSurvSpecsPref(self, view):
    # set survSpecs setting for file
        loadsettings = sublime.load_settings("DecipherLib.sublime-settings")
        if loadsettings.get("survey_specifics"):
            view.settings().set('survSpecs', True)
        else:
            view.settings().set('survSpecs', False)

    def checkTrainingModePref(self, view):
    # set trainingMode setting for file
        loadsettings = sublime.load_settings("DecipherLib.sublime-settings")
        if loadsettings.get("training_mode"):
            view.settings().set('trainingMode', True)
            view.settings().add_on_change('trainingMode', view.settings().set('trainingMode',))
        else:
            view.settings().set('trainingMode', False)

    def checkParseDefaultCommentsPref(self, view):
    # set parseDefaultComments setting for file
        loadsettings = sublime.load_settings("DecipherLib.sublime-settings")
        if loadsettings.get("parse_default_comments"):
            view.settings().set('parseDefaultComments', True)
        else:
            view.settings().set('parseDefaultComments', False)

    def setSurveyType(self, view):
        # set surveyType
        dirSep = r'[\\/]{1,2}' # re.escape(os.sep)
        fileVars = view.window().extract_variables()
        filePath = fileVars['file_path'] if 'file_path' in fileVars else ''

        surveyTagRegion = view.find('<survey(.|\n)*?>', view.text_point(0,0))
        surveyTagText = view.substr(surveyTagRegion) if surveyTagRegion else ''

        if re.search(r'{0}(1b00|675|3e66|3f8f){0}'.format(dirSep), filePath) or re.search(r'{0}((dyp|dsy)[0-9]{{4,8}})'.format(dirSep), filePath) or 'survey.streamteampanel.com' in surveyTagText or 'company/disneystreamteam' in surveyTagText:
            surveyType = 'dn'
        elif re.search(r'{0}(1344|3878){0}'.format(dirSep), filePath) or re.search(r'{0}(nke[0-9]{{4,8}})'.format(dirSep), filePath) or 'company/niketheme' in surveyTagText:
            surveyType = 'nk'
        elif re.search(r'{0}(a34|db9|53b|1b42){0}'.format(dirSep), filePath) or re.search(r'{0}(nfx[0-9]{{4,8}})'.format(dirSep), filePath) or 'netflixsurveys.com' in surveyTagText:
            surveyType = 'nf'
        elif re.search(r'{0}(9dc|2298|paypal{0}ips){0}'.format(dirSep), filePath) or re.search(r'{0}((ppt|pap|ppp)[0-9]{{4,8}})'.format(dirSep), filePath) or 'paypal-survey.com' in surveyTagText:
            surveyType = 'pp'
        elif re.search(r'{0}(1a07){0}'.format(dirSep), filePath) or re.search(r'{0}(pli[0-9]{{4,8}})'.format(dirSep), filePath):
            surveyType = 'pl'
        elif re.search(r'{0}(68a){0}'.format(dirSep), filePath) or re.search(r'{0}(wal[0-9]{{4,8}})'.format(dirSep), filePath) or 'walmart.decipherinc.com' in surveyTagText or 'customerfeedback.walmart.com' in surveyTagText:
            surveyType = 'wm'
        elif surveyTagText:
            surveyType = 'fv'
        else:
            surveyType = None
        surveyType = 'fv'

        # set surveyType
        if not view.settings().get('surveyTypeAskedForView'):
            view.settings().set('surveyType', surveyType)
        # set surveyHpr
        if not view.settings().get('useHprAsked'):
            view.settings().set('surveyHpr', True if re.search(r'uses=".*?hpr\.\d+.*?(,|")', surveyTagText, re.MULTILINE) else False)
        
        # set Found Comments setting
        if not view.settings().get('foundCommentAskedForView'):
            view.settings().set('setFoundComment', True)

    # check settings on new file
    def on_new(self, view):
        try:
            self.checkAutoParsePref(view)
            self.checkEscapeOnSavePref(view)
            self.checkErrorDialogsPref(view)
            self.checkSurvSpecsPref(view)
            self.checkTrainingModePref(view)
            self.checkParseDefaultCommentsPref(view)
        except Exception as e:
            view.run_command('show_status_err', {"on_command": "settingsChecker.on_new", "error_msg": str(e)})
            print (e)

    # check settings when file loads
    def on_load(self, view):
        try:
            self.checkAutoParsePref(view)
            self.checkEscapeOnSavePref(view)
            self.checkErrorDialogsPref(view)
            self.checkSurvSpecsPref(view)
            self.checkTrainingModePref(view)
            self.checkParseDefaultCommentsPref(view)
        except Exception as e:
            view.run_command('show_status_err', {"on_command": "settingsChecker.on_load", "error_msg": str(e)})
            print (e)

    # check settings before saving file
    def on_pre_save(self, view):
        try:
            if not view.settings().has('escapeOnSave'):
                self.checkEscapeOnSavePref(view)
            if not view.settings().has('errorDialogs'):
                self.checkErrorDialogsPref(view)
        except Exception as e:
            view.run_command('show_status_err', {"on_command": "settingsChecker.on_pre_save", "error_msg": str(e)})
            print (e)

    # check settings when view gets active input
    def on_activated(self, view):
        try:
            if not view.settings().has('autoParse'):
                self.checkAutoParsePref(view)
            if not view.settings().has('parseDefaultComments'):
                self.checkParseDefaultCommentsPref(view)
            self.checkTrainingModePref(view)

            if 'XML' in view.settings().get('syntax'):
                self.setSurveyType(view)
        except Exception as e:
            view.run_command('show_status_err', {"on_command": "settingsChecker.on_pre_save", "error_msg": str(e)})
            print (e)

    # set surveyType setting on newSurvey run
    def on_post_text_command(self, view, command_name, *args):
        try:
            if command_name == 'new_survey':
                if not view.settings().get('surveyTypeAskedForView'):
                    view.settings().erase('surveyType')
                    self.setSurveyType(view)
                self.checkSurvSpecsPref(view)
        except Exception as e:
            view.run_command('show_status_err', {"on_command": "settingsChecker.on_pre_save", "error_msg": str(e)})
            print (e)


# Escape special characters and set ISO-8859-1 encoding
class legacyEncodingSave(sublime_plugin.TextCommand):
    def is_visible(self):
        return not bool( self.view.settings().get('trainingMode') )
    def run(self, edit):
        try:
            if 'XML' in self.view.settings().get('syntax'):
                printPage = ''
                wholeViewReg = sublime.Region(0, self.view.size())
                input = self.view.substr(wholeViewReg)
                #replace smart quotes, ellipsis
                funkyChars = [('…','...'),('‘','\''),('’','\''),('“','"'),('”','"'),('–','-')]
                for pair in funkyChars:
                    input = input.replace(pair[0],pair[1])

                #replace ampersands with entities
                amperRe = re.compile('&(?!(#\d+|lt|gt|amp|quot|apos);)')
                escCnt = -2
                if amperRe.search(input):
                    for amp in amperRe.finditer(input):
                        ampPt = amp.span()[0]
                        if 'comment.block.xml' not in self.view.scope_name(ampPt) and 'string.quoted.double.xml' not in self.view.scope_name(ampPt) and 'string.unquoted.cdata.xml' not in self.view.scope_name(ampPt) and 'source.js' not in self.view.scope_name(ampPt) and 'source.css' not in self.view.scope_name(ampPt):
                            escCnt += 1
                            remainingInput = input[ampPt + (escCnt*5):]
                            escapedInput = re.sub('&(?!(#.+|lt|gt|amp|quot|apos);)', '&amp;', remainingInput, 1)
                            input = input.replace(remainingInput, escapedInput)

                # escapes [
                if re.compile('\[(?!pipe|res|rel|loopvar)(.*?)(\])').search(input):
                    input = re.sub('\[(?!pipe|res|rel|loopvar)(.*?)(\])', '&amp;#91;\g<1>\g<2>', input);

                # escape all special characters
                printPage = input.encode(encoding="ascii",errors="xmlcharrefreplace").decode()

                self.view.replace(edit, wholeViewReg, printPage)

                xmlDeclaration = self.view.find('<\?xml version="([\d\.]+)" encoding="(.+?)"\?>', self.view.text_point(0,0))
                encodingType = self.view.find('encoding="(.+?)"', xmlDeclaration.begin())
                if encodingType:
                    self.view.replace(edit, encodingType, 'encoding="ISO-8859-1"')

                self.view.set_encoding('Western (ISO 8859-1)')

                self.view.run_command('save')
        except Exception as e:
            self.view.run_command('show_status_err', {"on_command": "legacyEncodingSave", "error_msg": str(e)})
            print (e)


# fold xml by tags
class foldByTag(sublime_plugin.TextCommand):
    def run(self, edit):
        try:
            self.view.run_command("expand_selection", {"to": "tag"})
            sels = self.view.sel()
            sel = sels[0]

            self.view.sel().clear()
            self.view.sel().add( sel )

            if not sel.empty():

                start = self.view.substr( sel.begin() )
                end = self.view.substr( sel.end() - 1 )
                tagStart = self.view.word( sel.begin() )
                tagEnd = self.view.word( sel.end() + 1 ).cover( sublime.Region(sel.end()) )

                while ( self.view.line(tagStart).intersects(tagEnd) or ('<' in start and '>' in end) ):
                    self.view.run_command("expand_selection", {"to": "tag"})

                    start = self.view.substr( sels[0].begin() )
                    end = self.view.substr( sels[0].end() - 1 )
                    tagStart = self.view.word( sels[0].begin() )
                    tagEnd = self.view.word( sels[0].end() + 1 ).cover( sublime.Region(sels[0].end()) )


                if '>' in self.view.substr( tagStart ) and '<' in self.view.substr( tagEnd ):
                    lastLine = self.view.line( tagEnd )
                    sels.subtract( sublime.Region( lastLine.begin() - 1, lastLine.end() ) )

                    if self.view.is_folded( sels[0] ):
                        self.view.unfold( sels[0] )
                    else:
                        self.view.fold( sels[0] )
        except Exception as e:
            self.view.run_command('show_status_err', {"on_command": "foldByTag", "error_msg": str(e)})
            print (e)


# check surveyType and show/hide relative links in the menu
class OpenDecipherUrl(sublime_plugin.TextCommand):
    def run(self, edit, url):
        try:
            dirSep = r'[\\/]{1,2}'
            dServer = 'tes'
            fileVars = self.view.window().extract_variables()
            filePath = re.split(dirSep, fileVars['file_path'] if 'file_path' in fileVars else '')
            if 'nke' in filePath[-1] or self.view.settings().get('surveyType') == 'nk':
                dServer = 'nk'
            elif 'nflx' in filePath[-1] or self.view.settings().get('surveyType') == 'nf':
                dServer = 'nflx'
            elif self.view.settings().get('surveyType') == 'wm':
                dServer = 'walmart'
            else:
                dServer = 'tes'
            url = url.replace('tes.', dServer+'.')
            webbrowser.open_new_tab(url)
        except Exception as e:
            self.view.run_command('show_status_err', {"on_command": "OpenDecipherUrl", "error_msg": str(e)})
            print (e)

    def is_visible(self, url):
        surtype = self.view.settings().get('surveyType')
        if 'forstasurveys.zendesk' in url:
            return True
        elif 'decipherinc.' in url:
            return True
        if surtype in ['nk']:
            return 'nk.decipherinc' in url
        elif surtype in ['nf']:
            return 'nflx.decipherinc' in url
        elif surtype in ['wm']:
            return 'walmart.decipherinc' in url
                # elif surtype == 'v2':
        #     return ('surveys.lifepointspanel' in url and not 'qarts' in url)
        # elif surtype == 'fv':
        #     return 'tes.decipherinc' in url
        # elif surtype in ['M3', None]:
        #     return 'bright.decipherinc' in url

