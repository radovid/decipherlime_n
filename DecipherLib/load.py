import sublime, sublime_plugin

default_decipher_package, deipherlib_package = "decipher-sublimetext-clips-master", "DecipherLib"
subl_settings = "Preferences.sublime-settings"

def is_disabled(Package):
    settings = sublime.load_settings(subl_settings)
    return Package in set(settings.get("ignored_packages", []))

def plugin_loaded():
    from package_control import events
    if (events.install(deipherlib_package) or events.post_upgrade(deipherlib_package)) and not is_disabled(default_decipher_package):
        settings = sublime.load_settings(subl_settings)
        ignored = settings.get("ignored_packages", [])
        ignored.append(default_decipher_package)
        settings.set("ignored_packages", ignored)
        sublime.save_settings(subl_settings)

def plugin_unloaded():
    from package_control import events
    if events.remove(deipherlib_package) and is_disabled(default_decipher_package):
        settings = sublime.load_settings(subl_settings)
        ignored = settings.get("ignored_packages", [])
        ignored.remove(default_decipher_package)
        settings.set("ignored_packages", ignored)
        sublime.save_settings(subl_settings)
