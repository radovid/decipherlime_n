DecipherLib has been updated to version `1.29.10`.
- Language tags command improved to include all and use co/lang
- surveyType detections improved and change command back to menu and quick panel
- Many updates to the newSurvey command, Nike almost completed (missing nstyles file)
- goto tag command added
- title tag autocompletion added
- File() and ishuffle() py completions added
- Some changes to the menu and Parse default comments and Ask Survey Specifics preferences updated

---

DecipherLib has been updated to version `1.29.6`.
- Remove Qarts and other old commands from Quick panel and menu
- Add datasource tag commmand
- Forsta exec for keeping rows together
- Fix for [ escapes with loopvars
- Style to change dropdown (select) default text
- Add language tags command and updted to Virtual+Hidden - Country
- Add default complex samplesources
- Small updates for Parser's new surveyTypes and newSurvey templates

---

DecipherLib has been updated to version `1.29.0`.
- Big fix for multiline tags attr autocompletions
- [ escaped on save
- Swap rows-cols + rows-choices merged and can swap between all
- Add translateable to context menu and allowed on res tags
- [FAV- Added Ctrl+Alt+D for Radio, and Ctrl+\ for <note>
OURITE_BRAND] is now a valid Qn and other improvements to Qn labeling
- Updated US zip and states questions for Forsta servers;
- Started adding newSurvey templates for the main Forsta clients (still in early progress).
- ranksort addition to Vicky bot
- Default surveyType is set fv and Qarts parsing switched off (by default);
- Links updated to TES server already;
- Added new link to the logic nodes library;
- Also fixed that all Qns were only using radio attrs;
- Added command for recaptcha with term;
- "Funky" characters like these … ‘’ “ ” – (stylized punctuation) no longer replaced in EscapeOnSave+ others.
- makeHidden now adds translateable=0
- attr completions for terms - altlabels, incidence,  markers, sst, unique
- Tabs auto translated to spaces in DecipherXML

---

If you have any feedback, bugs to report, recommendations, or questions please contact radoslav.videnov@forsta.com.
