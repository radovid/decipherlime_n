import sublime, sublime_plugin, sys, re, os
try:
    from .Edit import Edit as Edit
except:
    from Edit import Edit as Edit


survCompat = '153'
popupHtml = """<body id="decipherlib-popup">
<style>
body {{
    font-size: .8rem;
}}
.header {{
    margin: .25em 0 0;
}}
#item-list {{
    margin: 2px 2px 5px;
    padding: 5px;
    max-width: 300px;
}}
#footer {{
    text-align: center;
    margin: 2px auto 0;
    padding: 7px 7px 1px;
}}
.done {{
    padding: 7px 50px 5px;
    margin-top: 1em;
    text-align: center;
    font-weight: 600;
    border: 1px solid var(--accent);
    border-radius: 5px;
    color: var(--background);
    background-color: var(--foreground);
    text-decoration: none;
}}
.sel-item {{
    padding: .3em;
    text-decoration: none;
    color: var(--foreground);
}}
#hr0 {{
    padding-top: .3em;
    margin-top: .4em;
    border-top: 1px solid #ebdbb299;
}}
#hr {{
    padding-top: .3em;
    margin-top: .4em;
    border-top: 1px solid var(--foreground);
}}
#hr2 {{
    padding-top: .2em;
    margin: .6em 0;
    border-top: 2px solid var(--foreground);
}}
#item-spec {{
    margin: 5px auto;
    padding: 6px 3px 8px 6px;
}}
</style>
<div id="footer">
    <a href="fin" class="done"><b>{1}</b></a>
</div>
<div id="item-list">
    <li id="hr"></li>
    {0}
</div>
</body>
"""

nstylesNK = """*survey.respview.html.footer:
</div>
<!-- /.survey-body -->
</div>
<div class="survey-section footerbottom">[dynamic survey.respview.footer]</div>
</div>
<div id="detectBreakpoint"></div>
<!-- /.survey-container -->
@if gv.hasQA() and gv.showCms() and not gv.isFinished()
</div>
<!-- /#surveyContainer (CMS) -->
@endif
</body>
</html>

"""

nstylesPP = """*respview.client.meta: 
"""


# Dummy command to listen for Enter presses
class returnKey(sublime_plugin.TextCommand):
    def run (self,edit):
        pass



# Convert survey to Delphi
class convertToDelphi(sublime_plugin.TextCommand):
    def run(self, edit):
        try:
            if 'XML' in self.view.settings().get('syntax'):
                printPage = ''
                surveyTagRegion = self.view.find('<survey(.|\n)*?>', self.view.text_point(1,0))
                surveyTagText = self.view.substr(surveyTagRegion) if surveyTagRegion else ''

                surveyTagText = re.sub('\s*autoRecover="\d+"', '', surveyTagText)
                surveyTagText = re.sub('\s*persistentExit="\d+"', '', surveyTagText)
                surveyTagText = re.sub('\s*trackVars="checkbox"', '', surveyTagText)
                surveyTagText = re.sub('indexed=".+"', 'indexed=""', surveyTagText)

                printPage = surveyTagText if 'delphi=' in surveyTagText else re.sub('((\s*)compat="\d+")', '\g<1>\g<2>delphi="1"', surveyTagText)

                self.view.replace(edit, surveyTagRegion, printPage)

        except Exception as e:
            self.view.run_command('show_status_err', {"on_command": "convertToDelphi", "error_msg": str(e)})
            print (e)


######################## NEW SURVEY LIB #########################

class newSurvey(sublime_plugin.TextCommand):
    clientsList = ('Nike', 'Paypal', 'Peerless', 'Disney', 'Walmart', 'Netflix', 'Generic')

    nkItems = (("Type (sample):"), ("sample_1","1 - Panel"), ("sample_2","2 - Client"), ("sample_3","3 - Both"))
    ppItems = (("Type (sample):"), ("sample_1","1 - Client"), ("sample_2","2 - Panel"), ("sample_3","3 - Venmo"))
    countries = (('US','United States'), ('UK','United Kingdom'), ('AU','Australia'), ('BR','Brazil'), ('CA','Canada'), ('CN','China'), ('FR','France'), ('DE','Germany'), ('IN','India'), ('IT','Italy'), ('JP','Japan'), ('ES','Spain'), ('KR','South Korea'), ('-'), ('AL','Albania'), ('AR','Argentina'), ('AM','Armenia'), ('AT','Austria'), ('BH','Bahrain'), ('BD','Bangladesh'), ('BY','Belarus'), ('BE','Belgium'), ('BO','Bolivia'), ('BA','Bosnia and Herzegovina'), ('BW','Botswana'), ('BG','Bulgaria'), ('KH','Cambodia'), ('CM','Cameroon'), ('CL','Chile'), ('CO','Colombia'), ('HR','Croatia'), ('CU','Cuba'), ('CY','Cyprus'), ('CZ','Czech Republic'), ('DK','Denmark'), ('EC','Ecuador'), ('EG','Egypt'), ('EE','Estonia'), ('ET','Ethiopia'), ('FI','Finland'), ('GE','Georgia'), ('GR','Greece'), ('HK','Hong Kong'), ('HU','Hungary'), ('IS','Iceland'), ('ID','Indonesia'), ('IR','Iran'), ('IQ','Iraq'), ('IE','Ireland'), ('IL','Israel'), ('JM','Jamaica'), ('JO','Jordan'), ('KZ','Kazakhstan'), ('KE','Kenya'), ('KW','Kuwait'), ('LA','Lao PDR'), ('LV','Latvia'), ('LT','Lithuania'), ('LU','Luxembourg'), ('MK','North Macedonia'), ('MG','Madagascar'), ('MT','Malta'), ('MX','Mexico'), ('MD','Moldova'), ('MC','Monaco'), ('ME','Montenegro'), ('MA','Morocco'), ('MM','Myanmar'), ('NP','Nepal'), ('NL','Netherlands'), ('NZ','New Zealand'), ('NG','Nigeria'), ('NO','Norway'), ('PK','Pakistan'), ('PG','Papua New Guinea'), ('PY','Paraguay'), ('PE','Peru'), ('PH','Philippines'), ('PL','Poland'), ('PT','Portugal'), ('PR','Puerto Rico'), ('QA','Qatar'), ('RO','Romania'), ('RU','Russia'), ('SA','Saudi Arabia'), ('RS','Serbia'), ('SG','Singapore'), ('SK','Slovakia'), ('SI','Slovenia'), ('ZA','South Africa'), ('SE','Sweden'), ('CH','Switzerland'), ('TW','Taiwan'), ('TH','Thailand'), ('TR','Turkey'), ('UA','Ukraine'), ('AE','United Arab Emirates'), ('UY','Uruguay'), ('VE','Venezuela'), ('VN','Vietnam'), ('YE','Yemen'))
    nkCountries = (('US','USA','1'), ('CA','Canada','101'), ('CN','Greater China','86'), ('UK','U.K.','44'), ('FR','France','33'), ('DE','Germany','49'), ('IT','Italy','39'), ('ES','Spain','34'), ('-'), ('NL','Netherlands','31'), ('PL','Poland','48'), ('AT','Austria','43'), ('HU','Hungary','36'), ('BE','Belgium','32'), ('LU','Luxembourg','352'), ('CH','Switzerland','41'), ('CZ','Czech Republic','420'), ('PO','Portugal','351'), ('IE','Ireland','353'), ('SE','Sweden','46'), ('DK','Denmark','45'), ('NO','Norway','47'), ('FI','Finland','358'), ('IS','Iceland','354'), ('SK','Slovakia','421'), ('SI','Slovenia','386'), ('BG','Bulgaria','359'), ('HR','Croatia','385'), ('CY','Cyprus','357'), ('RS','Serbia','381'), ('RO','Romania','40'), ('RU','Russia','7'), ('IS','Israel','972'), ('GR','Greece','30'), ('TR','Turkey','90'), ('EG','Egypt','20'), ('AE','UAE','971'), ('SA','Saudi Arabia','966'), ('MA','Morocco','212'), ('NG','Nigeria','234'), ('AL','Algeria','213'), ('SA','South Africa','27'), ('MX','Mexico','52'), ('BR','Brazil','55'), ('AR','Argentina','53'), ('PE','Peru','51'), ('CL','Chile','56'), ('CO','Colombia','57'), ('KR','S. Korea','82'), ('JP','Japan','81'), ('PH','The Philippines','63'), ('AU','Australia','61'), ('AU','India','91'), ('IN','Indonesia','62'), ('MY','Malaysia','60'), ('TH','Thailand','66'), ('VN','Vietnam','83'), ('NZ','New Zealand','64'))
    langs = (('english', 'us', 'en', "English"), ('french', 'fr', 'fr', "French"), ('german', 'de', 'de', "German"), ('italian', 'it', 'it', "Italian"), ('portuguese_br', 'br', 'pt_br', "Portuguese (Brazil)"), ('spanish', 'es', 'es', "Spanish"), ('canadian', 'fr_ca', 'fr_ca', "French (Canada)"), ('arabic', 'FIX', 'ar', "Arabic"), ('japanese', 'jp', 'ja', "Japanese"), ('korean', 'kr', 'ko', "Korean"), ('simplifiedchinese', 'cn', 'zh', "Chinese Simplified"), ('uk', 'uk', 'en_gb', "English (UK)"), ('-'), ('afrikaans', 'za', 'af', "Afrikaans"), ('albanian', 'al', 'sq', "Albanian"), ('arabic_egypt', 'eg', 'ar_eg', "Arabic (Egypt)"), ('arabic_iraq', 'iq', 'ar_iq', "Arabic (Iraq)"), ('arabic_jordan', 'jo', 'ar_jo', "Arabic (Jordan"), ('arabic_kuwait', 'kw', 'ar_kw', "Arabic (Kuwait"), ('arabic_lebanon', 'lb', 'ar_lb', "Arabic (Lebanon)"), ('arabic_morocco', 'ma', 'ar_ma', "Arabic (Morocco)"), ('arabic_qatar', 'qa', 'ar_qa', "Arabic (Qatar)"), ('arabic_saudiarabia', 'sa', 'ar_sa', "Arabic (Saudi Arabia)"), ('arabic_uae', 'ae', 'ar_ae', "Arabic (UAE)"), ('assamese', 'in_as', 'as', "Assamese"), ('azerbaijani', 'az', 'az', "Azerbaijani"), ('bengali', 'bd', 'bn', "Bengali"), ('bosnian', 'ba', 'bs', "Bosnian"), ('bulgarian', 'bg', 'bg', "Bulgarian"), ('burmese', 'mm', 'my', "Burmese (Myanmar)"), ('chinese', 'zh_cn', 'zh_HANS', "Chinese"), ('croatian', 'hr', 'hr', "Croatian"), ('czech', 'cz', 'cs', "Czech"), ('danish', 'dk', 'da', "Danish"), ('dutch', 'nl', 'nl', "Dutch"), ('dutch_belgium', 'nl_be', 'nl_be', "Dutch (Belgium)"), ('english.utf8', 'FIX', 'en', "English UTF8"), ('estonian', 'ee', 'et', "Estonian"), ('filipino', 'ph', 'tl', "Filipino"), ('finnish', 'fi', 'fi', "Finnish"), ('french_belgium', 'fr_be', 'fr_be', "French (Belgium)"), ('french_lu', 'fr_lu', 'fr_lu', "French (Luxemborg)"), ('french_ch', 'fr_ch', 'fr_ch', "French (Switzerland)"), ('georgian', 'ge', 'ka', "Georgian"), ('german_austria', 'at', 'de_at', "German (Austria)"), ('german_lu', 'lu', 'de_lu', "German (Luxemborg)"), ('german_ch', 'de_ch', 'de_ch', "German (Switzerland)"), ('greek', 'gr', 'el', "Greek"), ('greek_cyprus', 'cy', 'el_cy', "Greek (Cyprus)"), ('gujarati', 'in_gu', 'gu', "Gujarati"), ('hebrew', 'il', 'he', "Hebrew"), ('hindi', 'in', 'hi', "Hindi"), ('hungarian', 'hu', 'hu', "Hungarian"), ('icelandic', 'is', 'is', "Icelandic"), ('indonesian', 'id', 'id', "Indonesian"), ('italian_ch', 'it_ch', 'it_ch', "Italian (Switzerland)"), ('kannada', 'in_kn', 'kn', "Kannada"), ('kazakh', 'kz', 'kk', "Kazakh"), ('khmer', 'kh', 'km', "Khmer"), ('latvian', 'lv', 'lv', "Latvian"), ('lithuanian', 'lt', 'lt', "Lithuanian"), ('macedonian', 'mk', 'mk', "Macedonian"), ('malay', 'FIX', 'ms', "Malay"), ('malay_sg', 'ms_sg', 'ms_sg', "Malay (Singapore)"), ('malayalam', 'in_ml', 'ml', "Malayalam"), ('maltese', 'mt', 'mt', "Maltese"), ('marathi', 'in_mr', 'mr', "Marathi"), ('mongolian', 'mn', 'mn', "Mongolian"), ('norwegian', 'no', 'no', "Norwegian"), ('oriya', 'in_or', 'or', "Oriya"), ('persian', 'FIX', 'fa', "Persian"), ('polish', 'pl', 'pl', "Polish"), ('portuguese', 'pt', 'pt', "Portuguese"), ('punjabi', 'in_pa', 'pa', "Punjabi"), ('romanian', 'ro', 'ro', "Romanian"), ('russian', 'ru', 'ru', "Russian"), ('russian_belarus', 'by', 'ru_by', "Russian (Belarus)"), ('russian_kazakhstan', 'ru_kz', 'ru_kz', "Russian (Kazakhstan)"), ('samoan', 'ws', 'sm', "Samoan"), ('serbian', 'rs', 'sr', "Serbian"), ('simplifiedchinese_my', 'my', 'zh_my', "Simplified Chinese (Malaysia)"), ('simplifiedchinese_sg', 'sg', 'zh_sg', "Simplified Chinese (Singapore)"), ('slovak', 'sk', 'sk', "Slovak"), ('slovene', 'si', 'sl', "Slovene"), ('slovenian', '`si', 'sl', "Slovenian"), ('spanish_argentina', 'ar', 'es_ar', "Spanish (Argentina)"), ('spanish_brazil', 'es_br', 'es_br', "Spanish (Brazil)"), ('spanish_chile', 'cl', 'es_cl', "Spanish (Chile)"), ('spanish_colombia', 'co', 'es_co', "Spanish (Colombia)"), ('spanish_ecuador', 'ec', 'es_ec', "Spanish (Ecuador)"), ('spanish_eu', 'es_eu', 'es_eu', "Spanish (EU)"), ('spanish_peru', 'pe', 'es_pe', "Spanish (Peru)"), ('spanish_venezuela', 've', 'es_ve', "Spanish (Venezuela)"), ('spanish_costarica', 'cr', 'es_cr', "Spanish (Costa Rica)"), ('spanish_dominicanrepublic', 'do', 'es_do', "Spanish (Dominican Republic)"), ('spanish_latin', 'FIX', 'es_latin', "Spanish (Latin America)"), ('spanish_mexico', 'mx', 'es_mx', "Spanish (Mexico)"), ('spanish_south', 'FIX', 'es_sa', "Spanish (South America)"), ('swahili', 'FIX', 'sw', "Swahili"), ('swedish', 'se', 'sv', "Swedish"), ('tagalog', 'tl_ph', 'tl', "Tagalog"), ('tamil', 'in_ta', 'ta', "Tamil"), ('telugu', 'in_te', 'te', "Telugu"), ('thai', 'th', 'th', "Thai"), ('tongan', 'to', 'to', "Tongan"), ('traditionalchinese', 'hk', 'zh_hk', "Traditional Chinese (HK)"), ('traditionalchinese_tw', 'tw', 'zh_tw', "Traditional Chinese (Taiwan)"), ('turkish', 'tr', 'tr', "Turkish"), ('aus', 'au', 'en_ae', "English (Australia)"), ('english_ae', 'en_ae', 'en_ae', "English (UAE)"), ('english_ca', 'en_ca', 'en_ca', "English (Canada)"), ('english_china', 'en_cn', 'en_cn', "English (China)"), ('english_hk', 'en_hk', 'en_hk', "English (Hong Kong)"), ('english_id', 'en_id', 'en_id', "English (Indonesia)"), ('english_india', 'en_in', 'en_in', "English (India)"), ('english_ireland', 'en_ie', 'en_ie', "English (Ireland)"), ('english_my', 'en_my', 'en_my', "English (Malaysia)"), ('english_nigeria', 'en_ng', 'en_ng', "English (Nigeria)"), ('english_nz', 'en_nz', 'en_nz', "English (New Zealand)"), ('english_sg', 'en_sg', 'en_sg', "English (Singapore)"), ('english_tw', 'en_tw', 'en_tw', "English (Taiwan)"), ('english_za', 'en_za', 'en_za', "English (South Africa)"), ('ukrainian', 'ua', 'uk', "Ukrainian"), ('urdu', 'pk', 'ur', "Urdu (Pakistan)"), ('urdu_india', 'ur_in', 'ur_in', "Urdu (India)"), ('uzbek', 'uz', 'uz', "Uzbek"), ('vietnamese', 'vn', 'vi', "Vietnamese"), ('welsh', 'FIX', 'cy', "Welsh"), ('english_g1', 'FIX', 'en', "English (Generic 1)"), ('english_g2', 'FIX', 'en', "English (Generic 2)"), ('english_g3', 'FIX', 'en', "English (Generic 3)"), ('english_g4', 'FIX', 'en', "English (Generic 4)"), ('english_g5', 'FIX', 'en', "English (Generic 5)"), ('english_g6', 'FIX', 'en', "English (Generic 6)"), ('english_g7', 'FIX', 'en', "English (Generic 7)"), ('english_g8', 'FIX', 'en', "English (Generic 8)"), ('english_g9', 'FIX', 'en', "English (Generic 9)"), ('english_g10', 'FIX', 'en', "English (Generic 10)"))

    sample = ''
    nstylesFile = False

    def run(self, edit):
        self.selected = []
        self.selCountries = []

        self.clientIndex = -1
        self.surveyType = ''
        self.altName = ''

        # show quick panel list with clients
        self.view.window().show_quick_panel(
            self.clientsList,
            self.on_done_type,
            selected_index = 0
        )

    def on_done_type(self, index):
        # handle selected type; show input panel for alt name
        if index == -1:
            # pass
            self.surveyType = self.clientsList[0]
        else:
            self.surveyType = self.clientsList[index]

        self.view.window().show_input_panel(
            'Alt Name:',
            '',
            self.on_done_input,
            on_change=None,
            on_cancel=None
        )

    def on_done_input(self, input=None):
        # handle input for alt name; show input panel for client
        if input in [None,'']:
            self.altName = ''
        self.altName = str(input)
        if self.view.settings().get('survSpecs'):
            self.view.show_popup(self.makeHtml(), 0, -1, 400, 480, self.on_navigate, self.runNewSurvey)
        else:
            self.runNewSurvey()

    def on_navigate(self, href):
        if href == 'fin':
            self.view.hide_popup()
        elif '_' in href:
            survAttr = href.split('_')[0]
            for x in self.selected:
                if survAttr in x:
                    self.selected.remove(x)
            self.selected.append(href)
            survVal = href.split('_')[1]
            if survAttr == 'sample': self.sample = survVal
            elif survAttr == 'projecttype': self.projecttype = survVal
            self.view.update_popup(self.makeHtml())
        elif href in ['nstyles']:
            self.nstylesFile = not self.nstylesFile
        elif href not in self.selCountries:
            self.selCountries.append(href)
        else:
            self.selCountries.remove(href)

        self.view.update_popup(self.makeHtml())

    def makeHtml(self):
        page = popupHtml
        link = """<a href="{0}" class="sel-item">&#160; {1}</a><br>"""
        link_sel = """<a href="{0}" class="sel-item">✓ <b>{1}</b></a><br>"""
        nstyles_link = """<div id="item-spec"><a href="{0}" class="sel-item">{1} <b>↗</b></a></div>"""
        country_link = """<a href="{0}" class="sel-item">{1} ({0})</a><br>"""
        box = "☐ "
        check = "<b>✓ %s</b>"
        content = ""
        try:
            if self.surveyType in ['Nike']:
                for opt in self.nkItems:
                    if not type(opt) is tuple:
                        content += '<h4 class="header">{0}</h4>'.format(opt)
                    elif opt[0] in self.selected:
                        content += link_sel.format(opt[0], opt[1])
                    else:
                        content += link.format(opt[0], opt[1])
                content += '<li id="hr0"></li>'
            if self.surveyType in ['Paypal']:
                for opt in self.ppItems:
                    if not type(opt) is tuple:
                        content += '<h4 class="header">{0}</h4>'.format(opt)
                    elif opt[0] in self.selected:
                        content += link_sel.format(opt[0], opt[1])
                    else:
                        content += link.format(opt[0], opt[1])
                content += '<li id="hr"></li>'
            if self.surveyType in ['Nike','Paypal']:
                if self.nstylesFile:
                    content += nstyles_link.format('nstyles', check %'Load <i>nstyles</i> template')
                else:
                    content += nstyles_link.format('nstyles', box + 'Load <i>nstyles</i> template')
                content += '<li id="hr2"></li>'
            content += '<h4 class="header">Countries:</h4>'
            if self.surveyType == 'Nike':
                for country in self.nkCountries:
                    if country[0] == '-':
                        content += '<li id="hr"></li>'
                    elif country[0] in self.selCountries:
                        content += country_link.format(country[0], check %country[1])
                    else:
                        content += country_link.format(country[0], box + country[1])
            else:
                for country in self.countries:
                    if country[0] == '-':
                        content += '<li id="hr"></li>'
                    elif country[0] in self.selCountries:
                        content += country_link.format(country[0], check %country[1])
                    else:
                        content += country_link.format(country[0], box + country[1])
            html = page.format(content, 'CREATE')
            return html
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)


    def createNstyles(self, client):
        try:
            if client == 'Nike':
                cliNstyles = nstylesNK
            elif client == 'PayPal':
                cliNstyles = nstylesPP
            # Create nstyles file from the template 
            fileVars = self.view.window().extract_variables()
            if 'file_path' in fileVars:
                fpath = fileVars['file_path']
                # Create the nstyles file
                if not os.path.exists(fpath + os.sep + 'nstyles'):
                    with open(fpath + os.sep + 'nstyles', 'w+') as f:
                        f.write(cliNstyles)
            # Ask and open the nstyles in new file
            activeVwIdx = self.view.window().get_view_index(self.view.window().active_view())[1]
            self.view.window().new_file()
            windowViews = self.view.window().views_in_group(self.view.window().active_group())

            for vw in windowViews:
                vwIdx = self.view.window().get_view_index(vw)[1]
                if activeVwIdx == vwIdx - 1:
                    self.view.window().focus_view(vw)
                    vw.set_name('nstyles')
                    with Edit(vw) as edit:
                        edit.insert(0, cliNstyles)
                        break
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)


    def runNewSurvey(self):
        # make survey
        try:
            sels = self.view.sel()
            surveyText = ''
            urlvar = 'co'

            if self.surveyType == 'Nike':
                if self.sample == '1':
                    surveyText = nikePanel
                elif self.sample == '2':
                    surveyText = nikeClient
                else:
                    surveyText = nikeBoth
                if self.nstylesFile:
                    self.createNstyles('Nike')
            elif self.surveyType == 'Paypal':
                if self.sample == '1':
                    surveyText = paypalClient
                elif self.sample == '2':
                    surveyText = paypalPanel
                else:
                    surveyText = venmoSurv
                if self.nstylesFile:
                    self.createNstyles('PayPal')
            elif self.surveyType == 'Peerless':
                surveyText = peerlessSurv
            elif self.surveyType == 'Disney':
                surveyText = disneyFirst
            elif self.surveyType == 'Walmart':
                surveyText = walmartFirst
                urlvar = 'co'
            elif self.surveyType == 'Netflix':
                surveyText = netflixFirst
            else:
                surveyText = FvGeneric

            countryQn = ''
            countryVar = ''
            langTag = ''
            othLang = ''
            if len(self.selCountries) > 1:
                countryVar = """    <var name="{0}" required="1" values="{1}"/>\n""".format(urlvar, ','.join([cyAbr.lower() for cyAbr in self.selCountries]))
                countryRowsV = ''
                countryRowsH = ''
                countryPunchH = ''
                langTag = '  <language name="english" var="co" value="us"/>'
                othLang = ''
                if self.surveyType == 'Nike':
                    for r in self.nkCountries:
                        if r[0] in self.selCountries:
                            print([_[1] for _ in self.nkCountries if _[0] == r[0]])
                            plR = "{0}".format([_[1] for _ in self.nkCountries if _[0] == r[0]][0])
                            nkR = "{0}".format([_[2] for _ in self.nkCountries if _[0] == r[0]][0])
                            countryRowsV += """  <row label="{0}">{1}</row>\n""".format(r[0].lower(), plR)
                            countryRowsH += """  <row label="r{0}" value="{0}">{1}</row>\n""".format(nkR, plR)
                            countryPunchH += """if co.upper() in ['{0}']:
        HV_MARKET.val = HV_MARKET.r{1}.index\n""".format(r[0].upper(), nkR)
                            tLang = [_[0] for _ in self.langs if len(_)>1 and (_[1]==r[0].lower() or ('_' in _[1] and _[1].split('_')[1]==r[0].lower()))]
                            if len(tLang) and tLang[0] != 'english':
                                langTag += """\n  <language name="{0}" var="{1}" value="{2}"/>""".format(tLang[0], urlvar, r[0].lower())
                                othLang += "{1}{0}".format(tLang[0], ',' if othLang else '')
                else:
                    for r in sorted(self.selCountries):
                        plR = "{0}".format([_[1] for _ in self.countries if _[0] == r][0])
                        countryRowsV += """  <row label="{0}">{1}</row>\n""".format(r.lower(), plR)
                        countryRowsH += """  <row label="{0}">{1} ({0})</row>\n""".format(r.upper(), plR)
                        tLang = [_[0] for _ in self.langs if len(_)>1 and (_[1]==r.lower() or ('_' in _[1] and _[1].split('_')[1]==r.lower()))]
                        if len(tLang) and tLang[0] != 'english':
                            langTag += """\n  <language name="{0}" var="{1}" value="{2}"/>""".format(tLang[0], urlvar, r.lower())
                            othLang += "{1}{0}".format(tLang[0], ',' if othLang else '')
                
                langCont = """<languages>
{0}
</languages>

""".format(langTag) if langTag else ""
                if self.surveyType == 'Nike':
                    countryQn = """{3}<radio label="v{2}" title="Country" virtual="bucketize({2}.lower())">
{0}
</radio>

<radio label="HV_MARKET" where="execute" translateable="0" optional="1">
  <title>Hidden: Country</title>
  <exec>
{4}
  </exec>
{1}
</radio>
<suspend/>""".format(countryRowsV.strip('\n'), countryRowsH.strip('\n'), urlvar, langCont, countryPunchH.strip('\n'))
                else:
                     countryQn = """{3}<radio label="v{2}" title="Country" virtual="bucketize({2}.lower())">
{0}
</radio>

<radio label="hCountry" where="execute" translateable="0" optional="1">
  <title>Hidden: Country</title>
  <exec>
try:
    hCountry.val = hCountry.attr({2}.upper()).index
except:
    print "Country code not in list."
  </exec>
{1}
</radio>
<suspend/>""".format(countryRowsV.strip('\n'), countryRowsH.strip('\n'), urlvar, langCont)

            othLangAttr = """\n  otherLanguages=\"{0}\"""".format(othLang) if othLang else ""

            for sel in sels:
                printPage = ''
                inpContent = self.view.substr(sel).strip()

                printPage = surveyText.format(inpContent, alt=self.altName, compat=survCompat, cnryVar=countryVar, cnryQ=countryQn, othLangs=othLangAttr)
                with Edit(self.view) as edit:
                    edit.replace(sel, printPage)

            # Focus view one input / questions begining
            surveyContentReg = self.view.find(inpContent if len(inpContent)>4 else '\n{5,}', 0, flags=1 if len(inpContent)>4 else 0)
            self.view.sel().clear()
            self.view.show(surveyContentReg)

            try:
                self.view.set_syntax_file('Packages/DecipherXML/XML.sublime-syntax')
            except:
                self.view.set_syntax_file('Packages/XML/XML.sublime-syntax')

            if inpContent:
                self.view.sel().add(surveyContentReg)
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)
        # except IndexError:
        #    print ('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))



#############################################################################
#                               FV Generic                                  #
#############################################################################

FvGeneric = '''<?xml version="1.0" encoding="UTF-8"?>
<survey
  alt="{alt}"
  autosave="0"
  browserDupes="safe"
  builderCompatible="0"
  compat="{compat}"
  delphi="1"
  displayOnError="all"
  extraVariables="source,record,decLang,list,userAgent,ipAddress"
  fir="on"
  fullService="1"
  html:showNumber="0"
  mobile="compat"
  mobileDevices="smartphone,tablet,desktop"
  name="Survey"
  newStyle="1"
  secure="1"
  setup="term,quota,time"
  ss:disableBackButton="0"
  ss:enableNavigation="1"
  ss:hideProgressBar="0"
  ss:logoFile=""
  ss:logoPosition=""
  ss:includeLESS=""{othLangs}
  state="testing">

<samplesources default="0">
  <samplesource list="0">
    <title>Open Survey</title>
{cnryVar}    <invalid>You are missing information in the URL. Please verify the URL with the original invite.</invalid>
    <completed>It seems you have already entered this survey.</completed>
    <exit cond="terminated">We appreciate the time you spent but unfortunately the rest of this survey won't be relevant to you based on your answers.</exit>
    <exit cond="qualified">Thank you for taking our survey. Your efforts are greatly appreciated!</exit>
    <exit cond="overquota">Thank you for your participation. This study was extremely popular, so we had to close it before you were able to complete it.</exit>
  </samplesource>
</samplesources>

<exec when="started">
if gv.survey.root.state.live:
    gv.survey.root.styles.ss.enableNavigation = 0
    gv.survey.root.styles.ss.disableBackButton = 1
</exec>

<exec when="init">
def group_rows( question, grouped_rows, doShuffle=True, anchorLast=False ):
    current_order = [row.index for row in question.rows.order]
    new_order = []

    if doShuffle:
        if anchorLast:
            grouped_rows = ishuffle([question.attr(row).index for row in grouped_rows[:-1]]) + [question.attr(grouped_rows[-1]).index]
        else:
            grouped_rows = ishuffle([question.attr(row).index for row in grouped_rows])
    else:
        grouped_rows = [question.attr(row).index for row in grouped_rows]
        
    first_item_index = current_order.index(grouped_rows[0])
    current_order.insert(first_item_index, grouped_rows)

    for row in current_order:
        if row == grouped_rows:
            new_order = new_order + row
        elif row not in grouped_rows:
            new_order.append(row)
    question.rows.order = new_order
</exec>

{cnryQ}



{0}



</survey>
'''



#############################################################################
#                               Nike                                        #
#############################################################################
nikePanel = '''<?xml version="1.0" encoding="UTF-8"?>
<survey 
  alt="{alt}"
  builderCompatible="0"
  compat="{compat}"
  delphi="1"
  displayOnError="all"
  extraVariables="source,record,decLang,list,userAgent"
  fir="on"
  fullService="1"
  html:showNumber="0"
  mobile="compat"
  mobileDevices="smartphone,tablet,desktop"
  name="Survey"
  secure="1"
  setup="term,quota,time"
  ss:disableBackButton="0"
  ss:enableNavigation="1"
  ss:hideProgressBar="0"
  ss:logoFile="selfserve/1344/nike_logo.png"
  ss:logoPosition="left"
  theme="company/niketheme"{othLangs}
  state="testing">

<samplesources default="0">
  <samplesource list="0">
    <title>Open Survey</title>
{cnryVar}    <invalid>You are missing information in the URL. Please verify the URL with the original invite.</invalid>
    <completed>It seems you have already entered this survey.</completed>
    <exit cond="terminated">We appreciate the time you spent but unfortunately the rest of this survey won't be relevant to you based on your answers.</exit>
    <exit cond="qualified">Thank you for taking our survey. Your efforts are greatly appreciated!</exit>
    <exit cond="overquota">Thank you for your participation. This study was extremely popular, so we had to close it before you were able to complete it.</exit>
  </samplesource>
</samplesources>

<note>Only add Nike samplesource list</note>
<style name="respview.client.meta" cond="list in [ '1' ]"><![CDATA[
  <link rel="shortcut icon" href="/survey/selfserve/1344/favicon.ico" type="image/x-icon"/>
]]></style>

<exec when="started">
if gv.survey.root.state.live:
    gv.survey.root.styles.ss.enableNavigation = 0
    gv.survey.root.styles.ss.disableBackButton = 1
</exec>

<exec when="init">
def group_rows( question, grouped_rows, doShuffle=True, anchorLast=False ):
    current_order = [row.index for row in question.rows.order]
    new_order = []

    if doShuffle:
        if anchorLast:
            grouped_rows = ishuffle([question.attr(row).index for row in grouped_rows[:-1]]) + [question.attr(grouped_rows[-1]).index]
        else:
            grouped_rows = ishuffle([question.attr(row).index for row in grouped_rows])
    else:
        grouped_rows = [question.attr(row).index for row in grouped_rows]
        
    first_item_index = current_order.index(grouped_rows[0])
    current_order.insert(first_item_index, grouped_rows)

    for row in current_order:
        if row == grouped_rows:
            new_order = new_order + row
        elif row not in grouped_rows:
            new_order.append(row)
    question.rows.order = new_order
</exec>


{cnryQ}



{0}



</survey>
'''

nikeClient = '''<?xml version="1.0" encoding="UTF-8"?>
<survey 
  alt="{alt}"
  builderCompatible="0"
  compat="{compat}"
  delphi="1"
  displayOnError="all"
  extraVariables="source,record,decLang,list,userAgent"
  fir="on"
  fullService="1"
  html:showNumber="0"
  mobile="compat"
  mobileDevices="smartphone,tablet,desktop"
  name="Survey"
  secure="1"
  setup="term,quota,time"
  ss:disableBackButton="0"
  ss:enableNavigation="1"
  ss:hideProgressBar="0"
  ss:logoFile="selfserve/1344/nike_logo.png"
  ss:logoPosition="left"
  theme="company/niketheme"{othLangs}
  state="testing">

<samplesources default="0">
  <samplesource list="0">
    <title>Open Survey</title>
{cnryVar}    <invalid>You are missing information in the URL. Please verify the URL with the original invite.</invalid>
    <completed>It seems you have already entered this survey.</completed>
    <exit cond="terminated">We appreciate the time you spent but unfortunately the rest of this survey won't be relevant to you based on your answers.</exit>
    <exit cond="qualified">Thank you for taking our survey. Your efforts are greatly appreciated!</exit>
    <exit cond="overquota">Thank you for your participation. This study was extremely popular, so we had to close it before you were able to complete it.</exit>
  </samplesource>
</samplesources>

<note>Only add Nike samplesource list</note>
<style name="respview.client.meta" cond="list in [ '1' ]"><![CDATA[
  <link rel="shortcut icon" href="/survey/selfserve/1344/favicon.ico" type="image/x-icon"/>
]]></style>

<exec when="started">
if gv.survey.root.state.live:
    gv.survey.root.styles.ss.enableNavigation = 0
    gv.survey.root.styles.ss.disableBackButton = 1
</exec>

<exec when="init">
def group_rows( question, grouped_rows, doShuffle=True, anchorLast=False ):
    current_order = [row.index for row in question.rows.order]
    new_order = []

    if doShuffle:
        if anchorLast:
            grouped_rows = ishuffle([question.attr(row).index for row in grouped_rows[:-1]]) + [question.attr(grouped_rows[-1]).index]
        else:
            grouped_rows = ishuffle([question.attr(row).index for row in grouped_rows])
    else:
        grouped_rows = [question.attr(row).index for row in grouped_rows]
        
    first_item_index = current_order.index(grouped_rows[0])
    current_order.insert(first_item_index, grouped_rows)

    for row in current_order:
        if row == grouped_rows:
            new_order = new_order + row
        elif row not in grouped_rows:
            new_order.append(row)
    question.rows.order = new_order
</exec>

{cnryQ}



{0}



</survey>
'''

nikeBoth = '''<?xml version="1.0" encoding="UTF-8"?>
<survey 
  alt="{alt}"
  builderCompatible="0"
  compat="{compat}"
  delphi="1"
  displayOnError="all"
  extraVariables="source,record,decLang,list,userAgent"
  fir="on"
  fullService="0"
  html:showNumber="0"
  mobile="compat"
  mobileDevices="smartphone,tablet,desktop"
  name="Survey"
  secure="1"
  setup="term,quota,time"
  ss:disableBackButton="0"
  ss:enableNavigation="1"
  ss:hideProgressBar="0"
  ss:logoFile="selfserve/1344/nike_logo.png"
  ss:logoPosition="left"
  theme="company/niketheme"{othLangs}
  state="testing">

<samplesources default="0">
  <samplesource list="0">
    <title>Open Survey</title>
    <var name="stype" required="1" values="n,p"/>
{cnryVar}    <invalid>You are missing information in the URL. Please verify the URL with the original invite.</invalid>
    <completed>It seems you have already entered this survey.</completed>
    <exit cond="terminated">We appreciate the time you spent but unfortunately the rest of this survey won't be relevant to you based on your answers.</exit>
    <exit cond="qualified">Thank you for taking our survey. Your efforts are greatly appreciated!</exit>
    <exit cond="overquota">Thank you for your participation. This study was extremely popular, so we had to close it before you were able to complete it.</exit>
  </samplesource>
</samplesources>

<exec when="started">
if gv.survey.root.state.live:
    gv.survey.root.styles.ss.enableNavigation = 0
    gv.survey.root.styles.ss.disableBackButton = 1
</exec>

<style name="respview.client.meta" cond="stype in [ 'n' ]"><![CDATA[ 
  <link rel="shortcut icon" href="/survey/selfserve/1344/favicon.ico" type="image/x-icon"/> 
]]></style>

<exec when="init">
def group_rows( question, grouped_rows, doShuffle=True, anchorLast=False ):
    current_order = [row.index for row in question.rows.order]
    new_order = []

    if doShuffle:
        if anchorLast:
            grouped_rows = ishuffle([question.attr(row).index for row in grouped_rows[:-1]]) + [question.attr(grouped_rows[-1]).index]
        else:
            grouped_rows = ishuffle([question.attr(row).index for row in grouped_rows])
    else:
        grouped_rows = [question.attr(row).index for row in grouped_rows]
        
    first_item_index = current_order.index(grouped_rows[0])
    current_order.insert(first_item_index, grouped_rows)

    for row in current_order:
        if row == grouped_rows:
            new_order = new_order + row
        elif row not in grouped_rows:
            new_order.append(row)
    question.rows.order = new_order
</exec>

<style name='survey.logo'> <![CDATA[ 
\@if gv.inSurvey() and gv.survey.root.styles.ss.logoFile and stype in [ 'n' ] 
 <div class="logo logo-$(gv.survey.root.styles.ss.logoPosition)"> 
    <img src="/survey/$(gv.survey.root.styles.ss.logoFile)" class="logo-image" alt="Logo" /> 
 </div> 
 <!-- /.logo --> 
\@endif 
]]></style> 

<radio label="vstype" virtual="bucketize(stype)"> 
 <title>Bucketie: stype (survey type)</title> 
 <row label="n">Nike Sample</row> 
 <row label="p">Panel Sample</row> 
</radio> 

<note>Add all panel samplesources lists</note>
<style name='survey.logo' cond="list in [ '2','3','4','5','6' ]"> <![CDATA[ 
\@if 0 and gv.inSurvey() and gv.survey.root.styles.ss.logoFile
 <div class="logo logo-$(gv.survey.root.styles.ss.logoPosition)"> 
   <img src="[static]/survey/$(gv.survey.root.styles.ss.logoFile)" class="logo-image" alt="Logo" /> 
 </div> 
\@endif 
]]></style>


{cnryQ}



{0}



</survey>
'''


#############################################################################
#                              Peerless                                     #
#############################################################################
peerlessSurv = '''<?xml version="1.0" encoding="UTF-8"?>
<survey 
  alt="{alt}"
  autosaveKey="source,ID"
  builderCompatible="0"
  compat="153"
  delphi="1"
  displayOnError="all"
  extraVariables="source,record,decLang,list,userAgent"
  fir="on"
  fullService="1"
  html:showNumber="0"
  mobile="compat"
  mobileDevices="smartphone,tablet,desktop"
  name="Survey"
  secure="1"
  setup="term,quota,time"
  ss:disableBackButton="0"
  ss:enableNavigation="1"
  ss:hideProgressBar="0"{othLangs}
  state="testing">

<samplesources default="0">
  <samplesource list="0">
    <title>Open Survey</title>
{cnryVar}    <invalid>You are missing information in the URL. Please verify the URL with the original invite.</invalid>
    <completed>It seems you have already completed this survey.</completed>
    <var name="ID" unique="1"/>
    <exit cond="terminated">Thank you for taking our survey.</exit>
    <exit cond="qualified">Thank you for your participation! Those are all the questions we have for you today. <br /><br />Your responses have been recorded and the survey is complete. <br /><br />You can close this browser window now.</exit>
    <exit cond="overquota">Thank you for taking our survey. Your efforts are greatly appreciated!</exit>
  </samplesource>
</samplesources>

<exec when="started">
if gv.survey.root.state.live:
    gv.survey.root.styles.ss.enableNavigation = 0
    gv.survey.root.styles.ss.disableBackButton = 1
</exec>

<exec when="init">
def group_rows( question, grouped_rows, doShuffle=True, anchorLast=False ):
    current_order = [row.index for row in question.rows.order]
    new_order = []

    if doShuffle:
        if anchorLast:
            grouped_rows = ishuffle([question.attr(row).index for row in grouped_rows[:-1]]) + [question.attr(grouped_rows[-1]).index]
        else:
            grouped_rows = ishuffle([question.attr(row).index for row in grouped_rows])
    else:
        grouped_rows = [question.attr(row).index for row in grouped_rows]
        
    first_item_index = current_order.index(grouped_rows[0])
    current_order.insert(first_item_index, grouped_rows)

    for row in current_order:
        if row == grouped_rows:
            new_order = new_order + row
        elif row not in grouped_rows:
            new_order.append(row)
    question.rows.order = new_order
</exec>

<style name="respview.client.css"><![CDATA[
<style>
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button{{
  -webkit-appearance: none;
  margin: 0;
}}
.instruction-text {{ font-style: italic; }}
.ui-dialog {{
width:800px !important;
max-width: 95%;
max-height: 98%;
overflow-y: auto;
}}
ul li {{ list-style: disc !important; /* or another desired style */ }}
.section {{ margin-bottom: 30px; }}
        
.ui-dialog .ui-dialog-content, .learnMore {{ padding-left: 25px; }}
\@if device.desktop 
    textarea.textarea {{ 
        width: 100%; 
    }}
\@else 
    textarea.textarea {{ 
        width: auto; 
    }}
\@endif 
</style>
]]></style>
<style mode="after" name="respview.client.js" wrap="ready"><![CDATA[
$ ('input:text, textarea').on('cut copy paste', function(e) {{ 
     e.preventDefault(); 
 }});
]]></style>

{cnryQ}





{0}




</survey>
'''


#############################################################################
#                               Paypal                                      #
#############################################################################
paypalClient = '''<?xml version="1.0" encoding="UTF-8"?>
<survey
  alt="{alt}"
  autosave="1"
  autosaveKey="source" 
  builder:cname="www.paypal-survey.com"
  name="Survey"
  compat="{compat}"
  autosave="1"
  extraVariables="source,record,decLang,list,userAgent,stype"
  fir="on"
  autosaveKey="source"
  browserDupes="safe"
  builderCompatible="0"
  fullService="1"
  delphi="1"
  displayOnError="all" 
  mobile="compat" 
  mobileDevices="smartphone,tablet,desktop" 
  secure="1" 
  setup="term,quota,time" 
  ss:disableBackButton="0" 
  ss:enableNavigation="1"
  ss:logoFile="selfserve/9dc/PayPal_Logo_Latest.png"
  ss:logoPosition="left" 
  theme="frozen:company/paypal-rv-theme"
  lists="mail"{othLangs}
  state='testing'>

<samplesources default="1">
  <samplesource list="1" adb="1">
    <title>Open Survey</title>
    <invalid>You are missing information in the URL. Please verify the URL with the original invite.</invalid>
    <completed>It seems you have already entered this survey.</completed>
{cnryVar}    <var name="pp_cust_classification" values="CONSUMER,MERCHANT"/> 
    <exit cond="terminated">Thank you for your interest in taking our survey. Unfortunately, you did not qualify at this time.</exit>
    <exit cond="qualified">Thank you for taking our survey. Your feedback is greatly appreciated!</exit>
    <exit cond="overquota">Thank you for your interest in taking our survey. Unfortunately, you did not qualify at this time.</exit>
  </samplesource>
</samplesources>

<exec> 
#update the list number in the array for PayPal Client sample 
if list in [ "1" ]: 
    setExtra("stype","1") 
</exec> 

<exec when="started">
if gv.survey.root.state.live:
    gv.survey.root.styles.ss.enableNavigation = 0
    gv.survey.root.styles.ss.disableBackButton = 1
</exec>

<exec when="init">
def group_rows( question, grouped_rows, doShuffle=True, anchorLast=False ):
    current_order = [row.index for row in question.rows.order]
    new_order = []

    if doShuffle:
        if anchorLast:
            grouped_rows = ishuffle([question.attr(row).index for row in grouped_rows[:-1]]) + [question.attr(grouped_rows[-1]).index]
        else:
            grouped_rows = ishuffle([question.attr(row).index for row in grouped_rows])
    else:
        grouped_rows = [question.attr(row).index for row in grouped_rows]
        
    first_item_index = current_order.index(grouped_rows[0])
    current_order.insert(first_item_index, grouped_rows)

    for row in current_order:
        if row == grouped_rows:
            new_order = new_order + row
        elif row not in grouped_rows:
            new_order.append(row)
    question.rows.order = new_order
</exec>

<style name="respview.client.js"><![CDATA[  
    <script type="text/javascript"  
src="/survey/selfserve/9dc/paypal_js.js"></script>  
]]></style> 

<style name='survey.logo' cond="list=='1'"> <![CDATA[  
\@if 0 and gv.inSurvey() and gv.survey.root.styles.ss.logoFile  
    <div class="logo logo-$(gv.survey.root.styles.ss.logoPosition)">  
        <img src="[static]/survey/$(gv.survey.root.styles.ss.logoFile)" class="logo-image" alt="Logo" />  
    </div>  
\@endif  
]]></style> 

<style name="survey.respview.footer" cond="list=='1'"><![CDATA[  
    \@if device.desktop  
        <div class="footer"><img src="/survey/selfserve/9dc/PayPal_Logo_Latest.png" class="footerimg"/>${{v2_insertStyle('survey.respview.footer.support')}}</div>  
        \@else  
            <div class="footer">  
            <img src="/survey/selfserve/9dc/PayPal_Logo_Latest.png"/>  
            <br/><br/>  
            <div class="footer">${{v2_insertStyle('survey.respview.footer.support')}}</div>  
  \@endif  
]]></style> 

<style cond="list=='1'" mode="after" name="respview.client.meta"><![CDATA[  
    <link rel="shortcut icon" href="/survey/selfserve/229b/favicon.ico" type="image/x-icon"/>  
]]></style> 


<radio label="vpp_cust_classification" title="pp_cust_classification" virtual="bucketize(pp_cust_classification)">   
   <virtual>   
if pp_cust_classification in [ 'CONSUMER','MERCHANT' ]:   
    bucketize(pp_cust_classification)   
else:   
    vpp_cust_classification.val = vpp_cust_classification.NA.index   
  </virtual>   
  <row label="CONSUMER">CONSUMER</row>   
  <row label="MERCHANT">MERCHANT</row>   
  <row label="NA">NA</row>   
</radio>

{cnryQ}

<text label="SURVEY_LANGUAGE" size="40" optional="0">  
<virtual>  
SURVEY_LANGUAGE.val="english"  
</virtual>  
  <title>SURVEY_LANGUAGE</title>  
</text> 





{0}




<number 
  label="SD1" 
  size="3" 
  optional="0" verify="range(1,120)"> 
  <title>What is your age?</title> 
  <style copy="telImput"   name="el.text"/> 
</number> 
<suspend/> 

<radio 
  label="SD2"> 
  <title>Do you identify as...?</title> 
  <row label="r1">Male</row> 
  <row label="r2">Female</row> 
  <row label="r3">Prefer to describe myself in another way</row> 
</radio> 
<suspend/> 

<checkbox 
  label="vSDCheck" 
  where="execute" translateable="0"> 
  <title>Check Red Herring Questions</title> 
<exec> 
#This is so that we don't get a high number of terms when running SST. 
if gv.isSST(): 
    SD1.val = Q2.val 
    SD2.val = Q1.val 
 
#Update the code below to checked against the age/gender in the screener 
if SD1.val != Q2.val: 
    vSDCheck.r1.val = 1 
if SD2.val != Q1.val: 
    vSDCheck.r2.val = 1 
</exec> 
  <row label="r1">SD1 Flag</row> 
  <row label="r2">SD2 Flag</row> 
</checkbox> 
<suspend/> 


<!--
<pipe label="pQSweeps" capture=""> 
  <case label="r1" cond="co == 'us'">As a token of our appreciation, upon completion of the survey, we'd like to enter you into a sweepstakes for $1000 USD gift card.  For full Official Rules and alternate method of entry, click <a href="https://paypal.com/survey/selfserve/9dc/sweeps/us.html" target="_blank">here</a>.</case> 
  <case label="r2" cond="co == 'uk'">As a token of our appreciation, upon completion of the survey, we'd like to enter you into a sweepstakes for £1000 Visa gift card.  For full Official Rules and alternate method of entry, click <a href="https://paypal.com/survey/selfserve/9dc/sweeps/uk.html" target="_blank">here</a>.</case> 
  <case label="r3" cond="co == 'au'">As a token of our appreciation, upon completion of the survey, we'd like to enter you into a sweepstakes for AUD$1,000 Visa gift card.  For full Official Rules and alternate method of entry, click <a href="https://paypal.com/survey/selfserve/9dc/sweeps/au.html" target="_blank">here</a>.</case> 
  <case label="r4" cond="co == 'ca'">As a token of our appreciation, upon completion of the survey, we'd like to enter you into a sweepstakes for $1 000 CAD gift card.  For full Official Rules and alternate method of entry, click <a href="https://paypal.com/survey/selfserve/9dc/sweeps/ca.html" target="_blank">here</a>.</case> 
  <case label="r5" cond="co == 'fr'">As a token of our appreciation, upon completion of the survey, we'd like to enter you into a sweepstakes for 1 000 Euro USD gift card.  For full Official Rules and alternate method of entry, click <a href="https://paypal.com/survey/selfserve/9dc/sweeps/fr.html" target="_blank">here</a>.</case> 
  <case label="r6" cond="co == 'de'">As a token of our appreciation, upon completion of the survey, we'd like to enter you into a sweepstakes for 1 000 Euro gift card.  For full Official Rules and alternate method of entry, click <a href="https://paypal.com/survey/selfserve/9dc/sweeps/de.html" target="_blank">here</a>.</case> 
<case label="r99" cond="1">UNDEFINED</case> 
</pipe> 
 
<pipe label="pQSweepsr1" capture=""> 
  <case label="r1" cond="co == 'us'">Yes, I would like to enter the sweepstakes for $1000 USD gift card</case> 
  <case label="r2" cond="co == 'uk'">Yes, I would like to enter the sweepstakes for £1000 Visa gift card</case> 
  <case label="r3" cond="co == 'au'">Yes, I would like to enter the sweepstakes for AUD$1,000 Visa gift card</case> 
  <case label="r4" cond="co == 'ca'">Yes, I would like to enter the sweepstakes for $1 000 CAD gift card</case> 
  <case label="r5" cond="co == 'fr'">Yes, I would like to enter the sweepstakes for 1 000 Euro gift card</case> 
  <case label="r6" cond="co == 'de'">Yes, I would like to enter the sweepstakes for 1 000 Euro gift card</case> 
  <case label="r99" cond="1">UNDEFINED</case> 
</pipe> 
 
<pipe label="pQSweepsr2" capture=""> 
  <case label="r1" cond="co == 'us'">No thank you, I do not want to enter the sweepstakes for $1000 USD gift card</case> 
  <case label="r2" cond="co == 'uk'">No thank you, I do not want to enter the sweepstakes for £1000 Visa gift card</case> 
  <case label="r3" cond="co == 'au'">No thank you, I do not want to enter the sweepstakes for AUD$1,000 Visa gift card</case> 
  <case label="r4" cond="co == 'ca'">No thank you, I do not want to enter the sweepstakes for $1 000 CAD gift card</case> 
  <case label="r5" cond="co == 'fr'">No thank you, I do not want to enter the sweepstakes for 1 000 Euro gift card</case> 
  <case label="r6" cond="co == 'de'">No thank you, I do not want to enter the sweepstakes for 1 000 Euro gift card</case> 
  <case label="r99" cond="1">UNDEFINED</case> 
</pipe> 
 
<radio  
  label="QSweeps" 
  cond="<list and country logic goes here>"> 
  <title>[pipe: pQSweeps]</title> 
  <comment>Select one</comment> 
  <alt>As a token of our appreciation, upon completion of the survey, we'd like to enter you into a sweepstakes for $1000 USD gift card.  For full Official Rules and alternate method of entry, click <a href=' https://paypal.com/survey/selfserve/9dc/sweeps/us.html' target='_blank'>here</a>.</alt> 
  <row label="r1" alt="Yes">[pipe: pQSweepsr1]</row> 
  <row label="r2" alt="No">[pipe: pQSweepsr2]</row> 
</radio> 

US: https://paypal.com/survey/selfserve/9dc/sweeps/us.html 
US Venmo: https://paypal.com/survey/selfserve/9dc/sweeps/venmo.html  
UK: https://paypal.com/survey/selfserve/9dc/sweeps/uk.html 
DE: https://paypal.com/survey/selfserve/9dc/sweeps/de.html 
FR: https://paypal.com/survey/selfserve/9dc/sweeps/fr.html 
AU: https://paypal.com/survey/selfserve/9dc/sweeps/au.html 
CA: https://paypal.com/survey/selfserve/9dc/sweeps/ca.html 
-->


</survey>
'''

paypalPanel = '''<?xml version="1.0" encoding="UTF-8"?>
<survey
  alt="{alt}"
  autosave="1"
  autosaveKey="source" 
  name="Survey"
  compat="{compat}"
  autosave="1"
  extraVariables="source,record,decLang,list,userAgent,stype"
  fir="on"
  autosaveKey="source"
  browserDupes="safe"
  builderCompatible="0"
  fullService="1"
  delphi="1"
  displayOnError="all" 
  mobile="compat" 
  mobileDevices="smartphone,tablet,desktop" 
  secure="1" 
  setup="term,quota,time" 
  ss:disableBackButton="0" 
  ss:enableNavigation="1"
  ss:logoFile="selfserve/9dc/PayPal_Logo_Latest.png"
  ss:logoPosition="left" 
  theme="frozen:company/paypal-rv-theme"{othLangs}
  state='testing'>

<samplesources default="1">
  <samplesource list="1">
    <title>Open Survey</title>
    <invalid>You are missing information in the URL. Please verify the URL with the original invite.</invalid>
    <completed>It seems you have already entered this survey.</completed>
{cnryVar}    <exit cond="terminated and hasMarker('qcterm')">Thank you for your interest in taking our survey. Unfortunately, you did not qualify at this time.</exit>
    <exit cond="terminated">Thank you for your interest in taking our survey. Unfortunately, you did not qualify at this time.</exit>
    <exit cond="qualified">Thank you for taking our survey. Your feedback is greatly appreciated!</exit>
    <exit cond="overquota">Thank you for your interest in taking our survey. Unfortunately, you did not qualify at this time.</exit>
  </samplesource>
</samplesources>

<exec> 
#update the list number in the array for Panel sample 
if list in [ "3", "4" ]: 
    setExtra("stype","3")
</exec> 

<exec when="started">
if gv.survey.root.state.live:
    gv.survey.root.styles.ss.enableNavigation = 0
    gv.survey.root.styles.ss.disableBackButton = 1
</exec>

<exec when="init">
def group_rows( question, grouped_rows, doShuffle=True, anchorLast=False ):
    current_order = [row.index for row in question.rows.order]
    new_order = []

    if doShuffle:
        if anchorLast:
            grouped_rows = ishuffle([question.attr(row).index for row in grouped_rows[:-1]]) + [question.attr(grouped_rows[-1]).index]
        else:
            grouped_rows = ishuffle([question.attr(row).index for row in grouped_rows])
    else:
        grouped_rows = [question.attr(row).index for row in grouped_rows]
        
    first_item_index = current_order.index(grouped_rows[0])
    current_order.insert(first_item_index, grouped_rows)

    for row in current_order:
        if row == grouped_rows:
            new_order = new_order + row
        elif row not in grouped_rows:
            new_order.append(row)
    question.rows.order = new_order
</exec>

<style name="respview.client.js"><![CDATA[  
    <script type="text/javascript"  
src="/survey/selfserve/9dc/paypal_js.js"></script>  
]]></style> 

<style name='survey.logo' cond="<list logic goes here>"> <![CDATA[  
\@if 0 and gv.inSurvey() and gv.survey.root.styles.ss.logoFile  
    <div class="logo logo-$(gv.survey.root.styles.ss.logoPosition)">  
        <img src="[static]/survey/$(gv.survey.root.styles.ss.logoFile)" class="logo-image" alt="Logo" />  
    </div>  
\@endif  
]]></style> 

<style cond="0" mode="after" name="respview.client.meta"><![CDATA[  
    <link rel="shortcut icon" href="/survey/selfserve/229b/favicon.ico" type="image/x-icon"/>  
]]></style> 

<res label="privacyText">Privacy Policy</res> 
<res label="helpText">Help</res> 
<style cond="<list logic goes here>" name="survey.respview.footer.support"><![CDATA[ 
<a href="https://legal.forsta.com/legal/privacy-notice/" target="_blank">${{res.privacyText}}</a> - <a href="/support?decLang=${{gv.survey.language}}" target="_blank">${{res.helpText}}</a> 
]]></style> 


{cnryQ}

<text label="SURVEY_LANGUAGE" size="40" optional="0">  
<virtual>  
SURVEY_LANGUAGE.val="english"  
</virtual>  
  <title>SURVEY_LANGUAGE</title>  
</text> 

<block label="Recaptha_block" cond="list!='1'">
  <logic uses="recaptcha.2" label="recaptcha" sst="0"/> 
  <term label="RecaptchaTerm" cond="not recaptcha.human" markers="qcterm" sst="0" incidence="0"> 
</block>




{0}




<number 
  label="SD1" 
  size="3" 
  optional="0" verify="range(1,120)"> 
  <title>What is your age?</title> 
  <style copy="telImput"   name="el.text"/> 
</number> 
<suspend/> 

<radio 
  label="SD2"> 
  <title>Do you identify as...?</title> 
  <row label="r1">Male</row> 
  <row label="r2">Female</row> 
  <row label="r3">Prefer to describe myself in another way</row> 
</radio> 
<suspend/> 

<checkbox 
  label="vSDCheck" 
  where="execute" translateable="0"> 
  <title>Check Red Herring Questions</title> 
<exec> 
#This is so that we don't get a high number of terms when running SST. 
if gv.isSST(): 
    SD1.val = Q2.val 
    SD2.val = Q1.val 
 
#Update the code below to checked against the age/gender in the screener 
if SD1.val != Q2.val: 
    vSDCheck.r1.val = 1 
if SD2.val != Q1.val: 
    vSDCheck.r2.val = 1 
</exec> 
  <row label="r1">SD1 Flag</row> 
  <row label="r2">SD2 Flag</row> 
</checkbox> 
<suspend/> 

<term label="RDTerm" cond="list=='1' and vSDCheck.r1 and vSDCheck.r2" markers="qcterm">Panel: Failed both Red Herring questions</term> 

</survey>
'''

venmoSurv = '''<?xml version="1.0" encoding="UTF-8"?>
<survey
  alt="{alt}"
  autosave="1"
  autosaveKey="source" 
  name="Survey"
  compat="{compat}"
  autosave="1"
  extraVariables="source,record,decLang,list,userAgent,stype"
  fir="on"
  autosaveKey="source"
  browserDupes="safe"
  builderCompatible="0"
  fullService="1"
  delphi="1"
  displayOnError="all" 
  mobile="compat" 
  mobileDevices="smartphone,tablet,desktop" 
  secure="1" 
  setup="term,quota,time" 
  ss:disableBackButton="0" 
  ss:enableNavigation="1"
  ss:logoFile="selfserve/9dc/logoblue.png"
  ss:logoPosition="left" 
  theme="frozen:company/paypal-rv-theme"
  lists="mail"{othLangs}
  state='testing'>

<samplesources default="1">
  <samplesource list="1" adb="1">
    <title>Open Survey</title>
    <invalid>You are missing information in the URL. Please verify the URL with the original invite.</invalid>
    <completed>It seems you have already entered this survey.</completed>
{cnryVar}    <var name="pp_cust_classification" values="CONSUMER,MERCHANT"/> 
    <exit cond="terminated">Thank you for your interest in taking our survey. Unfortunately, you did not qualify at this time.</exit>
    <exit cond="qualified">Thank you for taking our survey. Your feedback is greatly appreciated!</exit>
    <exit cond="overquota">Thank you for your interest in taking our survey. Unfortunately, you did not qualify at this time.</exit>
  </samplesource>
</samplesources>

<exec> 
#update the list number in the array for Venmo Client sample 
if list in [ "2" ]: 
    setExtra("stype","2")
</exec> 

<exec when="started">
if gv.survey.root.state.live:
    gv.survey.root.styles.ss.enableNavigation = 0
    gv.survey.root.styles.ss.disableBackButton = 1
</exec>

<exec when="init">
def group_rows( question, grouped_rows, doShuffle=True, anchorLast=False ):
    current_order = [row.index for row in question.rows.order]
    new_order = []

    if doShuffle:
        if anchorLast:
            grouped_rows = ishuffle([question.attr(row).index for row in grouped_rows[:-1]]) + [question.attr(grouped_rows[-1]).index]
        else:
            grouped_rows = ishuffle([question.attr(row).index for row in grouped_rows])
    else:
        grouped_rows = [question.attr(row).index for row in grouped_rows]
        
    first_item_index = current_order.index(grouped_rows[0])
    current_order.insert(first_item_index, grouped_rows)

    for row in current_order:
        if row == grouped_rows:
            new_order = new_order + row
        elif row not in grouped_rows:
            new_order.append(row)
    question.rows.order = new_order
</exec>

<style name="respview.client.js"><![CDATA[  
    <script type="text/javascript"  
src="/survey/selfserve/9dc/paypal_js.js"></script>  
]]></style> 

<style name='survey.logo' cond="list=='1'"> <![CDATA[ 
    <div class="logo logo-$(gv.survey.root.styles.ss.logoPosition)"> 
        <img src="/survey/selfserve/9dc/logoblue.png" class="logo-image" alt="Logo" /> 
    </div> 
    <!-- /.logo --> 
]]></style> 

<style name="survey.respview.footer" cond="list=='1'"><![CDATA[  
    \@if device.desktop  
        <div class="footer"><img src="/survey/selfserve/9dc/logoblue.png" class="footerimg"/>${{v2_insertStyle('survey.respview.footer.support')}}</div>  
        \@else  
            <div class="footer">  
            <img src="/survey/selfserve/9dc/logoblue.png"/>  
            <br/><br/>  
            <div class="footer">${{v2_insertStyle('survey.respview.footer.support')}}</div>  
  \@endif  
]]></style> 

<style cond="list=='1'" mode="after" name="respview.client.meta"><![CDATA[  
    <link rel="shortcut icon" href="/survey/selfserve/229b/venmo_favicon.ico" type="image/x-icon"/>  
]]></style> 

<res label="privacyText">Privacy Policy</res> 
<res label="helpText">Help</res> 
<style cond="list=='1'" name="survey.respview.footer.support"><![CDATA[ 
<a href="https://venmo.com/legal/us-privacy-policy/" target="_blank">${{res.privacyText}}</a> - <a href="/support?decLang=${{gv.survey.language}}" target="_blank">${{res.helpText}}</a> 
]]></style> 


<radio label="vpp_cust_classification" title="pp_cust_classification" virtual="bucketize(pp_cust_classification)">   
   <virtual>   
if pp_cust_classification in [ 'CONSUMER','MERCHANT' ]:   
    bucketize(pp_cust_classification)   
else:   
    vpp_cust_classification.val = vpp_cust_classification.NA.index   
  </virtual>   
  <row label="CONSUMER">CONSUMER</row>   
  <row label="MERCHANT">MERCHANT</row>   
  <row label="NA">NA</row>   
</radio>

{cnryQ}

<text label="SURVEY_LANGUAGE" size="40" optional="0">  
<virtual>  
SURVEY_LANGUAGE.val="english"  
</virtual>  
  <title>SURVEY_LANGUAGE</title>  
</text> 





{0}




<number 
  label="SD1" 
  size="3" 
  optional="0" verify="range(1,120)"> 
  <title>What is your age?</title> 
  <style copy="telImput"   name="el.text"/> 
</number> 
<suspend/> 

<radio 
  label="SD2"> 
  <title>Do you identify as...?</title> 
  <row label="r1">Male</row> 
  <row label="r2">Female</row> 
  <row label="r3">Prefer to describe myself in another way</row> 
</radio> 
<suspend/> 

<checkbox 
  label="vSDCheck" 
  where="execute" translateable="0"> 
  <title>Check Red Herring Questions</title> 
<exec> 
#This is so that we don't get a high number of terms when running SST. 
if gv.isSST(): 
    SD1.val = Q2.val 
    SD2.val = Q1.val 
 
#Update the code below to checked against the age/gender in the screener 
if SD1.val != Q2.val: 
    vSDCheck.r1.val = 1 
if SD2.val != Q1.val: 
    vSDCheck.r2.val = 1 
</exec> 
  <row label="r1">SD1 Flag</row> 
  <row label="r2">SD2 Flag</row> 
</checkbox> 
<suspend/> 


<!--
<pipe label="pQSweeps" capture=""> 
  <case label="r1" cond="co == 'us'">As a token of our appreciation, upon completion of the survey, we'd like to enter you into a sweepstakes for $1000 USD gift card.  For full Official Rules and alternate method of entry, click <a href="https://paypal.com/survey/selfserve/9dc/sweeps/us.html" target="_blank">here</a>.</case> 
  <case label="r2" cond="co == 'uk'">As a token of our appreciation, upon completion of the survey, we'd like to enter you into a sweepstakes for £1000 Visa gift card.  For full Official Rules and alternate method of entry, click <a href="https://paypal.com/survey/selfserve/9dc/sweeps/uk.html" target="_blank">here</a>.</case> 
  <case label="r3" cond="co == 'au'">As a token of our appreciation, upon completion of the survey, we'd like to enter you into a sweepstakes for AUD$1,000 Visa gift card.  For full Official Rules and alternate method of entry, click <a href="https://paypal.com/survey/selfserve/9dc/sweeps/au.html" target="_blank">here</a>.</case> 
  <case label="r4" cond="co == 'ca'">As a token of our appreciation, upon completion of the survey, we'd like to enter you into a sweepstakes for $1 000 CAD gift card.  For full Official Rules and alternate method of entry, click <a href="https://paypal.com/survey/selfserve/9dc/sweeps/ca.html" target="_blank">here</a>.</case> 
  <case label="r5" cond="co == 'fr'">As a token of our appreciation, upon completion of the survey, we'd like to enter you into a sweepstakes for 1 000 Euro USD gift card.  For full Official Rules and alternate method of entry, click <a href="https://paypal.com/survey/selfserve/9dc/sweeps/fr.html" target="_blank">here</a>.</case> 
  <case label="r6" cond="co == 'de'">As a token of our appreciation, upon completion of the survey, we'd like to enter you into a sweepstakes for 1 000 Euro gift card.  For full Official Rules and alternate method of entry, click <a href="https://paypal.com/survey/selfserve/9dc/sweeps/de.html" target="_blank">here</a>.</case> 
<case label="r99" cond="1">UNDEFINED</case> 
</pipe> 
 
<pipe label="pQSweepsr1" capture=""> 
  <case label="r1" cond="co == 'us'">Yes, I would like to enter the sweepstakes for $1000 USD gift card</case> 
  <case label="r2" cond="co == 'uk'">Yes, I would like to enter the sweepstakes for £1000 Visa gift card</case> 
  <case label="r3" cond="co == 'au'">Yes, I would like to enter the sweepstakes for AUD$1,000 Visa gift card</case> 
  <case label="r4" cond="co == 'ca'">Yes, I would like to enter the sweepstakes for $1 000 CAD gift card</case> 
  <case label="r5" cond="co == 'fr'">Yes, I would like to enter the sweepstakes for 1 000 Euro gift card</case> 
  <case label="r6" cond="co == 'de'">Yes, I would like to enter the sweepstakes for 1 000 Euro gift card</case> 
  <case label="r99" cond="1">UNDEFINED</case> 
</pipe> 
 
<pipe label="pQSweepsr2" capture=""> 
  <case label="r1" cond="co == 'us'">No thank you, I do not want to enter the sweepstakes for $1000 USD gift card</case> 
  <case label="r2" cond="co == 'uk'">No thank you, I do not want to enter the sweepstakes for £1000 Visa gift card</case> 
  <case label="r3" cond="co == 'au'">No thank you, I do not want to enter the sweepstakes for AUD$1,000 Visa gift card</case> 
  <case label="r4" cond="co == 'ca'">No thank you, I do not want to enter the sweepstakes for $1 000 CAD gift card</case> 
  <case label="r5" cond="co == 'fr'">No thank you, I do not want to enter the sweepstakes for 1 000 Euro gift card</case> 
  <case label="r6" cond="co == 'de'">No thank you, I do not want to enter the sweepstakes for 1 000 Euro gift card</case> 
  <case label="r99" cond="1">UNDEFINED</case> 
</pipe> 
 
<radio  
  label="QSweeps" 
  cond="<list and country logic goes here>"> 
  <title>[pipe: pQSweeps]</title> 
  <comment>Select one</comment> 
  <alt>As a token of our appreciation, upon completion of the survey, we'd like to enter you into a sweepstakes for $1000 USD gift card.  For full Official Rules and alternate method of entry, click <a href=' https://paypal.com/survey/selfserve/9dc/sweeps/us.html' target='_blank'>here</a>.</alt> 
  <row label="r1" alt="Yes">[pipe: pQSweepsr1]</row> 
  <row label="r2" alt="No">[pipe: pQSweepsr2]</row> 
</radio> 

US: https://paypal.com/survey/selfserve/9dc/sweeps/us.html 
US Venmo: https://paypal.com/survey/selfserve/9dc/sweeps/venmo.html  
UK: https://paypal.com/survey/selfserve/9dc/sweeps/uk.html 
DE: https://paypal.com/survey/selfserve/9dc/sweeps/de.html 
FR: https://paypal.com/survey/selfserve/9dc/sweeps/fr.html 
AU: https://paypal.com/survey/selfserve/9dc/sweeps/au.html 
CA: https://paypal.com/survey/selfserve/9dc/sweeps/ca.html 
-->


</survey>
'''


#############################################################################
#                               Disney+                                     #
#############################################################################
disneyFirst = '''<?xml version="1.0" encoding="UTF-8"?>
<survey
  alt="{alt}"
  autosave="1" 
  autosaveKey="panelist_hash" 
  builder:cname="survey.streamteampanel.com" 
  browserDupes="safe"
  builderCompatible="0"
  compat="{compat}"
  delphi="1"
  displayOnError="all"
  extraVariables="source,record,decLang,list,userAgent,ipAddress"
  fir="on"
  fullService="1"
  html:showNumber="0"
  mobile="compat"
  mobileDevices="smartphone,tablet,desktop"
  name="Survey"
  newStyle="1"
  secure="1" 
  setup="term,quota,time"
  ss:disableBackButton="0"
  ss:enableNavigation="1"
  ss:hideProgressBar="0"
  ss:logoFile="selfserve/3e66/LA-0015_StreamTeam_Blue-Magenta_US_Logo.png" 
  ss:logoPosition=""
  ss:includeLESS=""
  precision="8"{othLangs}
  theme="company/disneystreamteam" 
  state="testing">


<samplesources default="0">
  <samplesource list="0">
    <title>Open Survey</title>
{cnryVar}    <invalid>You are missing information in the URL. Please verify the URL with the original invite.</invalid>
    <completed>It seems you have already entered this survey.</completed>
    <exit cond="terminated">We appreciate the time you spent but unfortunately the rest of this survey won't be relevant to you based on your answers.</exit>
    <exit cond="qualified">Thank you for taking our survey. Your efforts are greatly appreciated!</exit>
    <exit cond="overquota">Thank you for your participation. This study was extremely popular, so we had to close it before you were able to complete it.</exit>
  </samplesource>
</samplesources>

<exec when="started">
if gv.survey.root.state.live:
    gv.survey.root.styles.ss.enableNavigation = 0
    gv.survey.root.styles.ss.disableBackButton = 1
</exec>

<exec when="init">
def group_rows( question, grouped_rows, doShuffle=True, anchorLast=False ):
    current_order = [row.index for row in question.rows.order]
    new_order = []

    if doShuffle:
        if anchorLast:
            grouped_rows = ishuffle([question.attr(row).index for row in grouped_rows[:-1]]) + [question.attr(grouped_rows[-1]).index]
        else:
            grouped_rows = ishuffle([question.attr(row).index for row in grouped_rows])
    else:
        grouped_rows = [question.attr(row).index for row in grouped_rows]
        
    first_item_index = current_order.index(grouped_rows[0])
    current_order.insert(first_item_index, grouped_rows)

    for row in current_order:
        if row == grouped_rows:
            new_order = new_order + row
        elif row not in grouped_rows:
            new_order.append(row)
    question.rows.order = new_order
</exec>


{cnryQ}



{0}



</survey>
'''


#############################################################################
#                             Netflix                                       #
#############################################################################
netflixFirst = '''
<?xml version="1.0" encoding="UTF-8"?>
<survey 
  alt="{alt}"
  autosaveKey="CID"
  builder:cname="www1.netflixsurveys.com"
  builder:wizardCompleted="1"
  builderCompatible="0"
  cardsort:completionHTML=""
  cardsort:displayCounter="0"
  cardsort:displayNavigation="1"
  cardsort:displayProgress="1"
  cardsort:dragAndDrop="0"
  compat="{compat}"
  delphi="1"
  displayOnError="all"
  extraVariables="source,record,decLang,list,userAgent"
  fir="on"
  html:showNumber="0"
  mobile="compat"
  mobileDevices="smartphone,tablet,desktop"
  name="Survey"
  secure="1"
  setup="term,quota,time"
  ss:disableBackButton="1"
  ss:enableNavigation="1"
  ss:hideProgressBar="0"
  ss:logoAlt="Netflix"
  ss:logoFile="selfserve/a34/logo_2020.png"
  ss:logoPosition="left"
  theme="frozen:company/netflix-white"{othLangs}
  state="testing"


<res label="Text_error">Please enter an answer in the box.</res>
<res label="sys_survey.has-errors">Please make sure that you have followed the instructions below and completely answered the question.</res>
<res label="sys_support"/>
<res label="TITLE">Diamond Heist</res>
<res label="AboutUs">About us</res>
<res label="TemsOfParticipation">Terms of Participation</res>
<res label="TermsOfUse">Terms of Use</res>
<res label="PrivacyStatement">Privacy Statement</res>
<res label="HelpCenter">Help Center</res>
<res label="sys_invited.not">We’re sorry an error has occurred. Please make sure you clicked the right link or copied the URL exactly from the Netflix Preview Club screening invitation.
<br /><br />If you’re still experiencing this issue after retrying, please click “About Us” below to access the Help Center.</res>
<res label="sys_noaccess.title">We’re sorry an error has occurred. Please make sure you clicked the right link or copied the URL exactly from the Netflix Preview Club screening invitation.
<br /><br />If you’re still experiencing this issue after retrying, please click “About Us” below to access the Help Center.</res>
<languages default="english">
  <language name="english" value="en" var="uLANG"/>
  <language name="spanish" value="es" var="uLANG"/>
  <language name="german" value="de" var="uLANG"/>
  <language name="uk" value="en_GB" var="uLANG"/>
  <language name="spanish_eu" value="es_ES" var="uLANG"/>
  <language name="japanese" value="ja" var="uLANG"/>
</languages>

<samplesources default="1">
  <samplesource inuse="Our records show that you are already taking this survey." list="1">
    <title>Open Survey</title>
{cnryVar}    <invalid>You are missing information in the URL. Please verify the URL with the original invite.</invalid>
    <completed>You're all set! Thanks for filling out the survey about <b>${{res.TITLE}}</b>.</completed>
    <var name="uPID" unique="1"/>
    <var name="uHID"/>
    <var name="uSID"/>
    <var name="uA" values="1,2,3,4,5,6,7,8,9,99"/>
    <var name="uCA" values="1,2,3,4,5,6,7,8,9,99"/>
    <var name="uLANG" values="en,es,de,en_GB,es_ES,ja"/>
    <var name="uSTYPE" values="1,2,3,4,5"/>
    <var name="uAVT" values="1,2,3"/>
    <var name="uSMP" values="1,2,3"/>
    <var name="uBT" values="0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,407,408,409,410,569,411,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,162,160,161"/>
    <var name="uTT" values="1,2,3"/>
    <var name="uCO" values="US,ES,GB,DE,MX,JP"/>
    <var name="uG" values="1,2,3,4,99"/>
    <var name="uCG" values="1,2,3,4,99"/>
    <var name="uE"/>
    <var name="uGP"/>
    <var name="uGBTT" values="1,2,3"/>
    <exit cond="terminated and (Q_SCREEN_SPECIFICTECHISSUE.r1 or Q_SCREEN_SPECIFICTECHISSUE.r6 or Q_SCREEN_SPECIFICTECHISSUE.r7)">[pipe: DTEXT_SCREEN_TECHNICALISSUECLOSING_pipe]</exit>
    <exit cond="terminated">Thank you for taking our survey.</exit>
    <exit cond="qualified">Thanks for completing our survey! Your feedback plays an essential role in shaping future movies and shows on Netflix for the whole world to see!
<br /><br />
We appreciate your time as a valued Netflix Preview Club Member. We look forward to hearing from you again soon!</exit>
    <exit cond="overquota">Thank you for taking our survey.</exit>
  </samplesource>
</samplesources>

{cnryQ}




{0}




</survey
'''


