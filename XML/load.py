import sublime, sublime_plugin

xml_package, decipherxml_package = "XML", "DecipherXML"
subl_settings = "Preferences.sublime-settings"


def is_disabled(Package):
    settings = sublime.load_settings(subl_settings)
    return Package in set(settings.get("ignored_packages", []))

def plugin_loaded():
    from package_control import events
    xmlViews = []
    if (events.install(decipherxml_package) or events.post_upgrade(decipherxml_package)) and not is_disabled(xml_package):
        # Find XML views, record them and change to plain text
        for window in sublime.windows():
            for aView in window.views():
                if 'XML' in aView.settings().get('syntax'):
                    xmlViews.append(aView)
                    aView.assign_syntax('Packages/Text/Plain text.tmLanguage')
        # Disable sublime's XML package
        settings = sublime.load_settings(subl_settings)
        ignored = settings.get("ignored_packages", [])
        ignored.append(xml_package)
        settings.set("ignored_packages", ignored)
        sublime.save_settings(subl_settings)
        # Set the prerecorded views to XML (Decipher)
        for aView in xmlViews:
            aView.assign_syntax('Packages/DecipherXML/XML.sublime-syntax')


def plugin_unloaded():
    from package_control import events
    xmlViews = []
    if events.remove(decipherxml_package) and is_disabled(xml_package):
        # Find XML views, record them and change to plain text
        for window in sublime.windows():
            for aView in window.views():
                if 'XML' in aView.settings().get('syntax'):
                    xmlViews.append(aView)
                    aView.assign_syntax('Packages/Text/Plain text.tmLanguage')
        # Enable sublime's XML package
        settings = sublime.load_settings(subl_settings)
        ignored = settings.get("ignored_packages", [])
        ignored.remove(xml_package)
        settings.set("ignored_packages", ignored)
        sublime.save_settings(subl_settings)
        # Set the prerecorded views to XML
        for aView in xmlViews:
            aView.assign_syntax('Packages/XML/XML.sublime-syntax')
