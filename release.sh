#!/bin/bash

cd /home/boycheve/radovid.eu/decipherlime

read -p "Enter 'xml' for DecipherXML or just leave empty for DecipherLib: " chosenPack


if [[ $chosenPack =~ ^[Xx][Mm]?[Ll]?$ ]]; then
    echo "New version of DecipherXML will be released."
    package='DecipherXML'
    dir='XML'
    verPattern='^[0-9]\.[0-9]{1,2}$'
    versionExpl='Please use semantic versioning - MAJOR.MINOR, numbers only: '
else
    package='DecipherLib'
    dir='DecipherLib'
    verPattern='^[0-9]\.[0-9]{1,2}(\.[0-9]{1,2})?$'
    versionExpl='Please use semantic versioning - MAJOR.MINOR.PATCH, numbers only: '

    # read -p "Do you want the new version added to messages.json /to show Recent.md/? (Y/N): " answervar

    # case $answervar in
    #     [Yy])
    #     echo "Changelog/notes from Recent.md will be displayed for the new version."
    #     updateMessages=1
    #     ;;
    #     *)
    #     updateMessages=0
    #     ;;
    # esac

fi


newVersion=""
read -p  "Please specify the version you want to release: " version

if [[ -z $version ]]; then
    echo -e "\e[31mPlease specify a version.\e[39m"
    exit 2
elif [[ ! $version =~ $verPattern ]]; then
    echo -e "Invalid version entered."
    read -p "$versionExpl" version2
    if [[ $version2 =~ $verPattern ]]; then
        newVersion="$version2"
    else
        echo -e "\e[31mInvalid version entered again, killing script.\e[39m"
        exit 3
    fi
else
    newVersion="$version"
fi


# if [[ $updateMessages == 1 ]]; then
#     sed -i "s/}/, \"$newVersion\": \"Recent.md\" }/g" DecipherLib/messages.json
#     git stage DecipherLib/messages.json && git commit -m "Ver. $newVersion added to messages.json"
# fi


zip -r -j ./packages/$package-$newVersion.zip ./$dir -x '*.htaccess*'

./update-json.py $newVersion $package


git stage channel.json packages.json && git commit -m "Updated $package version to $newVersion"
git push origin

