import sublime, sublime_plugin, sys, re, webbrowser

CSS = '''
div.deipher-lib { max-wiidth: 800px; padding: 15px 3px 12px 30px; margin: 0; }
.deipher-lib h1, .deipher-lib h2, .deipher-lib h3,
.deipher-lib h4, .deipher-lib h5 {
    margin: 18px 25px 5px;
    {{'.string'|css}}
}
.deipher-lib blockquote, .deipher-lib h6 { {{'.comment'|css}} }
.deipher-lib h6 { font-size: .9em; }
.deipher-lib a { text-decoration: none; }
'''

frontmatter = {
    "markdown_extensions": [
        "markdown.extensions.admonition",
        "markdown.extensions.attr_list",
        "markdown.extensions.def_list",
        "markdown.extensions.nl2br",
        # Smart quotes always have corner cases that annoy me, so don't bother with them.
        {"markdown.extensions.smarty": {"smart_quotes": False}},
        "pymdownx.betterem",
        {
            "pymdownx.magiclink": {
                "repo_url_shortener": True,
                "repo_url_shorthand": True,
                "user": "radovid",
                "repo": "DecipherLib"
            }
        },
        "pymdownx.extrarawhtml",
        "pymdownx.keys",
        {"pymdownx.escapeall": {"hardbreak": True, "nbsp": True}},
        # Sublime doesn't support superscript, so no ordinal numbers
        {"pymdownx.smartsymbols": {"ordinal_numbers": False}}
    ]
}


class openDoc(sublime_plugin.WindowCommand):
    def on_navigate(self, href):
        # handle links
        webbrowser.open_new_tab(href)

    def run(self, page):
        try:
            import mdpopups
            has_phantom_support = (mdpopups.version() >= (1, 10, 0) or 1) and (int(sublime.version()) >= 3124)
            fmatter = mdpopups.format_frontmatter(frontmatter)
        except Exception:
            fmatter = ''
            has_phantom_support = False

        text = sublime.load_resource(page.replace('${packages}', 'Packages'))
        if text:
            view = self.window.new_file()
            if has_phantom_support:
                mdpopups.add_phantom(
                    view,
                    'changelog' if 'recent' in page else 'quickstart',
                    sublime.Region(0),
                    fmatter + text,
                    sublime.LAYOUT_BLOCK,
                    css=CSS,
                    wrapper_class="deipher-lib",
                    on_navigate=self.on_navigate
                )
            else:
                view.run_command('append', {"characters": text})
        else:
            sublime.run_command('open_file', {"file": page})

        view.set_name('DecipherLib: Recent Changelog' if 'recent' in page else 'DecipherLib: Quick Start')
        view.settings().set('gutter', False)
        view.settings().set('word_wrap', True)
        view.set_read_only(True)
        view.set_scratch(True)


# dialog box for switching autoParse
class setAutoParsePreference(sublime_plugin.ApplicationCommand):
    def is_visible(self):
        return not bool( sublime.active_window().active_view().settings().get('trainingMode') )

    def is_checked(self):
        if sublime.active_window().active_view().settings().has('autoParse'):
            return sublime.active_window().active_view().settings().get('autoParse')
        else:
            return sublime.load_settings("DecipherLib.sublime-settings").get("auto_parse", True)

    def run(self):
        try:
            self.loadsettings = sublime.load_settings("DecipherLib.sublime-settings")
            window = sublime.active_window()

            dialogText = """Enable/Disable Auto-Parser?

    If enabled every new question is auto-parsed after it's made,
    once a command is ran out of it, before save and with F7.

    Changing the "Current Survey Type" setting to None will completely disable parsing (even on F8) for active file."""

            if window.active_view().settings().get('autoParse'):
                yOption = "Disable"
            else:
                yOption = "Enable"

            if self.loadsettings.get("auto_parse", True):
                nOption = "Disable"
            else:
                nOption = "Enable"

            dialogSelected = sublime.yes_no_cancel_dialog(dialogText, "%s for current file" %yOption, "%s globally" %nOption )

            if dialogSelected == 1:
                window.active_view().settings().set('autoParse', not window.active_view().settings().get('autoParse'))

            elif dialogSelected == 2:
                self.loadsettings.set('auto_parse', not self.loadsettings.get("auto_parse", True))

                sublime.save_settings("DecipherLib.sublime-settings")

                self.settingsReload = sublime.load_settings("DecipherLib.sublime-settings")
                for vw in window.views():
                    if self.settingsReload.get("auto_parse", True):
                        vw.settings().set('autoParse', True)
                    else:
                        vw.settings().set('autoParse', False)
        except Exception as e:
            command = self.__class__.__name__
            sublime.active_window().active_view().run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)


# dialog box for switching esapeOnSave
class setEscapeOnSavePreference(sublime_plugin.ApplicationCommand):
    def is_visible(self):
        return not bool( sublime.active_window().active_view().settings().get('trainingMode') )

    def is_checked(self):
        if sublime.active_window().active_view().settings().has('escapeOnSave'):
            return sublime.active_window().active_view().settings().get('escapeOnSave')
        else:
            return sublime.load_settings("DecipherLib.sublime-settings").get("escape_on_save", True)

    def run(self):
        try:
            self.loadsettings = sublime.load_settings("DecipherLib.sublime-settings")
            window = sublime.active_window()

            dialogText = """Enable/Disable Escape-On-Save?

    If enabled special characters and ampersands will be replaced and escaped in XML files on each save."""

            if window.active_view().settings().get('escapeOnSave'):
                yOption = "Disable"
            else:
                yOption = "Enable"

            if self.loadsettings.get("escape_on_save", True):
                nOption = "Disable"
            else:
                nOption = "Enable"

            dialogSelected = sublime.yes_no_cancel_dialog(dialogText, "%s for current file" %yOption, "%s globally" %nOption )

            if dialogSelected == 1:
                window.active_view().settings().set('escapeOnSave', not window.active_view().settings().get('escapeOnSave'))

            elif dialogSelected == 2:
                self.loadsettings.set('escape_on_save', not self.loadsettings.get("escape_on_save", True))

                sublime.save_settings("DecipherLib.sublime-settings")

                self.settingsReload = sublime.load_settings("DecipherLib.sublime-settings")
                for vw in window.views():
                    if self.settingsReload.get("escape_on_save", True):
                        vw.settings().set('escapeOnSave', True)
                    else:
                        vw.settings().set('escapeOnSave', False)
        except Exception as e:
            command = self.__class__.__name__
            sublime.active_window().active_view().run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)


# dialog box for switching errorDialogs
class setErrorDialogsPreference(sublime_plugin.ApplicationCommand):
    def is_visible(self):
        return not bool( sublime.active_window().active_view().settings().get('trainingMode') )

    def is_checked(self):
        if sublime.active_window().active_view().settings().has('errorDialogs'):
            return sublime.active_window().active_view().settings().get('errorDialogs')
        else:
            return sublime.load_settings("DecipherLib.sublime-settings").get("error_dialogs", True)

    def run(self):
        try:
            self.loadsettings = sublime.load_settings("DecipherLib.sublime-settings")
            window = sublime.active_window()

            dialogText = """Enable/Disable Message Dialogs?

    Whether to show XML error messages in a popup on file save.
    (@Macros can sometimes trigger false errors.)"""

            if window.active_view().settings().get('errorDialogs'):
                yOption = "Disable"
            else:
                yOption = "Enable"

            if self.loadsettings.get("error_dialogs", True):
                nOption = "Disable"
            else:
                nOption = "Enable"

            dialogSelected = sublime.yes_no_cancel_dialog(dialogText, "%s for current file" %yOption, "%s globally" %nOption )

            if dialogSelected == 1:
                window.active_view().settings().set('errorDialogs', not window.active_view().settings().get('errorDialogs'))

            elif dialogSelected == 2:
                self.loadsettings.set('error_dialogs', not self.loadsettings.get("error_dialogs", True))

                sublime.save_settings("DecipherLib.sublime-settings")

                self.settingsReload = sublime.load_settings("DecipherLib.sublime-settings")
                for vw in window.views():
                    if self.settingsReload.get("error_dialogs", True):
                        vw.settings().set('errorDialogs', True)
                    else:
                        vw.settings().set('errorDialogs', False)
        except Exception as e:
            command = self.__class__.__name__
            sublime.active_window().active_view().run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)


# dialog box for switching survSpecs
class setSurvSpecsPreference(sublime_plugin.ApplicationCommand):
    def is_visible(self):
        return not bool( sublime.active_window().active_view().settings().get('trainingMode') )

    def is_checked(self):
        return sublime.load_settings("DecipherLib.sublime-settings").get("survey_specifics")

    def run(self):
        try:
            self.loadsettings = sublime.load_settings("DecipherLib.sublime-settings")
            window = sublime.active_window()

            dialogText = """Enable/Disable Kantar variables?

    Whether to show popup for selecting kantar.2 variables for New survey / Make survey Qarts.
    If off default values will be used."""

            if window.active_view().settings().get('survSpecs'):
                yOption = "Disable"
            else:
                yOption = "Enable"

            dialogSelected = sublime.ok_cancel_dialog(dialogText, yOption )

            if dialogSelected:
                self.loadsettings.set('survey_specifics', not self.loadsettings.get("survey_specifics", True))

                sublime.save_settings("DecipherLib.sublime-settings")

                self.settingsReload = sublime.load_settings("DecipherLib.sublime-settings")
                for vw in window.views():
                    if self.settingsReload.get("survey_specifics", True):
                        vw.settings().set('survSpecs', True)
                    else:
                        vw.settings().set('survSpecs', False)
        except Exception as e:
            command = self.__class__.__name__
            sublime.active_window().active_view().run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)


# preference for setting default comment when parsing
class setParseDefaultCommentsPreference(sublime_plugin.ApplicationCommand):
    def is_visible(self):
        return not bool( sublime.active_window().active_view().settings().get('trainingMode') )

    def is_checked(self):
        if sublime.active_window().active_view().settings().has('parseDefaultComments'):
            return sublime.active_window().active_view().settings().get('parseDefaultComments')
        else:
            return sublime.load_settings("DecipherLib.sublime-settings").get("parseDefaultComments", True)

    def run(self):
        try:
            self.loadsettings = sublime.load_settings("DecipherLib.sublime-settings")
            window = sublime.active_window()

            dialogText = """Enable/Disable Default Comments?

    Whether to set default comments (submessages/instructions) when parsing.
    (E.g., set "Select one" for radio questions)"""

            if window.active_view().settings().get('parseDefaultComments'):
                yOption = "Disable"
            else:
                yOption = "Enable"

            if self.loadsettings.get("parse_default_comments", True):
                nOption = "Disable"
            else:
                nOption = "Enable"

            dialogSelected = sublime.yes_no_cancel_dialog(dialogText, "%s for current file" %yOption, "%s globally" %nOption )

            if dialogSelected == 1:
                window.active_view().settings().set('parseDefaultComments', not window.active_view().settings().get('parseDefaultComments'))

            elif dialogSelected == 2:
                self.loadsettings.set('parse_default_comments', not self.loadsettings.get("parse_default_comments", True))

                sublime.save_settings("DecipherLib.sublime-settings")

                self.settingsReload = sublime.load_settings("DecipherLib.sublime-settings")
                for vw in window.views():
                    if self.settingsReload.get("parse_default_comments", True):
                        vw.settings().set('parseDefaultComments', True)
                    else:
                        vw.settings().set('parseDefaultComments', False)
        except Exception as e:
            command = self.__class__.__name__
            sublime.active_window().active_view().run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)


# set found comments for view
class setFoundCommentsPreference(sublime_plugin.ApplicationCommand):
    def is_visible(self):
        return not bool( sublime.active_window().active_view().settings().get('trainingMode') )

    def is_checked(self):
        return bool(sublime.active_window().active_view().settings().get('setFoundComment'))

    def run(self):
        try:
            window = sublime.active_window()

            dialogText = "Auto-set instruction texts found in questions texts (titles) as comment for the active file?"

            if window.active_view().settings().get('setFoundComment'):
                yOption = "Disable"
            else:
                yOption = "Enable"

            dialogSelected = sublime.ok_cancel_dialog(dialogText, yOption )

            if dialogSelected:
                window.active_view().settings().set('setFoundComment', not window.active_view().settings().get('setFoundComment'))

            window.active_view().settings().set('foundCommentAskedForView', True)
        except Exception as e:
            command = self.__class__.__name__
            sublime.active_window().active_view().run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)


# start/stop using HPR
class setHprUsage(sublime_plugin.ApplicationCommand):
    def is_visible(self):
        return bool(sublime.active_window().active_view().settings().get('surveyHpr') or sublime.active_window().active_view().settings().get('surveyType') in ['AG', 'M3'] or sublime.active_window().active_view().settings().get('useHpr'))
    def is_checked(self):
        return bool(sublime.active_window().active_view().settings().get('useHpr'))

    def run(self):
        try:
            window = sublime.active_window()

            dialogText = "Turn HPR usage On/Off for the current file?\n New elements will be created and parsed in HPR-mode when ON. It tries to follow HPR specifics and auto-put additional attributes."

            dialogSelected = sublime.yes_no_cancel_dialog(dialogText, "On", "Off")

            if dialogSelected == 1:
                window.active_view().settings().set('useHpr', True)
                window.active_view().settings().set('oldUseQarts', window.active_view().settings().get('useQarts'))
                window.active_view().settings().set('useQarts', False)
            elif dialogSelected == 2:
                window.active_view().settings().set('useHpr', False)
                window.active_view().settings().set('useQarts', window.active_view().settings().get('oldUseQarts'))
                window.active_view().settings().erase('oldUseQarts')
            window.active_view().settings().set('useHprAsked', True if dialogSelected else False)
        except Exception as e:
            command = self.__class__.__name__
            sublime.active_window().active_view().run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)


# set found comments for view
class setTrainingMode(sublime_plugin.ApplicationCommand):
    # def is_visible(self):
    #     return not bool( sublime.active_window().active_view().settings().get('trainingMode') )

    def is_checked(self):
        return bool(sublime.active_window().active_view().settings().get('trainingMode'))

    def run(self):
        self.loadsettings = sublime.load_settings("DecipherLib.sublime-settings")

        window = sublime.active_window()
        current = 'Enabled' if window.active_view().settings().get('trainingMode') else 'Disabled'
        dOption = 'Disable (key needed)' if window.active_view().settings().get('trainingMode') else 'Enable'

        dialogText = """Set Training mode On/Off.

Enabled Training mode stops the Parser, hides completions and prevents other automations.
The idea is that trainees should first learn how to do everything themselves and DecipherLib's helpers is too helpful.'

    Current: %s""" %current

        dialogSelected = sublime.ok_cancel_dialog(dialogText, dOption )
        
        if dialogSelected:
            if current == 'Disabled':
                self.loadsettings.set('training_mode', True)
                sublime.save_settings("DecipherLib.sublime-settings")
                self.settingsReload = sublime.load_settings("DecipherLib.sublime-settings")
                for vw in window.views():
                    if self.settingsReload.get("training_mode", True):
                        vw.settings().set('trainingMode', True)
                    else:
                        vw.settings().set('trainingMode', False)
            elif current == 'Enabled':
                window.show_input_panel(
                    'Training Mode Disabler Key:',
                    '',
                    self.on_done_input,
                    on_change=None,
                    on_cancel=None
                )

    def on_done_input(self, input=None):
        window = sublime.active_window()
        if input in ['i4vg8arl','weuzgx02','tz8050sm']:
            self.loadsettings.set('training_mode', False)
            sublime.save_settings("DecipherLib.sublime-settings")
            self.settingsReload = sublime.load_settings("DecipherLib.sublime-settings")
            for vw in window.views():
                if self.settingsReload.get("training_mode", True):
                    vw.settings().set('trainingMode', True)
                else:
                    vw.settings().set('trainingMode', False)
