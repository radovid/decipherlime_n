import sublime, sublime_plugin, sys, re

class eventChecker(sublime_plugin.ViewEventListener):
    # completion suggestions
    def on_query_completions(self, prefix, locations):
        try:
            if 'XML' in self.view.settings().get('syntax') and not self.view.settings().get('trainingMode'):

                # prefix lists
                tagCompls = ['quota', 'term', 'alt', 'block', 'comment', 'condition', 'exec', 'exit', 'html', 'loop', 'looprow', 'loopvar', 'marker', 'note', 'style', 'style', 'stylevar', 'suspend', 'suspend', 'block', 'res', 'validate', 'validateCell', 'validateCol', 'validateRow', 'var', 'virtual', 'br', 'br2', 'a href', 'img', 'popup', 'span', 'div', 'td', 'tr', 'table', 'li', 'ul', 'ol', 'sup']
                attrCompls = ['open', 'oe', 'aggregate', 'autosaveKey', 'alt', 'altlabel', 'amount', 'atleast', 'atmost', 'browserDupes', 'builderCompatible', 'class', 'choiceCond', 'colCond', 'colLegend', 'colLegendRows', 'cond', 'corner:left', 'displayOnError', 'exactly', 'exclusive', 'grouping', 'groups', 'final', 'fir', 'fullServices', 'height', 'hide:list', 'cs:hidelist', 'incidence', 'markers', 'minRanks', 'mls', 'onLoad', 'optional', 'points', 'randomize', 'range', 'ratingDirection', 'rowCond', 'rowLegend', 'rowShuffle', 'size', 'sortRows', 'sortChoices', 'shuffle', 'shuffleBy', 'sst', 'ss:', 'accordionDisplay', 'postText', 'questionClassNames', 'colClassNames', 'rowClassNames', 'groupClassNames', 'style', 'surveyDisplay', 'translateable', 'unique', 'type=rating', 'url', 'value', 'values', 'verify', 'virtual', 'when', 'where', 'width', 'source', 'as', 'exclude', 'strip', 'otherLanguages', 'unusedLanguages', 'disableBackButton', 'enableNavigation', 'hideProgressBar', 'fixedWidth', 'loadData', 'lists', 'lang', 'includeCSS', 'includeJS', 'includeLESS', 'listDisplay']
                pyCompls = ['eachRow', 'eachCol', '.val', '.ival', '.open', '.any', '.sum', '.displayed', '.styles', '.index', '.label', '.selected', '.attr', '.rows', '.cols', 'allQuestions', 'getattr', 'assignRandomOrder', 'for eachRow', 'for eachCol', 'for range', 'hasMarker', 'setMarker', 'removeMarker', 'getMarker', 'setLanguage', 'getQuotaCells', 'stoppedCells', 'random', 'error', 'hlang', 'timeSpent', 'setExtra', 'x for', 'File', 'ishuffle','group_rows','cells']
                hprCompls = ['hpr', 'theme', 'container', 'page', 'section', 'grid', 'rowTiles', 'type', 'cond', 'rowCond', 'placeholder', 'verify', 'oeError', 'dupesOff', 'validOff', 'sequenOff', 'min', 'max', 'amount', 'sumPretext', 'minDate', 'maxDate', 'onlyMY', 'dateFormat', 'dateFormatData', 'firstDay', 'validate', 'exec']

                # compat dependent
                surveyCompatReg = self.view.find(r'compat="(\d+)"', 0)
                surveyCompat = int(self.view.substr(surveyCompatReg).split('"')[1]) if surveyCompatReg else 143
                label = r'label="${1/^(?:[\s\(]*not[\s\(]*)?(\w[^\s\.\[\(]+).*/$1/}%s" ' %('term' if prefix.lower().startswith('t') else '_quota') if surveyCompat >= 143 else ''

                # surveyType dependent
                if self.view.settings().get('useHpr'):
                    oeTxt = 'open="1" openSize="25" randomize="0" hpr:rplaceholder="Please specify..."'
                elif self.view.settings().get('useQarts'):
                    oeTxt = 'open="1" openSize="25" randomize="0" qa:rplaceholder="Please specify..."'
                elif self.view.settings().get('surveyType') in ['AG', 'AG0']:
                    oeTxt = 'open="1" openSize="25" randomize="0" openOptional="1"'
                else:
                    oeTxt = 'open="1" openSize="25" randomize="0"'

                if self.view.settings().get('surveyType') in ['v3','v2']:
                    delimType = " "
                else:
                    delimType = "\n  "

                surveyTagRegion = self.view.find('<survey(.|\n)*?>', self.view.text_point(1,0))

                # process query completion
                for pt in locations:
                    # define delimiter - start with space or not
                    prevChar = self.view.substr(pt-2)
                    onFirstLine = bool( re.search(r'^\s*<', self.view.substr(self.view.line(pt))) )
                    emptyLine = bool( re.search(r'^\s*[A-Za-z\:]*>?$', self.view.substr(self.view.line(pt))) )

                    if re.search(r'[\s\n]+', prevChar):
                        attrSplitter = ""
                        delimPost = delimType if onFirstLine and "\n" in delimType else delimType.strip(" ")
                    else:
                        attrSplitter = " "
                        delimPost = delimType if onFirstLine else delimType.replace("  ", "")
                    if emptyLine:
                        attrDelim = delimPost.strip()
                    else:
                        attrDelim = delimPost
                    genAttrs = [
                                ["class\tCSS attr", "%sclass=\"${1:custom-${2}}\"" %attrSplitter],
                                ["cond\tattribute", "%scond=\"${1:0}\"" %attrSplitter],
                                ["randomize\trow/block attr", "%srandomize=\"0\"" %attrSplitter],
                                ["style\tCSS attr", "%sstyle=\"${1:color: ${2:blue};}\"" %attrSplitter],
                                ["translateable\tquest attr", "%stranslateable=\"0\"" %attrSplitter],
                                ["where\tattribute", "%swhere=\"${1:data,report}\"" %attrSplitter]
                            ]
                    surveyAttrs = [
                                ["autosaveKey\tKey for respondent sesion", "%sautosaveKey=\"${1:source}\"" %attrDelim],
                                ["browserDupes\tDuplicate resp. checks (\"\"|strict|safe)", "%sbrowserDupes=\"${1:safe}\"" %attrDelim],
                                ["builderCompatible\tBuilder compatible (0|1)", "%sbuilderCompatible=\"${1:0}\"" %attrDelim],
                                ["displayOnError\tShow all Qns on 1 page on error", "%sdisplayOnErrors=\"all\"" %attrDelim],
                                ["otherLanguages\tAdditional survey languages", "%sotherLanguages=\"${1:spanish,german,french}\"" %attrDelim],
                                ["unusedLanguages\tLanguages never used (for SST)", "%sunusedLanguages=\"${1:english}\"" %attrDelim],
                                ["ss:disableBackButton\tStop browser's back button", "%sss:disableBackButton=\"${1:0}\"" %attrDelim],
                                ["ss:enableNavigation\tAdd back button to survey", "%sss:enableNavigation=\"${1:1}\"" %attrDelim],
                                ["ss:hideProgressBar\tHide progress bar", "%sss:hideProgressBar=\"${1:0}\"" %attrDelim],
                                ["fixedWidth\tOE data limited to size/range", "%sfixedWidth=\"tight\"" %attrDelim],
                                ["loadData\tVariable to load existing data", "%sloadData=\"${1:ID}\"" %attrDelim],
                                ["lists\tFor email sample files and adb", "%slists=\"${1:mail}\"" %attrDelim],
                                ["lang\tMain language for survey", "%slang=\"${1:english}\"" %attrDelim],
                                ["ss:includeCSS\tLoad CSS file(s) from /static", "%sss:includeCSS=\"${2:[rel ${1}.css]}\"" %attrDelim],
                                ["ss:includeJS\tLoad JS file(s) from /static", "%sss:includeJS=\"${2:[rel ${1}.js]}\"" %attrDelim],
                                ["ss:includeLESS=""\tLoad Less file(s) from /static", "%sss:includeLESS=\"${2:[rel ${1:theme}.less]}\"" %attrDelim],
                                ["ss:listDisplay\t1-col Qns as list or as table", "%sss:listDisplay=\"${1:1}\"" %attrDelim],
                                ["fir\tSet styled rad/chkbox buttons", "%sfir=\"on\"\nfirStyle=\"${1:round}\"\nfirSize=\"${2:40px}\"" %attrDelim],
                                ["fullService\tNon-staff access", "%sfullService=\"1\"" %attrDelim]
                            ]
                    radioAttrs = [
                                ["altlabel\tquest attr", "%saltlabel=\"${1}\"" %attrDelim],
                                ["colCond\tquest attr",  "%scolCond=\"${1}[col]$0\"" %attrDelim],
                                ["colLegend\tquest attr",  "%scolLegend=\"${1:beforeGroup}\"" %attrDelim],
                                ["colLegendRows\tquest attr",  "%scolLegendRows=\"${1:6,11}\"" %attrDelim],
                                ["cond\tattribute", "%scond=\"${1:0}\"" %attrDelim],
                                ["corner:left\tquest attr", "%scorner:left=\"\\${res.${1}}\"" %attrDelim],
                                ["grouping\tquest attr", "%sgrouping=\"${1:cols}\"%sadim=\"$1\"" %(attrDelim, delimPost)],
                                ["hide:list\tquest attr", "%shide:list=\"$0\"" %attrDelim],
                                ["cs:hidelist\tquest attr", "%scs:hidelist=\"$0\"" %attrDelim],
                                ["onLoad\tquest attr", "%sonLoad=\"copy('${1}', ${2:rows}=True${3:, prepend=True}${5:, exclude='${4:r98}'}$0)\"" %attrDelim],
                                ["optional\tquest attr", "%soptional=\"1\"" %attrDelim],
                                ["ratingDirection\tquest attr", "%sratingDirection=\"reverse\"" %attrDelim],
                                ["rowCond\tquest attr", "%srowCond=\"${1}[row]$0\"" %attrDelim],
                                ["rowLegend\tquest attr", "%srowLegend=\"${1:both}\"" %attrDelim],
                                ["rowShuffle\tquest attr", "%srowShuffle=\"${1:flip}\"" %attrDelim],
                                ["sortRows\tquest attr", "%ssortRows=\"${1:asc},survey\"" %attrDelim],
                                ["shuffle\tquest attr", "%sshuffle=\"${1:rows}\"" %attrDelim],
                                ["shuffleBy\tquest attr", "%sshuffleBy=\"${1:A1}\"" %attrDelim],
                                ["sst\tquest attr", "%ssst=\"0\"" %attrDelim],
                                ["ss:accordionDisplay\tquest attr", "%sss:accordionDisplay=\"1\"" %attrDelim],
                                ["ss:postText\tquest attr", "%sss:postText=\"${2:\${res.$1\}}\"" %attrDelim],
                                ["ss:questionClassNames\tquest attr", "%sss:questionClassNames=\"${1:hidden}\"" %attrDelim],
                                ["surveyDisplay\tquest attr", "%ssurveyDisplay=\"${1:desktop}\"" %attrDelim],
                                ["translateable\tquest attr", "%stranslateable=\"0\"" %attrDelim],
                                ["type=rating\tquest attr", "%stype=\"rating\"" %attrDelim],
                                ["values\tquest attr", "%svalues=\"order\"" %attrDelim],
                                ["virtual\tquest attr", "%svirtual=\"${1:bucketize(${2:co})}\"" %attrDelim],
                                ["where\tattribute", "%swhere=\"${1:data,report}\"" %attrDelim],
                                ["mls\t lang attr", "%smls=\"${1:english}\"" %attrDelim]
                            ]
                    checkboxAttrs = [
                                ["altlabel\tquest attr", "%saltlabel=\"${1}\"" %attrDelim],
                                ["atleast\tquest attr",  "%satleast=\"${1:1}\"" %attrDelim],
                                ["atmost\tquest attr",  "%satmost=\"${1:3}\"" %attrDelim],
                                ["colCond\tquest attr",  "%scolCond=\"${1}[col]$0\"" %attrDelim],
                                ["colLegend\tquest attr",  "%scolLegend=\"${1:beforeGroup}\"" %attrDelim],
                                ["colLegendRows\tquest attr",  "%scolLegendRows=\"${1:6,11}\"" %attrDelim],
                                ["cond\tattribute", "%scond=\"${1:0}\"" %attrDelim],
                                ["corner:left\tquest attr", "%scorner:left=\"\\${res.${1}}\"" %attrDelim],
                                ["exactly\tquest attr",  "%sexactly=\"${1:3}\"" %attrDelim],
                                ["grouping\tquest attr", "%sgrouping=\"${1:cols}\"%sadim=\"$1\"" %(attrDelim, delimPost)],
                                ["hide:list\tquest attr", "%shide:list=\"$0\"" %attrDelim],
                                ["cs:hidelist\tquest attr", "%scs:hidelist=\"$0\"" %attrDelim],
                                ["onLoad\tquest attr", "%sonLoad=\"copy('${1}', ${2:rows}=True${3:, prepend=True}${5:, exclude='${4:r98}'}$0)\"" %attrDelim],
                                ["optional\tquest attr", "%soptional=\"1\"" %attrDelim],
                                ["rowCond\tquest attr", "%srowCond=\"${1}[row]$0\"" %attrDelim],
                                ["rowLegend\tquest attr", "%srowLegend=\"${1:both}\"" %attrDelim],
                                ["rowShuffle\tquest attr", "%srowShuffle=\"${1:flip}\"" %attrDelim],
                                ["sortRows\tquest attr", "%ssortRows=\"${1:asc},survey\"" %attrDelim],
                                ["shuffle\tquest attr", "%sshuffle=\"${1:rows}\"" %attrDelim],
                                ["shuffleBy\tquest attr", "%sshuffleBy=\"${1:A1}\"" %attrDelim],
                                ["sst\tquest attr", "%ssst=\"0\"" %attrDelim],
                                ["ss:postText\tquest attr", "%sss:postText=\"${2:\${res.$1\}}\"" %attrDelim],
                                ["ss:questionClassNames\tquest attr", "%sss:questionClassNames=\"${1:hidden}\"" %attrDelim],
                                ["surveyDisplay\tquest attr", "%ssurveyDisplay=\"${1:desktop}\"" %attrDelim],
                                ["translateable\tquest attr", "%stranslateable=\"0\"" %attrDelim],
                                ["virtual\tquest attr", "%svirtual=\"${1:bucketize(${2:co})}\"" %attrDelim],
                                ["where\tattribute", "%swhere=\"${1:data,report}\"" %attrDelim],
                                ["mls\t lang attr", "%smls=\"${1:english}\"" %attrDelim]
                            ]
                    selectAttrs = [
                                ["altlabel\tquest attr", "%saltlabel=\"${1}\"" %attrDelim],
                                ["choiceCond\tquest attr",  "%schoiceCond=\"choice.index gt ${1}$0\"" %attrDelim],
                                ["colCond\tquest attr",  "%scolCond=\"${1}[col]$0\"" %attrDelim],
                                ["colLegend\tquest attr",  "%scolLegend=\"${1:beforeGroup}\"" %attrDelim],
                                ["colLegendRows\tquest attr",  "%scolLegendRows=\"${1:6,11}\"" %attrDelim],
                                ["cond\tattribute", "%scond=\"${1:0}\"" %attrDelim],
                                ["corner:left\tquest attr", "%scorner:left=\"\\${res.${1}}\"" %attrDelim],
                                ["hide:list\tquest attr", "%shide:list=\"$0\"" %attrDelim],
                                ["cs:hidelist\tquest attr", "%scs:hidelist=\"$0\"" %attrDelim],
                                ["minRanks\tquest attr", "%sminRanks=\"${1:1}\"" %attrDelim],
                                ["onLoad\tquest attr", "%sonLoad=\"copy('${1}', ${2:rows}=True${3:, prepend=True}${5:, exclude='${4:r98}'}$0)\"" %attrDelim],
                                ["optional\tquest attr", "%soptional=\"1\"" %attrDelim],
                                ["ratingDirection\tquest attr", "%sratingDirection=\"reverse\"" %attrDelim],
                                ["rowCond\tquest attr", "%srowCond=\"${1}[row]$0\"" %attrDelim],
                                ["rowLegend\tquest attr", "%srowLegend=\"${1:both}\"" %attrDelim],
                                ["rowShuffle\tquest attr", "%srowShuffle=\"${1:flip}\"" %attrDelim],
                                ["choiceShuffle\tquest attr", "%schoiceShuffle=\"${1:flip}\"" %attrDelim],
                                ["sortRows\tquest attr", "%ssortRows=\"${1:asc},survey\"" %attrDelim],
                                ["sortChoices\tquest attr", "%ssortChoices=\"${1:asc},survey\"" %attrDelim],
                                ["shuffle\tquest attr", "%sshuffle=\"${1:rows}${2:,choices}\"" %attrDelim],
                                ["shuffleBy\tquest attr", "%sshuffleBy=\"${1:A1}\"" %attrDelim],
                                ["sst\tquest attr", "%ssst=\"0\"" %attrDelim],
                                ["ss:postText\tquest attr", "%sss:postText=\"${2:\${res.$1\}}\"" %attrDelim],
                                ["ss:questionClassNames\tquest attr", "%sss:questionClassNames=\"${1:hidden}\"" %attrDelim],
                                ["surveyDisplay\tquest attr", "%ssurveyDisplay=\"${1:desktop}\"" %attrDelim],
                                ["translateable\tquest attr", "%stranslateable=\"0\"" %attrDelim],
                                ["type=rating\tquest attr", "%stype=\"rating\"" %attrDelim],
                                ["unique\tquest attr", "%sunique=\"${1:cols}\"" %attrDelim],
                                ["values\tquest attr", "%svalues=\"order\"" %attrDelim],
                                ["where\tattribute", "%swhere=\"${1:data,report}\"" %attrDelim],
                                ["mls\t lang attr", "%smls=\"${1:english}\"" %attrDelim]
                            ]
                    numberAttrs = [
                                ["altlabel\tquest attr", "%saltlabel=\"${1}\"" %attrDelim],
                                ["amount\tquest attr",  "%samount=\"${1:100}\"" %attrDelim],
                                ["colCond\tquest attr",  "%scolCond=\"${1}[col]$0\"" %attrDelim],
                                ["colLegend\tquest attr",  "%scolLegend=\"${1:beforeGroup}\"" %attrDelim],
                                ["colLegendRows\tquest attr",  "%scolLegendRows=\"${1:6,11}\"" %attrDelim],
                                ["cond\tattribute", "%scond=\"${1:0}\"" %attrDelim],
                                ["corner:left\tquest attr", "%scorner:left=\"\\${res.${1}}\"" %attrDelim],
                                ["grouping\tquest attr", "%sgrouping=\"${1:cols}\"%sadim=\"$1\"" %(attrDelim, delimPost)],
                                ["hide:list\tquest attr", "%shide:list=\"$0\"" %attrDelim],
                                ["cs:hidelist\tquest attr", "%scs:hidelist=\"$0\"" %attrDelim],
                                ["onLoad\tquest attr", "%sonLoad=\"copy('${1}', ${2:rows}=True${3:, prepend=True}${5:, exclude='${4:r98}'}$0)\"" %attrDelim],
                                ["optional\tquest attr", "%soptional=\"1\"" %attrDelim],
                                ["points\tquest attr", "%spoints=\"${1:10}\"" %attrDelim],
                                ["range\tquest attr", "%srange=\"${1:18}-${2:99}\"" %attrDelim],
                                ["rowCond\tquest attr", "%srowCond=\"${1}[row]$0\"" %attrDelim],
                                ["rowLegend\tquest attr", "%srowLegend=\"${1:both}\"" %attrDelim],
                                ["rowShuffle\tquest attr", "%srowShuffle=\"${1:flip}\"" %attrDelim],
                                ["size\tquest attr", "%ssize=\"${1:3}\"" %attrDelim],
                                ["sortRows\tquest attr", "%ssortRows=\"${1:asc},survey\"" %attrDelim],
                                ["shuffle\tquest attr", "%sshuffle=\"${1:rows}\"" %attrDelim],
                                ["shuffleBy\tquest attr", "%sshuffleBy=\"${1:A1}\"" %attrDelim],
                                ["sst\tquest attr", "%ssst=\"0\"" %attrDelim],
                                ["ss:postText\tquest attr", "%sss:postText=\"${2:\${res.$1\}}\"" %attrDelim],
                                ["ss:questionClassNames\tquest attr", "%sss:questionClassNames=\"${1:hidden}\"" %attrDelim],
                                ["surveyDisplay\tquest attr", "%ssurveyDisplay=\"${1:desktop}\"" %attrDelim],
                                ["translateable\tquest attr", "%stranslateable=\"0\"" %attrDelim],
                                ["verify\tquest attr", "%sverify=\"${3:range(${2:0},${1:99})}\"" %attrDelim],
                                ["where\tattribute", "%swhere=\"${1:data,report}\"" %attrDelim],
                                ["mls\t lang attr", "%smls=\"${1:english}\"" %attrDelim]
                            ]
                    floatAttrs = [
                                ["altlabel\tquest attr", "%saltlabel=\"${1}\"" %attrDelim],
                                ["colCond\tquest attr",  "%scolCond=\"${1}[col]$0\"" %attrDelim],
                                ["colLegend\tquest attr",  "%scolLegend=\"${1:beforeGroup}\"" %attrDelim],
                                ["colLegendRows\tquest attr",  "%scolLegendRows=\"${1:6,11}\"" %attrDelim],
                                ["cond\tattribute", "%scond=\"${1:0}\"" %attrDelim],
                                ["corner:left\tquest attr", "%scorner:left=\"\\${res.${1}}\"" %attrDelim],
                                ["grouping\tquest attr", "%sgrouping=\"${1:cols}\"%sadim=\"$1\"" %(attrDelim, delimPost)],
                                ["hide:list\tquest attr", "%shide:list=\"$0\"" %attrDelim],
                                ["cs:hidelist\tquest attr", "%scs:hidelist=\"$0\"" %attrDelim],
                                ["onLoad\tquest attr", "%sonLoad=\"copy('${1}', ${2:rows}=True${3:, prepend=True}${5:, exclude='${4:r98}'}$0)\"" %attrDelim],
                                ["optional\tquest attr", "%soptional=\"1\"" %attrDelim],
                                ["range\tquest attr", "%srange=\"${1:18}-${2:99}\"" %attrDelim],
                                ["rowCond\tquest attr", "%srowCond=\"${1}[row]$0\"" %attrDelim],
                                ["rowLegend\tquest attr", "%srowLegend=\"${1:both}\"" %attrDelim],
                                ["rowShuffle\tquest attr", "%srowShuffle=\"${1:flip}\"" %attrDelim],
                                ["size\tquest attr", "%ssize=\"${1:10}\"" %attrDelim],
                                ["sortRows\tquest attr", "%ssortRows=\"${1:asc},survey\"" %attrDelim],
                                ["shuffle\tquest attr", "%sshuffle=\"${1:rows}\"" %attrDelim],
                                ["shuffleBy\tquest attr", "%sshuffleBy=\"${1:A1}\"" %attrDelim],
                                ["sst\tquest attr", "%ssst=\"0\"" %attrDelim],
                                ["ss:postText\tquest attr", "%sss:postText=\"${2:\${res.$1\}}\"" %attrDelim],
                                ["ss:questionClassNames\tquest attr", "%sss:questionClassNames=\"${1:hidden}\"" %attrDelim],
                                ["surveyDisplay\tquest attr", "%ssurveyDisplay=\"${1:desktop}\"" %attrDelim],
                                ["translateable\tquest attr", "%stranslateable=\"0\"" %attrDelim],
                                ["where\tattribute", "%swhere=\"${1:data,report}\"" %attrDelim],
                                ["mls\t lang attr", "%smls=\"${1:english}\"" %attrDelim]
                            ]
                    textAttrs = [
                                ["altlabel\tquest attr", "%saltlabel=\"${1}\"" %attrDelim],
                                ["colCond\tquest attr",  "%scolCond=\"${1}[col]$0\"" %attrDelim],
                                ["colLegend\tquest attr",  "%scolLegend=\"${1:beforeGroup}\"" %attrDelim],
                                ["colLegendRows\tquest attr",  "%scolLegendRows=\"${1:6,11}\"" %attrDelim],
                                ["cond\tattribute", "%scond=\"${1:0}\"" %attrDelim],
                                ["corner:left\tquest attr", "%scorner:left=\"\\${res.${1}}\"" %attrDelim],
                                ["hide:list\tquest attr", "%shide:list=\"$0\"" %attrDelim],
                                ["cs:hidelist\tquest attr", "%scs:hidelist=\"$0\"" %attrDelim],
                                ["onLoad\tquest attr", "%sonLoad=\"copy('${1}', ${2:rows}=True${3:, prepend=True}${5:, exclude='${4:r98}'}$0)\"" %attrDelim],
                                ["optional\tquest attr", "%soptional=\"1\"" %attrDelim],
                                ["rowCond\tquest attr", "%srowCond=\"${1}[row]$0\"" %attrDelim],
                                ["rowLegend\tquest attr", "%srowLegend=\"${1:both}\"" %attrDelim],
                                ["rowShuffle\tquest attr", "%srowShuffle=\"${1:flip}\"" %attrDelim],
                                ["size\tquest attr", "%ssize=\"${1:20}\"" %attrDelim],
                                ["sortRows\tquest attr", "%ssortRows=\"${1:asc},survey\"" %attrDelim],
                                ["shuffle\tquest attr", "%sshuffle=\"${1:rows}\"" %attrDelim],
                                ["shuffleBy\tquest attr", "%sshuffleBy=\"${1:A1}\"" %attrDelim],
                                ["sst\tquest attr", "%ssst=\"0\"" %attrDelim],
                                ["ss:postText\tquest attr", "%sss:postText=\"${2:\${res.$1\}}\"" %attrDelim],
                                ["ss:questionClassNames\tquest attr", "%sss:questionClassNames=\"${1:hidden}\"" %attrDelim],
                                ["surveyDisplay\tquest attr", "%ssurveyDisplay=\"${1:desktop}\"" %attrDelim],
                                ["translateable\tquest attr", "%stranslateable=\"0\"" %attrDelim],
                                ["verify\tquest attr", "%sverify=\"${1:email}\"" %attrDelim],
                                ["where\tattribute", "%swhere=\"${1:data,report}\"" %attrDelim],
                                ["mls\t lang attr", "%smls=\"${1:english}\"" %attrDelim]
                            ]
                    textareaAttrs = [
                                ["altlabel\tquest attr", "%saltlabel=\"${1}\"" %attrDelim],
                                ["colCond\tquest attr",  "%scolCond=\"${1}[col]$0\"" %attrDelim],
                                ["cond\tattribute", "%scond=\"${1:0}\"" %attrDelim],
                                ["grouping\tquest attr", "%sgrouping=\"${1:cols}\"%sadim=\"$1\"" %(attrDelim, delimPost)],
                                ["onLoad\tquest attr", "%sonLoad=\"copy('${1}', ${2:rows}=True${3:, prepend=True}${5:, exclude='${4:r98}'}$0)\"" %attrDelim],
                                ["optional\tquest attr", "%soptional=\"1\"" %attrDelim],
                                ["rowCond\tquest attr", "%srowCond=\"${1}[row]$0\"" %attrDelim],
                                ["rowCond\tquest attr", "%srowCond=\"${1}[row]$0\"" %attrDelim],
                                ["rowLegend\tquest attr", "%srowLegend=\"${1:both}\"" %attrDelim],
                                ["rowShuffle\tquest attr", "%srowShuffle=\"${1:flip}\"" %attrDelim],
                                ["sortRows\tquest attr", "%ssortRows=\"${1:asc},survey\"" %attrDelim],
                                ["shuffle\tquest attr", "%sshuffle=\"${1:rows}\"" %attrDelim],
                                ["shuffleBy\tquest attr", "%sshuffleBy=\"${1:A1}\"" %attrDelim],
                                ["sst\tquest attr", "%ssst=\"0\"" %attrDelim],
                                ["ss:questionClassNames\tquest attr", "%sss:questionClassNames=\"${1:hidden}\"" %attrDelim],
                                ["surveyDisplay\tquest attr", "%ssurveyDisplay=\"${1:desktop}\"" %attrDelim],
                                ["translateable\tquest attr", "%stranslateable=\"0\"" %attrDelim],
                                ["verify\tquest attr", "%sverify=\"${3:range(${2:0},${1:99})}\"" %attrDelim],
                                ["where\tattribute", "%swhere=\"${1:data,report}\"" %attrDelim],
                                ["width\ttextarea attribute", "%swidth=\"${1:75}\"" %attrDelim],
                                ["height\ttextarea attribute", "%sheight=\"${1:2}\"" %attrDelim],
                                ["mls\t lang attr", "%smls=\"${1:english}\"" %attrDelim]
                            ]

                    rowAttrs = [
                                ["open\trow attr",  attrSplitter + oeTxt ],
                                ["oe\trow attr",  attrSplitter + oeTxt ],
                                ["alt\trow attr", "%salt=\"${1:Other from ${2:A1}}\"" %attrSplitter],
                                ["cond\tattribute", "%scond=\"${1:0}\"" %attrSplitter],
                                ["exclusive\trow attr", "%sexclusive=\"1\" randomize=\"0\"" %attrSplitter],
                                ["groups\trow attr", "%sgroups=\"${2:g${1:1}}\"" %attrSplitter],
                                ["optional\trow attr", "%soptional=\"1\"" %attrSplitter],
                                ["percentages\trow attr","%spercentages=\"${1:1}\"" %attrSplitter],
                                ["randomize\trow attr", "%srandomize=\"0\"" %attrSplitter],
                                ["range\trow attr", "%srange=\"${1:18}-${2:99}\"" %attrSplitter],
                                ["rightLegend\trow attr", "%srightLegend=\"${1:That}\"" %attrSplitter],
                                ["ss:postText\trow attr", "%sss:postText=\"${2:\${res.$1\}}\"" %attrSplitter],
                                ["ss:rowClassNames\trow attr", "%sss:rowClassNames=\"${1:read_only}\"" %attrSplitter],
                                ["translateable\trow attr", "%stranslateable=\"0\"" %attrSplitter],
                                ["value\trow attr", "%svalue=\"${1:99}\"" %attrSplitter],
                                ["verify\trow attr", "%sverify=\"${3:range(${2:0},${1:99})}\"" %attrSplitter],
                                ["where\tattribute", "%swhere=\"${1:data,report}\"" %attrSplitter],
                                ["mls\t lang attr", "%smls=\"${1:english}\"" %attrSplitter]
                            ]
                    colAttrs = [
                                ["open\tcol attr",  attrSplitter + oeTxt ],
                                ["oe\tcol attr",  attrSplitter + oeTxt ],
                                ["alt\tcol attr", "%salt=\"${1:Other from ${2:A1}}\"" %attrSplitter],
                                ["amount\tcol attr",  "%samount=\"${1:100}\"" %attrSplitter],
                                ["cond\tattribute", "%scond=\"${1:0}\"" %attrSplitter],
                                ["exclusive\tcol attr", "%sexclusive=\"1\" randomize=\"0\"" %attrSplitter],
                                ["groups\tcol attr", "%sgroups=\"${2:g${1:1}}\"" %attrSplitter],
                                ["optional\tcol attr", "%soptional=\"1\"" %attrSplitter],
                                ["percentages\tcol attr","%spercentages=\"${1:1}\"" %attrSplitter],
                                ["randomize\col attr", "%srandomize=\"0\"" %attrSplitter],
                                ["range\tcol attr", "%srange=\"${1:18}-${2:99}\"" %attrSplitter],
                                ["ss:postText\tcol attr", "%sss:postText=\"${2:\${res.$1\}}\"" %attrSplitter],
                                ["ss:colClassNames\tcol attr", "%sss:colClassNames=\"${1:read_only}\"" %attrSplitter],
                                ["translateable\tcol attr", "%stranslateable=\"0\"" %attrSplitter],
                                ["value\trow/col attr", "%svalue=\"${1:99}\"" %attrSplitter],
                                ["verify\tcol attr", "%sverify=\"${3:range(${2:0},${1:99})}\"" %attrSplitter],
                                ["where\tattribute", "%swhere=\"${1:data,report}\"" %attrSplitter],
                                ["mls\t lang attr", "%smls=\"${1:english}\"" %attrSplitter]
                            ]
                    choiceAttrs = [
                                ["alt\tchoice attr", "%salt=\"${1:Other from ${2:A1}}\"" %attrSplitter],
                                ["cond\tattribute", "%scond=\"${1:0}\"" %attrSplitter],
                                ["groups\tchoice attr", "%sgroups=\"${2:g${1:1}}\"" %attrSplitter],
                                ["percentages\tchoice attr","%spercentages=\"${1:1}\"" %attrSplitter],
                                ["randomize\tchoice attr", "%srandomize=\"0\"" %attrSplitter],
                                ["range\tchoice attr", "%srange=\"${1:18}-${2:99}\"" %attrSplitter],
                                ["translateable\tchoice attr", "%stranslateable=\"0\"" %attrSplitter],
                                ["value\tchoice attr", "%svalue=\"${1:99}\"" %attrSplitter],
                                ["where\tattribute", "%swhere=\"${1:data,report}\"" %attrSplitter],
                                ["mls\tlang attr", "%smls=\"${1:english}\"" %attrSplitter]
                            ]
                    groupAttrs = [
                                ["alt\tgroup attr", "%salt=\"${1:Other from ${2:A1}}\"" %attrSplitter],
                                ["cond\tattribute", "%scond=\"${1:0}\"" %attrSplitter],
                                ["randomize\tgroup attr", "%srandomize=\"0\"" %attrSplitter],
                                ["ss:groupClassNames\tgroup attr", "%sss:groupClassNames=\"${1:read_only}\"" %attrSplitter],
                                ["translateable\tgroup attr", "%stranslateable=\"0\"" %attrSplitter],
                                ["where\tattribute", "%swhere=\"${1:none}\"" %attrSplitter],
                                ["mls\t lang attr", "%smls=\"${1:english}\"" %attrSplitter]
                            ]
                    resAttrs = [
                                ["translateable\tattribute", "%stranslateable=\"0\"" %attrSplitter],
                                ["mls\tlang attr", "%smls=\"${1:english}\"" %attrSplitter]
                            ]
                    execAttrs = [
                                ["cond\tattribute", "%scond=\"${1:0}\"" %attrSplitter],
                                ["when\texec attr", "%swhen=\"${1:init}\"" %attrSplitter]
                            ]

                    htmlAttrs = [
                                ["altlabel\tquest attr", "%saltlabel=\"${1}\"" %attrDelim],
                                ["cond\tattribute", "%scond=\"${1:0}\"" %attrDelim],
                                ["translateable\tattribute", "%stranslateable=\"0\"" %attrDelim],
                                ["mls\tlang attr", "%smls=\"${1:english}\"" %attrDelim],
                                ["where\tlang attr", "%swhere=\"${1:survey}\"" %attrDelim],
                                ["source\thtml attr", "%ssource=\"${1:ext_message.html}\"" %attrDelim],
                                ["final\tfinish survey", "%sfinal=\"1\"" %attrDelim]
                            ]
                    termAttrs = [
                                ["incidence\tterm attr", "%sincidence=\"0\"" %attrSplitter],
                                ["sst\tother attr", "%ssst=\"0\"" %attrSplitter],
                                ["markers\tterm attr", "%smarkers=\"${1:qcterm}\"" %attrSplitter]
                            ]
                    suspendAttrs = [
                                ["cond\tattribute", "%scond=\"${1:0}\"" %attrSplitter],
                                ["url\tsuspend attr", "%surl=\"${1:https://tes.decipherinc.com/survey/selfserve/${2:}?list=${3:1}&amp;co=${co}&amp;lang=${lang}}\"" %attrSplitter]
                            ]
                    samplesourceAttrs = [
                                ["list\tsamplesource attr", "%slist=\"${1:9}\"" %attrSplitter],
                                ["title\t samplesource attr", "%stitle=\"${1:$0 Sample}\"" %attrSplitter],
                                ["adb\tList to use  ADB database (Email sample)", "%sadb=\"${1:1}\"" %attrSplitter],
                                ["disableBrowserDupes\tsamplesource attr", "%sdisableBrowserDupes=\"${1:1}\"" %attrSplitter],
                                ["closed\tsamplesource attr", "%sclosed=\"${1:1}\"" %attrSplitter],
                                ["default\tsamplesourceS attr", "%sdefault=\"${1:1}\"" %attrSplitter],
                                ["eid\tsamplesource attr", "%seid=\"${2:s2s:${1:101}}\"" %attrSplitter],
                                ["keyring\tsamplesource attr", "%skeyring=\"${1:sys/dynata}\"" %attrSplitter],
                                ["sign\tsamplesource attr", "%ssign=\"${1:out,in}\"" %attrSplitter]
                            ]
                    varAttrs = [
                                ["name\tvar attr", "%scond=\"${1:0}\"" %attrSplitter],
                                ["required\tvar attr", "%srequired=\"${1:1}\"" %attrSplitter],
                                ["values\tvar susp attr", "%svalues=\"${1:1,2,3}\"" %attrSplitter],
                                ["unique\tvar attr", "%sunique=\"${1:1}\"" %attrSplitter],
                                ["filename\tvar attr", "%sfilename=\"${1:ids.dat}\"" %attrSplitter]
                            ]
                    exitAttrs = [
                                ["cond\tattribute", "%scond=\"${1:terminated and hasMarker('qcterm')}\"" %attrSplitter],
                                ["url\texit attr", "%surl=\"${1:https://tes.decipherinc.com/survey/selfserve/${2:}?list=${3:1}&amp;co=${co}&amp;lang=${lang}}\"" %attrSplitter],
                                ["timeout\texit attr", "%stimeout=\"${1:10}\"" %attrSplitter]
                            ]
                    insertAttrs = [
                                ["source\tdefine label", "%ssource=\"${1:listRows}\"" %attrSplitter],
                                ["as\trows/cols/choices", "%sas=\"${1:cols}\"" %attrSplitter],
                                ["exclude\texclude option", "%sexclude=\"${1:r99}\"" %attrSplitter],
                                ["strip\tstrip attribute", "%sstrip=\"${1:value}\"" %attrSplitter]
                            ]
                    blockAttrs = [
                                ["cond\tattribute", "%scond=\"${1:0}\"" %attrSplitter],
                                ["randomize\tblock attr", "%srandomize=\"0\"" %attrSplitter],
                                ["randomizeChildren\tblock attr", "%srandomizeChildren=\"1\"" %attrSplitter],
                            ]


                    hprSurveyAttrs = [
                                ["hpr:theme\tSet HPR theme", "%shpr:theme=\"${1:blue|bluegrey|brown|green|orange|purple|red|teal}\"" %attrDelim],
                                ["hpr:dupesOff\tAllow dupe entries", "%shpr:dupesOff=\"1\"" %attrDelim],
                                ["hpr:validOff\tStop standard AG validation", "%shpr:validOff=\"1\"" %attrDelim],
                                ["hpr:sequenOff\tAllow answers in non-sequential order", "%shpr:sequenOff=\"1\"" %attrDelim],
                                ["hpr:dateFormat\tDate format", "%shpr:dateFormat=\"${1:mm/dd/yyyy}\"" %attrDelim],
                                ["hpr:dateFormatData\tData date format", "%shpr:dateFormatData=\"${1:mm/dd/yyyy}\"" %attrDelim]
                            ]
                    hprBlockAttrs = [
                                ["hpr:cond\tHPR Javascript cond", "%shpr:cond=\"${1:A1.r1.val}\"" %attrDelim],
                                ["hpr:container\tHPR container", "%shpr:container=\"1\"" %attrDelim],
                                ["hpr:page\tNew page for block", "%shpr:page=\"1\"" %attrDelim],
                                ["hpr:section\tNew section for block", "%shpr:section=\"1\"" %attrDelim]
                            ]

                    hprRadioAttrs = [
                                ["hpr:cond\tHPR Javascript cond", "%shpr:cond=\"${1:A1.r1.val}\"" %attrDelim],
                                ["hpr:rowCond\tHPR Javascript rowCond", "%shpr:rowCond=\"${1:A1.r1.val}\"" %attrDelim],
                                ["hpr:grid\tQuestion width/layout (1-12) ", "%shpr:grid=\"${1:6}\"" %attrDelim],
                                ["hpr:rowTiles\tRow grid columns ", "%shpr:rowTiles=\"${1:1}\"" %attrDelim],
                                ["hpr:type\tdropdown|rank|date|autosum", "%shpr:type=\"${1:dropdown}\"" %attrDelim]
                            ]
                    hprCheckboxAttrs = [
                                ["hpr:cond\tHPR Javascript cond", "%shpr:cond=\"${1:A1.r1.val}\"" %attrDelim],
                                ["hpr:rowCond\tHPR Javascript rowCond", "%shpr:rowCond=\"${1:A1.r1.val}\"" %attrDelim],
                                ["hpr:grid\tQuestion width/layout (1-12) ", "%shpr:grid=\"${1:6}\"" %attrDelim],
                                ["hpr:rowTiles\tRow grid columns ", "%shpr:rowTiles=\"${1:1}\"" %attrDelim],
                                ["hpr:atleast\tHPR attr", "%shpr:atleast=\"${1:1}\"" %attrDelim],
                                ["hpr:atmost\tHPR attr", "%shpr:atmost=\"${1:3}\"" %attrDelim],
                                ["hpr:exactly\tHPR attr", "%shpr:exactly=\"${1:3}\"" %attrDelim]
                            ]
                    hprNumberAttrs = [
                                ["hpr:cond\tHPR Javascript cond", "%shpr:cond=\"${1:A1.r1.val}\"" %attrDelim],
                                ["hpr:rowCond\tHPR Javascript rowCond", "%shpr:rowCond=\"${1:A1.r1.val}\"" %attrDelim],
                                ["hpr:grid\tQuestion width/layout (1-12) ", "%shpr:grid=\"${1:6}\"" %attrDelim],
                                ["hpr:rowTiles\tRow grid columns ", "%shpr:rowTiles=\"${1:1}\"" %attrDelim],
                                ["hpr:type\tautosum|date|dropdown|rank", "%shpr:type=\"${1:autosum}\"" %attrDelim],
                                ["hpr:verify\tRegEx validation", "%shpr:verify=\"$0\"" %attrDelim],
                                ["hpr:oeError\tWrong OE input error text", "%shpr:oeError=\"$0\"" %attrDelim],
                                ["hpr:placeholder\tPlaceholder text for input boxes", "%shpr:placeholder=\"${1:Please specify...}\"" %attrDelim],
                                ["hpr:min\tNumeric minimum value", "%shpr:min=\"${1:0}\"" %attrDelim],
                                ["hpr:max\tNumeric maximum value", "%shpr:max=\"${1:999}\"" %attrDelim],
                                ["hpr:amount\tHPR attr", "%shpr:amount=\"${1:100}\"" %attrDelim],
                                ["hpr:sumPretext\tAutosum total pre-text", "%shpr:sumPretext=\"${1:Sum: }\"" %attrDelim]
                            ]
                    hprFloatAttrs = [
                                ["hpr:cond\tHPR Javascript cond", "%shpr:cond=\"${1:A1.r1.val}\"" %attrDelim],
                                ["hpr:rowCond\tHPR Javascript rowCond", "%shpr:rowCond=\"${1:A1.r1.val}\"" %attrDelim],
                                ["hpr:grid\tQuestion width/layout (1-12) ", "%shpr:grid=\"${1:6}\"" %attrDelim],
                                ["hpr:rowTiles\tRow grid columns ", "%shpr:rowTiles=\"${1:1}\"" %attrDelim],
                                ["hpr:type\tautosum|date|dropdown|rank", "%shpr:type=\"${1:autosum}\"" %attrDelim],
                                ["hpr:verify\tRegEx validation", "%shpr:verify=\"$0\"" %attrDelim],
                                ["hpr:oeError\tWrong OE input error text", "%shpr:oeError=\"$0\"" %attrDelim],
                                ["hpr:placeholder\tPlaceholder text for input boxes", "%shpr:placeholder=\"${1:Please specify...}\"" %attrDelim],
                                ["hpr:min\tNumeric minimum value", "%shpr:min=\"${1:0}\"" %attrDelim],
                                ["hpr:max\tNumeric maximum value", "%shpr:max=\"${1:999}\"" %attrDelim]
                            ]
                    hprTextAttrs = [
                                ["hpr:cond\tHPR Javascript cond", "%shpr:cond=\"${1:A1.r1.val}\"" %attrDelim],
                                ["hpr:rowCond\tHPR Javascript rowCond", "%shpr:rowCond=\"${1:A1.r1.val}\"" %attrDelim],
                                ["hpr:grid\tQuestion width/layout (1-12) ", "%shpr:grid=\"${1:6}\"" %attrDelim],
                                ["hpr:type\tautosum|date|dropdown|rank", "%shpr:type=\"${1:autosum}\"" %attrDelim],
                                ["hpr:verify\tRegEx validation", "%shpr:verify=\"$0\"" %attrDelim],
                                ["hpr:oeError\tWrong OE input error text", "%shpr:oeError=\"$0\"" %attrDelim],
                                ["hpr:placeholder\tPlaceholder text for input boxes", "%shpr:placeholder=\"${1:Please specify...}\"" %attrDelim],
                                ["hpr:dupesOff\tAllow dupe entries", "%shpr:dupesOff=\"1\"" %attrDelim],
                                ["hpr:validOff\tStop standard AG validation", "%shpr:validOff=\"1\"" %attrDelim],
                                ["hpr:sequenOff\tAllow answers in non-sequential order", "%shpr:sequenOff=\"1\"" %attrDelim]
                            ]
                    hprTextareaAttrs = [
                                ["hpr:cond\tHPR Javascript cond", "%shpr:cond=\"${1:A1.r1.val}\"" %attrDelim],
                                ["hpr:rowCond\tHPR Javascript rowCond", "%shpr:rowCond=\"${1:A1.r1.val}\"" %attrDelim],
                                ["hpr:grid\tQuestion width/layout (1-12) ", "%shpr:grid=\"${1:6}\"" %attrDelim],
                                ["hpr:verify\tRegEx validation", "%shpr:verify=\"$0\"" %attrDelim],
                                ["hpr:oeError\tWrong OE input error text", "%shpr:oeError=\"$0\"" %attrDelim],
                                ["hpr:placeholder\tPlaceholder text for input boxes", "%shpr:placeholder=\"${1:Please specify...}\"" %attrDelim],
                                ["hpr:dupesOff\tAllow dupe entries", "%shpr:dupesOff=\"1\"" %attrDelim],
                                ["hpr:validOff\tStop standard AG validation", "%shpr:validOff=\"1\"" %attrDelim],
                                ["hpr:sequenOff\tAllow answers in non-sequential order", "%shpr:sequenOff=\"1\"" %attrDelim]
                            ]
                    hprDateAttrs = [
                                ["hpr:cond\tHPR Javascript cond", "%shpr:cond=\"${1:A1.r1.val}\"" %attrDelim],
                                ["hpr:rowCond\tHPR Javascript rowCond", "%shpr:rowCond=\"${1:A1.r1.val}\"" %attrDelim],
                                ["hpr:grid\tQuestion width/layout (1-12) ", "%shpr:grid=\"${1:6}\"" %attrDelim],
                                ["hpr:rowTiles\tRow grid columns ", "%shpr:rowTiles=\"${1:1}\"" %attrDelim],
                                ["hpr:type\tautosum|date|dropdown|rank", "%shpr:type=\"${1:autosum}\"" %attrDelim],
                                ["hpr:placeholder\tPlaceholder text for input boxes", "%shpr:placeholder=\"${1:Please specify...}\"" %attrDelim],
                                ["hpr:minDate\tMinimal date allowed", "%shpr:minDate=\"${1:A1.date${2:.months(-12)}}\"" %attrDelim],
                                ["hpr:maxDate\tMaximum date allowed", "%shpr:maxDate=\"${1:today()${2:.years(-18)}}\"" %attrDelim],
                                ["hpr:onlyMY\tPick month & year only, no day", "%shpr:dateMonthYear=\"1\"" %attrDelim],
                                ["hpr:dateFormat\tDate format", "%shpr:dateFormat=\"${1:mm/dd/yyyy}\"" %attrDelim],
                                ["hpr:dateFormatData\tData date format", "%shpr:dateFormatData=\"${1:mm/dd/yyyy}\"" %attrDelim]
                            ]
                    hprRowAttrs = [
                                ["hpr:cond\tHPR Javascript cond", "%shpr:cond=\"${1:A1.r1.val}\"" %attrDelim],
                                ["hpr:verify\tRegEx validation", "%shpr:verify=\"$0\"" %attrDelim],
                                ["hpr:oeError\tWrong OE input error text", "%shpr:oeError=\"$0\"" %attrDelim],
                                ["hpr:placeholder\tPlaceholder text for input boxes", "%shpr:placeholder=\"${1:Please specify...}\"" %attrDelim],
                                ["hpr:dupesOff\tAllow dupe entries", "%shpr:dupesOff=\"1\"" %attrDelim],
                                ["hpr:validOff\tStop standard AG validation", "%shpr:validOff=\"1\"" %attrDelim]
                            ]
                    hprColAttrs = [
                                ["hpr:cond\tHPR Javascript cond", "%shpr:cond=\"${1:A1.r1.val}\"" %attrDelim],
                                ["hpr:verify\tRegEx validation", "%shpr:verify=\"$0\"" %attrDelim],
                                ["hpr:oeError\tWrong OE input error text", "%shpr:oeError=\"$0\"" %attrDelim],
                                ["hpr:placeholder\tPlaceholder text for input boxes", "%shpr:placeholder=\"${1:Please specify...}\"" %attrDelim]
                            ]

                    if ( 'meta.tag.xml' in self.view.scope_name(pt) or 'meta.tag.exec.begin.xml' in self.view.scope_name(pt) or 'meta.tag.noscript.begin.xml' in self.view.scope_name(pt) ) and 'string.quoted.double.xml' not in self.view.scope_name(pt) and 'punctuation.separator.key-value.xml' not in self.view.scope_name(pt):
                        attrCompletions = []
                        tagLine = ''
                        # attribute completions, shown only within tags
                        if surveyTagRegion.begin() < pt and surveyTagRegion.end() > pt:
                            for compl in attrCompls:
                                if compl.startswith(prefix) or (len(prefix) >= 2 and prefix in compl):
                                    attrCompletions += surveyAttrs
                                    break
                        elif '<' not in self.view.substr(self.view.line(pt)):
                            ptLine = self.view.rowcol(pt)[0]
                            for x in range(15):
                                nuPt = self.view.text_point(ptLine-x, 0)
                                if '<' in self.view.substr(self.view.line(nuPt)):
                                    tagLine = self.view.substr(self.view.line(nuPt))
                                    break
                        else:
                            tagLine = self.view.substr(self.view.line(pt))

                        if '<row' in tagLine:
                            for compl in attrCompls:
                                if compl.startswith(prefix) or (len(prefix) >= 2 and prefix in compl):
                                    attrCompletions += rowAttrs
                                    break
                        elif '<col' in tagLine:
                            for compl in attrCompls:
                                if compl.startswith(prefix) or (len(prefix) >= 2 and prefix in compl):
                                    attrCompletions += colAttrs
                                    break
                        elif '<choice' in tagLine:
                            for compl in attrCompls:
                                if compl.startswith(prefix) or (len(prefix) >= 2 and prefix in compl):
                                    attrCompletions += choiceAttrs
                                    break
                        elif '<group' in tagLine:
                            for compl in attrCompls:
                                if compl.startswith(prefix) or (len(prefix) >= 2 and prefix in compl):
                                    attrCompletions += groupAttrs
                                    break
                        elif '<radio' in tagLine:
                            for compl in attrCompls:
                                if compl.startswith(prefix) or (len(prefix) >= 2 and prefix in compl):
                                    attrCompletions += radioAttrs
                                    break
                        elif '<checkbox' in tagLine:
                            for compl in attrCompls:
                                if compl.startswith(prefix) or (len(prefix) >= 2 and prefix in compl):
                                    attrCompletions += checkboxAttrs
                                    break
                        elif '<select' in tagLine:
                            for compl in attrCompls:
                                if compl.startswith(prefix) or (len(prefix) >= 2 and prefix in compl):
                                    attrCompletions += selectAttrs
                                    break
                        elif '<number' in tagLine:
                            for compl in attrCompls:
                                if compl.startswith(prefix) or (len(prefix) >= 2 and prefix in compl):
                                    attrCompletions += numberAttrs
                                    break
                        elif '<float' in tagLine:
                            for compl in attrCompls:
                                if compl.startswith(prefix) or (len(prefix) >= 2 and prefix in compl):
                                    attrCompletions += floatAttrs
                                    break
                        elif '<textarea' in tagLine:
                            for compl in attrCompls:
                                if compl.startswith(prefix) or (len(prefix) >= 2 and prefix in compl):
                                    attrCompletions += textareaAttrs
                                    break
                        elif '<text' in tagLine:
                            for compl in attrCompls:
                                if compl.startswith(prefix) or (len(prefix) >= 2 and prefix in compl):
                                    attrCompletions += textAttrs
                                    break
                        elif '<exec' in tagLine:
                            for compl in attrCompls:
                                if compl.startswith(prefix) or (len(prefix) >= 2 and prefix in compl):
                                    attrCompletions += execAttrs
                                    break
                        elif '<res' in tagLine:
                            for compl in attrCompls:
                                if compl.startswith(prefix) or (len(prefix) >= 2 and prefix in compl):
                                    attrCompletions += resAttrs
                                    break
                        elif '<html' in tagLine:
                            for compl in attrCompls:
                                if compl.startswith(prefix) or (len(prefix) >= 2 and prefix in compl):
                                    attrCompletions += htmlAttrs
                                    break
                        elif '<term' in tagLine:
                            for compl in attrCompls:
                                if compl.startswith(prefix) or (len(prefix) >= 2 and prefix in compl):
                                    attrCompletions += termAttrs
                                    break
                        elif '<suspend' in tagLine:
                            for compl in attrCompls:
                                if compl.startswith(prefix) or (len(prefix) >= 2 and prefix in compl):
                                    attrCompletions += suspendAttrs
                                    break
                        elif '<samplesource' in tagLine:
                            for compl in attrCompls:
                                if compl.startswith(prefix) or (len(prefix) >= 2 and prefix in compl):
                                    attrCompletions += samplesourceAttrs
                                    break
                        elif '<var' in tagLine:
                            for compl in attrCompls:
                                if compl.startswith(prefix) or (len(prefix) >= 2 and prefix in compl):
                                    attrCompletions += varAttrs
                                    break
                        elif '<exit' in tagLine:
                            for compl in attrCompls:
                                if compl.startswith(prefix) or (len(prefix) >= 2 and prefix in compl):
                                    attrCompletions += exitAttrs
                                    break
                        elif '<insert' in tagLine:
                            for compl in attrCompls:
                                if compl.startswith(prefix) or (len(prefix) >= 2 and prefix in compl):
                                    attrCompletions += insertAttrs
                                    break
                        elif '<block' in tagLine:
                            for compl in attrCompls:
                                if compl.startswith(prefix) or (len(prefix) >= 2 and prefix in compl):
                                    attrCompletions += blockAttrs
                                    break
                        else:
                            for compl in attrCompls:
                                if compl.startswith(prefix) or (len(prefix) >= 3 and prefix in compl):
                                    attrCompletions += genAttrs
                                    break

                        # remove mls for Netflix
                        if self.view.settings().get('surveyType') == 'nf' and ["mls\t lang attr", "%smls=\"${1:english}\"" %attrSplitter] in attrCompletions:
                            attrCompletions.remove(["mls\t lang attr", "%smls=\"${1:english}\"" %attrSplitter])

                        if self.view.settings().get('surveyHpr'):
                        # HPR-specific attributes
                            if surveyTagRegion.begin() < pt and surveyTagRegion.end() > pt:
                                for compl in hprCompls:
                                    if compl.startswith(prefix) or (len(prefix) >= 2 and prefix in compl):
                                        attrCompletions += hprSurveyAttrs
                                        break
                            elif '<block' in tagLine:
                                for compl in hprCompls:
                                    if compl.startswith(prefix) or (len(prefix) >= 2 and prefix in compl):
                                        attrCompletions += hprBlockAttrs
                                        break
                            elif '<radio' in tagLine:
                                for compl in hprCompls:
                                    if compl.startswith(prefix) or (len(prefix) >= 2 and prefix in compl):
                                        attrCompletions += hprRadioAttrs
                                        break
                            elif '<checkbox' in tagLine:
                                for compl in hprCompls:
                                    if compl.startswith(prefix) or (len(prefix) >= 2 and prefix in compl):
                                        attrCompletions += hprCheckboxAttrs
                                        break
                            elif '<number' in tagLine:
                                for compl in hprCompls:
                                    if compl.startswith(prefix) or (len(prefix) >= 2 and prefix in compl):
                                        attrCompletions += hprNumberAttrs
                                        break
                            elif '<float' in tagLine:
                                for compl in hprCompls:
                                    if compl.startswith(prefix) or (len(prefix) >= 2 and prefix in compl):
                                        attrCompletions += hprFloatAttrs
                                        break
                            elif '<text ' in tagLine:
                                for compl in hprCompls:
                                    if compl.startswith(prefix) or (len(prefix) >= 2 and prefix in compl):
                                        attrCompletions += hprTextAttrs
                                        break
                            elif '<textarea' in tagLine:
                                for compl in hprCompls:
                                    if compl.startswith(prefix) or (len(prefix) >= 2 and prefix in compl):
                                        attrCompletions += hprTextareaAttrs
                                        break
                            elif '<text' in tagLine and 'hpr:type="date"' in tagLine:
                                for compl in hprCompls:
                                    if compl.startswith(prefix) or (len(prefix) >= 2 and prefix in compl):
                                        attrCompletions += hprDateAttrs
                                        break
                            elif '<row' in tagLine:
                                for compl in hprCompls:
                                    if compl.startswith(prefix) or (len(prefix) >= 2 and prefix in compl):
                                        attrCompletions += hprRowAttrs
                                        break
                            elif '<col' in tagLine:
                                for compl in hprCompls:
                                    if compl.startswith(prefix) or (len(prefix) >= 2 and prefix in compl):
                                        attrCompletions += hprColAttrs
                                        break

                        if attrCompletions:
                            return attrCompletions

                    xmlTags = [
                           # Tags
                            ["quota\ttag", "<quota %ssheet=\"${1:Overall}\" overquota=\"noqual\"/>" %label],
                            ["term\ttag", "<term %scond=\"${1:S}\">${1/^(?:[\s\(]*not[\s\(]*)?(\w[^\s\.\[\(]+).*/$1/}: ${2:$SELECTION}</term>" %label],
                            ["alt\ttag", "<alt>${1:$SELECTION}</alt>"],
                            ["block\ttag", "<block label=\"${1}\" cond=\"${2:1}\">\n${3:$SELECTION}\n</block>"],
                            ["comment\ttag", "<comment>${1:$SELECTION}</comment>"],
                            ["condition\ttag", "<condition label=\"${1}\" cond=\"${2}\"/>"],
                            ["define (reusable list)\ttag", "<define label=\"${1}\">\n${2:$SELECTION}\n</define>"],
                            ["exec\ttag", "<exec>\n${1:$SELECTION}\n</exec>"],
                            ["exit (samplesource)\ttag", "<exit cond=\"${1:terminated}\" url=\"${2}\"${3: timeout=\"10\"}>${4:$SELECTION}$0</exit>"],
                            ["goto\ttag", "<goto target=\"${1:$SELECTION}\" cond=\"${2}\"/>"],
                            ["html (survey comment)\ttag", "<html%slabel=\"${1}${2:_Intro}\"%swhere=\"survey\">\n${3:$SELECTION}\n</html>\n$0<suspend/>" %(delimType, delimType)],
                            ["insert (reusable list)\ttag", "<insert source=\"${1}\"${2: as=\"${3:cols}\"}/>"],
                            ["loop with block\ttag", "<loop label=\"${1}_Loop\" vars=\"${2}\" randomizeChildren=\"0\">\n  <block label=\"$1_Block\" cond=\"${4:${3:QN}.r[loopvar:label]}\">\n\n${5:$SELECTION}\n\n  </block>\n\n</loop>"],
                            ["looprow\ttag", "<looprow label=\"${1:1}\">\n  ${2:$SELECTION}\n</looprow>"],
                            ["loopvar\ttag", "<loopvar name=\"${1}\">${2:$SELECTION}</loopvar>"],
                            ["marker\ttag", "<marker name=\"${1:$SELECTION}\" cond=\"${2}\"/>"],
                            ["note\ttag", "<note>${1:$SELECTION}</note>"],
                            ["style copy\ttag", "<style copy=\"${1:$SELECTION}\"/>"],
                            ["style wrap=\"ready\"\ttag", "<style name=\"question.footer\" label=\"\" wrap=\"ready\" mode=\"after\"><![CDATA[\n  ${1:$SELECTION}\n]]></style>"],
                            ["stylevar\ttag", "<stylevar name=\"cs:${1:$SELECTION}\" type=\"${2:string}\"/>"],
                            ["suspend\ttag", "<suspend/>"],
                            ["suspend url\ttag", "<suspend url=\"${1:https://${2}}\" sst=\"0\"/>"],
                            ["block randomizeChildren\ttag", "<block label=\"${1}\" cond=\"${2:1}\" randomizeChildren=\"1\">\n${3:$SELECTION}\n</block>"],
                            ["res\ttag", "<res label=\"${1}\">${2:$SELECTION}</res>"],
                            ["title\ttag", "<title>${1:$SELECTION}</title>"],
                            ["validate\ttag", "<validate>\n${1:$SELECTION}\n</validate>"],
                            ["validateCell\ttag", "<validateCell>\n${1:if data${2}:\n    $0}\n</validateCell>"],
                            ["validateCol\ttag", "<validateCol>\n${1:if col.data[0]${2}:\n    $0}\n</validateCol>"],
                            ["validateRow\ttag", "<validateRow>\n${1:if row.data[0]${2}:\n    $0}\n</validateRow>"],
                            ["var (samplesource)\ttag", "<var name=\"${1:$SELECTION}\"${2: unique=\"1\"} required=\"${3:1}\"/>"],
                            ["virtual\ttag", "<virtual>\n${1:$SELECTION}\n</virtual>"],
                           # Text
                            ["br\tbreak row", "<br/>"],
                            ["br2\tempty row", "<br/><br/>"],
                            ["a href\thyperlink", "<a href=\"${1:http://}\" target=\"_blank\">${2:$1}</a>"],
                            ["img\t(responsive image)", "<div style=\"max-width: 900px\">\n  <img src=\"[rel ${1:$SELECTION}]\" style=\"max-width: 100%; height: auto;display:block;\"/>\n</div>"],
                            ["popup\tresponsive", "<a href=\"#popup-${1:1}\" class=\"popup-link\">${2:here}</a>\n<div id=\"popup-$1\" class=\"popup mfp-hide\" style=\"max-width:900px\">\n${3:$SELECTION}\n</div>"],
                            ["popup\tdecipher", "<a href=\"javascript:void(0)\" onclick=\"Survey.uidialog.make(\\$ (this).next('div').clone(), {width: 600, height: 400, title: ''} );\" style=\"color:blue;\">${1:here}</a>\n<div style=\"display: none;\">${2:$SELECTION}</div>"],
                            ["span", "<span style=\"${1:color:${2};}\">${3:$SELECTION}</span>"],
                            ["div", "<div${2: class=\"${1}\"}>${3:$SELECTION}</div>"],
                            ["td\ttable cell", "<td${2: class=\"$0\"}>${1:$SELECTION}</td>"],
                            ["tr\ttable row", "<tr>\n\t${1:$SELECTION}\n</tr>"],
                            ["table\ttable html", "<table${2: class=\"${1}\"}>\n${3:$SELECTION}$0\n</table>"],
                            ["li\tlist item (bullet)", "<li>${1:$SELECTION}</li>"],
                            ["ul\tunordered list (bullets)", "<ul>\n${1:$SELECTION}\n</ul>"],
                            ["ol\tordered list (numbers)", "<ol>\n${1:$SELECTION}\n</ol>"],
                            ["sup\tsuperscript text", "<sup>${1:$SELECTION}</sup>"]
                    ]
                    hprTags = [
                           # HPR Tags
                            ["block (HPR page)\ttag", "<block label=\"${1}\"${2: hpr:cond=\"${3:1}\"}${4: randomizeChildren=\"1\"}>\n${5:$SELECTION}\n</block>"],
                            ["block (HPR section)\ttag", "<block label=\"${1}\" hpr:section=\"1\"${3: hpr:cond=\"${2:1}\"}${4: randomize=\"1\"}>\n${5:$SELECTION}\n</block>"],
                            ["hpr:validate\ttag", "<hpr:validate>\n${1:$SELECTION}\n</hpr:validate>"],
                            # ["hpr:exec\ttag", "<hpr:exec>\n${1:$SELECTION}\n</hpr:exec>"]
                    ]

                    if 'meta.tag.xml' not in self.view.scope_name(pt) and 'string.unquoted.cdata.xml' not in self.view.scope_name(pt) and not ('meta.tag.' in self.view.scope_name(pt) and '.xml' in self.view.scope_name(pt)) and 'source.python' not in self.view.scope_name(pt) and 'source.js' not in self.view.scope_name(pt) and 'source.css' not in self.view.scope_name(pt):
                    # Tag completions
                        if self.view.settings().get('surveyHpr'):
                            for compl in tagCompls:
                                if compl.startswith(prefix) or (len(prefix) >= 2 and prefix in compl):
                                    return xmlTags + hprTags
                        else:
                            for compl in tagCompls:
                                if compl.startswith(prefix) or (len(prefix) >= 2 and prefix in compl):
                                    return xmlTags

                    pyDotCompletions = [
                                [".val", ".val"],
                                [".ival", ".ival"],
                                [".open", ".open"],
                                [".any", ".any"],
                                [".sum", ".sum"],
                                [".displayed", ".displayed"],
                                [".styles", ".styles."],
                                [".index", ".index"],
                                [".label", ".label"],
                                [".group", ".group."],
                                [".value", ".value"],
                                [".count", ".count"],
                                [".all", ".all"],
                                [".value", ".value"],
                                [".selected", ".selected."],
                                [".text", ".text"],
                                [".order", ".order"],
                                [".attr", ".attr(${1})"],
                                [".but", ".but(${1:$SELECTION}${1})$0"],
                                [".check", ".check(${1})"],
                                [".rows", ".rows${2:[${1}]}"],
                                [".cols", ".cols${2:[${1}]}"]
                            ]
                    pyFunCompletions = [
                                ["eachRow", "eachRow"],
                                ["eachCol", "eachCol"],
                                ["allQuestions\t  decipher fn", "allQuestions[${1:'${2:Q1_}%s' % ${3:eachRow.label[1:]}}]"], # access all questions
                                ["getattr\t  decipher fn", "getattr(${1:Q1}, ${3:'r%s' %${2:str(x)}})"], # access question element
                                ["assignRandomOrder\t  decipher fn", "assignRandomOrder('${1:Q1_Block}','${2:children}')"], # punch order in virtual
                                ["for eachRow\t  loop rows", "for eachRow in ${1}.rows:\n\t"], # iterate question rows
                                ["for eachCol\t  loop cols", "for eachCol in ${1}.cols:\n\t"], # iterate question cols
                                ["for range()\t  loop range", "for ${1:ir} in range(len(${2})):\n\t"], # iterate a number range
                                ["hasMarker\t  decipher fn", "hasMarker('${1:/Overall/}')"], # check if marker exists
                                ["setMarker\t  decipher fn", "setMarker('${1:}')"], # set new marker
                                ["removeMarker\t  decipher fn", "removeMarker('${1:}')"], # clear existing marker
                                ["getMarker\t  decipher fn", "getMarker('${1:}')"], # get marker count (quota)
                                ["setLanguage\t  decipher fn", "setLanguage('${1}')"], # set survey language
                                ["cells\t  decipher fn", "cells = gv.survey.root.quota.getQuotaCells()"], # get all quota counts - current, total, oq
                                ["getQuotaCells\t  decipher fn", "cells = gv.survey.root.quota.getQuotaCells()"], # get all quota counts - current, total, oq
                                ["stoppedCells\t  decipher fn", "stopped = gv.survey.root.quota.stoppedCells"], # get stopped quotas
                                ["random shuffle\t  fn", "random.shuffle(${1})"], # randomize a list
                                ["error()\t  decipher fn", "error(${1:res.${2}})"], # throw an error in validate
                                ["hlang\t  decipher fn", "hlang.get('${1}')"], # get system message
                                ["timeSpent\t  decipher fn", "timeSpent() $0"], # time spent in survey (sec.)
                                ["setExtra\t  decipher fn", "setExtra('${1:link_var}', '${2:value}')"], # set value to a link variable
                                ["x for x\t  loop-to-list", "[$1 for ${1:eachR} in ${2}.rows${3: if $1}$0]"], # 1-line loop that creates a list
                                ["File()\t  decipher fn", "${1:dataFile} = File(\"${2:include.dat}\", \"${3:source}\")$0]"], # load a tab-delimited file
                                ["ishuffle\t  decipher fn", "ishuffle(${1:dataFile})"], # ishuffle() command returns a shuffled copy of the list
                                ["group_rows\t  keep rows together", "group_rows(${1:Q3}, ['r${2:5}','r${3:6}'${4:,'r${5:7}'}])"] # keep elements together during shuffle (must be defined)
                            ]

                    if 'source.python.embedded.xml' in self.view.scope_name(pt):
                        pyCompletions = pyFunCompletions + pyDotCompletions
                    # Python completions, shown only within execs/validates/virtuals
                        for compl in pyCompls:
                            if compl.startswith(prefix) or (len(prefix) >= 2 and prefix in compl):
                                return pyCompletions

        except Exception as e:
            self.view.run_command('show_status_err', {"on_command": "eventChecker.on_query_completions", "error_msg": str(e)})
            print (e)
