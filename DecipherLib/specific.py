import sublime, sublime_plugin, sys, re, datetime
try:
    from .Edit import Edit as Edit
except:
    from Edit import Edit as Edit

# clean whitespace
def cleanInput(input):
    #CLEAN UP THE TABS
    input = re.sub("\t+", " ", input)
    #CLEAN UP SPACES
    input = re.sub("\n +\n", "\n\n", input)
    #CLEAN UP THE EXTRA LINE BREAKS
    input = re.sub("\n{2,}", "\n", input)
    return input

def getAttrDelimiter(self):
    if self.view.settings().get('surveyType') in ['v3']:
        delmiter = " "
    else:
        delmiter = "\n  "
    return delmiter

class kantarOnly(sublime_plugin.TextCommand):
    def is_visible(self):
        return self.view.settings().get('surveyType') in ['v3', 'v2', 'AG', 'AG0', None] or True
    def run (self, edit):
        pass

class qartsOnly(sublime_plugin.TextCommand):
    def is_visible(self):
        return bool( self.view.settings().get('surveyQarts') or self.view.settings().get('useQarts') ) or True
    def run (self, edit):
        pass

class menuGone(sublime_plugin.TextCommand):
    def is_visible(self):
        return False
        # self.set_menu_visible(False);
    def run (self, edit):
        pass


######################## US QUESTIONS #########################

class usStatesDivRecode(sublime_plugin.TextCommand):
    def run (self,edit):
        try:
            attrDelim = getAttrDelimiter(self)
            qartsV = self.view.settings().get('qartsVersion')
            sels = self.view.sel()
            input = ''
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel).strip()

                #isolate label and the rest
                try:
                    label = re.split(r"^((?:\w|-(?=\w)|\.(?=\w))+(?:\[loopvar:\s*\w+\]|\$\(\w+\))?(?:\w|-(?=\w)|\.(?=\w))*)(\.|-|:|\)|_?\s)", input, 1)[1].replace('-','_').replace('.','_')
                    input = re.split(r"^((?:\w|-(?=\w)|\.(?=\w))+(?:\[loopvar:\s*\w+\]|\$\(\w+\))?(?:\w|-(?=\w)|\.(?=\w))*)(\.|-|:|\)|_?\s)", input, 1)[-1]
                except:
                    label = "QState"

                input = cleanInput(input)

                #Add a q if the first character is a digit
                if label[0].isdigit():
                    label = "Q" + label

                title = input if len(input) > 1 else "In which state do you live?"

                if self.view.settings().get('useQarts'):
                    questionType = "radio"
                    specificStyles = """{1}qa:atype="text"{1}qa:tool="rp"{1}uses="qarts.{0}\"""".format(qartsV, attrDelim)
                    elementType = "row"
                    elementLabel = "r"
                else:
                    questionType = "select"
                    specificStyles = """{0}optional="0\"""".format(attrDelim)
                    elementType = "choice"
                    elementLabel = "ch"

                #Add the states
                stateChoices = """
  <{etype} label="{lbl}1" cs:division="r6" cs:region="r3">Alabama</{etype}>
  <{etype} label="{lbl}2" cs:division="r9" cs:region="r4">Alaska</{etype}>
  <{etype} label="{lbl}3" cs:division="r8" cs:region="r4">Arizona</{etype}>
  <{etype} label="{lbl}4" cs:division="r7" cs:region="r3">Arkansas</{etype}>
  <{etype} label="{lbl}5" cs:division="r9" cs:region="r4">California</{etype}>
  <{etype} label="{lbl}6" cs:division="r8" cs:region="r4">Colorado</{etype}>
  <{etype} label="{lbl}7" cs:division="r1" cs:region="r1">Connecticut</{etype}>
  <{etype} label="{lbl}8" cs:division="r5" cs:region="r3">Delaware</{etype}>
  <{etype} label="{lbl}9" cs:division="r5" cs:region="r3">District of Columbia</{etype}>
  <{etype} label="{lbl}10" cs:division="r5" cs:region="r3">Florida</{etype}>
  <{etype} label="{lbl}11" cs:division="r5" cs:region="r3">Georgia</{etype}>
  <{etype} label="{lbl}12" cs:division="r9" cs:region="r4">Hawaii</{etype}>
  <{etype} label="{lbl}13" cs:division="r8" cs:region="r4">Idaho</{etype}>
  <{etype} label="{lbl}14" cs:division="r3" cs:region="r2">Illinois</{etype}>
  <{etype} label="{lbl}15" cs:division="r3" cs:region="r2">Indiana</{etype}>
  <{etype} label="{lbl}16" cs:division="r4" cs:region="r2">Iowa</{etype}>
  <{etype} label="{lbl}17" cs:division="r4" cs:region="r2">Kansas</{etype}>
  <{etype} label="{lbl}18" cs:division="r6" cs:region="r3">Kentucky</{etype}>
  <{etype} label="{lbl}19" cs:division="r7" cs:region="r3">Louisiana</{etype}>
  <{etype} label="{lbl}20" cs:division="r1" cs:region="r1">Maine</{etype}>
  <{etype} label="{lbl}21" cs:division="r5" cs:region="r3">Maryland</{etype}>
  <{etype} label="{lbl}22" cs:division="r1" cs:region="r1">Massachusetts</{etype}>
  <{etype} label="{lbl}23" cs:division="r3" cs:region="r2">Michigan</{etype}>
  <{etype} label="{lbl}24" cs:division="r4" cs:region="r2">Minnesota</{etype}>
  <{etype} label="{lbl}25" cs:division="r6" cs:region="r3">Mississippi</{etype}>
  <{etype} label="{lbl}26" cs:division="r4" cs:region="r2">Missouri</{etype}>
  <{etype} label="{lbl}27" cs:division="r8" cs:region="r4">Montana</{etype}>
  <{etype} label="{lbl}28" cs:division="r4" cs:region="r2">Nebraska</{etype}>
  <{etype} label="{lbl}29" cs:division="r8" cs:region="r4">Nevada</{etype}>
  <{etype} label="{lbl}30" cs:division="r1" cs:region="r1">New Hampshire</{etype}>
  <{etype} label="{lbl}31" cs:division="r2" cs:region="r1">New Jersey</{etype}>
  <{etype} label="{lbl}32" cs:division="r8" cs:region="r4">New Mexico</{etype}>
  <{etype} label="{lbl}33" cs:division="r2" cs:region="r1">New York</{etype}>
  <{etype} label="{lbl}34" cs:division="r5" cs:region="r3">North Carolina</{etype}>
  <{etype} label="{lbl}35" cs:division="r4" cs:region="r2">North Dakota</{etype}>
  <{etype} label="{lbl}36" cs:division="r3" cs:region="r2">Ohio</{etype}>
  <{etype} label="{lbl}37" cs:division="r7" cs:region="r3">Oklahoma</{etype}>
  <{etype} label="{lbl}38" cs:division="r9" cs:region="r4">Oregon</{etype}>
  <{etype} label="{lbl}39" cs:division="r2" cs:region="r1">Pennsylvania</{etype}>
  <{etype} label="{lbl}40" cs:division="r1" cs:region="r1">Rhode Island</{etype}>
  <{etype} label="{lbl}41" cs:division="r5" cs:region="r3">South Carolina</{etype}>
  <{etype} label="{lbl}42" cs:division="r4" cs:region="r2">South Dakota</{etype}>
  <{etype} label="{lbl}43" cs:division="r6" cs:region="r3">Tennessee</{etype}>
  <{etype} label="{lbl}44" cs:division="r7" cs:region="r3">Texas</{etype}>
  <{etype} label="{lbl}45" cs:division="r8" cs:region="r4">Utah</{etype}>
  <{etype} label="{lbl}46" cs:division="r1" cs:region="r1">Vermont</{etype}>
  <{etype} label="{lbl}47" cs:division="r5" cs:region="r3">Virginia</{etype}>
  <{etype} label="{lbl}48" cs:division="r9" cs:region="r4">Washington</{etype}>
  <{etype} label="{lbl}49" cs:division="r5" cs:region="r3">West Virginia</{etype}>
  <{etype} label="{lbl}50" cs:division="r3" cs:region="r2">Wisconsin</{etype}>
  <{etype} label="{lbl}51" cs:division="r8" cs:region="r4">Wyoming</{etype}>""".format(etype=elementType, lbl=elementLabel)

                printP1 = """<stylevar name="cs:region" type="string"/>
<stylevar name="cs:division" type="string"/>

<{qType}{delim}label="{lbl}"{surveyTypeSpecific}>
  <title>{title}</title>{elems}
</{qType}>
<suspend/>""".format(lbl=label.strip(), title=title.strip(), qType=questionType, surveyTypeSpecific=specificStyles, elems=stateChoices, delim=attrDelim)
                
                printP2 = """
<radio{delim}label="{lbl}_Region"{delim}optional="1"{delim}where="execute,survey,report">
  <title>Hidden: State to Region recode</title>
  <exec>
if {lbl}.any:
    {lbl}_Region.val = {lbl}_Region.attr({lbl}.selected.styles.cs.region).index
  </exec>
  <row label="r1">Northeast</row>
  <row label="r2">Midwest</row>
  <row label="r3">South</row>
  <row label="r4">West</row>
</radio>

<radio{delim}label="{lbl}_Division"{delim}optional="1"{delim}where="execute,survey,report">
  <title>Hidden: State to Division recode</title>
  <exec>
if {lbl}.any:
    {lbl}_Division.val = {lbl}_Division.attr({lbl}.selected.styles.cs.division).index
  </exec>
  <row label="r1">New England</row>
  <row label="r2">Middle Atlantic</row>
  <row label="r3">East North Central</row>
  <row label="r4">West North Central</row>
  <row label="r5">South Atlantic</row>
  <row label="r6">East South Central</row>
  <row label="r7">West South Central</row>
  <row label="r8">Mountain</row>
  <row label="r9">Pacific</row>
</radio>

<suspend/>""".format(lbl=label.strip(), delim=attrDelim)
                printPage = printP1 + printP2

                self.view.replace(edit,sel, printPage)
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)

class usStatesRecode(sublime_plugin.TextCommand):
    def run (self,edit):
        try:
            attrDelim = getAttrDelimiter(self)
            qartsV = self.view.settings().get('qartsVersion')
            sels = self.view.sel()
            input = ''
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel).strip()

                #isolate label and the rest
                try:
                    label = re.split(r"^((?:\w|-(?=\w)|\.(?=\w))+(?:\[loopvar:\s*\w+\]|\$\(\w+\))?(?:\w|-(?=\w)|\.(?=\w))*)(\.|-|:|\)|_?\s)", input, 1)[1].replace('-','_').replace('.','_')
                    input = re.split(r"^((?:\w|-(?=\w)|\.(?=\w))+(?:\[loopvar:\s*\w+\]|\$\(\w+\))?(?:\w|-(?=\w)|\.(?=\w))*)(\.|-|:|\)|_?\s)", input, 1)[-1]
                except:
                    label = "QState"
                input = cleanInput(input)

                #Add a q if the first character is a digit
                if label[0].isdigit():
                    label = "Q" + label

                title = input if len(input) > 1 else "In which state do you live?"

                if self.view.settings().get('useQarts'):
                    questionType = "radio"
                    specificStyles = """{1}qa:atype="text"{1}qa:tool="rp"{1}uses="qarts.{0}\"""".format(qartsV, attrDelim)
                    elementType = "row"
                    elementLabel = "r"
                else:
                    questionType = "select"
                    specificStyles = """{0}optional="0\"""".format(attrDelim)
                    elementType = "choice"
                    elementLabel = "ch"



                #Add the states
                stateChoices = """
  <{etype} label="{lbl}1" cs:region="r3">Alabama</{etype}>
  <{etype} label="{lbl}2" cs:region="r4">Alaska</{etype}>
  <{etype} label="{lbl}3" cs:region="r4">Arizona</{etype}>
  <{etype} label="{lbl}4" cs:region="r3">Arkansas</{etype}>
  <{etype} label="{lbl}5" cs:region="r4">California</{etype}>
  <{etype} label="{lbl}6" cs:region="r4">Colorado</{etype}>
  <{etype} label="{lbl}7" cs:region="r1">Connecticut</{etype}>
  <{etype} label="{lbl}8" cs:region="r3">Delaware</{etype}>
  <{etype} label="{lbl}9" cs:region="r3">District of Columbia</{etype}>
  <{etype} label="{lbl}10" cs:region="r3">Florida</{etype}>
  <{etype} label="{lbl}11" cs:region="r3">Georgia</{etype}>
  <{etype} label="{lbl}12" cs:region="r4">Hawaii</{etype}>
  <{etype} label="{lbl}13" cs:region="r4">Idaho</{etype}>
  <{etype} label="{lbl}14" cs:region="r2">Illinois</{etype}>
  <{etype} label="{lbl}15" cs:region="r2">Indiana</{etype}>
  <{etype} label="{lbl}16" cs:region="r2">Iowa</{etype}>
  <{etype} label="{lbl}17" cs:region="r2">Kansas</{etype}>
  <{etype} label="{lbl}18" cs:region="r3">Kentucky</{etype}>
  <{etype} label="{lbl}19" cs:region="r3">Louisiana</{etype}>
  <{etype} label="{lbl}20" cs:region="r1">Maine</{etype}>
  <{etype} label="{lbl}21" cs:region="r3">Maryland</{etype}>
  <{etype} label="{lbl}22" cs:region="r1">Massachusetts</{etype}>
  <{etype} label="{lbl}23" cs:region="r2">Michigan</{etype}>
  <{etype} label="{lbl}24" cs:region="r2">Minnesota</{etype}>
  <{etype} label="{lbl}25" cs:region="r3">Mississippi</{etype}>
  <{etype} label="{lbl}26" cs:region="r2">Missouri</{etype}>
  <{etype} label="{lbl}27" cs:region="r4">Montana</{etype}>
  <{etype} label="{lbl}28" cs:region="r2">Nebraska</{etype}>
  <{etype} label="{lbl}29" cs:region="r4">Nevada</{etype}>
  <{etype} label="{lbl}30" cs:region="r1">New Hampshire</{etype}>
  <{etype} label="{lbl}31" cs:region="r1">New Jersey</{etype}>
  <{etype} label="{lbl}32" cs:region="r4">New Mexico</{etype}>
  <{etype} label="{lbl}33" cs:region="r1">New York</{etype}>
  <{etype} label="{lbl}34" cs:region="r3">North Carolina</{etype}>
  <{etype} label="{lbl}35" cs:region="r2">North Dakota</{etype}>
  <{etype} label="{lbl}36" cs:region="r2">Ohio</{etype}>
  <{etype} label="{lbl}37" cs:region="r3">Oklahoma</{etype}>
  <{etype} label="{lbl}38" cs:region="r4">Oregon</{etype}>
  <{etype} label="{lbl}39" cs:region="r1">Pennsylvania</{etype}>
  <{etype} label="{lbl}40" cs:region="r1">Rhode Island</{etype}>
  <{etype} label="{lbl}41" cs:region="r3">South Carolina</{etype}>
  <{etype} label="{lbl}42" cs:region="r2">South Dakota</{etype}>
  <{etype} label="{lbl}43" cs:region="r3">Tennessee</{etype}>
  <{etype} label="{lbl}44" cs:region="r3">Texas</{etype}>
  <{etype} label="{lbl}45" cs:region="r4">Utah</{etype}>
  <{etype} label="{lbl}46" cs:region="r1">Vermont</{etype}>
  <{etype} label="{lbl}47" cs:region="r3">Virginia</{etype}>
  <{etype} label="{lbl}48" cs:region="r4">Washington</{etype}>
  <{etype} label="{lbl}49" cs:region="r3">West Virginia</{etype}>
  <{etype} label="{lbl}50" cs:region="r2">Wisconsin</{etype}>
  <{etype} label="{lbl}51" cs:region="r4">Wyoming</{etype}>""".format(etype=elementType, lbl=elementLabel)

                printP1 = """<stylevar name="cs:region" type="string"/>

<{qType}{delim}label="{lbl}"{surveyTypeSpecific}>
  <title>{title}</title>{elems}
</{qType}>
<suspend/>""".format(lbl=label.strip(), title=title.strip(), qType=questionType, surveyTypeSpecific=specificStyles, elems=stateChoices, delim=attrDelim)


                printP2 = """
<radio{delim}label="{lbl}_Region"{delim}optional="1"{delim}where="execute,survey,report">
  <title>Hidden: State to Region recode</title>
  <exec>
if {lbl}.any:
    {lbl}_Region.val = {lbl}_Region.attr({lbl}.selected.styles.cs.region).index
  </exec>
  <row label="r1">Northeast (ME, NH, VT, MA, RI, CT, NY, NJ, PA)</row>
  <row label="r2">Midwest (WI, IL, MI, IN, OH, ND, SD, NE, KS, MN, IA, MO)</row>
  <row label="r3">South (KY, TN, MS, AL, FL, GA, SC, NC, VA, WV, DC, MD, DE, TX, OK, AR, LA)</row>
  <row label="r4">West (MT, ID, WY, NV, UT, CO, AZ, NM, WA, OR, CA, AK, HI)</row>
</radio>""".format(lbl=label.strip(), delim=attrDelim)
                printPage = printP1 + printP2

                self.view.replace(edit,sel, printPage)
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)

class makeZipUs(sublime_plugin.TextCommand):
    def run (self,edit):
        try:
            attrDelim = getAttrDelimiter(self)
            qartsV = self.view.settings().get('qartsVersion')
            sels = self.view.sel()
            input = ''
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel).strip()

                #isolate label and the rest
                try:
                    label = re.split(r"^((?:\w|-(?=\w)|\.(?=\w))+(?:\[loopvar:\s*\w+\]|\$\(\w+\))?(?:\w|-(?=\w)|\.(?=\w))*)(\.|-|:|\)|_?\s)", input, 1)[1].replace('-','_').replace('.','_')
                    input = re.split(r"^((?:\w|-(?=\w)|\.(?=\w))+(?:\[loopvar:\s*\w+\]|\$\(\w+\))?(?:\w|-(?=\w)|\.(?=\w))*)(\.|-|:|\)|_?\s)", input, 1)[-1]
                except:
                    label = "QZip"
                input = cleanInput(input)

                #Add a q if the first character is a digit
                if label[0].isdigit():
                    label = "Q" + label

                title = input if len(input) > 1 else "What is your zip code?"

                if self.view.settings().get('useQarts'):
                    specificStyles = """ qa:atype="oe-short" qa:tool="rp" uses="qarts.{0}" qa:validation="zipcode" qa:rerror="Enter valid zipcode" qa:rplaceholder="Enter 5-digit zipcode\"""".format(qartsV)
                else:
                    specificStyles = ""


                if self.view.settings().get('surveyType') in ['v3', 'v2', 'AG', 'AG0']:
                    initExec = """<exec when="init">
#Reading the file in text mode and uzing a Dictionary for performance.
import csv
zipDict = {}

with open('/home/gmi/v2/static/local/bor/ZipcodesUS.csv', 'r') as d:
    dataRef = csv.reader(d, delimiter=',')
    for dRow in dataRef:
        zipDict[dRow[0]] = dRow[1],dRow[3]

def ZipValidate(qID):
    ansZip = qID.val
    ansZip = str(ansZip)
    if not (ansZip in zipDict):
        error(res.ZipError)

def ZipRecodeState(qID_zip, qID_USState):
    ansZip = qID_zip.val
    ansZip = str(ansZip)
    if ansZip in zipDict:
        tempUSstate = zipDict[ansZip][0]
        for eachState in qID_USState.rows:
            if tempUSstate == eachState.styles.cs.stateAbr:
                qID_USState.val = eachState.index
                break
</exec>"""

                else:
                    initExec = """<exec when="init">
zipcodes = File("int/zipcode/zip.txt", "ZipCode")

def ZipValidate(qID):
    thisZipcode = zipcodes.get(qID.val) 
    if not thisZipcode: 
        error(res.ZipError)

def ZipRecodeState(qID_zip, qID_USState):
    zipData = zipcodes.get(qID_zip.val)
    if zipData:
        tempUSstate = zipData['state']
        for eachState in qID_USState.rows:
            if tempUSstate == eachState.styles.cs.stateAbr:
                qID_USState.val = eachState.index
                break
</exec>

<stylevar name="cs:region"/>
<stylevar name="cs:division"/>
<stylevar name="cs:stateAbr"/>
<res label="ZipError">Enter a valid zipcode.</res>"""



                printPage = """{exec}


<text{delim}label="{lbl}"{delim}optional="0"{delim}size="40"{delim}verify="zipcode">
  <title>{title}</title>
  <validate>
ZipValidate({lbl})
  </validate>
</text>
<suspend/>

<radio{delim}label="{lbl}_State"{delim}optional="1"{delim}where="execute,survey,report">
  <title>Hidden: Zip to State recode</title>
  <exec>
ZipRecodeState({lbl},{lbl}_State)
  </exec>
  <row label="r1" cs:division="r6" cs:region="r3" cs:stateAbr="AL">Alabama</row>
  <row label="r2" cs:division="r9" cs:region="r4" cs:stateAbr="AK">Alaska</row>
  <row label="r3" cs:division="r8" cs:region="r4" cs:stateAbr="AZ">Arizona</row>
  <row label="r4" cs:division="r7" cs:region="r3" cs:stateAbr="AR">Arkansas</row>
  <row label="r5" cs:division="r9" cs:region="r4" cs:stateAbr="CA">California</row>
  <row label="r6" cs:division="r8" cs:region="r4" cs:stateAbr="CO">Colorado</row>
  <row label="r7" cs:division="r1" cs:region="r1" cs:stateAbr="CT">Connecticut</row>
  <row label="r8" cs:division="r5" cs:region="r3" cs:stateAbr="DE">Delaware</row>
  <row label="r9" cs:division="r5" cs:region="r3" cs:stateAbr="DC">District of Columbia</row>
  <row label="r10" cs:division="r5" cs:region="r3" cs:stateAbr="FL">Florida</row>
  <row label="r11" cs:division="r5" cs:region="r3" cs:stateAbr="GA">Georgia</row>
  <row label="r12" cs:division="r9" cs:region="r4" cs:stateAbr="HI">Hawaii</row>
  <row label="r13" cs:division="r8" cs:region="r4" cs:stateAbr="ID">Idaho</row>
  <row label="r14" cs:division="r3" cs:region="r2" cs:stateAbr="IL">Illinois</row>
  <row label="r15" cs:division="r3" cs:region="r2" cs:stateAbr="IN">Indiana</row>
  <row label="r16" cs:division="r4" cs:region="r2" cs:stateAbr="IA">Iowa</row>
  <row label="r17" cs:division="r4" cs:region="r2" cs:stateAbr="KS">Kansas</row>
  <row label="r18" cs:division="r6" cs:region="r3" cs:stateAbr="KY">Kentucky</row>
  <row label="r19" cs:division="r7" cs:region="r3" cs:stateAbr="LA">Louisiana</row>
  <row label="r20" cs:division="r1" cs:region="r1" cs:stateAbr="ME">Maine</row>
  <row label="r21" cs:division="r5" cs:region="r3" cs:stateAbr="MD">Maryland</row>
  <row label="r22" cs:division="r1" cs:region="r1" cs:stateAbr="MA">Massachusetts</row>
  <row label="r23" cs:division="r3" cs:region="r2" cs:stateAbr="MI">Michigan</row>
  <row label="r24" cs:division="r4" cs:region="r2" cs:stateAbr="MN">Minnesota</row>
  <row label="r25" cs:division="r6" cs:region="r3" cs:stateAbr="MS">Mississippi</row>
  <row label="r26" cs:division="r4" cs:region="r2" cs:stateAbr="MO">Missouri</row>
  <row label="r27" cs:division="r8" cs:region="r4" cs:stateAbr="MT">Montana</row>
  <row label="r28" cs:division="r4" cs:region="r2" cs:stateAbr="NE">Nebraska</row>
  <row label="r29" cs:division="r8" cs:region="r4" cs:stateAbr="NV">Nevada</row>
  <row label="r30" cs:division="r1" cs:region="r1" cs:stateAbr="NH">New Hampshire</row>
  <row label="r31" cs:division="r2" cs:region="r1" cs:stateAbr="NJ">New Jersey</row>
  <row label="r32" cs:division="r8" cs:region="r4" cs:stateAbr="NM">New Mexico</row>
  <row label="r33" cs:division="r2" cs:region="r1" cs:stateAbr="NY">New York</row>
  <row label="r34" cs:division="r5" cs:region="r3" cs:stateAbr="NC">North Carolina</row>
  <row label="r35" cs:division="r4" cs:region="r2" cs:stateAbr="ND">North Dakota</row>
  <row label="r36" cs:division="r3" cs:region="r2" cs:stateAbr="OH">Ohio</row>
  <row label="r37" cs:division="r7" cs:region="r3" cs:stateAbr="OK">Oklahoma</row>
  <row label="r38" cs:division="r9" cs:region="r4" cs:stateAbr="OR">Oregon</row>
  <row label="r39" cs:division="r2" cs:region="r1" cs:stateAbr="PA">Pennsylvania</row>
  <row label="r40" cs:division="r1" cs:region="r1" cs:stateAbr="RI">Rhode Island</row>
  <row label="r41" cs:division="r5" cs:region="r3" cs:stateAbr="SC">South Carolina</row>
  <row label="r42" cs:division="r4" cs:region="r2" cs:stateAbr="SD">South Dakota</row>
  <row label="r43" cs:division="r6" cs:region="r3" cs:stateAbr="TN">Tennessee</row>
  <row label="r44" cs:division="r7" cs:region="r3" cs:stateAbr="TX">Texas</row>
  <row label="r45" cs:division="r8" cs:region="r4" cs:stateAbr="UT">Utah</row>
  <row label="r46" cs:division="r1" cs:region="r1" cs:stateAbr="VT">Vermont</row>
  <row label="r47" cs:division="r5" cs:region="r3" cs:stateAbr="VA">Virginia</row>
  <row label="r48" cs:division="r9" cs:region="r4" cs:stateAbr="WA">Washington</row>
  <row label="r49" cs:division="r5" cs:region="r3" cs:stateAbr="WV">West Virginia</row>
  <row label="r50" cs:division="r3" cs:region="r2" cs:stateAbr="WI">Wisconsin</row>
  <row label="r51" cs:division="r8" cs:region="r4" cs:stateAbr="WY">Wyoming</row>
</radio>

<suspend/>

<radio{delim}label="{lbl}_Region"{delim}optional="1"{delim}where="execute,survey,report">
  <title>Hidden: State to Region recode</title>
  <exec>
if {lbl}_State.any:
    {lbl}_Region.val = {lbl}_Region.attr({lbl}_State.selected.styles.cs.region).index
  </exec>
  <row label="r1">Northeast</row>
  <row label="r2">Midwest</row>
  <row label="r3">South</row>
  <row label="r4">West</row>
</radio>

<radio{delim}label="{lbl}_Division"{delim}optional="1"{delim}where="execute,survey,report">
  <title>Hidden: State to Division recode</title>
  <exec>
if {lbl}_State.any:
    {lbl}_Division.val = {lbl}_Division.attr({lbl}_State.selected.styles.cs.division).index
  </exec>
  <row label="r1">New England</row>
  <row label="r2">Middle Atlantic</row>
  <row label="r3">East North Central</row>
  <row label="r4">West North Central</row>
  <row label="r5">South Atlantic</row>
  <row label="r6">East South Central</row>
  <row label="r7">West South Central</row>
  <row label="r8">Mountain</row>
  <row label="r9">Pacific</row>
</radio>

<suspend/>
""".format(lbl=label.strip(), title=title.strip(), exec=initExec, delim=attrDelim)

                self.view.replace(edit,sel, printPage)
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)

######################## EU QUESTIONS #########################

class makeZipFr(sublime_plugin.TextCommand):
    def run (self,edit):
        try:
            attrDelim = getAttrDelimiter(self)
            qartsV = self.view.settings().get('qartsVersion')
            sels = self.view.sel()
            input = ''
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel).strip()

                #isolate label and the rest
                try:
                    label = re.split(r"^((?:\w|-(?=\w)|\.(?=\w))+(?:\[loopvar:\s*\w+\]|\$\(\w+\))?(?:\w|-(?=\w)|\.(?=\w))*)(\.|-|:|\)|_?\s)", input, 1)[1].replace('-','_').replace('.','_')
                    input = re.split(r"^((?:\w|-(?=\w)|\.(?=\w))+(?:\[loopvar:\s*\w+\]|\$\(\w+\))?(?:\w|-(?=\w)|\.(?=\w))*)(\.|-|:|\)|_?\s)", input, 1)[-1]
                except:
                    label = "QPostal"
                input = cleanInput(input)

                #Add a q if the first character is a digit
                if label[0].isdigit():
                    label = "Q" + label

                title = input if len(input) > 1 else "Quel est le code postal de votre domicile ?"

                printPage = """<res label="wrongLen">Merci de saisir les 5 chiffres de votre code postal. Ex: 75012</res>

<text{delim}label="{lbl}"{delim}optional="0"{delim}size="5">
    <title>{title}</title>
    <validate>
if len(this.val) != 5 or not this.val.isdigit():
    error(res.wrongLen)
    </validate>
</text>
<suspend/>

<select{delim}label="{lbl}_REGION">
  <title>In which region do you primarily live?</title>
  <choice label="ch1">Auvergne-Rhône-Alpes</choice>
  <choice label="ch2">Bourgogne- Franche-Comté</choice>
  <choice label="ch3">Bretagne</choice>
  <choice label="ch4">Centre - Val de Loire</choice>
  <choice label="ch5">Corse</choice>
  <choice label="ch6">Grand Est</choice>
  <choice label="ch7">Hauts-de-France</choice>
  <choice label="ch8">Île-de-France</choice>
  <choice label="ch9">Normandie</choice>
  <choice label="ch10">Nouvelle Aquitaine</choice>
  <choice label="ch11">Occitanie</choice>
  <choice label="ch12">Pays de la Loire</choice>
  <choice label="ch13">Provence-Alpes-Côte d'Azur</choice>
</select>
<suspend/>
""".format(lbl=label.strip(), title=title.strip(), delim=attrDelim)

                self.view.replace(edit,sel, printPage)
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)


class makeZipDe(sublime_plugin.TextCommand):
    def run (self,edit):
        try:
            attrDelim = getAttrDelimiter(self)
            qartsV = self.view.settings().get('qartsVersion')
            sels = self.view.sel()
            input = ''
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel).strip()

                #isolate label and the rest
                try:
                    label = re.split(r"^((?:\w|-(?=\w)|\.(?=\w))+(?:\[loopvar:\s*\w+\]|\$\(\w+\))?(?:\w|-(?=\w)|\.(?=\w))*)(\.|-|:|\)|_?\s)", input, 1)[1].replace('-','_').replace('.','_')
                    input = re.split(r"^((?:\w|-(?=\w)|\.(?=\w))+(?:\[loopvar:\s*\w+\]|\$\(\w+\))?(?:\w|-(?=\w)|\.(?=\w))*)(\.|-|:|\)|_?\s)", input, 1)[-1]
                except:
                    label = "QPostal"
                input = cleanInput(input)

                #Add a q if the first character is a digit
                if label[0].isdigit():
                    label = "Q" + label

                title = input if len(input) > 1 else "Wie lautet die Postleitzahl Ihres Hauptwohnsitzes?"

                printPage = """<res label="wrongLen">Bitte geben Sie Ihre 5-stellige Postleitzahl ein</res>

<text{delim}label="{lbl}"{delim}optional="0"{delim}size="5"{delim}verify="zipcode"{delim}alt="What is your zipcode?"{surveyTypeSpecific}>
  <title>{title}</title>
  <validate>
if len(this.val) != 5 or not this.val.isdigit():
    error(res.wrongLen)
  </validate>
  <row label="r1" verify="zipcode"/>
</text>
<suspend/>

<select{delim}label="{lbl}_STATE">
  <title>In which state do you primarily live?</title>
  <choice label="ch1">Baden-Württemberg</choice>
  <choice label="ch2">Bayern</choice>
  <choice label="ch3">Berlin</choice>
  <choice label="ch4">Brandenburg</choice>
  <choice label="ch5">Bremen</choice>
  <choice label="ch6">Hamburg</choice>
  <choice label="ch7">Hessen</choice>
  <choice label="ch8">Mecklenburg-Vorpommern</choice>
  <choice label="ch9">Niedersachsen</choice>
  <choice label="ch10">Nordrhein-Westfalen</choice>
  <choice label="ch11">Rheinland-Pfalz</choice>
  <choice label="ch12">Saarland</choice>
  <choice label="ch13">Sachsen</choice>
  <choice label="ch14">Sachsen-Anhalt</choice>
  <choice label="ch15">Schleswig-Holstein</choice>
  <choice label="ch16">Thüringen</choice>
</select>
<suspend/>
""".format(lbl=label.strip(), title=title.strip(), delim=attrDelim)

                self.view.replace(edit,sel, printPage)
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)


class makeZipUk(sublime_plugin.TextCommand):
    def run (self,edit):
        try:
            attrDelim = getAttrDelimiter(self)
            sels = self.view.sel()
            input = ''
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel).strip()

                #isolate label and the rest
                try:
                    label = re.split(r"^((?:\w|-(?=\w)|\.(?=\w))+(?:\[loopvar:\s*\w+\]|\$\(\w+\))?(?:\w|-(?=\w)|\.(?=\w))*)(\.|-|:|\)|_?\s)", input, 1)[1].replace('-','_').replace('.','_')
                    input = re.split(r"^((?:\w|-(?=\w)|\.(?=\w))+(?:\[loopvar:\s*\w+\]|\$\(\w+\))?(?:\w|-(?=\w)|\.(?=\w))*)(\.|-|:|\)|_?\s)", input, 1)[-1]
                except:
                    label = "QPostal"
                input = cleanInput(input)

                #Add a q if the first character is a digit
                if label[0].isdigit():
                    label = "Q" + label

                title = input if len(input) > 1 else "What is the postcode of your residence?"
                printPage = """<res label="PostError">Enter a valid postcode.</res>

<text{delim}label="{lbl}"{delim}size="40"{delim}optional="0">
  <title>{title}</title>
  <comment>Please enter your response in the text box below.</comment>
  <validate>
if not re.match(r"^[a-zA-Z]{{1,2}}\d[a-zA-Z\d]?\s?\d[a-zA-Z]{{2}}$", {lbl}.val):
    error(res.PostError)
  </validate>
</text>
<suspend/>

 <select{delim}label="{lbl}_COUNTY">
    <title>Which county/area do you live in?</title>
    <choice label="ch1">Bedfordshire</choice>
    <choice label="ch2">Berkshire</choice>
    <choice label="ch3">Bristol</choice>
    <choice label="ch4">Buckinghamshire</choice>
    <choice label="ch5">Cambridgeshire</choice>
    <choice label="ch6">Cheshire</choice>
    <choice label="ch7">Cornwall</choice>
    <choice label="ch8">County Durham</choice>
    <choice label="ch9">Cumbria</choice>
    <choice label="ch10">Derbyshire</choice>
    <choice label="ch11">Devon</choice>
    <choice label="ch12">Dorset</choice>
    <choice label="ch13">East Riding of Yorkshire</choice>
    <choice label="ch14">East Sussex</choice>
    <choice label="ch15">Essex</choice>
    <choice label="ch16">Gloucestershire</choice>
    <choice label="ch17">Greater London</choice>
    <choice label="ch18">Greater Manchester</choice>
    <choice label="ch19">Hampshire</choice>
    <choice label="ch20">Herefordshire</choice>
    <choice label="ch21">Hertfordshire</choice>
    <choice label="ch22">Isle of Wight</choice>
    <choice label="ch23">Kent</choice>
    <choice label="ch24">Lancashire</choice>
    <choice label="ch25">Leicestershire</choice>
    <choice label="ch26">Lincolnshire</choice>
    <choice label="ch27">Merseyside</choice>
    <choice label="ch28">Norfolk</choice>
    <choice label="ch29">North Somerset</choice>
    <choice label="ch30">North Yorkshire</choice>
    <choice label="ch31">Northamptonshire</choice>
    <choice label="ch32">Northumberland</choice>
    <choice label="ch33">Nottinghamshire</choice>
    <choice label="ch34">Oxfordshire</choice>
    <choice label="ch35">Rutland</choice>
    <choice label="ch36">Shropshire</choice>
    <choice label="ch37">Somerset</choice>
    <choice label="ch38">South Gloucestershire</choice>
    <choice label="ch39">South Yorkshire</choice>
    <choice label="ch40">Staffordshire</choice>
    <choice label="ch41">Suffolk</choice>
    <choice label="ch42">Surrey</choice>
    <choice label="ch43">Tyne &amp; Wear</choice>
    <choice label="ch44">Warwickshire</choice>
    <choice label="ch45">West Midlands</choice>
    <choice label="ch46">West Sussex</choice>
    <choice label="ch47">West Yorkshire</choice>
    <choice label="ch48">Wiltshire</choice>
    <choice label="ch49">Worcestershire</choice>
    <choice label="ch50">Ireland</choice>
    <choice label="ch51">Scotland</choice>
    <choice label="ch52">Wales</choice>
  </select>
  <suspend/>

  <radio{delim}label="{lbl}_REGION"{delim}optional="1"{delim}translateable="0"{delim}where="execute">
    <title>HIDDEN VARIABLE TO CAPTURE UK REGION BASED ON {lbl}_COUNTY</title>
    <exec>
if {lbl}_COUNTY.ch8 or {lbl}_COUNTY.ch32 or {lbl}_COUNTY.ch43:
    {lbl}_REGION.val = {lbl}_REGION.r1.index
elif {lbl}_COUNTY.ch6 or {lbl}_COUNTY.ch9 or {lbl}_COUNTY.ch18 or {lbl}_COUNTY.ch24 or {lbl}_COUNTY.ch27:
    {lbl}_REGION.val = {lbl}_REGION.r2.index
elif {lbl}_COUNTY.ch39 or {lbl}_COUNTY.ch47 or {lbl}_COUNTY.ch30 or {lbl}_COUNTY.ch13:
    {lbl}_REGION.val = {lbl}_REGION.r3.index
elif {lbl}_COUNTY.ch20 or {lbl}_COUNTY.ch36 or {lbl}_COUNTY.ch40 or {lbl}_COUNTY.ch44 or {lbl}_COUNTY.ch45 or {lbl}_COUNTY.ch49:
    {lbl}_REGION.val = {lbl}_REGION.r4.index
elif {lbl}_COUNTY.ch10 or {lbl}_COUNTY.ch25 or {lbl}_COUNTY.ch26 or {lbl}_COUNTY.ch33 or {lbl}_COUNTY.ch31 or {lbl}_COUNTY.ch35:
    {lbl}_REGION.val = {lbl}_REGION.r5.index
elif {lbl}_COUNTY.ch1 or {lbl}_COUNTY.ch15 or {lbl}_COUNTY.ch21 or {lbl}_COUNTY.ch5 or {lbl}_COUNTY.ch28 or {lbl}_COUNTY.ch41:
    {lbl}_REGION.val = {lbl}_REGION.r6.index
elif {lbl}_COUNTY.ch3 or {lbl}_COUNTY.ch7 or {lbl}_COUNTY.ch11 or {lbl}_COUNTY.ch12 or {lbl}_COUNTY.ch16 or {lbl}_COUNTY.ch37 or {lbl}_COUNTY.ch48 or {lbl}_COUNTY.ch29 or {lbl}_COUNTY.ch38:
    {lbl}_REGION.val = {lbl}_REGION.r7.index
elif {lbl}_COUNTY.ch2 or {lbl}_COUNTY.ch4 or {lbl}_COUNTY.ch14 or {lbl}_COUNTY.ch19 or {lbl}_COUNTY.ch22 or {lbl}_COUNTY.ch23 or {lbl}_COUNTY.ch34 or {lbl}_COUNTY.ch42 or {lbl}_COUNTY.ch46:
    {lbl}_REGION.val = {lbl}_REGION.r8.index
elif {lbl}_COUNTY.ch17:
    {lbl}_REGION.val = {lbl}_REGION.r9.index
elif {lbl}_COUNTY.ch52:
    {lbl}_REGION.val = {lbl}_REGION.r10.index
elif {lbl}_COUNTY.ch51:
    {lbl}_REGION.val = {lbl}_REGION.r11.index
elif {lbl}_COUNTY.ch50:
    {lbl}_REGION.val = {lbl}_REGION.r12.index
    </exec>
    <row label="r1">North East</row>
    <row label="r2">North West</row>
    <row label="r3">Yorkshire &amp; the Humber</row>
    <row label="r4">West Midlands</row>
    <row label="r5">East Midlands</row>
    <row label="r6">East of England</row>
    <row label="r7">South West</row>
    <row label="r8">South East</row>
    <row label="r9">London</row>
    <row label="r10">Wales</row>
    <row label="r11">Scotland</row>
    <row label="r12">Northern Ireland</row>
  </radio>
  <suspend/>
""".format(lbl=label.strip(), title=title.strip(), delim=attrDelim)

                self.view.replace(edit,sel, printPage)
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)


class makeZipAus(sublime_plugin.TextCommand):
    def run (self,edit):
        try:
            attrDelim = getAttrDelimiter(self)
            sels = self.view.sel()
            input = ''
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel).strip()

                #isolate label and the rest
                try:
                    label = re.split(r"^((?:\w|-(?=\w)|\.(?=\w))+(?:\[loopvar:\s*\w+\]|\$\(\w+\))?(?:\w|-(?=\w)|\.(?=\w))*)(\.|-|:|\)|_?\s)", input, 1)[1].replace('-','_').replace('.','_')
                    input = re.split(r"^((?:\w|-(?=\w)|\.(?=\w))+(?:\[loopvar:\s*\w+\]|\$\(\w+\))?(?:\w|-(?=\w)|\.(?=\w))*)(\.|-|:|\)|_?\s)", input, 1)[-1]
                except:
                    label = "QPostal"
                input = cleanInput(input)

                #Add a q if the first character is a digit
                if label[0].isdigit():
                    label = "Q" + label

                title = input if len(input) > 1 else "What is the postcode of your residence?"
                printPage = """<res label="PostError">Enter a valid postcode.</res>

<text{delim}label="{lbl}"{delim}size="40"{delim}optional="0">
  <title>{title}</title>
  <comment>Please enter your response in the text box below.</comment>
  <validate>
if not re.match(r"^(0[289][0-9]{2})|([123456789][0-9]{3})$", D3AUS.val):
    error(res.PostError)
  </validate>
</text>
<suspend/>
""".format(lbl=label.strip(), title=title.strip(), delim=attrDelim)

                self.view.replace(edit,sel, printPage)
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)


######################## ONEPLATFORM QUESTIONS #########################

popupHtml = '''<body id="decipherlib-popup">
<style>
body {{
    font-size: .8rem;
}}
#item-list {{
    margin: 2px 2px 5px;
    padding: 5px;
    max-width: 340px;
}}
#header {{
    text-align: center;
    margin: 2px auto 0;
    padding: 6px 8px 1px;
}}
#footer {{
    text-align: center;
    margin: 0 auto 2px;
    padding: 1px 8px 6px;
}}
.done {{
    padding: 7px 50px 5px;
    margin-top: 1em;
    text-align: center;
    font-weight: 600;
    border: 1px solid var(--accent);
    border-radius: 6px;
    color: var(--background);
    background-color: var(--foreground);
    text-decoration: none;
}}
.sel-item {{
    padding: .3em;
    text-decoration: none;
    color: var(--foreground);
}}
#hr {{
    padding-top: .3em;
    margin-top: .5em;
    border-top: 1px solid #ebdbb299;
}}
</style>
<div id="header">
    <a href="fin" class="done">Done</a>
</div>
<div id="item-list">
    <li id="hr"></li>
    {0}
    <li id="hr"></li>
</div>
<div id="footer">
    <a href="fin" class="done">Done</a>
</div>
</body>
'''


class makeVirtualHiddenCountry(sublime_plugin.TextCommand):
    def is_visible(self):
        return self.view.settings().get('surveyType') in ['v3', 'v2', 'AG', 'AG0', None] or True
    items = (('US','United States'), ('GB','United Kingdom'), ('AU','Australia'), ('BR','Brazil'), ('CA','Canada'), ('CN','China'), ('FR','France'), ('DE','Germany'), ('IN','India'), ('IT','Italy'), ('JP','Japan'), ('ES','Spain'), ('KR','South Korea'), ('-'), ('AL','Albania'), ('AR','Argentina'), ('AM','Armenia'), ('AT','Austria'), ('BH','Bahrain'), ('BD','Bangladesh'), ('BY','Belarus'), ('BE','Belgium'), ('BO','Bolivia'), ('BA','Bosnia and Herzegovina'), ('BW','Botswana'), ('BG','Bulgaria'), ('KH','Cambodia'), ('CM','Cameroon'), ('CL','Chile'), ('CO','Colombia'), ('HR','Croatia'), ('CU','Cuba'), ('CY','Cyprus'), ('CZ','Czech Republic'), ('DK','Denmark'), ('EC','Ecuador'), ('EG','Egypt'), ('EE','Estonia'), ('ET','Ethiopia'), ('FI','Finland'), ('GE','Georgia'), ('GR','Greece'), ('HK','Hong Kong'), ('HU','Hungary'), ('IS','Iceland'), ('ID','Indonesia'), ('IR','Iran'), ('IQ','Iraq'), ('IE','Ireland'), ('IL','Israel'), ('JM','Jamaica'), ('JO','Jordan'), ('KZ','Kazakhstan'), ('KE','Kenya'), ('KW','Kuwait'), ('LA','Lao PDR'), ('LV','Latvia'), ('LT','Lithuania'), ('LU','Luxembourg'), ('MK','North Macedonia'), ('MG','Madagascar'), ('MT','Malta'), ('MX','Mexico'), ('MD','Moldova'), ('MC','Monaco'), ('ME','Montenegro'), ('MA','Morocco'), ('MM','Myanmar'), ('NP','Nepal'), ('NL','Netherlands'), ('NZ','New Zealand'), ('NG','Nigeria'), ('NO','Norway'), ('PK','Pakistan'), ('PG','Papua New Guinea'), ('PY','Paraguay'), ('PE','Peru'), ('PH','Philippines'), ('PL','Poland'), ('PT','Portugal'), ('PR','Puerto Rico'), ('QA','Qatar'), ('RO','Romania'), ('RU','Russia'), ('SA','Saudi Arabia'), ('RS','Serbia'), ('SG','Singapore'), ('SK','Slovakia'), ('SI','Slovenia'), ('ZA','South Africa'), ('SE','Sweden'), ('CH','Switzerland'), ('TW','Taiwan'), ('TH','Thailand'), ('TR','Turkey'), ('UA','Ukraine'), ('AE','United Arab Emirates'), ('UY','Uruguay'), ('VE','Venezuela'), ('VN','Vietnam'), ('YE','Yemen'))

    def makeHtml(self):
        page = popupHtml
        link = """<a href="{0}" class="sel-item">{1} ({0})</a><br>"""
        box = "☐ "
        check = "<b>✓ %s</b>"
        content = ""
        try:
            for country in self.items:
                if country[0] == '-':
                    content += '<li id="hr"></li>'
                elif country[0] in self.selected:
                    content += link.format(country[0], check %country[1])
                else:
                    content += link.format(country[0], box + country[1])
            html = page.format(content)
            return html
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)

    def run(self,edit):
        self.selected = []
        try:
            self.view.show_popup(self.makeHtml(), 0, -1, 420, 480, self.on_navigate, self.on_hide)
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)

    def on_navigate(self, href):
        if href == 'fin':
            self.view.hide_popup()
        elif href not in self.selected:
            self.selected.append(href)
        else:
            self.selected.remove(href)
        self.view.update_popup(self.makeHtml())

    def on_hide(self):
        try:
            if self.view.find('<var name="countryCode" required="1"', self.view.text_point(1,0)):
                urlvar = 'countryCode'
            else:
                urlvar = 'co'
            if len(self.selected) > 1:
                sels = self.view.sel()
                printRowsV = ''
                printRowsH = ''
                for r in sorted(self.selected):
                    plR = "{0}".format([x[1] for x in self.items if x[0] == r][0])
                    printRowsV += """  <row label="{0}">{1}</row>\n""".format(r.lower(), plR)
                    printRowsH += """  <row label="{0}">{1} ({0})</row>\n""".format(r.upper(), plR)

                printPage = """
<radio label="v{2}" title="Country" virtual="bucketize({2}.lower())">
{0}
</radio>

<radio label="hCountry" where="execute" translateable="0" optional="1">
  <title>Hidden: Country</title>
  <exec>
try:
    hCountry.val = hCountry.attr({2}.upper()).index
except:
    print "Country code not in list."
  </exec>
{1}
</radio>
<suspend/>""".format(printRowsV.strip('\n'), printRowsH.strip('\n'), urlvar)
                self.rows = []
                for sel in sels:
                    with Edit(self.view) as edit:
                        edit.insert(sel.end(), printPage)
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)


class makeLanguages(sublime_plugin.TextCommand):
    def is_visible(self):
        return True
    items = (('english', 'us', 'en', "English"), ('french', 'fr', 'fr', "French"), ('german', 'de', 'de', "German"), ('italian', 'it', 'it', "Italian"), ('portuguese_br', 'br', 'pt_br', "Portuguese (Brazil)"), ('spanish', 'es', 'es', "Spanish"), ('canadian', 'fr_ca', 'fr_ca', "French (Canada)"), ('arabic', 'FIX', 'ar', "Arabic"), ('japanese', 'jp', 'ja', "Japanese"), ('korean', 'kr', 'ko', "Korean"), ('simplifiedchinese', 'cn', 'zh', "Chinese Simplified"), ('uk', 'uk', 'en_gb', "English (UK)"), ('-'), ('afrikaans', 'za', 'af', "Afrikaans"), ('albanian', 'al', 'sq', "Albanian"), ('arabic_egypt', 'eg', 'ar_eg', "Arabic (Egypt)"), ('arabic_iraq', 'iq', 'ar_iq', "Arabic (Iraq)"), ('arabic_jordan', 'jo', 'ar_jo', "Arabic (Jordan"), ('arabic_kuwait', 'kw', 'ar_kw', "Arabic (Kuwait"), ('arabic_lebanon', 'lb', 'ar_lb', "Arabic (Lebanon)"), ('arabic_morocco', 'ma', 'ar_ma', "Arabic (Morocco)"), ('arabic_qatar', 'qa', 'ar_qa', "Arabic (Qatar)"), ('arabic_saudiarabia', 'sa', 'ar_sa', "Arabic (Saudi Arabia)"), ('arabic_uae', 'ae', 'ar_ae', "Arabic (UAE)"), ('assamese', 'in_as', 'as', "Assamese"), ('azerbaijani', 'az', 'az', "Azerbaijani"), ('bengali', 'bd', 'bn', "Bengali"), ('bosnian', 'ba', 'bs', "Bosnian"), ('bulgarian', 'bg', 'bg', "Bulgarian"), ('burmese', 'mm', 'my', "Burmese (Myanmar)"), ('chinese', 'cn', 'zh', "Chinese"), ('croatian', 'hr', 'hr', "Croatian"), ('czech', 'cz', 'cs', "Czech"), ('danish', 'dk', 'da', "Danish"), ('dutch', 'nl', 'nl', "Dutch"), ('dutch_belgium', 'nl_be', 'nl_be', "Dutch (Belgium)"), ('english.utf8', 'FIX', 'en', "English UTF8"), ('estonian', 'ee', 'et', "Estonian"), ('filipino', 'ph', 'tl', "Filipino"), ('finnish', 'fi', 'fi', "Finnish"), ('french_belgium', 'fr_be', 'fr_be', "French (Belgium)"), ('french_lu', 'fr_lu', 'fr_lu', "French (Luxemborg)"), ('french_ch', 'fr_ch', 'fr_ch', "French (Switzerland)"), ('georgian', 'ge', 'ka', "Georgian"), ('german_austria', 'at', 'de_at', "German (Austria)"), ('german_lu', 'lu', 'de_lu', "German (Luxemborg)"), ('german_ch', 'de_ch', 'de_ch', "German (Switzerland)"), ('greek', 'gr', 'el', "Greek"), ('greek_cyprus', 'cy', 'el_cy', "Greek (Cyprus)"), ('gujarati', 'in_gu', 'gu', "Gujarati"), ('hebrew', 'il', 'he', "Hebrew"), ('hindi', 'in', 'hi', "Hindi"), ('hungarian', 'hu', 'hu', "Hungarian"), ('icelandic', 'is', 'is', "Icelandic"), ('indonesian', 'id', 'id', "Indonesian"), ('italian_ch', 'it_ch', 'it_ch', "Italian (Switzerland)"), ('kannada', 'in_kn', 'kn', "Kannada"), ('kazakh', 'kz', 'kk', "Kazakh"), ('khmer', 'kh', 'km', "Khmer"), ('latvian', 'lv', 'lv', "Latvian"), ('lithuanian', 'lt', 'lt', "Lithuanian"), ('macedonian', 'mk', 'mk', "Macedonian"), ('malay', 'FIX', 'ms', "Malay"), ('malay_sg', 'sg', 'ms_sg', "Malay (Singapore)"), ('malayalam', 'in_ml', 'ml', "Malayalam"), ('maltese', 'mt', 'mt', "Maltese"), ('marathi', 'in_mr', 'mr', "Marathi"), ('mongolian', 'mn', 'mn', "Mongolian"), ('norwegian', 'no', 'no', "Norwegian"), ('oriya', 'in_or', 'or', "Oriya"), ('persian', 'FIX', 'fa', "Persian"), ('polish', 'pl', 'pl', "Polish"), ('portuguese', 'pt', 'pt', "Portuguese"), ('punjabi', 'in_pa', 'pa', "Punjabi"), ('romanian', 'ro', 'ro', "Romanian"), ('russian', 'ro', 'ru', "Russian"), ('russian_belarus', 'by', 'ru_by', "Russian (Belarus)"), ('russian_kazakhstan', 'kz', 'ru_kz', "Russian (Kazakhstan)"), ('samoan', 'ws', 'sm', "Samoan"), ('serbian', 'rs', 'sr', "Serbian"), ('simplifiedchinese_my', 'my', 'zh_my', "Simplified Chinese (Malaysia)"), ('simplifiedchinese_sg', 'sg', 'zh_sg', "Simplified Chinese (Singapore)"), ('slovak', 'sk', 'sk', "Slovak"), ('slovene', 'si', 'sl', "Slovene"), ('slovenian', '`si', 'sl', "Slovenian"), ('spanish_argentina', 'ar', 'es_ar', "Spanish (Argentina)"), ('spanish_brazil', 'br', 'es_br', "Spanish (Brazil)"), ('spanish_chile', 'cl', 'es_cl', "Spanish (Chile)"), ('spanish_colombia', 'co', 'es_co', "Spanish (Colombia)"), ('spanish_ecuador', 'ec', 'es_ec', "Spanish (Ecuador)"), ('spanish_eu', 'es', 'es_eu', "Spanish (EU)"), ('spanish_peru', 'pe', 'es_pe', "Spanish (Peru)"), ('spanish_venezuela', 've', 'es_ve', "Spanish (Venezuela)"), ('spanish_costarica', 'cr', 'es_cr', "Spanish (Costa Rica)"), ('spanish_dominicanrepublic', 'do', 'es_do', "Spanish (Dominican Republic)"), ('spanish_latin', 'FIX', 'es_latin', "Spanish (Latin America)"), ('spanish_mexico', 'mx', 'es_mx', "Spanish (Mexico)"), ('spanish_south', 'FIX', 'es_sa', "Spanish (South America)"), ('swahili', 'FIX', 'sw', "Swahili"), ('swedish', 'se', 'sv', "Swedish"), ('tagalog', 'ph', 'tl', "Tagalog"), ('tamil', 'in_ta', 'ta', "Tamil"), ('telugu', 'in_te', 'te', "Telugu"), ('thai', 'th', 'th', "Thai"), ('tongan', 'to', 'to', "Tongan"), ('traditionalchinese', 'hk', 'zh_hk', "Traditional Chinese (HK)"), ('traditionalchinese_tw', 'tw', 'zh_tw', "Traditional Chinese (Taiwan)"), ('turkish', 'tr', 'tr', "Turkish"), ('aus', 'au', 'en_ae', "English (Australia)"), ('english_ae', 'en_ae', 'en_ae', "English (UAE)"), ('english_ca', 'en_ca', 'en_ca', "English (Canada)"), ('english_china', 'en_cn', 'en_cn', "English (China)"), ('english_hk', 'en_hk', 'en_hk', "English (Hong Kong)"), ('english_id', 'en_id', 'en_id', "English (Indonesia)"), ('english_india', 'en_in', 'en_in', "English (India)"), ('english_ireland', 'en_ie', 'en_ie', "English (Ireland)"), ('english_my', 'en_my', 'en_my', "English (Malaysia)"), ('english_nigeria', 'en_ng', 'en_ng', "English (Nigeria)"), ('english_nz', 'en_nz', 'en_nz', "English (New Zealand)"), ('english_sg', 'en_sg', 'en_sg', "English (Singapore)"), ('english_tw', 'en_tw', 'en_tw', "English (Taiwan)"), ('english_za', 'en_za', 'en_za', "English (South Africa)"), ('ukrainian', 'ua', 'uk', "Ukrainian"), ('urdu', 'pk', 'ur', "Urdu (Pakistan)"), ('urdu_india', 'ur_in', 'ur_in', "Urdu (India)"), ('uzbek', 'uz', 'uz', "Uzbek"), ('vietnamese', 'vn', 'vi', "Vietnamese"), ('welsh', 'FIX', 'cy', "Welsh"), ('english_g1', 'FIX', 'en', "English (Generic 1)"), ('english_g2', 'FIX', 'en', "English (Generic 2)"), ('english_g3', 'FIX', 'en', "English (Generic 3)"), ('english_g4', 'FIX', 'en', "English (Generic 4)"), ('english_g5', 'FIX', 'en', "English (Generic 5)"), ('english_g6', 'FIX', 'en', "English (Generic 6)"), ('english_g7', 'FIX', 'en', "English (Generic 7)"), ('english_g8', 'FIX', 'en', "English (Generic 8)"), ('english_g9', 'FIX', 'en', "English (Generic 9)"), ('english_g10', 'FIX', 'en', "English (Generic 10)"))
    var = 'co'
    def makeHtml(self):
        page = popupHtml
        if self.var == 'lang':
            varSel = """<a href="_co" class="sel-item">☐ CO</a><br>
            <a href="_lang" class="sel-item">✓ <b>LANG</b></a><br>"""
        else:
            varSel = """<a href="_co" class="sel-item">✓ <b>CO</b></a><br>
            <a href="_lang" class="sel-item">☐ LANG</a><br>"""
        link = """<a href="{0}" class="sel-item">{1}</a><br>"""
        box = "☐ "
        check = "<b>✓ %s</b>"
        content = '<b>Link Var to use:</b><br>' + varSel + '<li id="hr"></li> <h4 class="header">Languages:</h4>'
        try:
            for country in self.items:
                if country[0] == '-':
                    content += '<li id="hr"></li>'
                elif country[0] in self.selected:
                    content += link.format(country[0], check %country[3])
                else:
                    content += link.format(country[0], box + country[3])
            html = page.format(content)
            return html
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)

    def run(self,edit):
        self.selected = []
        try:
            self.selected = ['english']
            self.view.show_popup(self.makeHtml(), 0, -1, 380, 480, self.on_navigate, self.on_hide)
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)

    def on_navigate(self, href):
        if href == 'fin':
            self.view.hide_popup()
        elif href.startswith('_'):
            self.var = href.strip('_')
        elif href not in self.selected:
            self.selected.append(href)
        else:
            self.selected.remove(href)
        self.view.update_popup(self.makeHtml())

    def on_hide(self):
        try:
            if len(self.selected) > 1:
                sels = self.view.sel()
                printRows = ''
                printSurvey = ''

                surveyTagRegion = self.view.find('<survey(.|\n)*?>', self.view.text_point(0,0))
                surveyTagText = self.view.substr(surveyTagRegion) if surveyTagRegion else ''
                survOthLang = re.search(r'otherLanguages\s*=\s*\"([\w,\s]+?)\"', surveyTagText).group(1) if re.search(r'otherLanguages\s*=\s*\"([\w,\s]+?)\"', surveyTagText) else ''
                newOthLang = survOthLang

                for r in sorted(self.selected):
                    if r != 'english' and not newOthLang:
                        newOthLang = r
                    elif r != 'english' and not re.search(r'\b{0}\b'.format(r), newOthLang):
                        newOthLang += ',{0}'.format(r)
                    if self.var == 'lang':
                        plR = [x[2] for x in self.items if x[0] == r][0]
                    else:
                        plR = [x[1] for x in self.items if x[0] == r][0]
                    printRows += """  <language name="{0}" var="{2}" value="{1}"/>\n""".format(r, plR, self.var)

                if 'otherLanguages' in surveyTagText:
                    printSurvey = re.sub(r'(otherLanguages\s*=\s*\")[\w,\s]*?(\")', r'\g<1>{0}\g<2>'.format(newOthLang), surveyTagText)
                else:
                    printSurvey = re.sub(r'((\s*)state="(?:dev|testing|live|closed)")', r'\g<1>\g<2>otherLanguages="{0}"'.format(newOthLang), surveyTagText)
                printPage = """<languages default="english">
{0}
</languages>\n""".format(printRows.strip('\n'))
                self.rows = []
                with Edit(self.view) as edit:
                    edit.insert(sels[0].end(), printPage)
                    edit.replace(surveyTagRegion, printSurvey)
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)

class makeVirtualSamplePartners(sublime_plugin.TextCommand):
    def is_visible(self):
        return self.view.settings().get('surveyType') in ['v3', 'v2', 'AG', 'AG0'] or True

    items = (('1','Red Planet'), ('2','SSI'), ('4','ERI (and ERI Medical), RN US'), ('6','Tech Panel'), ('7','Offerwise, RN'), ('8','Toluna'), ('9','Lightspeed GTM'), ('10','Local partner GEN46'), ('11', 'Lightspeed MYS'), ('13','Mainstream MST52'), ('14','EMI'), ('15','Critical Mix'), ('16','Asking CA'), ('17','Federated'), ('18','Cint '), ('20','Local partner GEN48'), ('21','Local partner GEN47'), ('22', 'LifePoints'), ('23','Local partner GEN24'), ('24','Local partner GEN25'), ('25','Local partner GEN26'), ('26','Local partner GEN27'), ('27','Local partner GEN28'), ('28','Local partner GEN28'), ('29','Conecta'), ('30','Borderless Access'),  ('35','NetQuest'), ('36','LATAM Non-SD - GTM and all partners'), ('37','PanelBiz (OPL PPID)'), ('38','Respondi'), ('39','Respondi REC'), ('40','Tiburon'), ('41','TIBURON SEP 2015'), ('42','MumsViews'), ('43','ODR'), ('44','MB Poland'), ('45','Market Agent'), ('46','Norstat'), ('47','M3'), ('48','DIMARSO'), ('49','Userneeds'), ('50','GMI'), ('51','GMI-DIMARSO'), ('52','GMI-P&amp;G'), ('53','GMI (For Comtech)'), ('54','AIP'), ('55','Rakuten AIP (IMRB scripting)'), ('56','Cross Marketing (GDC scripting)'), ('57','Paneland'), ('58','iPanel'), ('59','RNSSI'), ('60','EmailCash'), ('61','IX'), ('62','GoSurvey'), ('63','MDQ'), ('64','DataSpring'), ('65','Viewstap'), ('66','QQ Survey'), ('67','Data100'), ('68','Tillion'), ('69','Embrian'), ('70','MO WEB'), ('71','Link'), ('73','uSamp'), ('74','Bilendi'), ('75','Maximiles'), ('76','PanelBiz'), ('77','TNS Nipo'), ('79','Vice Voices'), ('80','QuickRewards.net'), ('81','Vivatic'), ('82','Persona.ly'), ('83','Tellwut'), ('84','Branded Surveys'), ('85','Dale'), ('86','Everyday Family Surveys'), ('87','FreeSurveysASIA'), ('88','MyFocusline Surveys'), ('89','PointClub'), ('90','Take Surveys'), ('91','Market Cube'), ('92','IdeaShifters'), ('93','Paradigm Sample'), ('94','Pureprofile'))

    def makeHtml(self):
        page = popupHtml
        link = """<a href="{0}" class="sel-item">{1}</a><br>"""
        box = "☐ %s"
        check = "<b>✓ %s</b>"
        content = ""
        try:
            for pp in self.items:
                if len(pp[0]) == 1: atxt = '&#160;&#160;{0}. {1}'.format(pp[0], pp[1])
                elif len(pp[0]) == 2: atxt = '&#160;{0}. {1}'.format(pp[0], pp[1])
                else: atxt = '{0}. {1}'.format(pp[0], pp[1])
                if pp[0] in self.selected:
                    content += link.format(pp[0], check %atxt)
                else:
                    content += link.format(pp[0], box %atxt)
            html = page.format(content)
            return html
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)

    def run(self,edit):
        self.selected = ['18','22']
        try:
            self.view.show_popup(self.makeHtml(), 0, -1, 430, 475, self.on_navigate, self.on_hide)
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)

    def on_navigate(self, href):
        if href == 'fin':
            self.view.hide_popup()
        elif href not in self.selected:
            self.selected.append(href)
        else:
            self.selected.remove(href)
        self.view.update_popup(self.makeHtml())

    def on_hide(self):
        try:
            if len(self.selected) > 1:
                sels = self.view.sel()
                printRows = ''
                for r in sorted(self.selected, key=lambda x: int(x)):
                    plR = [x[1] for x in self.items if x[0] == r][0]
                    printRows += """  <row label="{0}">Sample {0}</row> <!-- {1} -->\n""".format(r, plR)

                printPage = """<note>Add a row in vs for every new panel partner. In EMEA use Sample X names. In AMS use partner's real name.</note>
<radio label="vs" title="Panel Partner" virtual="bucketize(s)">
{0}
</radio>""".format(printRows.strip('\n'))
                self.rows = []
                for sel in sels:
                    with Edit(self.view) as edit:
                        edit.insert(sel.end(), printPage)
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)




class addSamplesource(sublime_plugin.TextCommand):
    panels = {
      'dynata': """  <samplesource eid="s2s:101" keyring="sys/dynata" list="1" sign="out,in">   
    <title>Dynata Sample</title>     
    <invalid>You are missing information in the URL. Please verify the URL with the original invite. </invalid>     
    <completed>It seems you have already entered this survey. </completed>     
    <var name="psid" unique="1"/>     
    <exit cond="terminated and hasMarker('qcterm')" url="https://dkr1.ssisurveys.com/projects/end?rst=2&amp;qflag=2&amp;psid=${psid}&amp;_d=${gv.survey.path}&amp;transactionId=${p.transactionId}"/>     
    <exit cond="qualified" url="https://dkr1.ssisurveys.com/projects/end?rst=1&amp;psid=${psid}&amp;_d=${gv.survey.path}&amp;transactionId=${p.transactionId}"/>     
    <exit cond="terminated" url="https://dkr1.ssisurveys.com/projects/end?rst=2&amp;psid=${psid}&amp;_d=${gv.survey.path}&amp;transactionId=${p.transactionId}"/>     
    <exit cond="overquota" url="https://dkr1.ssisurveys.com/projects/end?rst=3&amp;psid=${psid}&amp;_d=${gv.survey.path}&amp;transactionId=${p.transactionId}"/>     
  </samplesource>
""",
      'sago': """  <samplesource list="1" eid="s2s:142">   
    <title>Sago</title>     
    <var name="RID" unique="1"/>     
    <exit cond="terminated and hasMarker('qcterm')" url="https://surveys.sample-cube.com/ending?RS=4&amp;RID=${RID}"/>     
    <exit cond="qualified" url="https://surveys.sample-cube.com/ending?RS=1&amp;RID=${RID}&amp;secret=14739"/>     
    <exit cond="terminated" url="https://surveys.sample-cube.com/ending?RS=3&amp;RID=${RID}"/>     
    <exit cond="overquota" url="https://surveys.sample-cube.com/ending?RS=2&amp;RID=${RID}"/>     
  </samplesource>
""",    'prodege': """   <samplesource list="1">
    <var name="transaction_id" unique="1"/>     
    <exit cond="terminated and hasMarker('qcterm')" url="https://api.prodegepeeq.com/prodegemr/transaction-completion?apik=PAT!HMpwEa8t$I&amp;completion_type=7&amp;signature=6829dcda5c84c024a0cf&amp;transaction_id=${transaction_id}"/>     
    <exit cond="qualified" url="https://api.prodegepeeq.com/prodegemr/transaction-completion?apik=PAT!HMpwEa8t$I&amp;completion_type=1&amp;signature=17d75dbcf512c43525e6&amp;transaction_id=${transaction_id}"/>     
    <exit cond="terminated" url="https://api.prodegepeeq.com/prodegemr/transaction-completion?apik=PAT!HMpwEa8t$I&amp;completion_type=3&amp;signature=7c5d8df734e9972f0267&amp;transaction_id=${transaction_id}"/>     
    <exit cond="overquota" url="https://api.prodegepeeq.com/prodegemr/transaction-completion?apik=PAT!HMpwEa8t$I&amp;completion_type=2&amp;signature=2b76507663ce6a14ea46&amp;transaction_id=${transaction_id}"/>     
  </samplesource>
""",    'questmindshare': """   <samplesource list="1"> 
    <var name="ID" unique="1"/>     
    <exit cond="terminated and hasMarker('qcterm')" url="https://redirect.mindsharesurveys.com/v1/M7FMAqNSgNBiTKRoDbiNJ4YiXPN2cBin?status=4&amp;id=${ID}"/>     
    <exit cond="qualified" url="https://redirect.mindsharesurveys.com/v1/M7FMAqNSgNBiTKRoDbiNJ4YiXPN2cBin?proj=${proj}&amp;id=${ID}&amp;status=1"/>     
    <exit cond="terminated" url="https://redirect.mindsharesurveys.com/v1/M7FMAqNSgNBiTKRoDbiNJ4YiXPN2cBin?status=2&amp;id=${ID}"/>     
    <exit cond="overquota" url="https://redirect.mindsharesurveys.com/v1/M7FMAqNSgNBiTKRoDbiNJ4YiXPN2cBin?status=3&amp;id=${ID}"/>     
  </samplesource>
""",    'innovate': """  <samplesource list="1" eid="s2s:128">   
    <title>Innovate MR</title>     
    <var name="tk" unique="1"/>     
    <exit cond="terminated and hasMarker('qcterm')" url="https://edgeapi.innovatemr.net/surveyDone?sc=4&amp;tk=${tk}"/>     
    <exit cond="qualified" url="https://edgeapi.innovatemr.net/surveyDone?sc=1&amp;ejid=vqool1e6&amp;tk=${tk}"/>     
    <exit cond="terminated" url="https://edgeapi.innovatemr.net/surveyDone?sc=2&amp;tk=${tk}"/>     
    <exit cond="overquota" url="https://edgeapi.innovatemr.net/surveyDone?sc=3&amp;tk=${tk}"/>     
  </samplesource>
""",    'toluna': """  <samplesource list="1">   
    <title>Toluna</title>     
    <var name="sname" required="1"/>     
    <var name="gid" unique="1"/>     
    <exit cond="terminated and hasMarker('qcterm')" url="https://ups.surveyrouter.com/TrafficUI/MSCUI/sofraud.aspx?sname=${sname}&amp;gid=${gid}"/>     
    <exit cond="qualified" url="http://ups.surveyrouter.com/trafficui/mscui/SOQualified.aspx?sname=${sname}&amp;TolunaEnc=${QTolunaEnc.val}&amp;gid=${gid}"/>     
    <exit cond="terminated" url="http://ups.surveyrouter.com/trafficui/mscui/SOTerminated.aspx?sname=${sname}&amp;gid=${gid}"/>     
    <exit cond="overquota" url="http://ups.surveyrouter.com/trafficui/mscui/SOQuotafull.aspx?sname=${sname}&amp;gid=${gid}"/>     
  </samplesource>
<!--
<number   
  label="QTolunaEnc"   
  size="40"   
  where="execute,survey,report">   
  <title>Toluna Encryption ID</title>   
  <exec>     
try:     
    QTolunaEnc.val = ((int(gid) + 191984281) * 65537) % 2047483673     
except:     
    pass   
  </exec>   
</number>   
<suspend/>   
  

<exec>if QTolunaEnc.val in ['',None]: setMarker('badgid')</exec>   

<html label="TolunaBadGid" cond="hasMarker('badgid')" where="survey" final="1">   
${hlang.get("closed")}   
</html>   
<suspend/>
-->
""",    'repdata': """  <samplesource list="1">   
    <title>RepData</title>     
    <var name="RDUD" unique="1"/>     
    <exit cond="terminated and hasMarker('qcterm')" url="https://www.rdsecured.com/return?inbound_code=3000&amp;rdud=${RDUD}"/>     
    <exit cond="qualified" url="https://www.rdsecured.com/return?inbound_code=1000&amp;rdud=${RDUD}"/>     
    <exit cond="terminated" url="https://www.rdsecured.com/return?inbound_code=2000&amp;rdud=${RDUD}"/>     
    <exit cond="overquota" url="https://www.rdsecured.com/return?inbound_code=4000&amp;rdud=${RDUD}"/>     
  </samplesource>
""",    'cintS2S': """  <samplesource list="140">
    <title>Cint S2S</title>
    <completed>It seems you have already entered this survey.</completed>
    <invalid>You are missing information in the URL. Please verify the URL with the original invite.</invalid>
    <var name="RID" unique="1"/>
    <exit cond="qualified" url="https://s.cint.com/survey/return/${RID}"/>
    <exit cond="terminated" url="https://s.cint.com/survey/return/${RID}"/>
    <exit cond="overquota" url="https://s.cint.com/survey/return/${RID}"/>
  </samplesource>
<!--
<block label="block_ln5" cond="list in ['140'] and gv.survey.root.state.live and not (gv.isStaff() or gv.isUser())">
  <logic label="ln5"
    lucint:integration="S2S"
    lucint:integration_key="INSERT_ENCRYPTED_INTEGRATION_KEY_HERE"
    lucint:respondent_id="${RID}"
    uses="lucint.1">
    <title>Cint / Lucid Secure Sample</title>
  </logic>
</block>
-->
""",    'lucidSHA1': """  <samplesource list="139">
    <completed>It seems you have already entered this survey.</completed>
    You are missing information in the URL. Please verify the URL with the original invite.
    <var name="RID" unique="1"/>
    <exit cond="qualified" url="https://www.samplicio.us/s/ClientCallBack.aspx?RIS=10&amp;RID=${RID}&amp;hash=${p.url_hash}"/>
    <exit cond="terminated" url="https://www.samplicio.us/s/ClientCallBack.aspx?RIS=20&amp;RID=${RID}"/>
    <exit cond="overquota" url="https://www.samplicio.us/s/ClientCallBack.aspx?RIS=40&amp;RID=${RID}"/>
  </samplesource>
<!--
<block label="block_ln5" cond="list in ['139'] and gv.survey.root.state.live and not (gv.isStaff() or gv.isUser())">
  <logic label="ln5"
    lucint:integration="SHA-1"
    lucint:integration_key="INSERT_ENCRYPTED_INTEGRATION_KEY_HERE"
    lucint:respondent_id="${RID}"
    uses="lucint.1">
    <title>Cint / Lucid Secure Sample</title>
  </logic>
</block>













-->
""",    'Pure Spectrum': """  <samplesource list="127" keyring="sys/ps" sign="out" title="Pure Spectrum">   
    <invalid>You are missing information in the URL. Please verify the URL with the original invite.</invalid>     
    <completed>It seems you have already entered this survey.</completed>     
    <var name="transaction_id" unique="1"/>     
    <var name="PSID"/>     
    <var name="supplier_id"/>     
    <var name="survey_id"/>     
    <exit cond="terminated and hasMarker('qcterm')" url="https://spectrumsurveys.com/surveydone?st=20&amp;transaction_id=${transaction_id}"/>     
    <exit cond="qualified" url="https://spectrumsurveys.com/surveydone?st=21&amp;transaction_id=${transaction_id}"/>     
    <exit cond="terminated" url="https://spectrumsurveys.com/surveydone?st=18&amp;transaction_id=${transaction_id}"/>     
    <exit cond="overquota" url="https://spectrumsurveys.com/surveydone?st=17&amp;transaction_id=${transaction_id}"/>   
  </samplesource>

<res label="access_token" translateable="0">eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmZDI3NjFlM2JhYzllNjBkOWViMWZmYiIsInVzcl9pZCI6IjUwMTMiLCJpYXQiOjE2MDc2MjgzMTh9.VVgso3HBn9I_yPNyGuONqr_nFvO0f4veFiw-J_YJvdQ</res>

<note>Pure Spectrum - end of survey</note>
<block label="Complete_Survey_API" cond="list in ['127']" builder:title="Block Scripts" sst="0">   

  <suspend/>   

  <textarea label="ndpAPI_Loading_1">
    <title><div style="text-align: center"><img src="https://ps-surveys.s3.amazonaws.com/SP-important/loading.gif" width="200px" /></div></title>
    <style name="question.after">
      <![CDATA[
<style>
.answers{
    display : none !important;
}
</style>
<script>
$ (document).ready(function(){
  postIt();
});
</script>
      ]]>
    </style>
  </textarea>
  <suspend/>   

  <exec>
import re


p.sendAlert = False
p.callCompleteAPI = False
p.retryCompleteAPI = False
p.currentCallCount = 0
p.maxTries = 3
p.buyerQuotaIds = ''


def variables() :
  return {
    'transactionId' : transaction_id,
    'surveyId' : survey_id,
    'surveyPath' : gv.survey.path,
    'isDecipherImported' : True,
    'buyerToken' : res.access_token,
    'mpURLHost' : 'https://spectrumsurveys.com',
    'alertAPIHost' : 'https://decipherstaging.spectrumsurveys.com'
  }

def test() :
  validMPUrls = [
    'https://staging.spectrumsurveys.com',
    'https://spectrumsurveys.com'
  ]
  validAlertAPIHosts = [
    'https://decipherstaging.spectrumsurveys.com',
    'https://decipher.spectrumsurveys.com'
  ]
  xvars = variables()
  if not len(xvars['buyerToken']) :
      raise Exception('Invalid Buyer Token')
  if xvars['mpURLHost'] not in validMPUrls :
      raise Exception('Invalid Complete API Host')
  if xvars['alertAPIHost'] not in validAlertAPIHosts :
      raise Exception('Invalid Alert API Host')



def return_buyer_quota_ids() :
  if 'markers' not in p or not variables()['isDecipherImported']:
      return ''
  markersString = ''
  replacePattern = re.compile(r'[^a-zA-Z0-9]+')
  for marker in p['markers'] :
      buyerQuotaId = replacePattern.sub('', marker)
      if markersString:
          markersString = markersString + ',' + buyerQuotaId
      else:
          markersString = buyerQuotaId
  return markersString


def return_request_data(markers) :
  headerDict={
    'Access-Token': variables()['buyerToken'],
    'Content-type': 'application/json',
    'Accept': 'application/json, text/plain, */*'
  }
  paramsDict = {
    'st' : '21'
  }
  if len(markers) &lt; 2000 :
    paramsDict['quota_id'] = markers
  return {
  'headers' : headerDict,
  'params' : paramsDict
  }


def start() :
  if gv.inSurvey() :
    try:
      completeAPIStatus.val = 'False'
      completeQuotasSynced.val = 'False'
      global completeAPIParams
      global completeAPIHeaders
      p.buyerQuotaIds = return_buyer_quota_ids()
      requestData = return_request_data(p.buyerQuotaIds)
      completeAPIParams= requestData['params']
      completeAPIHeaders = requestData['headers']
      p.callCompleteAPI = True
    except Exception as e:
      completeAPIResponse.val = repr(e)
  elif gv.isSST() :
    test()

start()
  </exec>   
  <suspend/>   

     
  <suspend/>   

  <exec cond="p.callCompleteAPI">
p.currentCallCount = p.currentCallCount + 1

def check_complete_API_response() :
  try:
    if completeAPI.status == 200 :
      completeAPIStatus.val = 'True'
      completeQuotasSynced.val = 'True' if (len(p.buyerQuotaIds) < 2000 and len(p.buyerQuotaIds) &gt; 0) else 'False'
    elif do_retry(completeAPI.status):
      p.retryCompleteAPI = True
      return
    else :
      p.sendAlert = should_send_alert()
    p.retryCompleteAPI = False
    completeAPIResponse.val = 'Response ' + repr(completeAPI.r)
  except Exception as e:
    p.retryCompleteAPI = False
    completeAPIResponse.val = repr(e)
    p.sendAlert = should_send_alert()

def do_retry(statusCode) :
   statusCode in [500, 502, 503, 504, 599] and p.currentCallCount < p.maxTries

def should_send_alert():
   not is_testing() and variables()['isDecipherImported']

def is_testing() :
  return variables()['surveyPath'].find('/temp-edit-live') &gt;= 0 or gv.isUser()

check_complete_API_response()
  </exec>   

  <suspend/>   

  <goto cond="p.retryCompleteAPI" target="completeAPI"/>   

  <suspend/>   

  <text     
    label="completeAPIStatus"     
    where="execute,survey,report">     
         
  </text>   

  <textarea     
    label="completeAPIResponse"     
    where="execute,survey,report">     
         
  </textarea>   

  <text     
    label="completeQuotasSynced"     
    where="execute,survey,report">     
    <title>Quotas Synced?</title>     
  </text>   

  <textarea     
    label="failAlert"     
    cond="failAlert.val"     
    where="execute,survey,report">     
    <title>Complete Fail Alert</title>     
  </textarea>   

  <suspend/>   

</block>
""",    'opensurvey': """  <samplesource list="0">
    <title>Open Survey</title>
    <completed>It seems you have already entered this survey.</completed>
    <invalid>You are missing information in the URL. Please verify the URL with the original invite.</invalid>
    <exit cond="terminated">We appreciate the time you spent but unfortunately the rest of this survey won't be relevant to you based on your answers.</exit>
    <exit cond="qualified">Thank you for taking our survey. Your efforts are greatly appreciated!</exit>
    <exit cond="overquota">Thank you for your participation. This study was extremely popular, so we had to close it before you were able to complete it.</exit>
  </samplesource>
"""
    }
    def run (self,edit, which):
        try:
            sels = self.view.sel()
            printPage = self.panels[which]
            for sel in sels:
                self.view.insert(edit,sel.end(), printPage)
        except Exception as e:
            print (e)


######################## HPR SPECIFIC #########################

class addHprContainer(sublime_plugin.TextCommand):
    def is_visible(self):
        return bool(self.view.settings().get('surveyHpr'))

    def run (self,edit):
        try:
            sels = self.view.sel()
            printPage = """
<block label="hpr_container" hpr:container="1">

<block hpr:page="1" label="page1">

<block hpr:section="1" label="sec1">

{0}

</block> <!-- close section -->

</block> <!-- close page -->

</block> <!-- close container -->"""
            for sel in sels:
                self.view.insert(edit,sel.end(), printPage)
        except Exception as e:
            print (e)


class addHprSection(sublime_plugin.TextCommand):
    def is_visible(self):
        return bool(self.view.settings().get('surveyHpr'))

    def run (self,edit):
        try:
            sels = self.view.sel()
            input = ''
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel).strip()
                printPage = """<block hpr:section="1" label="">

<html label="sec1_intro" where="survey">Section Header</html>

{0}

</block>""".format(input)
                self.view.replace(edit,sel, printPage)
        except Exception as e:
            print (e)


class addHprPage(sublime_plugin.TextCommand):
    def is_visible(self):
        return bool(self.view.settings().get('surveyHpr'))

    def run (self,edit):
        try:
            sels = self.view.sel()
            input = ''
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel).strip()
                printPage = """
<block hpr:page="1" label="page1">

<block hpr:section="1" label="sec1">


{0}


</block> 

</block>
""".format(input)
                self.view.replace(edit,sel, printPage)
        except Exception as e:
            print (e)

################################################################

class addRdReview(sublime_plugin.TextCommand):
    def is_visible(self):
        return True #bool(self.view.settings().get('surveyHpr'))

    def run (self,edit):
        try:
            dirSep = r'[\\/]{1,2}'
            dServer = 'tes'
            fileVars = view.window().extract_variables()
            filePath = re.split(dirSep, fileVars['file_path'] if 'file_path' in fileVars else '')
            if 'nke' in filePath[-1]:
                dServer = 'nk'
            elif 'nflx' in filePath[-1]:
                dServer = 'nflx'
            survId = dServer + '-' + filePath[-2] + '-' + filePath[-1]

            sels = self.view.sel()
            input = ''
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel).strip()
                printPage = """{0}
<note>Put after question w/o suspend. Edit q_id for the needed question (Q5, S8.r9.opem,...).
<logic  
  label="ln1" 
  rd_review:platform_id="[INSERT_ENCRYPTED_KEY]"
  rd_review:q_id="FIX_ME"  
  rd_review:sn_ud="uuid"  
  rd_review:sy_nr="{1}"  
  uses="rd_review.2"> 
    <title>Research Defender - Review Node</title> 
</logic>
<suspend/>""".format(input, survId)
                self.view.replace(edit,sel, printPage)
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)


class addDatasource(sublime_plugin.TextCommand):
    label = "Resp_DS"
    title = ''
    filename = 'resp-data.txt'
    ourKey = 'source'
    datasourceKey = 'id'
    normalizeKey = ''

    def run(self, edit):

        self.view.window().show_input_panel(
            'Datasource label:',
            'Resp_DS',
            self.on_done_input,
            on_change=None,
            on_cancel=None
        )

    def on_done_input(self, input=None):
        if input not in [None,'']:
            self.label = input

        self.view.window().show_input_panel(
            'Filename:',
            'resp-data.txt',
            self.on_done_input2,
            on_change=None,
            on_cancel=None
        )

    def on_done_input2(self, input=None):
        if input not in [None,'']:
            self.filename = input

        self.view.window().show_input_panel(
            'ourKey (in survey):',
            'source',
            self.on_done_input3,
            on_change=None,
            on_cancel=None
        )

    def on_done_input3(self, input=None):
        if input not in [None,'']:
            self.ourKey = input

        self.view.window().show_input_panel(
            'DatasourceKey (in file):',
            'id',
            self.on_done_input4,
            on_change=None,
            on_cancel=None
        )

    def on_done_input4(self, input=None):
        try:
            if input not in [None,'']:
                self.datasourceKey = input

            if sublime.ok_cancel_dialog('Add normalizeKey="lower" for checking IDs in lowercase?', ok_title='Yes'):
                self.normalizeKey='''\n    normalizeKey="lower"'''

            dsTag = """<datasource label="{0}"
    title="{1}"
    filename="{2}"
    ourKey="{3}"
    datasourceKey="{4}"{5}>
<note> Add dataSource="{0}" to question. Add dataRef="FILE_COL" to specific element. </note>
    """.format(self.label, self.title, self.filename, self.ourKey, self.datasourceKey, self.normalizeKey)
            sels = self.view.sel()
            input = ''
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel).strip()
                printPage = "{0}\n{1}".format(input, dsTag)
                with Edit(self.view) as edit:
                    edit.replace(sel, printPage)
            getSelection(self, sels)
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)



class addExecRowsOrd(sublime_plugin.TextCommand):
    def run (self,edit):
        try:
            sels = self.view.sel()
            input = ''
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel).strip()
                printPage = """{0}
<note>group_rows(Q1, ['r7','r8']) ### call in exec to keep r7+r8 together when shuffling</note>
<exec when="init">
def group_rows( question, grouped_rows, doShuffle=True, anchorLast=False ):
    current_order = [row.index for row in question.rows.order]
    new_order = []
    if doShuffle:
        if anchorLast:
            grouped_rows = ishuffle([question.attr(row).index for row in grouped_rows[:-1]]) + [question.attr(grouped_rows[-1]).index]
        else:
            grouped_rows = ishuffle([question.attr(row).index for row in grouped_rows])
    else:
        grouped_rows = [question.attr(row).index for row in grouped_rows]

    first_item_index = current_order.index(grouped_rows[0])
    current_order.insert(first_item_index, grouped_rows)

    for row in current_order:
        if row == grouped_rows:
            new_order = new_order + row
        elif row not in grouped_rows:
            new_order.append(row)

    question.rows.order = new_order
</exec>""".format(input)
                self.view.replace(edit,sel, printPage)
        except Exception as e:
            command = self.__class__.__name__
            self.view.run_command('show_status_err', {"on_command": str(command), "error_msg": str(e)})
            print (e)

class addSpeederTerm(sublime_plugin.TextCommand):
    def run (self,edit):
        try:
            sels = self.view.sel()
            input = ''
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel).strip()
                printPage = """<term label="Speederterm" cond="timeSpent() lt {0}" markers="qcterm" incidence="0">Speeder</term>""".format(input)
                self.view.replace(edit,sel, printPage)
        except Exception as e:
            print (e)


class addVchange(sublime_plugin.TextCommand):
    def run (self,edit):
        try:
            sels = self.view.sel()
            input = ''
            for sel in sels:
                printPage = ''
                input = self.view.substr(sel).strip()
                printPage = """<exec when="started">
setMarker('_changes{0}')
</exec>

<radio label="vChange">
  <title>Changes tracker</title>
  <virtual>
if '_changes{0}' in markers:
    vChange.val = vChange.r2.index
else:
    vChange.val = vChange.r1.index
  </virtual>
  <row label="r1">Initial survey</row>
  <row label="r2">Changes .. ({1})</row>
</radio>
""".format(datetime.datetime.now().strftime('%m%d%y'), datetime.datetime.now().strftime('%m/%d/%Y'))
                self.view.replace(edit,sel, printPage)
        except Exception as e:
            print (e)

